import React, { useState, useEffect, useRef } from 'react'
import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';
import { PersistGate } from 'redux-persist/integration/react'
import { store, persistor } from './src/redux/store'
import {Provider}from 'react-redux';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import App from './App';
import {name as appName} from './app.json';


const RNRedux = (props) => {
    
    return (
        <SafeAreaProvider>
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <App/>
            </PersistGate>
        </Provider>
        </SafeAreaProvider>
    )
}

AppRegistry.registerComponent(appName, () => RNRedux);

// AppRegistry.registerComponent(appName, () => App);
