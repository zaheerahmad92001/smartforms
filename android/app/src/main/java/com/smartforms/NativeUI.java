package com.smartforms;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class NativeUI extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_native_ui);
    }
}