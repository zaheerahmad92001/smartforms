export const Remember='remember'
export const status ='success'
export const __error ='error'
export const USER = 'user'
export const ON_BOARDING = 'onboarding'
export const SERVER_ERROR = 'Internal Server Error'
export const STOCK_OUT ='Out Of Stock'
export const STOCK_OUT_MSG ='Required Quantity is Out Of Stock'
export const INVALID_VALUE = 'Invalid Values Selected'
export const TO_FROM_VALUE = "'From' and 'To' Values Should be different"

import {logo} from './images';
export const slides = [
    {
      key: 1,
      appNameText1: 'SMART',
      appNameText2: 'FORMS',
      infoText1 : 'Track your money',
      infoText2 : 'everywhere.',
      image :logo
    },
    {
      key: 2,
      appNameText1: 'SMART',
      appNameText2: 'FORMS',
      infoText1 : 'Explore secure and',
      infoText2 : 'fast payment',
      image: logo
    },
    
  ];