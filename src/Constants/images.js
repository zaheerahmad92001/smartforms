export const logo = require('../../src/Assets/images/logo.png')
export const menu = require('../../src/Assets/images/menu.png')
export const accTransfer = require('../../src/Assets/images/accTransfer.png')
export const journal = require('../../src/Assets/images/journal.png')
export const expense = require('../../src/Assets/images/expense.png')
export const userImg = require('../../src/Assets/images/user.png')
export const account = require('../../src/Assets/images/account.png')
export const inventory = require('../../src/Assets/images/inventory.png')
export const file = require('../../src/Assets/images/file.png')
export const settings = require('../../src/Assets/images/settings.png')
export const setups = require('../../src/Assets/images/setups.png')
export const transactions = require('../../src/Assets/images/transactions.png')
export const reports = require('../../src/Assets/images/reports.png')
export const sale = require('../../src/Assets/images/sale.png')
export const production = require('../../src/Assets/images/production.png')
export const attendance = require('../../src/Assets/images/attendance.png')
export const search = require('../../src/Assets/images/search.png')
export const drawer = require('../../src/Assets/images/drawer.png')
export const signoutIcon = require('../../src/Assets/images/signOut.png')

export const saleInvoice = require('../../src/Assets/images/saleinvoice.png')
export const purchase = require('../../src/Assets/images/purchase.png')
export const journalVoucher = require('../../src/Assets/images/journalVoucher.png')
export const leaveApply = require('../../src/Assets/images/leaveApply.png')






