export const mediumText = 14
export const largeText = 18
export const xlText = 20
export const xsmallText = 10
export const smallText = 12
export const xxlText = 30