import React, { useEffect, useRef, useReducer } from "react";
import { View, Text, ActivityIndicator, FlatList } from "react-native";
import { Icon } from "native-base";
import styles from "../../../View/AccountTransfer/VoucherSearch/styles";
import AppHeader from "../../../Components/AppHeader";
import SearchView from '../../../Components/SearchView'
import { Modalize } from 'react-native-modalize';
import { appColor, darkSky, white } from "../../../Constants/Colors";
import SaveBtn from "../../../Components/SaveBtn";
import { Pressable } from "react-native";
import DialogBox from 'react-native-dialogbox';
import axios from "axios";
import { baseUrl } from "../../../Constants/server";
import { connect } from 'react-redux'
import { SERVER_ERROR, status } from "../../../Constants/constValues";


function IssueNote_Search({ navigation, user }) {
    let modalizeRef = useRef()
    let dialogboxRef = useRef()

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.access_token}`
            },
            searchValue: '',
            selectedItem: '',
            v_list: [],
            loading: false,
        }
    )
    const {
        headers,
        searchValue,
        selectedItem,
        v_list,
        loading,
    } = state


    useEffect(() => {
        let unmounted = false;
            loadRecord()

        const unsubscribe = navigation.addListener('focus', async () => {
            loadRecord()
            updateState({searchValue:''})
            });
         return unsubscribe;
    }, []);

    function loadRecord() {
        updateState({ loading: true })
        let tempVar = user?.sessions
        let formData = {
            userid: tempVar?.userid,
            companyid: tempVar?.companyid,
            projectid: tempVar?.projectid
        }

        axios.post(`${baseUrl}/inventory/issuenotes`, formData, {
            headers: headers
        }).then((respone) => {
            const { data } = respone
            console.log('response ', data)
            if (data.status === status) {
                updateState({
                    v_list: data?.inv_issue_notes,
                    loading: false,
                })
            } else {
                updateState({ loading: false })
                alertMsg(SERVER_ERROR)
            }
        })
    }


    function confirmModal() {
        dialogboxRef.current?.confirm({
            title: 'Message',
            content: ['Are You Sure You want to delete!', 'Action Can`t Undo'],
            ok: {
                text: 'Yes',
                style: styles.yesBtnStyle,
                callback: () => { __Delete() },
            },
            cancel: {
                text: 'No',
                style: styles.noBtnStyle,
                callback: () => { },
            },
        });
    }

    function alertMsg(msg1, msg2) {
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2],
            btn: {
                text: 'OK',
                callback: () => { },
            },
        })

    }


    function handleSearchText(value) {
        updateState({ searchValue: value })
    }
    function goBack() { navigation.pop() }

    const onOpenModalize = (item, index) => {
        // console.log('item select' , item )
        updateState({ selectedItem: item })
        modalizeRef.current?.open()
    }
    const onClosedModalize = () => { console.log('closed') }

    function closeModalize() { modalizeRef.current?.close() }

    function deleteRecord() {
        ////// just open confirm modal
        closeModalize()
        setTimeout(() => {
            confirmModal()
        }, 400)
    }

    function __Delete() {
        console.log('selected item', selectedItem)
        let id = selectedItem.id
        axios.get(`${baseUrl}/inventory/issuenotes/delete/${id}`, {
            headers: headers
        }).then((response) => {
            const { data } = response
            console.log('delete', data)
            loadRecord()
        })


    }

    function editRecord() {
        console.log('selected item', selectedItem)
        closeModalize()
        navigation.navigate('IssueNote_edit', {
            item: selectedItem,
        })
    }

    const renderSheet = () => {
        return (
            <View style={styles.sheetStyle}>
                <Pressable
                    style={styles.closeBtn}
                    onPress={closeModalize}
                >
                    <Icon
                        name={'closecircleo'}
                        type={'AntDesign'}
                        style={{ color: appColor }}
                    />
                </Pressable>
                <SaveBtn
                    onPress={deleteRecord}
                    title={'Delete'}
                    buttonContainer={styles.buttonContainer}
                />
                <SaveBtn
                    onPress={editRecord}
                    title={'Edit'}
                    buttonContainer={styles.buttonContainer}
                />

            </View>
        )
    }


    function renderItem({ item, index }) {
        if (item.remarks.toLowerCase().includes(searchValue.toLowerCase())) {
            return (
                <SearchView
                    item={item}
                    serialNo={index + 1}
                    onLongPress={() => onOpenModalize(item, index)}
                />
            )
        } 
        
    }
    return (
        <View style={styles.container}>
            <AppHeader
                rightIconName={'search'}
                rightIconType={'EvilIcons'}
                leftIconName={'arrow-back'}
                leftIconType={'Ionicons'}
                leftBtnPress={goBack}
                search={true}
                onChangeText={handleSearchText}
                value={searchValue}
            />
            <View style={[styles.blueBG]}></View>
    
            {loading ?
                <View style={styles.loadingView}>
                    <ActivityIndicator
                        color={darkSky}
                        size={'small'}
                    />
                </View>
                : !loading && v_list.length > 0 ?
                    <FlatList
                        data={v_list}
                        showsVerticalScrollIndicator={false}
                        keyExtractor={item => item.id + Math.random()}
                        initialNumToRender={20}
                        maxToRenderPerBatch={20}
                        renderItem={renderItem}
                    />
                    :
                    <View style={styles.loadingView}>
                        <Text style={styles.noVoucherFound}>{'No voucher found'}</Text>
                    </View>
            }

            <Modalize
                ref={modalizeRef}
                adjustToContentHeight
                onClose={() => onClosedModalize()}
            >
                {renderSheet()}
            </Modalize>

            <DialogBox ref={dialogboxRef} />
        </View>
    )
}
const mapStateToProps = (state) => ({
    user: state.User.userInfo,
})
const mapDispatchToProps = (dispatch) => ({

});
export default connect(mapStateToProps,null)(IssueNote_Search)


