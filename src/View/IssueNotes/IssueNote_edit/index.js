import React, { useEffect, useRef, useReducer } from "react";
import { View, Text, Image, } from "react-native";
import { Container, Content, Icon } from "native-base";
import { logo } from "../../../Constants/images";
import InputField from "../../../Components/InputField";
import DatePicker from "../../../Components/DatePicker";
import _Calendar from '../../../Components/Calendar';
import styles from "../styles";
import AppHeader from "../../../Components/AppHeader";
import DropDown from "../../../Components/Dropdown";
import DropdownModal from "../../../Components/dropDownModal";
import SaveBtn from "../../../Components/SaveBtn";
import CancelBtn from "../../../Components/CancelBtn";
import DocPicker from "../../../Components/DocPicker";
import DocumentPicker from 'react-native-document-picker'
import DialogBox from 'react-native-dialogbox';
import Error from "../../../Components/Error";
import { Product_dd, Location_dd } from '../../../redux/actions/Dropdowns'
import { NetworkInfo } from "react-native-network-info";
import { SERVER_ERROR, status, STOCK_OUT, STOCK_OUT_MSG, __error } from "../../../Constants/constValues";
import axios from "axios";
import { baseUrl } from "../../../Constants/server";
import { connect } from "react-redux";
import moment from 'moment'


let filteredLocationFrom = ''
let filteredLocationTo = ''
let filteredProduct = ''

function IssueNote_Edit({ navigation, user, route, loc_dd, product_dd }) {

    let dialogboxRef = useRef()
    let item = route.params.item

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.access_token}`
            },
            formData: {
                userid: user?.sessions?.userid,
                companyid: user?.sessions?.companyid,
                locationid: user?.sessions?.locationid,
            },

            l_dd: [],
            l_dd_List: [],
            location_id: item?.locationfromid,
            locationTo_id: item?.locationtoid,

            p_dd: [],
            p_dd_List: [],
            product_id: item?.productid,

            voucherNumber: item?.vno,
            ipAddress: '',
            sending: false,
            vnoError: false,
            isdropdownLoading: false,
            isCalendar_Visible: false,
            currentDate: '',
            markedDates: {},
            voucherDate: '',
            vdateError: false,
            chequeNumber: '',
            chequeNoError: false,
            locationTo: item?.locationtoname,
            locationToError: false,
            searchLocationTo: '',
            showLocationTo: false,
            location: item?.locationfromname,
            locationError: false,
            searchLocationFrom: '',
            is_location_dd_Visible: false,
            is_product_Visible: false,
            quantity: Number(item?.qty).toFixed(0),
            qtyError: false,
            product: item?.productname,
            productError: false,
            searchProduct: '',
            filePath: '',
            fileType: '',
            errMsg: '',
            startDay:'' , 
            endDay:''

        }
    )
    const {
        headers,
        formData,
        voucherNumber,
        vnoError,
        ipAddress,
        currentDate,
        markedDates,
        voucherDate,
        vdateError,
        locationTo,
        locationToError,
        isdropdownLoading,
        searchLocationTo,
        sending,
        l_dd,
        l_dd_List,
        location_id,
        locationTo_id,

        p_dd,
        p_dd_List,
        product_id,

        showLocationTo,
        isCalendar_Visible,
        location,
        locationError,
        searchLocationFrom,
        is_location_dd_Visible,
        is_product_Visible,
        product,
        productError,
        searchProduct,
        quantity,
        qtyError,
        filePath,
        fileType,
        errMsg,
        startDay , 
        endDay
    } = state

    useEffect(() => {

        NetworkInfo.getIPAddress().then(ip => {
            updateState({ ipAddress: ip })
        });

        populateLocation_dd()
        populateProduct_dd()

        let [_d, _y] = item?.vdate.split(' ')
        let [y, m, d] = _d?.split('-')

        let date = `${y}-${m}-${d}`
        let _show = `${d}-${m}-${y}`

        let fullYear = y
        let fullMonth = m -1
        var my_date = new Date(fullYear, fullMonth);

        var first_date = new Date(my_date.getFullYear(), my_date.getMonth(), 1);
        var last_date = new Date(my_date.getFullYear(), my_date.getMonth() +1, 0);

        let _first = moment.utc(first_date).local().format('YYYY-MM-DD HH:mm:ss')
        let [firstDay, _time] = _first.split(' ')

        let _last = moment.utc(last_date).local().format('YYYY-MM-DD HH:mm:ss')
        let [last_day, l_time] = _last.split(' ')


        let selected = {}
        selected[date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        updateState({
            currentDate: date,
            voucherDate: _show,
            markedDates: selected,

            startDay:firstDay,
            endDay:last_day
        })

    }, [])

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', async () => {
            populateLocation_dd()
            populateProduct_dd()
        });
        return unsubscribe;
    }, [])


    function alertMsg(msg1, msg2) {
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2],
            btn: {
                text: 'OK',
                callback: () => { navigation.pop() },
            },
        })
    }

    function populateLocation_dd() {
        updateState({ isdropdownLoading: true })
        let ddValues = []
        if (loc_dd?.length > 0) {
            loc_dd.map((item, index) => {
                ddValues.push(item.vname)
            })
            updateState({
                isdropdownLoading: false,
                l_dd_List: ddValues,// for filter 
                l_dd: loc_dd,
            })
        } else {
            updateState({ isdropdownLoading: false })
        }
    }

    function populateProduct_dd() {
        updateState({ isdropdownLoading: true })
        let ddValues = []
        if (product_dd?.length > 0) {
            product_dd.map((item, index) => {
                ddValues.push(item.vname)
            })

            updateState({
                isdropdownLoading: false,
                p_dd_List: ddValues,// for filter 
                p_dd: product_dd,
            })
        } else { updateState({ isdropdownLoading: false }) }
    }

    function goBack() { navigation.pop() }

    async function selectDocument() {
        try {
            const results = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });

            updateState({
                filePath: results,
                fileType: results[0].type
            })
            for (const res of results) {
                console.log(
                    res.uri,
                    res.type, // mime type
                    res.name,
                    res.size,
                )
            }

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                console.log('Document picker cancel')
            } else {
                throw err;
            }
        }
    }

    function cancelDocument() {
        updateState({
            filePath: '',
            fileType: ''
        })
    }

    function showCalendar() {
        let isVisible = isCalendar_Visible
        updateState({ isCalendar_Visible: !isVisible })
    }

    function onDayPress(date) {

        let [y, m, d] = date.dateString.split('-')
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates: selected,
            currentDate: date.dateString,
            voucherDate: _show,
            vdateError: false,
            errMsg: '',
        })
    }


    function closeLocationModal() {
        updateState({ is_location_dd_Visible: false })
    }

    function closeProdutModal() {
        updateState({ is_product_Visible: false })
    }

    function clsoeLocationToModal() {
        updateState({ showLocationTo: false })
    }

    function selectValue_location(value, index) {
        var FOUND = l_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })
        updateState({
            location: value,
            location_id: FOUND.id,
            searchLocationFrom: '',
            is_location_dd_Visible: false,
            locationError: false,
            errMsg: '',
        })
    }
    const handleSearchLocationFrom = (value) => {
        updateState({ searchLocationFrom: value })
    }


    function selectValue_product(value, index) {
        var FOUND = p_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })
        updateState({
            product: value,
            product_id: FOUND.id,
            searchProduct: '',
            is_product_Visible: false,
            productError: false,
            errMsg: '',
        })
    }
    const handleSearchProduct = (value) => {
        updateState({ searchProduct: value })
    }

    function selectLocationTo(value, index) {
        var FOUND = l_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })
        updateState({
            locationTo: value,
            locationTo_id: FOUND.id,
            searchLocationTo: '',
            showLocationTo: false,
            locationToError: false,
            errMsg: '',
        })
    }
    const handleSearchLocationTo = (value) => {
        updateState({ searchLocationTo: value })
    }


   async function saveIssueNotes() {
        let err = false
        let _equal = ''
        if (location) {
            _equal = location.localeCompare(locationTo);
        }
        if (!voucherDate) {
            updateState({ vdateError: true, })
            err = true
        }
        if (!location) {
            updateState({ locationError: true, })
            err = true
        }
        if (!locationTo) {
            updateState({ locationToError: true, })
            err = true
        }
        if (!product) {
            updateState({ productError: true, })
            err = true
        }
        if (!quantity || isNaN(quantity) || quantity < 1 ) {
            updateState({ qtyError: true, })
            err = true
        }
        if (!err) {
            // api 

            updateState({ sending: true })
            let tempVar = user?.sessions

            const __headers = {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${user?.access_token}`
            }

            let formData = new FormData()

            formData.append('vdate', voucherDate)
            formData.append('locationfrom', location_id)
            formData.append('locationto', locationTo_id)
            if(filePath){
            formData.append('file', {
                name: filePath[0]?.name,
                type: filePath[0]?.type,
                uri: Platform.OS === 'ios' ?
                    filePath[0]?.uri.replace('file://', '')
                    : filePath[0]?.uri,
            })
         }
            formData.append('companyid', tempVar?.companyid)
            formData.append('locationid', tempVar?.locationid)
            formData.append('projectid', tempVar?.projectid)
            formData.append('insertedby', tempVar?.userid)
            formData.append('insertedip', ipAddress)
            formData.append('productid', product_id)
            formData.append('qty', quantity)


            // updateState({sending:true})
            // let tempVar =user?.sessions 
            // let formData={
            //    vdate:voucherDate,
            //    locationfrom:location_id,
            //    locationto:locationTo_id,
            //    companyid:tempVar?.companyid,
            //    locationid:tempVar?.locationid,
            //    projectid:tempVar?.projectid,
            //    insertedby:tempVar?.userid,
            //    insertedip:ipAddress,
            //    productid:product_id,
            //    qty:quantity
            // }

            console.log('issue Notes data to server', formData)
           await axios.post(`${baseUrl}/inventory/issuenotes/update/${item.id}`, formData, {
                headers: __headers
            }).then((response) => {
                const { data } = response
                if (data.status === status) {
                    updateState({ sending: false, })
                
                    alertMsg('Issue Voucher Saved Successfully')

                } else if (data?.status === __error) {
                    updateState({ sending: false })
                    alertMsg(data?.message)
                    console.log('error in else if', data)
                } else {
                    updateState({ sending: false })
                    alertMsg(SERVER_ERROR)
                    console.log('error in else', data)
                }
            }).catch((err) => {
                updateState({ sending: false })
                alertMsg(SERVER_ERROR)
                console.log('err in catch', err)
            })
        } else {
            updateState({ errMsg: 'Please fill required fields' })
        }
    }

    function cancelIssueNote() { navigation.pop() }


    /*********************** custom dd location  *************************/
    const filterLocationFromData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return l_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return l_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }

    /*********************** custom dd product  *************************/
    const filterProductData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return p_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return p_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }

    /*********************** custom dd location To  *************************/
    const filteredLocationToData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return l_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return l_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }

    /*********************** custom dd location To  *************************/
    filteredLocationFrom = filterLocationFromData(searchLocationFrom)
    filteredProduct = filterProductData(searchProduct)
    filteredLocationTo = filteredLocationToData(searchLocationTo)

    return (
        <Container>
            <AppHeader
                headingText={'Edit Issue Note'}
                leftIconName={'arrow-back'}
                leftIconType={'Ionicons'}
                leftBtnPress={goBack}
                dashbord={false}
                heading={true}
                search={false}
                edit={true}
            />
            <View style={[styles.blueBG]}></View>

            <Content style={styles.container}>
                <InputField
                    label="Voucher Number"
                    placeholder={'Voucher Number'}
                    editable={false}
                    onChangeText={(v) => updateState({
                        voucherNumber: v,
                        vnoError: false,
                        errMsg: '',
                    })}
                    value={voucherNumber}
                    error={vnoError}
                />
                <DatePicker
                    label="Voucher Date"
                    onPress={showCalendar}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={voucherDate}
                    error={vdateError}
                />
                <DropDown
                    label="Location From"
                    placeholder={'Location From'}
                    value={location}
                    // onPress={() => updateState({ is_location_dd_Visible: true })}
                    error={locationError}
                />

                <DropDown
                    label="Location To"
                    placeholder={'Location To'}
                    value={locationTo}
                    // onPress={() => updateState({ showLocationTo: true })}
                    error={locationToError}
                />

                <DropDown
                    label="Product"
                    placeholder={'Product'}
                    value={product}
                    // onPress={() => updateState({ is_product_Visible: true })} 
                    error={productError}
                />
                <InputField
                    label="Quantity"
                    placeholder={'Quantity'}
                    keyboardType={'number-pad'}
                    onChangeText={(v) => updateState({
                        quantity: v,
                        qtyError: false,
                        errMsg: '',
                    })}
                    value={quantity}
                    error={qtyError}
                />

                {errMsg ?
                    <Error
                        error={errMsg}
                    /> : null}

                    {qtyError ?
                    <Error
                        error={'Quantity shoud be greater than 0'}
                    /> : null}

                <View style={styles.btnView}>
                    <SaveBtn
                        onPress={saveIssueNotes}
                        title={'Save'}
                        saving={sending}
                    />
                    <CancelBtn
                        onPress={cancelIssueNote}
                        title={'Cancel'}
                    />
                    <DocPicker
                        onPress={selectDocument}
                        fileType={fileType}
                        filePath={filePath}
                        cancelFile={cancelDocument}
                    />
                </View>
            </Content>

            <_Calendar
                is_Visible={isCalendar_Visible}
                hideDatePicker={showCalendar}
                onDayPress={onDayPress}
                currentDate={currentDate}
                markedDates={markedDates}
                DoneBtnPress={showCalendar}
                minDate={startDay}
                maxDate={endDay}
            />
            <DropdownModal
                isVisible={is_location_dd_Visible}
                closeFromModal={closeLocationModal}
                data={filteredLocationFrom}
                onChangeText={handleSearchLocationFrom}
                selectValue={selectValue_location}
                isLoading ={isdropdownLoading}
            />
            <DropdownModal
                isVisible={is_product_Visible}
                closeFromModal={closeProdutModal}
                data={filteredProduct}
                onChangeText={handleSearchProduct}
                selectValue={selectValue_product}
                isLoading ={isdropdownLoading}

            />
            <DropdownModal
                isVisible={showLocationTo}
                closeFromModal={clsoeLocationToModal}
                data={filteredLocationTo}
                onChangeText={handleSearchLocationTo}
                selectValue={selectLocationTo}
                isLoading ={isdropdownLoading}
            />

            <DialogBox ref={dialogboxRef} />
        </Container>
    )
}
const mapStateToProps = (state) => ({
    user: state.User.userInfo,
    loc_dd: state.Dropdown.locationDDs,
    product_dd: state.Dropdown.productDDs,
})
const mapDispatchToProps = (dispatch) => ({
    saveLocation: (v) => dispatch(Location_dd(v)),
    saveProduct: (v) => dispatch(Product_dd(v)),
});
export default connect(mapStateToProps, mapDispatchToProps)(IssueNote_Edit)
