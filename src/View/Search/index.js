import React, { useEffect, useRef, useReducer } from "react";
import { View, Text, } from "react-native";
import { Icon } from "native-base";
import styles from "./styles";
import AppHeader from "../../Components/AppHeader";
import SearchComponent from "../../Components/SearchComponent";
import { FlatList,} from "react-native-gesture-handler";
import { Modalize } from 'react-native-modalize';
import { appColor, white } from "../../Constants/Colors";
import SaveBtn from "../../Components/SaveBtn";
import { Pressable } from "react-native";
import DialogBox from 'react-native-dialogbox';

let data = [
    { id: 1 , value:'Jubili' ,quote:'QB123' , customer:'Jupiter Network Trading Estz'},
    { id: 2 ,value:'Jubili',quote:'ZX-1333' , customer:'Zeeta Technology'},
    { id: 3 ,value:'Jubili',quote:'wr-899' , customer:'Squadly bizz'},
    { id: 4 ,value:'Jubili',quote:'xcv-90' , customer:'My Squadly'},
    { id: 5,value:'Jubili' ,quote:'xxx-rrrr' , customer:'Yellow School'},
]

function Search({ navigation }) {
    let modalizeRef = useRef()
    let dialogboxRef = useRef()

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            searchValue: '',
            selectedItem:'',
        }
    )
    const { 
        searchValue,
        selectedItem 
    } = state


    useEffect(() => {

    });

    function handleSearchText(value) {
        updateState({ searchValue: value })
    }
    function goBack() {navigation.pop()}

    const onOpenModalize =(item ,index)=>{
        // console.log('item select' , item )
        updateState({selectedItem:item})
        modalizeRef.current?.open()
    }
    const onClosedModalize =()=>{
        console.log('cloeed')
     }
    function closeModalize(){
        modalizeRef.current?.close()
     }
     function deleteRecord(){
        closeModalize()
    setTimeout(()=>{

        dialogboxRef.current?.confirm({
            title: 'Message',
            content: ['Are You Sure You want to delete!','Action Can`t Undo'],
            ok: {
                text: 'Yes',
                style:styles.yesBtnStyle,
                callback: () => {},
            },
            cancel: {
                text: 'No',
                style:styles.noBtnStyle,
                callback: () => {},
            },
        });

    },400)
     }
     function editRecord(){
       navigation.navigate('expenses')
     }

      const renderSheet=()=>{
         return(
             <View style={styles.sheetStyle}>
                 <Pressable
                  style={styles.closeBtn} 
                  onPress={closeModalize}
                 >
                     <Icon
                      name={'closecircleo'}
                      type={'AntDesign'}
                      style={{color:appColor}}
                     />
                 </Pressable>
                 <SaveBtn
                  onPress={deleteRecord}
                  title={'Delete'}
                  buttonContainer={styles.buttonContainer}
                 />
                 <SaveBtn
                  onPress={editRecord}
                  title={'Edit'}
                  buttonContainer={styles.buttonContainer}
                 />
                 
             </View>
         )
     }


    const renderItem = ({ item, index }) => {
        // if (item.name.toUpperCase().includes(searchText.toUpperCase()))
        if(item.customer.toLowerCase().includes(searchValue.toLowerCase())){
        return (
            <SearchComponent
                item={item}
                srerial={index + 1}
                onLongPress={()=>onOpenModalize(item , index)}
            />
        )
    }
    }

    // filteredData = 


    return (
        <View style={styles.container}>
            <AppHeader
                rightIconName={'search'}
                rightIconType={'EvilIcons'}
                leftIconName={'arrow-back'}
                leftIconType={'Ionicons'}
                leftBtnPress={goBack}
                search={true}
                onChangeText={handleSearchText}
                value={searchValue}
            />
            <View style={[styles.blueBG]}></View>
            <View style={{ flex: 1, }}>
                <FlatList
                    data={data}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(item) => { item.id }}
                    renderItem={renderItem}
                />

            </View>
            <Modalize
                ref={modalizeRef}
                adjustToContentHeight
                onClose={()=>onClosedModalize()}
                >
            {renderSheet()}
          </Modalize>

          <DialogBox ref={dialogboxRef} />
        </View>
    )
}
export default Search

