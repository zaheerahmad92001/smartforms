import React, { useEffect, useRef, useReducer } from "react";
import { View, Text, Image, } from "react-native";
import { Container, Content, Icon } from "native-base";
import InputField from "../../../Components/InputField";
import DatePicker from "../../../Components/DatePicker";
import _Calendar from '../../../Components/Calendar';
import styles from "../styles";
import AppHeader from "../../../Components/AppHeader";
import DropDown from "../../../Components/Dropdown";
import DropdownModal from "../../../Components/dropDownModal";
import SaveBtn from "../../../Components/SaveBtn";
import CancelBtn from "../../../Components/CancelBtn";
import DocPicker from "../../../Components/DocPicker";
import Error from "../../../Components/Error";
import DialogBox from 'react-native-dialogbox';
import DocumentPicker from 'react-native-document-picker'
import { Product_dd, Location_dd } from '../../../redux/actions/Dropdowns'
import { NetworkInfo } from "react-native-network-info";
import { SERVER_ERROR, status, __error } from "../../../Constants/constValues";
import axios from "axios";
import { baseUrl } from "../../../Constants/server";
import { connect } from "react-redux";
import { Platform } from "react-native";
import moment from 'moment'


let filteredLocation = ''
let filteredProduct = ''

function Wasted_edit({ navigation, user, route, loc_dd, product_dd }) {

    let dialogboxRef = useRef()
    let item = route.params.item


    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.access_token}`
            },
            formData: {
                userid: user?.sessions?.userid,
                companyid: user?.sessions?.companyid,
                locationid: user?.sessions?.locationid,
            },
            isdropdownLoading: false,
            ipAddress: '',
            sending: false,
            voucherNumber: item?.vno,
            vnoError: false,
            isCalendar_Visible: false,
            currentDate: '',
            markedDates: {},
            voucherDate: '',
            vdateError: false,
            showProduct: false,

            l_dd: [],
            l_dd_List: [],
            location_id:item?.locationid,

            p_dd: [],
            p_dd_List: [],
            product_id:item?.productid,

            product:item?.productname,
            productError: false,

            location:item?.locationname,
            locationError: false,
            searchLocation: '',

            searchProduct: '',

            showLocations: false,
            quantity:Number(item?.qty).toFixed(0),
            qtyError: false,

            filePath: '',
            fileType: '',
            errMsg: '',
            startDay:'' , 
            endDay:''

        }
    )
    const {
        headers,
        formData,
        voucherNumber,
        isdropdownLoading,
        vnoError,
        ipAddress,
        sending,
        isCalendar_Visible,
        currentDate,
        markedDates,
        voucherDate,
        vdateError,
        showProduct,
        l_dd,
        l_dd_List,
        location_id,

        p_dd,
        p_dd_List,
        product_id,

        product,
        productError,
        searchProduct,
        location,
        locationError,
        searchLocation,
        showLocations,
        quantity,
        qtyError,
        fileType,
        filePath,
        errMsg,
        startDay , 
        endDay

    } = state

    useEffect(() => {

        NetworkInfo.getIPAddress().then(ip => {
            updateState({ ipAddress: ip })
        });

        populateLocation_dd()
        populateProduct_dd()


        let [_d, _y] = item?.vdate.split(' ')
        let [y, m, d] = _d?.split('-')

        let date = `${y}-${m}-${d}`
        let _show = `${d}-${m}-${y}`

        let fullYear = y
        let fullMonth = m -1
        var my_date = new Date(fullYear, fullMonth);

        var first_date = new Date(my_date.getFullYear(), my_date.getMonth(), 1);
        var last_date = new Date(my_date.getFullYear(), my_date.getMonth() +1, 0);

        let _first = moment.utc(first_date).local().format('YYYY-MM-DD HH:mm:ss')
        let [firstDay, _time] = _first.split(' ')

        let _last = moment.utc(last_date).local().format('YYYY-MM-DD HH:mm:ss')
        let [last_day, l_time] = _last.split(' ')


        let selected = {}
        selected[date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        updateState({
            currentDate: date,
            voucherDate: _show,
            markedDates: selected,
            startDay:firstDay,
            endDay:last_day
        })

    }, [])

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', async () => {
            populateLocation_dd()
            populateProduct_dd()
        });
        return unsubscribe;
    }, [])

    function alertMsg(msg1, msg2) {
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2],
            btn: {
                text: 'OK',
                callback: () => { navigation.pop()},
            },
        })
    }

    function populateLocation_dd() {
        updateState({ isdropdownLoading: true })
        let ddValues = []
        if (loc_dd?.length > 0) {
            loc_dd.map((item, index) => {
                ddValues.push(item.vname)
            })

            updateState({
                isdropdownLoading: false,
                l_dd_List: ddValues,// for filter 
                l_dd: loc_dd,
            })
        } else {
            updateState({ isdropdownLoading: false })
        }
    }

    function populateProduct_dd() {
        updateState({ isdropdownLoading: true })
        let ddValues = []
        if (product_dd?.length > 0) {
            product_dd.map((item, index) => {
                ddValues.push(item.vname)
            })

            updateState({
                isdropdownLoading: false,
                p_dd_List: ddValues,// for filter 
                p_dd: product_dd,
            })
        } else {
            updateState({ isdropdownLoading: false })
        }
    }

    function goBack() { navigation.pop() }

    async function selectDocument() {
        try {
            const results = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });

            updateState({
                filePath: results,
                fileType: results[0].type
            })
            for (const res of results) {
                console.log(
                    res.uri,
                    res.type, // mime type
                    res.name,
                    res.size,
                )
            }

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                console.log('Document picker cancel')
            } else {
                throw err;
            }
        }
    }

    function cancelDocument() {
        updateState({
            filePath: '',
            fileType: ''
        })
    }

    function showCalendar() {
        let isVisible = isCalendar_Visible
        updateState({ isCalendar_Visible: !isVisible })
    }

    function onDayPress(date) {

        let [y, m, d] = date.dateString.split('-')
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates: selected,
            currentDate:date.dateString,
            voucherDate: _show,
            isCalendar_Visible: false,
            vdateError: false,
            errMsg: ''
        })
    }



    function closeProductModal() {
        updateState({ showProduct: false })
    }

    function selectProduct(value, index) {
        var FOUND = p_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })

        updateState({
            product: value,
            product_id: FOUND.id,
            showProduct: false,
            searchProduct: '',
            productError: false,
            errMsg: '',
        })
    }
    const handleSearchProduct = (value) => {
        updateState({ searchProduct: value })
    }


    function selectLocation(value, index) {
        var FOUND = l_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })
        updateState({
            location: value,
            location_id: FOUND.id,
            showLocations: false,
            searchLocation: '',
            locationError: false,
            errMsg: '',
        })
    }
    const handleSearchLocation = (value) => {
        updateState({ searchLocation: value })
    }

    function closeLocationModal() {
        updateState({ showLocations: false })
    }

   async function saveWasted() {
        let err = false
        if (!voucherDate) {
            updateState({ vdateError: true, })
            err = true
        }
        if (!location) {
            updateState({ locationError: true, })
            err = true
        }
        if (!product) {
            updateState({ productError: true, })
            err = true
        }
        if (!quantity || isNaN(quantity) || quantity < 1) {
            updateState({ qtyError: true, })
            err = true
        }
        if (!err) {
            // api 
            const __headers = {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${user?.access_token}`
            }

            updateState({ sending: true })
            let tempVar = user?.sessions

            let formData = new FormData()
            formData.append('vdate',voucherDate)
            formData.append('productid',product_id)
            formData.append('qty',quantity)
            if(filePath){
            formData.append('file' ,{
             name: filePath[0]?.name,
             type: filePath[0]?.type,
             uri: Platform.OS === 'ios' ? 
                  filePath[0]?.uri.replace('file://', '')
                  : filePath[0]?.uri,
           })
          }
            formData.append('locationid',location_id)
            formData.append('companyid',tempVar?.companyid)
            formData.append('projectid',tempVar?.projectid)
            formData.append('insertedby',tempVar?.userid)
            formData.append('insertedip',ipAddress)

            // let formData = {
            //     vdate: voucherDate,
            //     productid: product_id,
            //     qty: quantity,
            //     locationid: location_id,
            //     companyid: tempVar?.companyid,
            //     projectid: tempVar?.projectid,
            //     insertedby: tempVar?.userid,
            //     insertedip: ipAddress,
            // }

            console.log('Wasted form data send to server', formData)
        await axios.post(`${baseUrl}/inventory/wasted/update/${item.id}`, formData, {
                headers: __headers
            }).then((response) => {
                const { data } = response
                console.log('response ', data)
                if (data.status === status) {

                    alertMsg(data?.message)
                    updateState({sending: false,})

                } else if (data?.status === __error) {
                    updateState({ sending: false })
                    alertMsg(data?.message)
                    console.log('error in else', data)
                }
            }).catch((err) => {
                updateState({ sending: false })
                alertMsg(SERVER_ERROR)
                console.log('err in catch', err)
            })
        } else {
            updateState({ errMsg: 'Please fill required fields' })
        }
    }

    function cancelWasted() {
        updateState({sending: false,})
        navigation.pop()
    }




    /*********************** custom  dd Product  *************************/
    const filterProductData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return p_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return p_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }

    /*********************** custom  dd location  *************************/
    const filteredLocationData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return l_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return l_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom dd location  *************************/
    filteredLocation = filteredLocationData(searchLocation)
    filteredProduct = filterProductData(searchProduct)


    return (
        <Container>
            <AppHeader
                headingText={'Edit Wasted'}
                rightIconName={'search'}
                leftIconName={'arrow-back'}
                leftIconType={'Ionicons'}
                leftBtnPress={goBack}
                dashbord={false}
                heading={true}
                search={false}
                edit={true}
            />
            <View style={[styles.blueBG]}></View>

            <Content style={styles.container}>

                <InputField
                    label="Voucher Number"
                    placeholder={'Voucher Number'}
                    editable={false}
                    onChangeText={(v) => updateState({
                        voucherNumber: v,
                        vnoError: false,
                        errMsg: '',
                    })}
                    value={voucherNumber}
                    error={vnoError}
                />

                <DatePicker
                    label="Voucher Date"
                    onPress={showCalendar}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={voucherDate}
                    error={vdateError}
                />

                <DropDown
                    label="Location"
                    placeholder={'Location'}
                    value={location}
                    // onPress={() => updateState({ showLocations: true })}
                    error={locationError}
                />

                <DropDown
                    label="Product"
                    placeholder={'Product'}
                    value={product}
                    // onPress={() => updateState({ showProduct: true })}
                    error={productError}
                />

                <InputField
                    label="Quantity"
                    placeholder={'Quantity'}
                    keyboardType={'number-pad'}
                    onChangeText={(v) => updateState({
                        quantity: v,
                        qtyError: false,
                        errMsg: '',
                    })}
                    value={quantity}
                    error={qtyError}
                />

                {errMsg ?
                    <Error
                        error={errMsg}
                    /> : null
                }
                {qtyError ?
                    <Error
                        error={'Quantity shoud be greater than 0'}
                    /> : null
                    }

                <View style={styles.btnView}>
                    <SaveBtn
                        onPress={saveWasted}
                        title={'Save'}
                        saving={sending}
                    />
                    <CancelBtn
                        onPress={cancelWasted}
                        title={'Cancel'}
                    />
                    <DocPicker
                        onPress={selectDocument}
                        fileType={fileType}
                        filePath={filePath}
                        cancelFile={cancelDocument}
                    />
                </View>
            </Content>




            <_Calendar
                is_Visible={isCalendar_Visible}
                hideDatePicker={showCalendar}
                onDayPress={onDayPress}
                currentDate={currentDate}
                markedDates={markedDates}
                minDate={startDay}
                maxDate={endDay}
             />

            <DropdownModal
                isVisible={showProduct}
                closeFromModal={closeProductModal}
                data={filteredProduct}
                onChangeText={handleSearchProduct}
                selectValue={selectProduct}
                isLoading ={isdropdownLoading}
            />
            <DropdownModal
                isVisible={showLocations}
                closeFromModal={closeLocationModal}
                data={filteredLocation}
                onChangeText={handleSearchLocation}
                selectValue={selectLocation}
                isLoading ={isdropdownLoading}
            />

            <DialogBox ref={dialogboxRef} />
        </Container>
    )
}
const mapStateToProps = (state) => ({
    user: state.User.userInfo,
    loc_dd: state.Dropdown.locationDDs,
    product_dd: state.Dropdown.productDDs,
})
const mapDispatchToProps = (dispatch) => ({
    saveLocation: (v) => dispatch(Location_dd(v)),
    saveProduct: (v) => dispatch(Product_dd(v)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Wasted_edit)