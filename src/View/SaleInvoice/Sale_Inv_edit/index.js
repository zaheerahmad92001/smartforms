import React, { useEffect, useRef, useReducer } from "react";
import { View, Text, Image, } from "react-native";
import { Container, Content, Icon } from "native-base";
import InputField from "../../../Components/InputField";
import DatePicker from "../../../Components/DatePicker";
import _Calendar from '../../../Components/Calendar';
import styles from "../styles";
import AppHeader from "../../../Components/AppHeader";
import DropDown from "../../../Components/Dropdown";
import DropdownModal from "../../../Components/dropDownModal";
import SaveBtn from "../../../Components/SaveBtn";
import DialogBox from 'react-native-dialogbox';
import CancelBtn from "../../../Components/CancelBtn";
import DocPicker from "../../../Components/DocPicker";
import DocumentPicker from 'react-native-document-picker'
import Error from "../../../Components/Error";
import { NetworkInfo } from "react-native-network-info";
import { status, STOCK_OUT, STOCK_OUT_MSG, __error } from "../../../Constants/constValues";
import axios from "axios";
import { baseUrl } from "../../../Constants/server";
import { Customer_dd, Bank_dd, Product_dd, Location_dd } from '../../../redux/actions/Dropdowns'
import { connect } from "react-redux";
import moment from 'moment'


let filteredCustomer = ''
let filteredBankAcc = ''
let filteredLocation = ''
let filteredProduct = ''


function SaleInvoice({ navigation, user,route, loc_dd, product_dd, cust_dd , bank_dd }) {
    let dialogboxRef = useRef()
    let item = route.params.item


    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.access_token}`
            },
            formData: {
                userid: user?.sessions?.userid,
                companyid: user?.sessions?.companyid,
                locationid: user?.sessions?.locationid,
            },

            voucherNumber:item?.vno,
            ipAddress: '',
            sending: false,
            vnoError: false,
            isCalendar_Visible: false,
            isCalendar_Visible_cheque: false,
            isdropdownLoading: false,
            currentDate: '',
            c_currentDate:'',
            markedDates: {},
            markedDates_cheque: {},
            showBankList: false,
            voucherDate: '',
            vdateError: false,
            chequeNumber:item?.chequeno,
            chequeNoError: false,
            showCustomerList: false,
            customer:item?.customername,
            customerError: false,
            searchCustomer: '',
            bankAcc:item?.bankaccountname,
            bankAccError: false,
            searchBank: '',

            c_dd: [],
            c_dd_List: [],
            customer_id:item?.customerid,

            b_dd: [],
            b_dd_List: [],
            bank_id:item?.bankid,

            p_dd: [],
            p_dd_List: [],
            product_id:item?.productid,

            l_dd: [],
            l_dd_List: [],
            location_id:item?.locationid,

            chequeDate: '',
            chequeDateError: false,
            location:item?.locationname,
            locationError: false,
            searchLocation: '',
            product:item?.productname,
            productError: false,
            searchProduct: '',
            showLocation: false,
            showProducts: false,
            productName:'',
            quantity:Number(item?.qty).toFixed(0),
            qtyError: false,
            rate:item?.vrate,
            rateError: false,
            filePath: '',
            fileType: '',
            errMsg: '',

            c_startDay:'' , 
            c_endDay:'',
            startDay:'' , 
            endDay:''

        }
    )
    const {
        headers,
        formData,
        ipAddress,
        sending,
        voucherNumber,
        vnoError,
        currentDate,
        c_currentDate,
        markedDates,
        voucherDate,
        vdateError,
        chequeNumber,
        chequeNoError,
        isdropdownLoading,

        c_dd,
        c_dd_List,
        customer_id,

        p_dd,
        p_dd_List,
        product_id,

        b_dd,
        b_dd_List,
        bank_id,

        l_dd,
        l_dd_List,
        location_id,

        showCustomerList,
        isCalendar_Visible,
        isCalendar_Visible_cheque,
        showBankList,
        customer,
        customerError,
        searchCustomer,
        bankAcc,
        bankAccError,
        searchBank,
        chequeDate,
        chequeDateError,
        markedDates_cheque,
        location,
        locationError,
        searchLocation,
        product,
        productError,
        searchProduct,
        showLocation,
        showProducts,
        quantity,
        qtyError,
        rate,
        rateError,
        fileType,
        filePath,
        errMsg,

        c_startDay , 
        c_endDay,
        startDay , 
        endDay

    } = state

    useEffect(() => {

        NetworkInfo.getIPAddress().then(ip => {
            updateState({ ipAddress: ip })
        });

        populateCustomer_dd()
        populateBank_dd()
        populateLocation_dd()
        populateProduct_dd()

        
        let [_d ,_y] = item?.vdate.split(' ')
        let [y,m,d] = _d?.split('-')

        let date = `${y}-${m}-${d}`
        let _show = `${d}-${m}-${y}`

        let fullYear = y
        let fullMonth = m -1
        var my_date = new Date(fullYear, fullMonth);

        var first_date = new Date(my_date.getFullYear(), my_date.getMonth(), 1);
        var last_date = new Date(my_date.getFullYear(), my_date.getMonth() +1, 0);

        let _first = moment.utc(first_date).local().format('YYYY-MM-DD HH:mm:ss')
        let [firstDay, _time] = _first.split(' ')

        let _last = moment.utc(last_date).local().format('YYYY-MM-DD HH:mm:ss')
        let [last_day, l_time] = _last.split(' ')

        // console.log('f' , firstDay , 'last ' , last_day)


        let [_cd ,_cy] = item?.chequedate.split(' ')
        let [cy,cm,cd] = _cd?.split('-')

        let c_date =`${cy}-${cm}-${cd}`
        let c_show = `${cd}-${cm}-${cy}`

        let c_fullYear = cy
        let c_fullMonth = cm - 1
        var c_my_date = new Date(c_fullYear, c_fullMonth);
        var c_first_date = new Date(c_my_date.getFullYear(), c_my_date.getMonth(), 1);
        var c_last_date = new Date(c_my_date.getFullYear(), c_my_date.getMonth() +1, 0);

        let c_first = moment.utc(c_first_date).local().format('YYYY-MM-DD HH:mm:ss')
        let [c_firstDay, c_time] = c_first.split(' ')

        let c_last = moment.utc(c_last_date).local().format('YYYY-MM-DD HH:mm:ss')
        let [c_last_day, c_l_time] = c_last.split(' ')

        console.log('f' , c_firstDay , 'last ' , c_last_day)


        let selected = {}
        selected[date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }

        let c_selected = {}
        c_selected[c_date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        updateState({
            currentDate: date,
            c_currentDate: c_date,
            voucherDate: _show,
            chequeDate: c_show,
            markedDates: selected,
            markedDates_cheque: c_selected,

            c_startDay:c_firstDay,
            c_endDay:c_last_day,
            startDay:firstDay,
            endDay:last_day
        })

    }, [])

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', async () => {
            populateCustomer_dd()
            populateBank_dd()
            populateLocation_dd()
            populateProduct_dd()
        });
        return unsubscribe;
    }, [])

    function alertMsg(msg1, msg2) {
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2],
            btn: {
                text: 'OK',
                callback: () => {navigation.pop()},
            },
        })

    }

    function populateCustomer_dd() {
        updateState({ isdropdownLoading: true })
                let ddValues = []
                if (cust_dd?.length > 0) {
                    cust_dd.map((item, index) => {
                        ddValues.push(item.vname)
                    })

                    updateState({
                        isdropdownLoading: false,
                        c_dd_List: ddValues,// for filter 
                        c_dd:cust_dd,
                    })
                } else {
                    updateState({ isdropdownLoading: false })
                }
            }

    function populateBank_dd() {
        updateState({ isdropdownLoading: true })
        
                let ddValues = []
                if (bank_dd?.length > 0) {
                    bank_dd.map((item, index) => {
                        ddValues.push(item.vname)
                    })

                    updateState({
                        isdropdownLoading: false,
                        b_dd_List: ddValues,// for filter 
                        b_dd:bank_dd,
                    })
                } else {
                    updateState({ isdropdownLoading: false })
                }
            }

    function populateLocation_dd() {
        updateState({ isdropdownLoading: true })
                let ddValues = []
                if (loc_dd?.length > 0) {
                    loc_dd.map((item, index) => {
                        ddValues.push(item.vname)
                    })

                    updateState({
                        isdropdownLoading: false,
                        l_dd_List: ddValues,// for filter 
                        l_dd:loc_dd,
                    })
                } else {
                    updateState({ isdropdownLoading: false })
                }
            }

    function populateProduct_dd() {
        updateState({ isdropdownLoading: true })
                let ddValues = []
                if (product_dd?.length > 0) {
                    product_dd.map((item, index) => {
                        ddValues.push(item.vname)
                    })

                    updateState({
                        isdropdownLoading: false,
                        p_dd_List: ddValues,// for filter 
                        p_dd:product_dd,
                    })
                } else {
                    updateState({ isdropdownLoading: false })
                }
            }


    function goBack() {navigation.pop();}

    async function selectDocument() {
        try {
            const results = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });

            updateState({
                filePath: results,
                fileType: results[0].type
            })
            for (const res of results) {
                console.log(
                    res.uri,
                    res.type, // mime type
                    res.name,
                    res.size,
                )
            }

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                console.log('Document picker cancel')
            } else {
                throw err;
            }
        }
    }

    function cancelDocument() {
        updateState({
            filePath: '',
            fileType: ''
        })
    }

    function showCalendar() {
        let isVisible = isCalendar_Visible
        updateState({ isCalendar_Visible: !isVisible })
    }

    function showCalendar_cheque() {
        let isVisible = isCalendar_Visible_cheque
        updateState({ isCalendar_Visible_cheque: !isVisible })

    }

    function closeBankModal() {
        updateState({ showBankList: false })
    }

    function selectBank(value, index) {
        var FOUND = b_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })
        updateState({
            bankAcc: value,
            bank_id: FOUND.id,
            showBankList: false,
            searchBank: '',
            bankAccError: false,
            errMsg: '',
        })
    }
    const handleSearchBank = (value) => {
        updateState({ searchBank: value })
    }

    function closeProductModal() {
        updateState({ showProducts: false })
    }

    function selectProduct(value, index) {
        var FOUND = p_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })
        updateState({
            product: value,
            product_id: FOUND.id,
            showProducts: false,
            searchProduct: '',
            productError: false,
            errMsg: '',
        })
    }
    const handleSearchProduct = (value) => {
        updateState({ searchProduct: value })
    }


    function onDayPress(date) {

        let [y, m, d] = date.dateString.split('-')
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates: selected,
            voucherDate: _show,
            isCalendar_Visible: false,
            vdateError: false,
            errMsg: '',
        })
    }

    function onDayPress_cheque(date) {

        let [y, m, d] = date.dateString.split('-')
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates_cheque: selected,
            chequeDate: _show,
            isCalendar_Visible_cheque: false,
            chequeDateError: false,
            errMsg: '',
        })
    }

    function selectCustomer(value, index) {
        var FOUND = c_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })
        updateState({
            customer: value,
            customer_id: FOUND.id,
            showCustomerList: false,
            searchCustomer: '',
            customerError: false,
            errMsg: '',
        })
    }
    const handleSearchCustomer = (value) => {
        updateState({ searchCustomer: value })
    }


    function selectLocation(value, index) {
        var FOUND = l_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })
        updateState({
            location: value,
            location_id: FOUND.id,
            showLocation: false,
            searchLocation: '',
            locationError: false,
            errMsg: ''

        })
    }
    const handleSearchLocation = (value) => {
        updateState({ searchLocation: value })
    }


    function closeCustomerModal() {
        updateState({ showCustomerList: false })
    }

    function closeLocationModal() {
        updateState({ showLocation: false })
    }

   async function saveSaleInvoice() {
        let err = false
        if (!voucherDate) {
            updateState({ vdateError: true, })
            err = true
        }
        if (!chequeDate) {
            updateState({ chequeDateError: true })
            err = true
        }
        if (!customer) {
            updateState({ customerError: true })
            err = true
        }
        if (!bankAcc) {
            updateState({ bankAccError: true })
            err = true
        }
        if (!chequeNumber) {
            updateState({ chequeNoError: true })
            err = true
        }
        if (!location) {
            updateState({ locationError: true })
            err = true
        }
        if (!product) {
            updateState({ productError: true })
            err = true
        }
        if (!quantity || isNaN(quantity) || quantity < 1) {
            updateState({ qtyError: true })
            err = true
        }
        if (!rate || isNaN(rate)) {
            updateState({ rateError: true })
            err = true
        }
        if (!err) {
            // api 
            const __headers = {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${user?.access_token}`
            }
            updateState({ sending: true })
            let tempVar = user?.sessions

            let __formData = new FormData()
            __formData.append('vdate',voucherDate)
            __formData.append('customerid', customer_id)
            __formData.append('bankid',bank_id)
            __formData.append('chequeno', chequeNumber)
            __formData.append('chequedate',chequeDate)
            __formData.append('productid',product_id)
            __formData.append('qty',quantity)
            __formData.append('vrate',rate)
            __formData.append('locationid',location_id)
            __formData.append('companyid',tempVar?.companyid)
            __formData.append('projectid',tempVar?.projectid)
            __formData.append('insertedby',tempVar?.userid)
            __formData.append('insertedip',ipAddress)
            if(filePath){
            __formData.append('file' ,{
                name: filePath[0]?.name,
                type: filePath[0]?.type,
                uri: Platform.OS === 'ios' ? 
                     filePath[0]?.uri.replace('file://', '')
                     : filePath[0]?.uri,
              })
            }
              
            // let __formData = {
            //     vdate: voucherDate,
            //     customerid: customer_id,
            //     bankid: bank_id,
            //     chequeno: chequeNumber,
            //     chequedate: chequeDate,
            //     productid: product_id,
            //     qty: quantity,
            //     vrate: rate,
            //     locationid: location_id,
            //     companyid: tempVar?.companyid,
            //     projectid: tempVar?.projectid,
            //     insertedby: tempVar?.userid,
            //     insertedip: ipAddress,
            // }

            console.log('sale data send to server', __formData)
           await axios.post(`${baseUrl}/sale/invoices/update/${item?.id}`, __formData, {
                headers: __headers
            }).then((response) => {
                const { data } = response
                console.log('response ', data)
                if (data.status === status) {
                    alertMsg(data?.message)
                    updateState({sending: false, })

                } else if(data?.status===__error){
                    updateState({sending:false})
                    alertMsg(data?.message)
                    console.log('error in else if' , data)
                }else{
                    updateState({sending:false})
                    alertMsg(SERVER_ERROR)
                    console.log('error in else ' , data)
                }
            }).catch((err) => {
                updateState({ sending: false })
                alertMsg(SERVER_ERROR)
                console.log('err in catch', err)
            })
        } else {
            updateState({ errMsg: 'Please fill required fields' })
        }

    }

    function cancelSaleInvoice() {
        updateState({sending: false,})
        navigation.pop()
    }



    /*********************** custom dd vendor  *************************/
    const filterCustomerData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return c_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return c_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom  dd Product  *************************/
    const filterProductData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return p_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return p_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom  dd Bank  *************************/
    const filteredBankData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return b_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return b_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom  dd location  *************************/
    const filteredLocationData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return l_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return l_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom dd location  *************************/
    filteredCustomer = filterCustomerData(searchCustomer)
    filteredProduct = filterProductData(searchProduct)
    filteredBankAcc = filteredBankData(searchBank)
    filteredLocation = filteredLocationData(searchLocation)


    return (
        <Container>
            <AppHeader
                headingText={'Edit Sale Invoice'}
                leftIconName={'arrow-back'}
                leftIconType={'Ionicons'}
                leftBtnPress={goBack}
                dashbord={false}
                heading={true}
                search={false}
                edit={true}
            />
            <View style={[styles.blueBG]}></View>

            <Content style={styles.container}>
                <InputField
                    label="Voucher Number"
                    placeholder={'Voucher Number'}
                    editable={false}
                    onChangeText={(v) => updateState({
                        voucherNumber: v,
                        vnoError: false,
                        errMsg: '',
                    })}
                    value={voucherNumber}
                    error={vnoError}
                />
                <DatePicker
                    label="Voucher Date"
                    onPress={showCalendar}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={voucherDate}
                    error={vdateError}
                />

                <DropDown
                    label="Customer"
                    placeholder={'Customer'}
                    value={customer}
                    // onPress={() => updateState({ showCustomerList: true })}
                    error={customerError}
                />

                <DropDown
                    label="Bank Account"
                    placeholder={'Bank Account'}
                    value={bankAcc}
                    // onPress={() => updateState({ showBankList: true })}
                    error={bankAccError}
                />


                <InputField
                    label="Cheque Number"
                    placeholder={'Cheque Number'}
                    onChangeText={(v) => updateState({
                        chequeNumber: v,
                        chequeNoError: false,
                        errMsg: '',
                    })}
                    value={chequeNumber}
                    error={chequeNoError}
                />
                <DatePicker
                    label="Cheque Date"
                    onPress={showCalendar_cheque}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={chequeDate}
                    error={chequeDateError}
                />


                <DropDown
                    label="Location"
                    placeholder={'Location'}
                    value={location}
                    // onPress={() => updateState({ showLocation: true })}
                    error={locationError}
                />

                <DropDown
                    label="Product"
                    placeholder={'Product'}
                    value={product}
                    // onPress={() => updateState({ showProducts: true })}
                    error={productError}
                />

                <InputField
                    label="Quantity"
                    placeholder={'Quantity'}
                    keyboardType={'number-pad'}
                    onChangeText={(v) => updateState({
                        quantity: v,
                        qtyError: false,
                        errMsg: '',
                    })}
                    value={quantity}
                    error={qtyError}
                />
                <InputField
                    label="Rate"
                    placeholder={'Rate'}
                    keyboardType={'decimal-pad'}
                    onChangeText={(v) => updateState({
                        rate: v,
                        rateError: false,
                        errMsg: ''
                    })}
                    value={rate}
                    error={rateError}
                />

                {errMsg ?
                    <Error
                        error={errMsg}
                    /> : null
                }
                {qtyError ?
                    <Error
                        error={'Quantity shoud be greater than 0'}
                    /> : null
                    }
                <View style={styles.btnView}>
                    <SaveBtn
                        onPress={saveSaleInvoice}
                        title={'Save'}
                        saving={sending}
                    />
                    <CancelBtn
                        onPress={cancelSaleInvoice}
                        title={'Cancel'}
                    />
                    <DocPicker
                        onPress={selectDocument}
                        fileType={fileType}
                        filePath={filePath}
                        cancelFile={cancelDocument}
                    />
                </View>
            </Content>




            <_Calendar
                is_Visible={isCalendar_Visible}
                hideDatePicker={showCalendar}
                onDayPress={onDayPress}
                currentDate={currentDate}
                markedDates={markedDates}
                minDate={startDay}
                maxDate={endDay}
            />

            <_Calendar
                is_Visible={isCalendar_Visible_cheque}
                hideDatePicker={showCalendar_cheque}
                onDayPress={onDayPress_cheque}
                currentDate={c_currentDate}
                markedDates={markedDates_cheque}
                minDate={c_startDay}
                maxDate={c_endDay}
            />

            <DropdownModal
                isVisible={showCustomerList}
                closeFromModal={closeCustomerModal}
                data={filteredCustomer}
                onChangeText={handleSearchCustomer}
                selectValue={selectCustomer}
                isLoading ={isdropdownLoading}
            />

            <DropdownModal
                isVisible={showLocation}
                closeFromModal={closeLocationModal}
                data={filteredLocation}
                onChangeText={handleSearchLocation}
                selectValue={selectLocation}
                isLoading ={isdropdownLoading}

            />

            <DropdownModal
                isVisible={showBankList}
                closeFromModal={closeBankModal}
                data={filteredBankAcc}
                onChangeText={handleSearchBank}
                selectValue={selectBank}
                isLoading ={isdropdownLoading}

            />

            <DropdownModal
                isVisible={showProducts}
                closeFromModal={closeProductModal}
                data={filteredProduct}
                onChangeText={handleSearchProduct}
                selectValue={selectProduct}
                isLoading ={isdropdownLoading}

            />

            <DialogBox ref={dialogboxRef} />
        </Container>
    )

}
const mapStateToProps = (state) => ({
    user: state.User.userInfo,
    bank_dd: state.Dropdown.bankDDs,
    loc_dd: state.Dropdown.locationDDs,
    cust_dd: state.Dropdown.customerDDs,
    product_dd: state.Dropdown.productDDs,  
})
const mapDispatchToProps = (dispatch) => ({
    saveCustomer: (v) => dispatch(Customer_dd(v)),
    saveBank: (v) => dispatch(Bank_dd(v)),
    saveLocation: (v) => dispatch(Location_dd(v)),
    saveProduct: (v) => dispatch(Product_dd(v)),
});
export default connect(mapStateToProps, mapDispatchToProps)(SaleInvoice)

