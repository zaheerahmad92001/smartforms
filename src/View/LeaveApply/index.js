import React, { useEffect, useRef, useReducer } from 'react'
import { View, Text, Keyboard } from 'react-native'
import { Container, Content, Icon, Item } from "native-base";
import AppHeader from "../../Components/AppHeader";
import SaveBtn from "../../Components/SaveBtn";
import CancelBtn from "../../Components/CancelBtn";
import styles from './styles'
import Category from '../../Components/LeaveCategory';
import { FlatList } from 'react-native';
import DropdownModal from '../../Components/dropDownModal';
import DropDown from '../../Components/Dropdown';
import DialogBox from 'react-native-dialogbox';
import _Calendar from '../../Components/Calendar';
import DatePicker from "../../Components/DatePicker";
import InputField from '../../Components/InputField';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import DocPicker from "../../Components/DocPicker";
import DocumentPicker from 'react-native-document-picker'
import Error from '../../Components/Error';
import { NetworkInfo } from "react-native-network-info";
import { SERVER_ERROR, status, __error } from "../../Constants/constValues";
import axios from "axios";
import { baseUrl } from "../../Constants/server";
import { Employee_dd, leave_Types } from '../../redux/actions/Dropdowns'
import { connect } from "react-redux";
import BottomTabs from '../../Components/BottomTab';


let filteredEmployee = ''

function LeaveApply({ navigation, user, saveEmployee, saveLeaveCategory }) {

    let dialogboxRef = useRef()

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.access_token}`
            },
            leave_type_id: '',
            leaveType: '',
            leaves_list: '',
            showEmployeeCodeList: false,
            markedDates_from: {},
            markedDatesTo: {},
            emp_id: '',
            emp_name: '',
            emp_code_err: false,
            searchEmployeeCode: '',
            employee_list: [],
            employee_dd: [],
            remarks: '',
            remarksError: false,
            fromDate: new Date(),
            toDate: new Date(),
            from: '',
            fromDateError: false,
            to: '',
            toDateError: false,
            isFromDate_visible: false,
            isToDate_visible: false,
            filePath: '',
            fileType: '',
            errMsg: '',
            isdropdownLoading: false,
            ipAddress: '',
            sending: false,
            last_date: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0)

        }
    )

    const {
        headers,
        leave_type_id,
        leaveType,
        leaves_list,
        markedDates_from,
        emp_code_err,
        markedDates_to,
        showEmployeeCodeList,
        emp_name,
        emp_id,
        searchEmployeeCode,
        employee_list,
        employee_dd,
        remarks,
        remarksError,
        fromDate,
        fromDateError,
        toDateError,
        toDate,
        from,
        to,
        isFromDate_visible,
        isToDate_visible,
        fileType,
        filePath,
        errMsg,
        ipAddress,
        sending,
        last_date,
        isdropdownLoading,

    } = state

    useEffect(() => {

        load_LeaveType()
        load_Employee()

        NetworkInfo.getIPAddress().then(ip => {
            updateState({ ipAddress: ip })
        });

        let d = new Date().getDate()
        let m = new Date().getMonth() + 1
        let y = new Date().getFullYear()

        if (d <= 9) {
            d = '0' + d
        }
        if (m <= 9) {
            m = '0' + m
        }
        let date = `${y}-${m}-${d}`
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        updateState({
            from:_show,
            to:_show,
            markedDates_from: selected,
            markedDates_to: selected,
        })

        const unsubscribe = navigation.addListener('focus', async () => {
            cancelLeave()
        });

        return unsubscribe;
    }, [])

    function navigateTo() { navigation.navigate('leave_search') }

    function load_LeaveType() {
        updateState({ isdropdownLoading: true })

        let tempVar = user?.sessions
        let formData = {
            userid: tempVar?.userid,
            companyid: tempVar?.companyid,
            locationid: tempVar?.locationid,
            //  accountgroupcode: 'B'
        }

        axios.post(`${baseUrl}/attendance/leave-types`, formData, {
            headers: headers
        }).then((response) => {
            const { data } = response
            console.log('leave types', data)
            if (data?.status === status) {

                saveLeaveCategory(data?.leave_types)

                updateState({
                    leaves_list: data?.leave_types,
                    isdropdownLoading: false
                })
            } else if (data?.state === __error) {
                alertMsg(data?.message)
                updateState({
                    isdropdownLoading: false,
                    leaves_list: ''
                })
            }
        }).catch((err) => {
            console.log('err in catch', err)
            alertMsg(SERVER_ERROR)
            updateState({
                isdropdownLoading: false,
                leaves_list: ''
            })
        })
    }

    function load_Employee() {
        updateState({ isdropdownLoading: true })

        let tempVar = user?.sessions
        let formData = {
            userid: tempVar?.userid,
            companyid: tempVar?.companyid,
            locationid: tempVar?.locationid,
            isactive: 1
        }
        axios.post(`${baseUrl}/attendance/employees`, formData, {
            headers: headers
        }).then((response) => {
            const { data } = response
            if (data.status === status) {

                /********* Redux  *********/
                saveEmployee(data.employees)

                let ddValue = []
                data.employees.map((item, index) => {
                    ddValue.push(item.employeename)
                })
                updateState({
                    employee_list: ddValue,
                    employee_dd: data.employees
                })
            } else {
                updateState({ isdropdownLoading: false })
                alertMsg(SERVER_ERROR)
                console.log('error in else',)
            }
        }).catch((err) => {
            updateState({ isdropdownLoading: false })
            alertMsg(SERVER_ERROR)
            console.log('error in catch', err)
        })
    }

    function openMenu() {
        Keyboard.dismiss()
        setTimeout(() => {
            navigation.openDrawer();
        }, 500)
    }

    async function selectDocument() {
        try {
            const results = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });

            updateState({
                filePath: results,
                fileType: results[0].type
            })
            for (const res of results) {
                console.log(
                    res.uri,
                    res.type, // mime type
                    res.name,
                    res.size,
                )
            }

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                console.log('Document picker cancel')
            } else {
                throw err;
            }
        }
    }

    function cancelDocument() {
        updateState({
            filePath: '',
            fileType: ''
        })
    }

    function closeEmployeeCodeList() {
        updateState({ showEmployeeCodeList: false })
    }

    function selectCode(value, index) {
        var FOUND = employee_dd.find(function (post, index) {
            if (post.employeename == value) {
                console.log('post', post)
                return post;
            }
        })
        updateState({
            emp_id: FOUND.id,
            emp_name: value,
            showEmployeeCodeList: false,
            searchEmployeeCode: '',
            emp_code_err: false,
            errMsg: '',
        })
    }
    const handleSearchEmployeeCode = (value) => {
        updateState({ searchEmployeeCode: value })
    }

    const renderItem = ({ item, index }) => {
        return (
            <Category
                key={item.vcode}
                onPress={() => selectCategory(item, index)}
                item={item}
                index={index}
                selectedIndex={leave_type_id}

            />
        )
    }

    function selectCategory(item, index) {
        updateState({
            leave_type_id: item.id,
            leaveType: item.vname
        })
    }

    function alertMsg(msg1, msg2) {
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2],
            btn: {
                text: 'OK',
                callback: () => { },
            },
        })

    }

    function showCalendar_from() {
        let isVisible = isFromDate_visible
        updateState({ isFromDate_visible: !isVisible })
    }

    function onDayPress_from(date) {

        let [y, m, d] = date.dateString.split('-')
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[fromDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates_from: selected,
            from: _show,
            fromDateError: false,
            fromDate: date.dateString,
            isFromDate_visible: false,
        })
    }

    function showCalendar_to() {
        let isVisible = isToDate_visible
        updateState({ isToDate_visible: !isVisible })
    }

    function onDayPress_to(date) {

        let [y, m, d] = date.dateString.split('-')
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[toDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates_to: selected,
            to: _show,
            toDateError: false,
            toDate: date.dateString,
            isToDate_visible: false,
        })
    }

    async function saveLeave() {
      let  f_date = new Date(fromDate)
      let t_date = new Date(toDate)

        let err = false
        if (!leaveType) {
            alertMsg('Please select leave type')
            err = true
        }

        if (!from) {
            updateState({ fromDateError: true })
            err = true
        }
        if (!to) {
            updateState({ toDateError: true })
            err = true
        }
        if(f_date > t_date ){
            err = true
            alertMsg(`'From Date' should be less or equal to 'To Date'`)
        }else{
            console.log('errr' , f_date > t_date)
        }
        

        if (!emp_name) {
            updateState({ emp_code_err: true })
            err = true
        }
        if (!remarks) {
            updateState({ remarksError: true })
            err = true
        }
        if (!err) {
            // api
            const __headers = {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${user?.access_token}`
            }
            updateState({ sending: true })
            let tempVar = user?.sessions

            let formData = new FormData()
            formData.append('leavetypeid', leave_type_id)
            formData.append('employeeid', emp_id)
            formData.append('datein', from)
            formData.append('dateout', to)
            formData.append('remarks', remarks)
            formData.append('companyid', tempVar.companyid)
            formData.append('locationid', tempVar.locationid)
            formData.append('insertedby', tempVar?.userid)
            formData.append('insertedip', ipAddress)
            formData.append('updatedby', tempVar?.userid)
            formData.append('updatedip', ipAddress)
            if (filePath) {
                formData.append('file', {
                    name: filePath[0]?.name,
                    type: filePath[0]?.type,
                    uri: Platform.OS === 'ios' ?
                        filePath[0]?.uri.replace('file://', '')
                        : filePath[0]?.uri,
                })
            }


            await axios.post(`${baseUrl}/attendance/leaves/create`, formData, {
                headers: __headers
            }).then((response) => {
                const { data } = response
                console.log('Leave apply', data)
                if (data.status === status) {
                    updateState({
                        from: '',
                        to: '',
                    })
                    cancelLeave()
                    
                    alertMsg('Applied Successfully !')
                } else if (data?.status === __error) {
                    console.log('err in else if ', data)
                    updateState({ sending: false })
                    alertMsg(data?.message)
                } else {
                    console.log('err in else ', data)
                    updateState({ sending: false })
                    alertMsg(SERVER_ERROR)
                }
            }).catch((err) => {
                console.log('err in catch ', err)
                updateState({ sending: false })
                alertMsg(SERVER_ERROR)
            })

        } else {
            updateState({ errMsg: 'Please fill required fields' })
        }
    }

    function cancelLeave() {

        updateState({
            sending: false,
            // from: '',
            // to: '',
            emp_name: '',
            remarks: '',
            leaveType: '',
            fileType: '',
            filePath: '',
            leave_type_id: '',
            errMsg: '',

            fromDateError: false,
            toDateError: false,
            emp_code_err: false,
            remarksError: false,
        })
    }

    /*********************** custom dd vendor  *************************/
    const filterEmployeeData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return employee_list
            }
            const regex = new RegExp([query.trim()], 'i')
            return employee_list.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom dd vendor  *************************/
    filteredEmployee = filterEmployeeData(searchEmployeeCode)

    return (
        <Container>
            <AppHeader
                rightIconName={'search'}
                rightIconType={'EvilIcons'}
                rightBtnPress={navigateTo}
                headingText={'Leave Apply'}
                leftBtnPress={openMenu}
                dashbord={false}
                heading={true}
                search={false}
            />
            <View style={[styles.blueBG]}></View>
            <Content style={styles.container}>
                <Text style={styles.category}>Leave Category</Text>

                <FlatList
                    data={leaves_list}
                    keyExtractor={(item) => { `${item.id}` }}
                    renderItem={renderItem}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    style={{ marginBottom: 10, }}
                />

                <DropDown
                    label="Employee"
                    placeholder={'Employee'}
                    value={emp_name}
                    onPress={() => updateState({ showEmployeeCodeList: true })}
                    error={emp_code_err}
                />
                <View style={styles.dateView}>
                    {/* <TimePickerField
                        label="From"
                        placeholder={'20-12-2021'}
                        iconName={'calendar-month-outline'}
                        iconType={'MaterialCommunityIcons'}
                        value={from}
                        onPress={() => updateState({ isFromDate_visible: true })}
                        inputViewStyle={{ width: wp(43) }}
                    /> */}
                    <DatePicker
                        label="From"
                        onPress={showCalendar_from}
                        iconName={'calendar-month-outline'}
                        iconType={'MaterialCommunityIcons'}
                        date={from}
                        inputViewStyle={{ width: wp(44) }}
                        error={fromDateError}

                    />
                    <DatePicker
                        label="to"
                        onPress={showCalendar_to}
                        iconName={'calendar-month-outline'}
                        iconType={'MaterialCommunityIcons'}
                        date={to}
                        inputViewStyle={{ width: wp(44) }}
                        error={toDateError}
                    />

                </View>

                <InputField
                    label="Remarks"
                    placeholder={'Remarks'}
                    onChangeText={(v) => updateState({
                        remarks: v,
                        remarksError: false,
                        errMsg: '',
                    })}
                    value={remarks}
                    multiline={true}
                    inputViewStyle={{ paddingVertical: 7 }}
                    error={remarksError}
                />
                {errMsg ?
                    <Error
                        error={errMsg}
                    /> : null
                }
                <View style={styles.btnView}>
                    <SaveBtn
                        onPress={saveLeave}
                        title={'Save'}
                        saving={sending}
                    />
                    <CancelBtn
                        title={'Cancel'}
                        onPress={cancelLeave}
                    />
                    <DocPicker
                        onPress={selectDocument}
                        fileType={fileType}
                        filePath={filePath}
                        cancelFile={cancelDocument}
                    />
                </View>
            </Content>

            <DropdownModal
                isVisible={showEmployeeCodeList}
                closeFromModal={closeEmployeeCodeList}
                data={filteredEmployee}
                onChangeText={handleSearchEmployeeCode}
                selectValue={selectCode}
                isLoading={isdropdownLoading}
            />

            <_Calendar
                is_Visible={isFromDate_visible}
                hideDatePicker={showCalendar_from}
                onDayPress={onDayPress_from}
                currentDate={fromDate}
                markedDates={markedDates_from}
                maxDate={last_date}
            />
            <_Calendar
                is_Visible={isToDate_visible}
                hideDatePicker={showCalendar_to}
                onDayPress={onDayPress_to}
                currentDate={toDate}
                markedDates={markedDates_to}
                maxDate={last_date}
            />
            <BottomTabs
                navigation={navigation}
                jvPress={() => navigation.navigate('journalVoucher')}
                purchasePress={() => navigation.navigate('Purchase')}
                saleInvoicePress={() => navigation.navigate('saleInvoice')}
                leaveApplyPress={() => navigation.navigate('leaveApply')}
            />
            <DialogBox ref={dialogboxRef} />
        </Container>
    )
}
const mapStateToProps = (state) => ({
    user: state.User.userInfo,
})
const mapDispatchToProps = (dispatch) => ({
    saveEmployee: (d) => dispatch(Employee_dd(d)),
    saveLeaveCategory: (d) => dispatch(leave_Types(d)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LeaveApply)


