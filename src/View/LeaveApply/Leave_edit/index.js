import React, { useEffect, useRef, useReducer } from 'react'
import { View, Text, Keyboard } from 'react-native'
import { Container, Content, Icon, Item } from "native-base";
import AppHeader from "../../../Components/AppHeader";
import SaveBtn from "../../../Components/SaveBtn";
import CancelBtn from "../../../Components/CancelBtn";
import styles from '../styles'
import Category from '../../../Components/LeaveCategory';
import { FlatList } from 'react-native';
import DropdownModal from '../../../Components/dropDownModal';
import DropDown from '../../../Components/Dropdown';
import DialogBox from 'react-native-dialogbox';
import _Calendar from '../../../Components/Calendar';
import DatePicker from "../../../Components/DatePicker";
import InputField from '../../../Components/InputField';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import DocPicker from "../../../Components/DocPicker";
import DocumentPicker from 'react-native-document-picker'
import Error from '../../../Components/Error';

import { NetworkInfo } from "react-native-network-info";
import { SERVER_ERROR, status, __error } from "../../../Constants/constValues";
import axios from "axios";
import { baseUrl } from "../../../Constants/server";
import { connect } from "react-redux";
import moment from 'moment'



let filteredEmployee = ''

function Leave_Edit({ navigation, user, route, leave_cat, emp_dd }) {
    let item = route.params.item
    let dialogboxRef = useRef()

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.access_token}`
            },
            leave_type_id:item?.leavetypeid,
            leaveType:item?.leavename,
            leaves_list: leave_cat,
            showEmployeeCodeList: false,
            markedDates_from: {},
            markedDatesTo: {},
            emp_id:item?.employeeid,
            emp_name:item?.employeename,
            emp_code_err: false,
            searchEmployeeCode: '',
            employee_list: [],
            employee_dd: [],
            remarks:item?.remarks,
            remarksError: false,
            fromDate: '',
            toDate: '',
            from: '',
            to: '',
            isFromDate_visible: false,
            isToDate_visible: false,
            filePath: '',
            fileType: '',
            errMsg: '',
            isdropdownLoading: false,
            ipAddress: '',
            sending: false,
            last_date: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0),

            c_startDay:'' , 
            c_endDay:'',
            startDay:'' , 
            endDay:''

        }
    )

    const {
        headers,
        leave_type_id,
        leaveType,
        leaves_list,
        markedDates_from,
        emp_code_err,
        markedDates_to,
        showEmployeeCodeList,
        emp_name,
        emp_id,
        searchEmployeeCode,
        employee_list,
        employee_dd,
        remarks,
        remarksError,
        fromDate,
        toDate,
        from,
        to,
        isFromDate_visible,
        isToDate_visible,
        fileType,
        filePath,
        errMsg,
        ipAddress,
        sending,
        isdropdownLoading,
        last_date,

        c_startDay , 
        c_endDay,
        startDay , 
        endDay

    } = state

    useEffect(() => {

        load_Employee()

        NetworkInfo.getIPAddress().then(ip => {
            updateState({ ipAddress: ip })
        });

        let [_d,_t] = item?.datein.split(' ')
        let [y,m,d] = _d?.split('-')
 
        let f_date = `${y}-${m}-${d}`
        let f_show = `${d}-${m}-${y}`

        let fullYear = y
        let fullMonth = m -1
        var my_date = new Date(fullYear, fullMonth);

        var first_date = new Date(my_date.getFullYear(), my_date.getMonth(), 1);
        var last_date = new Date(my_date.getFullYear(), my_date.getMonth() +1, 0);

        let firstDay = moment.utc(first_date).local().format('YYYY-MM-DD')
        let last_day = moment.utc(last_date).local().format('YYYY-MM-DD')


        let [t_d,t_t] = item?.dateout.split(' ')
        let [t_y,t_m,t_dd] = t_d?.split('-')
 
        let t_date = `${t_y}-${t_m}-${t_dd}`
        let t_show = `${t_dd}-${t_m}-${t_y}`

        let c_fullYear = t_y
        let c_fullMonth = t_m - 1

        var c_my_date = new Date(c_fullYear, c_fullMonth);
        var c_first_date = new Date(c_my_date.getFullYear(), c_my_date.getMonth(), 1);
        var c_last_date = new Date(c_my_date.getFullYear(), c_my_date.getMonth() +1, 0);

        let c_firstDay = moment.utc(c_first_date).local().format('YYYY-MM-DD')
        let c_last_day = moment.utc(c_last_date).local().format('YYYY-MM-DD')


        let f_selected = {}
        f_selected[f_date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        let t_selected = {}
        t_selected[t_date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        updateState({
            from: f_show,
            to: t_show,
            fromDate:f_date,
            toDate:t_date,
            markedDates_from: f_selected,
            markedDates_to: t_selected,

            c_startDay:c_firstDay,  // to date
            c_endDay:c_last_day,  // to date
 
            startDay:firstDay,  // from date
            endDay:last_day      // from date 
        })


    }, [])



    function load_Employee() {
        updateState({ isdropdownLoading: true })
        if (emp_dd?.length > 0) {
            let ddValue = []
            emp_dd.map((item, index) => {
                ddValue.push(item.employeename)
            })
            updateState({
                employee_list: ddValue,
                employee_dd: emp_dd
            })
        } else {
            updateState({ isdropdownLoading: false })
        }

    }

    function goBack() {navigation.pop()}


    async function selectDocument() {
        try {
            const results = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });

            updateState({
                filePath: results,
                fileType: results[0].type
            })
            for (const res of results) {
                console.log(
                    res.uri,
                    res.type, // mime type
                    res.name,
                    res.size,
                )
            }

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                console.log('Document picker cancel')
            } else {
                throw err;
            }
        }
    }

    function cancelDocument() {
        updateState({
            filePath: '',
            fileType: ''
        })
    }

    function closeEmployeeCodeList() {
        updateState({ showEmployeeCodeList: false })
    }

    function selectCode(value, index) {
        var FOUND = employee_dd.find(function (post, index) {
            if (post.employeename == value) {
                return post;
            }
        })
        updateState({
            emp_id: FOUND.id,
            emp_name: value,
            showEmployeeCodeList: false,
            searchEmployeeCode: '',
            emp_code_err: false,
            errMsg: '',
        })
    }
    const handleSearchEmployeeCode = (value) => {
        updateState({ searchEmployeeCode: value })
    }

    const renderItem = ({ item, index }) => {
        return (
            <Category
                key={item.vcode}
                onPress={() => selectCategory(item, index)}
                item={item}
                index={index}
                selectedIndex={leave_type_id}

            />
        )
    }

    function selectCategory(item, index) {
        updateState({
            leave_type_id:item?.id,
            leaveType: item.vname
        })
    }

    

    function alertMsg(msg1, msg2) {
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2],
            btn: {
                text: 'OK',
                callback: () => { },
            },
        })

    }

    function showCalendar_from() {
        let isVisible = isFromDate_visible
        updateState({ isFromDate_visible: !isVisible })
    }

    function onDayPress_from(date) {

        let [y, m, d] = date.dateString.split('-')
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[fromDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates_from: selected,
            from: _show,
            fromDate: date.dateString,
            isFromDate_visible: false,
        })
    }

    function showCalendar_to() {
        let isVisible = isToDate_visible
        updateState({ isToDate_visible: !isVisible })
    }

    function onDayPress_to(date) {

        let [y, m, d] = date.dateString.split('-')
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[toDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates_to: selected,
            to: _show,
            toDate: date.dateString,
            isToDate_visible: false,
        })
    }

   async function saveLeave() {
        let err = false
        if (!leaveType) {
            alertMsg('Please select leave type')
            err = true
        }
        if (!emp_name) {
            updateState({ emp_code_err: true })
            err = true
        }
        if (!remarks) {
            updateState({ remarksError: true })
            err = true
        }
        if (!err) {
            // api
            const __headers = {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${user?.access_token}`
            }

        updateState({ sending: true })
        let tempVar = user?.sessions

       let formData = new FormData()
        formData.append('leavetypeid',leave_type_id)
        formData.append('employeeid',emp_id)
        formData.append('datein',from)
        formData.append('dateout',to)
        formData.append('remarks',remarks)
        formData.append('companyid',tempVar.companyid)
        formData.append('locationid',tempVar.locationid)
        formData.append('insertedby',tempVar?.userid)
        formData.append('insertedip',ipAddress)
        formData.append('updatedby',tempVar?.userid)
        formData.append('updatedip',ipAddress)
        if(filePath){
        formData.append('file' ,{
            name: filePath[0]?.name,
            type: filePath[0]?.type,
            uri: Platform.OS === 'ios' ? 
                 filePath[0]?.uri.replace('file://', '')
                 : filePath[0]?.uri,
          })
        }
            
        await axios.post(`${baseUrl}/attendance/leaves/update/${item?.id}`, formData, {
                headers: __headers
            }).then((response) => {
                const { data } = response
                console.log('Leave apply', data)
                if (data.status === status) {
                    updateState({
                        sending: false,
                    })
                    alertMsg('Applied Successfully !')
                } else if (data?.status === __error) {
                    updateState({ sending: false })
                    alertMsg(data?.message)
                } else {
                    updateState({ sending: false })
                    alertMsg(SERVER_ERROR)
                }
            }).catch((err) => {
                updateState({ sending: false })
                alertMsg(SERVER_ERROR)
            })

        } else {
            updateState({ errMsg: 'Please fill required fields' })
        }
    }

    function cancelLeave() {
        navigation.pop()
    }

    /*********************** custom dd vendor  *************************/
    const filterEmployeeData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return employee_list
            }
            const regex = new RegExp([query.trim()], 'i')
            return employee_list.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom dd vendor  *************************/
    filteredEmployee = filterEmployeeData(searchEmployeeCode)

    return (
        <Container>
            <AppHeader
                headingText={'Edit Leave'}
                leftIconName={'arrow-back'}
                leftIconType={'Ionicons'}
                leftBtnPress={goBack}
                dashbord={false}
                heading={true}
                search={false}
                edit={true}
            />
            <View style={[styles.blueBG]}></View>
            <Content style={styles.container}>
                <Text style={styles.category}>Leave Category</Text>

                <FlatList
                    data={leaves_list}
                    keyExtractor={(item) => { `${item.id}` }}
                    renderItem={renderItem}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    style={{ marginBottom: 10, }}
                />

                <DropDown
                    label="Employee"
                    placeholder={'Employee'}
                    value={emp_name}
                    onPress={() => updateState({ showEmployeeCodeList: true })}
                    error={emp_code_err}
                />
                <View style={styles.dateView}>

                    <DatePicker
                        label="From"
                        onPress={showCalendar_from}
                        iconName={'calendar-month-outline'}
                        iconType={'MaterialCommunityIcons'}
                        date={from}
                        inputViewStyle={{ width: wp(44) }}
                    />
                    <DatePicker
                        label="to"
                        onPress={showCalendar_to}
                        iconName={'calendar-month-outline'}
                        iconType={'MaterialCommunityIcons'}
                        date={to}
                        inputViewStyle={{ width: wp(44) }}

                    />

                </View>

                <InputField
                    label="Remarks"
                    placeholder={'Remarks'}
                    onChangeText={(v) => updateState({
                        remarks: v,
                        remarksError: false,
                        errMsg: '',
                    })}
                    value={remarks}
                    multiline={true}
                    inputViewStyle={{ paddingVertical: 7 }}
                    error={remarksError}
                />
                {errMsg ?
                    <Error
                        error={errMsg}
                    /> : null
                }
                <View style={styles.btnView}>
                    <SaveBtn
                        onPress={saveLeave}
                        title={'Save'}
                        saving={sending}
                    />
                    <CancelBtn
                        title={'Cancel'}
                        onPress={cancelLeave}
                    />
                    <DocPicker
                        onPress={selectDocument}
                        fileType={fileType}
                        filePath={filePath}
                        cancelFile={cancelDocument}
                    />
                </View>
            </Content>

            <DropdownModal
                isVisible={showEmployeeCodeList}
                closeFromModal={closeEmployeeCodeList}
                data={filteredEmployee}
                onChangeText={handleSearchEmployeeCode}
                selectValue={selectCode}
                isLoading={isdropdownLoading}
            />

            <_Calendar
                is_Visible={isFromDate_visible}
                hideDatePicker={showCalendar_from}
                onDayPress={onDayPress_from}
                currentDate={fromDate}
                markedDates={markedDates_from}
                maxDate={last_date}
                minDate={startDay}
                maxDate={endDay}

            />
            <_Calendar
                is_Visible={isToDate_visible}
                hideDatePicker={showCalendar_to}
                onDayPress={onDayPress_to}
                currentDate={toDate}
                markedDates={markedDates_to}
                maxDate={last_date}
                minDate={c_startDay}
                maxDate={c_endDay}

            />
            <DialogBox ref={dialogboxRef} />
        </Container>
    )
}
const mapStateToProps = (state) => ({
    user: state.User.userInfo,
    leave_cat: state.Dropdown.leave_category,
    emp_dd: state.Dropdown.employeeDDs,
})
const mapDispatchToProps = (dispatch) => ({
    // saveEmployee:(d)=>dispatch(Employee_dd(d)),
    // saveLeaveCategory:(d)=>dispatch(leave_Types(d)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Leave_Edit)


