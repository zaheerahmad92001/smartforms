import React, { useEffect, useRef, useReducer } from "react";
import { View, Text, Image, } from "react-native";
import { Container, Content, Icon } from "native-base";
import InputField from "../../Components/InputField";
import DatePicker from "../../Components/DatePicker";
import _Calendar from '../../Components/Calendar';
import styles from "./styles";
import AppHeader from "../../Components/AppHeader";
import DropDown from "../../Components/Dropdown";
import DropdownModal from "../../Components/dropDownModal";
import DialogBox from 'react-native-dialogbox';
import SaveBtn from "../../Components/SaveBtn";
import CancelBtn from "../../Components/CancelBtn";
import DocPicker from "../../Components/DocPicker";
import DocumentPicker from 'react-native-document-picker'
import Error from "../../Components/Error";
import {Vendor_dropdowns ,Bank_dd,Service_dd}from '../../redux/actions/Dropdowns'
import { NetworkInfo } from "react-native-network-info";
import { SERVER_ERROR, status, STOCK_OUT, STOCK_OUT_MSG, __error } from "../../Constants/constValues";
import axios from "axios";
import { baseUrl } from "../../Constants/server";
import BottomTabs from "../../Components/BottomTab";
import { connect } from "react-redux";
import { Keyboard } from "react-native";
import { Platform } from "react-native";


let filteredVendro = ''
let filteredBankAcc = ''
let filteredService = ''

function ServiceInvoice({ navigation ,user , saveVendors ,saveBank,saveService }) {

    let dialogboxRef = useRef()

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            headers : {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.access_token}`
                 },
              formData : {
                  userid: user?.sessions?.userid,
                  companyid: user?.sessions?.companyid,
                  locationid: user?.sessions?.locationid,
              },

            ipAddress:'',
            sending:false,
            voucherNumber: '', 
            vnoError: false,
            isCalendar_Visible: false,
            isCalendar_Visible_cheque:false,
            currentDate: '',
            c_currentDate:'',
            markedDates: {},
            markedDates_cheque: {},
            showBankList: false,
            voucherDate:'',
            vdateError: false,
            chequeNumber:'',
            chequeNoError: false,
            is_vendor_dd_Visible:false,
            vendor: '',
            vendorError: false,

            v_dd:[],
            v_dd_List:[],
            vendor_id:'',

            b_dd:[],
            b_dd_List:[],
            bank_id:'',

            s_dd:[],
            s_dd_List:[],
            service_id:'',


            searchVendor:'',
            bankAcc: '',
            bankAccError:false,
            searchBank:'',
            dueDate:'',
            dueDateError:false,
            service:'',
            serviceError:false,
            searchService:'',
            is_service_dd_Visible:false,
            quantity:'',
            qtyError:false,
            rate:'',
            rateError:false,
            filePath:'',
            fileType:'',
            errMsg:'',
            
        }
    )
    const {
        headers,
        formData,
        voucherNumber,
        vnoError,
        ipAddress,
        sending,
        currentDate,
        c_currentDate,
        markedDates,
        voucherDate,
        vdateError,
        chequeNumber,
        chequeNoError,
        is_vendor_dd_Visible,
        isCalendar_Visible,
        isCalendar_Visible_cheque,
        showBankList,
        vendor,
        vendorError,
        searchVendor,

        v_dd,
        v_dd_List,
        vendor_id,

        b_dd,
        b_dd_List,
        bank_id,

        s_dd,
        s_dd_List,
        service_id,

        searchService,
        searchBank,
        bankAcc,
        bankAccError,

        dueDate,
        dueDateError,
        markedDates_cheque,
        service,
        serviceError,

        is_service_dd_Visible,
        quantity,
        qtyError,

        rate,
        rateError,

        fileType,
        filePath,
        errMsg,

    } = state

    useEffect(() => {

        NetworkInfo.getIPAddress().then(ip => {
            updateState({ipAddress:ip})
          });

        populateVendor_dd()
        populateBank_dd()
        populateService_dd()

        let d = new Date().getDate()
        let m = new Date().getMonth()+1
        let y = new Date().getFullYear()

        if(d<=9){
          d = '0'+d
        }
        if(m<=9){
            m = '0'+m
        }
        let date =`${y}-${m}-${d}`
        let _show= `${d}-${m}-${y}`
        
        let selected = {}
        selected[date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        updateState({
            currentDate: date,
            c_currentDate:date,
            voucherDate:_show,
            dueDate:_show,
            markedDates: selected,
            markedDates_cheque:selected,
        })

    }, [])

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', async () => {
            populateVendor_dd()
            populateBank_dd()
            populateService_dd()
            cancelServiceInvoice()
        });
        return unsubscribe;
    },[])


    function alertMsg(msg1 ,msg2){
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2],
            btn: {
                text: 'OK',
                callback: () => { },
            },
        })

    }

    function populateVendor_dd(){
        updateState({ isdropdownLoading: true })
            axios.post(`${baseUrl}/inventory/vendors`, formData, {
                headers: headers
            }).then((res) => {
                const { data } = res
                if (data.status === status) {
                    /*********** dd Redux **********/
                    saveVendors(data.vendors)
    
                    let ddValues = []
                    if (data.vendors?.length > 0) {
                        data.vendors.map((item, index) => {
                            ddValues.push(item.vname)
                        })
    
                        updateState({
                            isdropdownLoading: false,
                            v_dd_List: ddValues,// for filter 
                            v_dd: data.vendors,
                        })
                    } else {
                        updateState({ isdropdownLoading: false })
                    }
                } else {
                    alertMsg(data.message)
                }
            }) 
        }
    
        function populateBank_dd(){
            updateState({ isdropdownLoading: true })
                axios.post(`${baseUrl}/banks`, formData, {
                    headers: headers
                }).then((res) => {
                    const { data } = res
                    if (data.status === status) {
                        /*********** dd Redux **********/
                        saveBank(data.banks)
        
                        let ddValues = []
                        if (data.banks?.length > 0) {
                            data.banks.map((item, index) => {
                              ddValues.push(item.vname)
                            })
        
                            updateState({
                                isdropdownLoading: false,
                                b_dd_List: ddValues,// for filter 
                                b_dd: data.banks,
                            })
                        } else {
                            updateState({ isdropdownLoading: false })
                        }
                    } else {
                        alertMsg(data.message)
                    }
                }) 
            }

            function populateService_dd(){
                updateState({ isdropdownLoading: true })
                    axios.post(`${baseUrl}/inventory/services`, formData, {
                        headers: headers
                    }).then((res) => {
                        const { data } = res
                        if (data.status === status) {
                            console.log('response' , data)

                            /*********** dd Redux **********/
                            saveService(data.services)
            
                            let ddValues = []
                            if (data.services?.length > 0) {
                                data.services.map((item, index) => {
                                    ddValues.push(item.vname)
                                })
            
                                updateState({
                                    isdropdownLoading: false,
                                    s_dd_List: ddValues,// for filter 
                                    s_dd: data.services,
                                })
                            } else {
                                updateState({ isdropdownLoading: false })
                            }
                        } else {
                            alertMsg(data.message)
                        }
                    }) 
                }


    function openMenu() { 
        Keyboard.dismiss()
        setTimeout(()=>{
            navigation.openDrawer();
        },500)
    }
    function navigateTo(){navigation.navigate('serviceInv_search')}

    async function selectDocument(){
        try {
            const results = await DocumentPicker.pick({
              type: [DocumentPicker.types.images],
            });
            
            updateState({
              filePath: results,
              fileType: results[0].type
            })
            for (const res of results) {
                console.log(
                  res.uri,
                  res.type, // mime type
                  res.name,
                  res.size,
                )
              }
    
          } catch (err) {
            if (DocumentPicker.isCancel(err)) {
              console.log('Document picker cancel')
            } else {
              throw err;
            }
          }
        }
    
       function cancelDocument (){
        updateState({
            filePath:'',
            fileType:''
        })
       }

    function showCalendar() {
        let isVisible = isCalendar_Visible
        updateState({ isCalendar_Visible: !isVisible })
    }

    function  showCalendar_cheque(){
        let isVisible = isCalendar_Visible_cheque
        updateState({ isCalendar_Visible_cheque: !isVisible })

    }

    function closeBankModal() {
        updateState({ showBankList: false })
    }
    function closeVendorModal(){
        updateState({is_vendor_dd_Visible:false})
    }
    
    function closeServiceModal(){
        updateState({is_service_dd_Visible:false})
    }

    function onDayPress(date) {

        let [y,m,d] = date.dateString.split('-')
        let _show= `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates: selected,
            voucherDate:_show,
            currentDate:date.dateString,
            isCalendar_Visible:false,
            vdateError:false,
            errMsg:'',
        })
    }

    function onDayPress_cheque(date) {

        let [y,m,d] = date.dateString.split('-')
        let _show= `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates_cheque: selected,
            dueDate:_show,
            c_currentDate:date.dateString,
            isCalendar_Visible_cheque:false,
            dueDateError:false,
            errMsg:'',
        })
    }


    function selectBank(value, index) {
        var FOUND = b_dd.find(function (post, index) {
            if (post.vname == value) {
                  return post;
            }
        })
        updateState({ 
            bankAcc:value,
            bank_id:FOUND.id,
            showBankList:false,
            searchBank:'',
            bankAccError:false,
            errMsg:'',
         })
    }
    const handleSearchBank = (value) => {
        updateState({ searchBank: value })
    }

    function selectValue_Vendor(value,index){
        var FOUND = v_dd.find(function (post, index) {
            if (post.vname == value) {
                  return post;
            }
        })
        updateState({
            vendor: value,
            vendor_id:FOUND.id,
            is_vendor_dd_Visible: false,
            searchVendor: '',
            vendorError:false,
            errMsg:'',
        })
     }

    const handleSearchVendor = (value) => {
        updateState({ searchVendor: value })
    }

    function selectValue_service(value,index){
        var FOUND = s_dd.find(function (post, index) {
            if (post.vname == value) {
                  return post;
            }
        })
        updateState({
            service:value,
            service_id:FOUND.id,
            searchService:'',
            is_service_dd_Visible:false,
            serviceError:false,
            errMsg:'',
        })
    }
    const handleSearchService = (value) => {
        updateState({ searchService: value })
    }


async function saveServiceInovice(){
    let err = false

    if (!voucherDate) {
        updateState({ vdateError: true, })
        err = true
    }
    if (!vendor) {
        updateState({ vendorError: true, })
        err = true
    }
    if (!bankAcc) {
        updateState({ bankAccError: true, })
        err = true
    }
    if (!chequeNumber) {
        updateState({ chequeNoError: true, })
        err = true
    }
    if (!dueDate) {
        updateState({ dueDateError: true, })
        err = true
    }
    if (!service) {
        updateState({ serviceError: true, })
        err = true
    }
    if (!quantity || isNaN(quantity) || quantity <1) {
        updateState({ qtyError: true, })
        err = true
    }
    
    if (!rate || isNaN(rate)) {
        updateState({ rateError: true, })
        err = true
    }
    if (!err) {
        // api 
        const __headers = {
            'Accept': 'application/json',
            'Content-Type': 'multipart/form-data',
            'Authorization': `Bearer ${user?.access_token}`
        }

        updateState({sending:true})
        let tempVar =user?.sessions 
        let __formData= new FormData()

            __formData.append('insertedby',tempVar?.userid)
            __formData.append('insertedip',ipAddress)
            __formData.append('vdate',voucherDate)
            __formData.append('companyid',tempVar?.companyid)
            __formData.append('projectid',tempVar?.projectid)
            __formData.append('serviceid',service_id)
            __formData.append('qty',quantity)
            __formData.append('vrate',rate)
            __formData.append('vendorid',vendor_id)
            __formData.append('bankid',bank_id)
            __formData.append('chequeno',chequeNumber)
            __formData.append('chequedate',dueDate)
            if(filePath){
            __formData.append('file' ,{
                name: filePath[0]?.name,
                type: filePath[0]?.type,
                uri: Platform.OS === 'ios' ? 
                     filePath[0]?.uri.replace('file://', '')
                     : filePath[0]?.uri,
              })
            }
         
         console.log('service data send to server' , __formData)
           await  axios.post(`${baseUrl}/inventory/service-invoices/create`,__formData,{
                headers: __headers
            }).then((response)=>{
                const {data} = response
                console.log('response ' , data)
                if(data.status===status){
                    alertMsg('Service Invoice Voucher Saved Successfully')
                    updateState({
                        voucherDate:'',
                        dueDate:'',
                    })
                    cancelServiceInvoice()

                }else if(data?.status===__error){
                    updateState({sending:false})
                    alertMsg(data?.message)
                    console.log('error in else if' , data)
                }else{
                    updateState({sending:false})
                    alertMsg(SERVER_ERROR)
                    console.log('error in else ' , data)
                }
            }).catch((err)=>{
                updateState({sending:false})
                alertMsg(SERVER_ERROR)
                console.log('err in catch' , err)
            })
    } else {
        updateState({ errMsg: 'Please fill required fields' })
    }
}

function cancelServiceInvoice(){
    updateState({
        sending:false,
        // voucherDate:'',
        // dueDate:'',
        vendor:'',
        bankAcc:'',
        chequeNumber:"",
        service:'',
        quantity:'',
        rate:"",
        filePath:'',
        fileType:'',
        errMsg:'',

        vdateError:false,
        dueDateError:false,
        vendorError:false,
        bankAccError:false,
        chequeNoError:false,
        serviceError:false,
        qtyError:false,
        rateError:false,
    })
}


/*********************** custom dd vendor  *************************/
              const filterVendorData = (query) => {
                    if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
                        if (query === '') {
                            return v_dd_List
                        }
                        const regex = new RegExp([query.trim()], 'i')
                        return v_dd_List.filter((c) => c.search(regex) >= 0)
                    } else {
                        console.log('invalid query', query)
                    }
                }

/*********************** custom  dd Product  *************************/
          const filterServiceData = (query) => {
                    if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
                        if (query === '') {
                            return s_dd_List
                        }
                        const regex = new RegExp([query.trim()], 'i')
                        return s_dd_List.filter((c) => c.search(regex) >= 0)
                    } else {
                        console.log('invalid query', query)
                    }
                }

/*********************** custom  dd Bank  *************************/
          const filteredBankData = (query) => {
                if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
                    if (query === '') {
                        return b_dd_List
                    }
                    const regex = new RegExp([query.trim()], 'i')
                    return b_dd_List.filter((c) => c.search(regex) >= 0)
                } else {
                    console.log('invalid query', query)
                }
            }
/*********************** custom dd Bank  *************************/

    filteredVendro = filterVendorData(searchVendor)
    filteredService = filterServiceData(searchService)
    filteredBankAcc = filteredBankData(searchBank)


    return (
        <Container>
            <AppHeader
                rightIconName={'search'}
                rightIconType={'EvilIcons'}
                rightBtnPress={navigateTo}
                headingText={'Service Invoice'}
                leftBtnPress={openMenu}
                dashbord={false}
                heading={true}
                search={false}
            />
            <View style={[styles.blueBG]}></View>

            <Content style={styles.container}>
                {/* <InputField
                    label="Voucher Number"
                    placeholder={'Voucher Number'}
                    onChangeText={(v) => updateState({ 
                        voucherNumber: v,
                        vnoError:false,
                        errMsg:'',
                     })}
                    value={voucherNumber}
                    error={vnoError}
                /> */}
                <DatePicker
                    label="Voucher Date"
                    onPress={showCalendar}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={voucherDate}
                    error={vdateError}
                />

                <DropDown
                  label="Vendor"
                  placeholder={'Vendor'}
                  value={vendor}
                  onPress={()=>updateState({is_vendor_dd_Visible:true})}
                  error={vendorError}
               />

             <DropDown
                  label="Bank Account"
                  placeholder={'Bank Account'}
                  value={bankAcc}
                  onPress={()=>updateState({showBankList:true})}
                  error={bankAccError}
               />
             
                
             <InputField
                    label="Cheque Number"
                    placeholder={'Cheque Number'}
                    onChangeText={(v) => updateState({ 
                        chequeNumber: v,
                        chequeNoError:false,
                        errMsg:'',
                     })}
                    value={chequeNumber}
                    error={chequeNoError}
                />
                <DatePicker
                    label="Cheque Date"
                    onPress={showCalendar_cheque}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={dueDate}
                    error={dueDateError}
                />

                <DropDown
                  label="Service"
                  placeholder={'Service'}
                  value={service}
                  onPress={()=>updateState({is_service_dd_Visible:true})}
                  error={serviceError}

               />
            
               
                 <InputField
                    label="Quantity"
                    placeholder={'Quantity'}
                    keyboardType={'number-pad'}
                    onChangeText={(v) => updateState({ 
                        quantity: v ,
                        qtyError:false,
                        errMsg:'',
                     })}
                    value={quantity}
                    error={qtyError}
                />

                 <InputField
                    label="Rate"
                    placeholder={'Rate'}
                    keyboardType={'decimal-pad'}
                    onChangeText={(v) => updateState({ 
                        rate: v,
                        rateError:false,
                        errMsg:'',
                     })}
                    value={rate}
                    error={rateError}
                />

                {errMsg ?
                    <Error
                      error={errMsg}
                    /> : null
                }
                {qtyError ?
                    <Error
                        error={'Quantity shoud be greater than 0'}
                    /> : null
                    }
                <View style={styles.btnView}>
                <SaveBtn
                  onPress={saveServiceInovice}
                  title={'Save'}
                  saving={sending}
                />
                    <CancelBtn
                    onPress={cancelServiceInvoice}
                     title={'Cancel'}
                    />
                  <DocPicker
                      onPress={selectDocument}
                      fileType={fileType}
                      filePath={filePath}
                      cancelFile={cancelDocument}
                     />
                </View>
            </Content>




            <_Calendar
                    is_Visible={isCalendar_Visible}
                    hideDatePicker={showCalendar}
                    onDayPress={onDayPress}
                    currentDate={currentDate}
                    markedDates={markedDates}
                    // todayPress={todayPress}
                    // tomorrowPress={tomorrowPress}
                    // inTwoDayPress={inTwoDayPress}
                    // DoneBtnPress={showCalendar}
                />
                <_Calendar
                    is_Visible={isCalendar_Visible_cheque}
                    hideDatePicker={showCalendar_cheque}
                    onDayPress={onDayPress_cheque}
                    currentDate={c_currentDate}
                    markedDates={markedDates_cheque}
                    // todayPress={todayPress_cheque}
                    // tomorrowPress={tomorrowPress_cheque}
                    // inTwoDayPress={inTwoDayPress_cheque}
                    // DoneBtnPress={showCalendar_cheque}
                />
                <DropdownModal
                    isVisible={is_vendor_dd_Visible}
                    closeFromModal={closeVendorModal}
                    data={filteredVendro}
                    onChangeText={handleSearchVendor}
                    selectValue={selectValue_Vendor}
                />
                
              <DropdownModal
                 isVisible={is_service_dd_Visible}
                 closeFromModal={closeServiceModal}
                 data={filteredService}
                 onChangeText={handleSearchService}
                 selectValue={selectValue_service}
                />

            <DropdownModal
                isVisible={showBankList}
                closeFromModal={closeBankModal}
                data={filteredBankAcc}
                onChangeText={handleSearchBank}
                selectValue={selectBank}
            />
        <BottomTabs     
                navigation={navigation}
                jvPress={()=>navigation.navigate('journalVoucher')}
                purchasePress={()=>navigation.navigate('Purchase')}
                saleInvoicePress={()=>navigation.navigate('saleInvoice')}
                leaveApplyPress={()=>navigation.navigate('leaveApply')}
            />
          <DialogBox ref={dialogboxRef}/>
        </Container>
    )

}
const mapStateToProps = (state) => ({
    user: state.User.userInfo,
})
const mapDispatchToProps = (dispatch) => ({
    saveVendors:(v)=>dispatch(Vendor_dropdowns(v)),
    saveBank:(v)=>dispatch(Bank_dd(v)),
    saveService:(v)=>dispatch(Service_dd(v)),
});
export default connect(mapStateToProps,mapDispatchToProps)(ServiceInvoice)

