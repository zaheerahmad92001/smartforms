import { Platform, StyleSheet } from 'react-native';
import { appColor, white ,lightGrey, red, grey, black } from '../../Constants/Colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { largeText, mediumText, xsmallText } from '../../Constants/FontSize';
import { LatoBlack, LatoBold, LatoLight, LatoRegular } from '../../Constants/Fonts';
import { darkSky } from "../../Constants/Colors";
export default styles = StyleSheet.create({
    wraper: {
        flex: 1,
        backgroundColor: appColor
    },
    slide: {
        flex: 1
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    logoStyle: {
        width: 150,
        height: 150,
    },
    middleContent: {
        marginTop: hp(10),
        justifyContent: 'center',
        alignItems: 'center',
    },
    appName: {
        marginTop: -10,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    firstName: {
        color: white,
        fontSize: 15,
        fontFamily: LatoBlack,
        fontWeight: '900',
        marginRight: 5,
    },
    lastName: {
        color: white,
        fontSize: mediumText,
        fontFamily: LatoRegular,
        fontWeight: '500',
    },
    borderStyle:{
        borderColor:darkSky,
        borderWidth:0.5,
        marginTop:8,
        width:wp(80),
        alignSelf:'center',
    },
    smallTextStyle:{
        color:lightGrey,
        fontWeight:'400',
        fontSize:xsmallText,
        fontFamily:LatoRegular,
        alignSelf:'center',
        marginTop:6,
    },
    container:{
        backgroundColor:white, 
        marginTop:15,
        paddingTop:hp(8),
        paddingHorizontal:20,
        borderTopEndRadius:20,
        borderTopLeftRadius:20,
        height:Platform.OS=='android'? hp(67):hp(67)
    },
    rememberView:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        marginTop:hp(1)
    },
    forgotPass:{
        color:red,
        fontSize:mediumText,
        fontStyle:'normal',
        fontWeight: Platform.OS=='ios' ? '400' :'500' ,
        fontFamily:LatoRegular,
    },
    btnView:{
        justifyContent:'center',
        alignItems:'center',
        marginTop:hp(4)
    },
    notAcc:{
        marginTop:hp(2),
        justifyContent:'center',
        alignItems:'center'
    },
    textStyle:{
     fontFamily:LatoRegular,
     fontWeight:Platform.OS=='ios'?'400':'500',
     fontSize:mediumText,
     fontStyle:'normal',
     color:grey
    },
    modalView:{
        backgroundColor:white,
        borderRadius:20,
        paddingVertical:20,
        paddingHorizontal:20,
        // justifyContent:'center',
        // alignItems:'center'
    },
    forgetPass:{
        color:black,
        fontFamily:LatoBold,
        fontWeight:'600',
        fontSize:mediumText,
        fontStyle:'normal'
    },
    resepass:{
        color:black,
        fontFamily:LatoRegular,
        fontSize:mediumText,
        fontWeight:Platform.OS=='ios'? '400':'400',
        marginTop:5,
        
    },
    rowCenter:{
        justifyContent:'center',
        alignItems:'center',
        marginBottom:10
    },
    rowStyle:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center'
    },
    btnWraper:{
        width:wp(30),
        marginTop:hp(2),
        alignSelf:'center'
    },
    errorText:{
        marginHorizontal:15,
        marginBottom:10,
    }
})