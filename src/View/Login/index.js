import React, { useEffect, useRef, useReducer } from "react";
import { View, Text, Image, TouchableOpacity, TouchableWithoutFeedback, Platform, } from "react-native";
import { Container, Content } from "native-base";
import { logo } from "../../Constants/images";
import InputField from "../../Components/InputField";
import SimpleTextField from "../../Components/SimpleTextField";
import CheckBox from "../../Components/CheckBox";
import CustomButton from "../../Components/CustomButton";
import Modal from 'react-native-modal';
import AppButton from "../../Components/AppButton";
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { baseUrl } from '../../Constants/server'
import {validateEmail}from '../../Constants/RandomFun'
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios'
import styles from './styles'
import Error from "../../Components/Error";
import { ActivityIndicator } from "react-native";
import { appColor } from "../../Constants/Colors";
import { connect } from 'react-redux'
import {loginUser}from '../../redux/actions/User'
import {From_To_dropdown} from '../../redux/actions/Dropdowns'
import{Remember ,status ,USER }from '../../Constants/constValues'
import DialogBox from 'react-native-dialogbox';
import { StackActions } from '@react-navigation/native';
import { ScrollView } from "react-native";




function Login({ navigation,saveUser , saveDropdown }) {

  let dialogboxRef = useRef()

  const [state, updateState] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
      email: "",
      password: '',
      rememberMe: false,
      isVisible: false,
      emailError:false,
      passwordError:false,
      fetching:false,
      dropdownList:[],
      showPass:false
    }
  )
  const { email,
    password,
    rememberMe,
    isVisible,
    emailError,
    passwordError,
    fetching,
    showPass,
    dropdownList
  } = state

useEffect(()=>{
  isRemember()
},[])
 
async function isRemember(){

  await AsyncStorage.getItem(Remember).then((res)=>{
     if(res){
       updateState({
         email:res,
         rememberMe:true
        })
     }else{
      updateState({
        email:'',
        rememberMe:false
       })
     }
  })
}

 async function RememberMe() {
    let remember = !rememberMe
    updateState({ rememberMe: remember })
   
  }

  function closeModal() {
    updateState({ isVisible: false })
  }
  function showModal() {
    updateState({ isVisible: true })
  }


  async function SingIn() {

      let _data ={
        email:email,
        password:password
      }

    if (validateEmail(email.trim()) && password.trim()) {
       updateState({fetching:true})

// Remember me
       if(rememberMe){
        await AsyncStorage.setItem(Remember,email)
       }else{
         await AsyncStorage.removeItem(Remember)
       }

      axios.post(`${baseUrl}/login`, _data).then(async(response) => {
         const {data} = response
         console.log('login user', data)
         if(data.status===status){

           updateState({fetching:false})
        /******** Redux *******/
           saveUser(data)
           await AsyncStorage.setItem(USER ,JSON.stringify(data))
            // navigation.navigate('MyDrawer')
            navigation.dispatch(StackActions.replace('MyDrawer'));


        }else{
          updateState({fetching:false})
          dialogboxRef.current.tip({
            title: 'Message',
            content: [data.message],
            btn: {
              text: 'OK',
              callback: () => {},
            },
          })
         }

      }).catch((err)=>{
        updateState({fetching:false})
          dialogboxRef.current.tip({
            title: 'Message',
            content: ['Internal Server Error'],
            btn: {
              text: 'OK',
              callback: () => {},
            },
          })
      })
    }


    if (!validateEmail(email.trim())) {
        updateState({emailError:true})
    }
    if (!password.trim()) {
       updateState({passwordError:true})
    }

    // navigation.navigate('MyDrawer')
  }
  function sendLink() {

  }

  function ForgotPassword() {
    return (
      <Modal
        isVisible={isVisible}
        onBackButtonPress={closeModal}
        onBackdropPress={closeModal}
      >
        <View style={styles.modalView}>

          <View style={styles.rowCenter}>
            <Text style={styles.forgetPass}>Forget Password</Text>
            <Text style={styles.resepass}>we send  you a link to reset password</Text>
          </View>

          <SimpleTextField
            placeholder={'Email'}
            onChangeText={(e) => updateState({ email: e , })}
            value={email}
            keyboardType={'default'}
            inputContainer={{ marginBottom: 10, }}
          />

          <View style={styles.rowStyle}>
            <Text style={styles.resepass}>Not a member ?</Text>
            <TouchableOpacity
              onPress={closeModal}
            >
              <Text style={styles.resepass}>Create an account</Text>
            </TouchableOpacity>
          </View>

          <AppButton
            title={'Submit'}
            onPress={sendLink}
            btnWraper={styles.btnWraper}
          />

        </View>
        {
          Platform.OS === 'ios' ?
            <KeyboardSpacer /> : null
        }
      </Modal>
    )
  }

  function handlePassVisibility(){
    updateState({showPass:!showPass})
  }

  return (
    <Container style={styles.wraper}>
      <ScrollView>
        
        <View style={styles.middleContent}>
          <Image
            resizeMode={'contain'}
            source={logo}
            style={styles.logoStyle}
          />
          <View style={styles.appName}>
            <Text style={styles.firstName}>{'SMART'}</Text>
            <Text style={styles.lastName}>{'FORMS'}</Text>
          </View>
        </View>

        <View style={styles.borderStyle}></View>
        <Text style={styles.smallTextStyle}>Please Login to your Account</Text>

        <View style={styles.container}>

          <SimpleTextField
            placeholder={'Email'}
            onChangeText={(e) => updateState({ email: e , emailError:false })}
            value={email}
            keyboardType={'default'}
            inputContainer={{ marginBottom: 20, }}
          />
          <SimpleTextField
            placeholder={'Password'}
            onChangeText={(p) => updateState({ password: p , passwordError:false })}
            value={password}
            iconName={showPass? 'eye-outline' :'ios-eye-off-outline'}
            iconType={'Ionicons'}
            secureTextEntry={!showPass}
            keyboardType={'default'}
            onPress={handlePassVisibility}
          />

    { emailError || passwordError ?
          <Error
            error={'Check your credentials'}
            style={styles.errorText}
          />: null }

          <View style={styles.rememberView}>
            <CheckBox
              title='Remember me'
              onPress={RememberMe}
              isCheck={rememberMe}
            />
            <TouchableOpacity
              onPress={showModal}>
              <Text style={styles.forgotPass}>Forget Password</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.btnView}>

            {fetching?
            <ActivityIndicator
              color={appColor}
              size={'small'}
            />
            :
            <CustomButton
              title={'Sing in'}
              onPress={SingIn}
              iconName='arrowright'
              iconType={'AntDesign'}
            />
            
            }

          </View>

          <View style={styles.notAcc}>
            <TouchableOpacity>
              <Text style={styles.textStyle}>Not Account ? SingUp</Text>
            </TouchableOpacity>
          </View>
        </View>

        {ForgotPassword()}
      </ScrollView>
      <DialogBox ref={dialogboxRef} />

    </Container>
  )
}


const mapStateToProps = (state) => ({
  user: state.User
})
const mapDispatchToProps = (dispatch) => ({
  saveUser: (user) => dispatch(loginUser(user)),
  saveDropdown:(d)=>dispatch(From_To_dropdown(d))
});

export default connect(mapStateToProps, mapDispatchToProps)(Login)
