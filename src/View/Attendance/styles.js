import { Platform } from 'react-native';
import { StyleSheet } from 'react-native'
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { appColor, black, borderColor, danger, darkGrey, lightBlack, lightGrey, lightYellow, white } from "../../Constants/Colors";
import { LatoBlack, LatoBold, LatoRegular } from "../../Constants/Fonts";
import { largeText, mediumText } from "../../Constants/FontSize";

export default styles = StyleSheet.create({
    container: {
        marginHorizontal: 15,
        paddingTop: 15,
        flex:1
    },
    blueBG: {
        backgroundColor: appColor,
        height: Platform.OS == 'android' ? hp(4) : hp(4),
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
    },
    category: {
        color: darkGrey,
        fontFamily: LatoBlack,
        fontSize: mediumText,

    },
    // btnView: {
    //     flexDirection: 'row',
    //     alignItems: 'center',
    //     justifyContent: 'space-between',
    //     marginTop: 20,
    //     marginBottom: 30,
    // },
    dateView:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
    },
    buttonStyle:{
        backgroundColor:appColor,
    },
    tinyCircle:{
        width:10,
        height:10,
        borderRadius:10/2,
        marginRight:5,
    },
    typeView:{
        flexDirection:'row',
        alignItems:'center',
    },
    textStyle:{
        color:darkGrey,
        fontSize:mediumText,
        fontWeight:'500',
        fontFamily:LatoRegular
    },
    currentDateContainer:{
        backgroundColor:white,
        borderRadius:5,
        borderColor:'#007BFF',
        borderWidth:1
      },
      currentDateText: {
        color:appColor,
        fontWeight: '500'
      },
      dateText:{
        color:white,
        fontWeight: '500'
      },
      dateContainer:{
        backgroundColor:appColor,
        borderRadius:5,
      },
      indicator:{
          flex:1,
          justifyContent:'center',
          alignItems:'center',
      },
      nodata:{
          color:lightBlack,
          fontSize:mediumText,
          fontWeight:'500'
      },
      btnView:{
          flexDirection:'row',
          alignItems:'center',
          justifyContent:'space-between'
      }


})