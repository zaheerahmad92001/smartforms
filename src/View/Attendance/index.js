import React, { useEffect, useRef, useReducer } from 'react'
import { View, Text } from 'react-native'
import { Container, Content, Icon, Item } from "native-base";
import AppHeader from "../../Components/AppHeader";
import SaveBtn from "../../Components/SaveBtn";
import styles from './styles'
import TimePicker from "../../Components/TimePicker";
import _Calendar from '../../Components/Calendar';
import DatePicker from "../../Components/DatePicker";
import DialogBox from 'react-native-dialogbox';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { smallText } from '../../Constants/FontSize';
import { appColor, danger, lightRed, red } from '../../Constants/Colors';
import AttendanceTimming from '../../Components/AttendanceTimming';
import moment from 'moment'
import { FlatList } from 'react-native';
import { Keyboard } from 'react-native';
import { connect } from 'react-redux'
import { SERVER_ERROR, status } from '../../Constants/constValues';
import axios from 'axios'
import { baseUrl } from '../../Constants/server';
import { ActivityIndicator } from 'react-native';
import { NetworkInfo } from "react-native-network-info";
import BottomTabs from '../../Components/BottomTab';


function Attendance({ navigation, user }) {
    let dialogboxRef = useRef()

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.access_token}`
            },
            currentDate: new Date(),
            markedDates: {},
            isDateVisible: false,
            isTimeIn_visible: false,
            time_in: new Date(),
            timeIn: '',
            isTimeOut_visible: false,
            time_out: new Date(),
            timeOut: '',
            _date: '',
            __dateError: false,
            emp_Attendance: [],
            fetching: false,
            sending: false,
            s_t_in_index: '',
            s_t_in_item: '',
            s_t_out_index: '',
            s_t_out_item: '',
            ipAddress: '',
            maxDate: '',
            minDate: ''


        }
    )

    const {
        headers,
        currentDate,
        _date,
        __dateError,
        isDateVisible,
        isTimeIn_visible,
        time_in,
        timeIn,
        isTimeOut_visible,
        time_out,
        timeOut,
        markedDates,
        sending,
        fetching,
        emp_Attendance,
        s_t_in_index,
        s_t_in_item,
        s_t_out_index,
        s_t_out_item,
        ipAddress,
        maxDate,
        minDate,


    } = state

    useEffect(() => {

        NetworkInfo.getIPAddress().then(ip => {
            updateState({ ipAddress: ip })
        });

        let d = new Date().getDate()
        let m = new Date().getMonth() + 1
        let y = new Date().getFullYear()

        if (d <= 9) {
            d = '0' + d
        }
        if (m <= 9) {
            m = '0' + m
        }
        let date = `${y}-${m}-${d}`
        let _show = `${d}-${m}-${y}`



        var first_date = new Date(new Date().getFullYear(), new Date().getMonth(), 1);
        var last_date = new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0);

        let firstDay = moment.utc(first_date).local().format('YYYY-MM-DD')
        let last_day = moment.utc(last_date).local().format('YYYY-MM-DD')


        let selected = {}
        selected[date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        updateState({
            currentDate: date,
            // _date: '',
            _date: _show,
            maxDate: last_day,
            minDate: firstDay,
            markedDates: selected
        })

    }, [])

    function openMenu() {
        Keyboard.dismiss()
        setTimeout(() => {
            navigation.openDrawer();
        }, 500)
    }

    // function onDateChange(d) {
    //     updateState({ currentDate: d })
    // }

    // function handleDate() {
    //     let [date, time] = currentDate.toISOString().split('T')
    //     updateState({
    //         _date: date.replace(/-/g, '/'),
    //         isDateVisible: false
    //     })
    // }

    function showCalendar() {
        let isVisible = isDateVisible
        updateState({ isDateVisible: !isVisible })
    }

    function onDayPress(date) {

        let [y, m, d] = date.dateString.split('-')
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates: selected,
            _date: _show,
            __dateError: false,
            currentDate: date.dateString,
            isDateVisible: false,
        })
    }


    function onTimeInChange(t, item, index) {
        updateState({ time_in: t })
    }


    function handleTimeIn() {

        let __time = ''
        let Time = ''

        if (time_in) {
            let dateTime = moment.utc(time_in).local().format('YYYY-MM-DD: HH:mm:ss')
            let [date, _time] = dateTime.split(' ')
            let startTime = _time

            /// start from here 

            let __dateTime = moment.utc(time_out).local().format('YYYY-MM-DD: HH:mm:ss')
            let [___date, ___time] = __dateTime.split(' ')
            let endTime = ___time


            let [h, m, s] = _time.split(':')
            __time = `${h}:${m}` /// for update time in 

            var format = h >= 12 ? 'PM' : 'AM';
            h = h % 12;
            h = h ? h : 12;
            m = m
            Time = `${h}:${m} ${format}`

            var regex = new RegExp(':', 'g')

            let list = emp_Attendance.slice()
            list.map((res, i) => {
                if (s_t_in_index == i && res.employeeid === s_t_in_item.employeeid) {
                    if (s_t_out_index == i && res.employeeid === s_t_out_item.employeeid) {
                        if (list[i].dateout != '00:00') {
                            if (parseInt(startTime.replace(regex, ''), 10) < parseInt(endTime.replace(regex, ''), 10)) {
                                console.log('strart Time is less then endtime');

                                list[i].datein = __time
                                updateState({
                                    // timeIn:'',
                                    isTimeIn_visible: false,
                                    emp_Attendance: list
                                })

                            } else {
                                list[i].datein = '00:00'
                                updateState({
                                    // timeIn:'',
                                    isTimeIn_visible: false,
                                    emp_Attendance: list
                                })
                                alertMsg(`'Time in' should be less then 'Time out'`)
                            }
                        } else {
                            list[i].datein = __time
                            updateState({
                                // timeIn:'',
                                isTimeIn_visible: false,
                                emp_Attendance: list
                            })

                        }
                    }
                }

            })
        }
        
    }

    function closeTimeInPicker() {
        updateState({ isTimeIn_visible: false })
    }

    function TimeOutPress(item, index) {
        updateState({
            isTimeOut_visible: true,
            s_t_out_index: index,
            s_t_out_item: item
        })
    }

    function TimeInPress(item, index) {

        updateState({
            isTimeIn_visible: true,
            s_t_in_index: index,
            s_t_in_item: item
        })
    }

    function onTimeOutChange(t) {
        updateState({ time_out: t })
    }

    function handleTimeOut() {

        let __time = ''
        let Time = ''

        //  console.log('time in' , time_in , 'time out' , time_out , 'comparsion' , time_out === timeOut)

        if (time_out) {
            let dateTime = moment.utc(time_out).local().format('YYYY-MM-DD: HH:mm:ss')
            let [date, _time] = dateTime.split(' ')
            let endTime = _time

            /// start from here 

            let __dateTime = moment.utc(time_in).local().format('YYYY-MM-DD: HH:mm:ss')
            let [___date, ___time] = __dateTime.split(' ')
            let startTime = ___time

            var regex = new RegExp(':', 'g')

            if (parseInt(startTime.replace(regex, ''), 10) < parseInt(endTime.replace(regex, ''), 10)) {
                console.log('strart Time is less then endtime');

                let [h, m, s] = _time.split(':')
                __time = `${h}:${m}` /// for update time out 

                var format = h >= 12 ? 'PM' : 'AM';
                h = h % 12;
                h = h ? h : 12;
                m = m
                Time = `${h}:${m} ${format}`

                let list = emp_Attendance.slice()
                list.map((res, i) => {
                    if (s_t_out_index == i && res.employeeid === s_t_out_item.employeeid) {
                        list[i].dateout = __time
                    }
                })
                updateState({
                    // timeOut: Time,
                    isTimeOut_visible: false,
                    emp_Attendance: list
                })

            } else {
                console.log('end time is less ')
                let list = emp_Attendance.slice()

                list.map((res, i) => {
                    if (s_t_out_index == i && res.employeeid === s_t_out_item.employeeid) {
                        console.log('list[i].dateout', list[i].dateout)
                        list[i].dateout = '00:00'
                    }
                })

                updateState({
                    // timeOut:'',
                    isTimeOut_visible: false,
                    emp_Attendance: list
                })
                alertMsg(`'Time in' should be less then 'Time out'`)
            }

            //     let [h, m, s] = _time.split(':')
            //     __time = `${h}:${m}` /// for update time out 

            //     var format = h >= 12 ? 'PM' : 'AM';
            //     h = h % 12;
            //     h = h ? h : 12;
            //     m = m
            //     let Time = `${h}:${m} ${format}`

            // }
            // let list = emp_Attendance.slice()
            // list.map((res, i) => {
            //     if (s_t_out_index == i && res.employeeid === s_t_out_item.employeeid) {
            //         list[i].dateout = __time
            //     }
            // })
            // updateState({
            //     timeOut: Time,
            //     isTimeOut_visible: false,
            //     emp_Attendance: list
            // })
        }

    }

    function closeTimeOutPicker() {
        updateState({ isTimeOut_visible: false })
    }



    const renderItem = ({ item, index }) => {
        return (

            <AttendanceTimming
                key={index}
                item={item}
                TimeInPress={() => TimeInPress(item, index)}
                showTimeIn={isTimeIn_visible}
                time_in={time_in}
                onTimeInChange={onTimeInChange}
                handleTimeIn={handleTimeIn}
                closeTimeInPicker={closeTimeInPicker}
                // timeIn={timeIn}

                TimeOutPress={() => TimeOutPress(item, index)}
                showTimeOut={isTimeOut_visible}
                time_out={time_out}
                onTimeOutChange={onTimeOutChange}
                handleTimeOut={handleTimeOut}
                closeTimeOutPicker={closeTimeOutPicker}
            // timeOut={timeOut}

            />
        )
    }
    function alertMsg(msg1, msg2) {
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2],
            btn: {
                text: 'OK',
                callback: () => { },
            },
        })
    }



    function getAttendance() {

        let err = false
        if (!_date) {
            updateState({ __dateError: true })
            err = true
        } else if (!err) {
            updateState({ fetching: true })
            let tempVar = user?.sessions

            let formData = {
                userid: tempVar?.userid,
                companyid: tempVar?.companyid,
                locationid: tempVar?.locationid,
                projectid: tempVar?.projectid,
                datein: _date,
                dateout: _date,
                employeeid: 0,
            }
            axios.post(`${baseUrl}/attendances`, formData, {
                headers: headers
            }).then((response) => {
                const { data } = response
                if (data.status === status) {
                    console.log('Attendances are ', data)
                    updateState({
                        fetching: false,
                        emp_Attendance: data?.attendance
                    })
                } else {
                    updateState({ fetching: false })
                    alertMsg(data?.message)
                    console.log('error in else ', data)
                }
            }).catch((error) => {
                console.log('err in catch', error)
                updateState({ fetching: false })
                alertMsg(SERVER_ERROR)
            })
        }
    }



    async function saveAttendance() {
        updateState({ sending: true })
        let tempVar = user?.sessions
        let companyid = tempVar?.companyid
        let locationid = tempVar?.locationid
        let insertedby = tempVar?.userid
        let updatedby = tempVar?.userid

        let temp = []
        emp_Attendance.map((item, i) => {

            console.log('date in', item.datein, 'dateOut', item.dateout)
            let obj = {
                employeeid: item.employeeid,
                datein: item.datein,
                dateout: item.dateout,
                companyid: companyid,
                locationid: locationid,
                insertedby: insertedby,
                insertedip: ipAddress,
                updatedby: updatedby,
                updatedip: ipAddress,
                id: item?.id

            }
            temp.push(obj)
        })

        let att = JSON.stringify(temp)
        let formData = {
            data: att,
            date: _date
        }
        console.log('data sending to server', formData)
        axios.post(`${baseUrl}/attendances/create`, formData, {
            headers: headers
        }).then((response) => {
            const { data } = response
            if (data.status === status) {
                console.log('Attendances created ', data)
                updateState({
                    sending: false,
                    // emp_Attendance: data?.attendance
                })
            } else {
                updateState({ sending: false })
                alertMsg(data?.message)
                console.log('error in else ', data)
            }
        }).catch((error) => {
            console.log('err in catch', error)
            updateState({ sending: false })
            alertMsg(SERVER_ERROR)
        })
    }




    return (
        <Container>
            <AppHeader
                rightIconName={'search'}
                rightIconType={'EvilIcons'}
                // rightBtnPress={navigateTo}
                headingText={'Attendance Entry'}
                leftBtnPress={openMenu}
                dashbord={false}
                heading={true}
                search={false}
                edit={true}
            />

            <View style={[styles.blueBG]}></View>
            <View style={styles.container}>
                {/* <View style={styles.dateView}> */}

                <DatePicker
                    label="Date"
                    onPress={showCalendar}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={_date}
                    error={__dateError}
                // inputViewStyle={{ width: wp(50) }}
                />

                <View style={styles.btnView}>
                    <SaveBtn
                        title={'Save'}
                        onPress={saveAttendance}
                        buttonContainer={{ top: 5, width: wp(30) }}
                        textStyle={{ fontSize: smallText }}
                        saving={sending}
                    />
                    <SaveBtn
                        title={'Fetch Record'}
                        onPress={getAttendance}
                        buttonContainer={{ top: 5, width: wp(30) }}
                        textStyle={{ fontSize: smallText }}
                        saving={fetching}
                    />
                </View>

                <View style={[styles.typeView, { marginTop: 15, }]}>
                    <View style={styles.typeView}>
                        <View style={[styles.tinyCircle, { backgroundColor: red }]}></View>
                        <Text style={styles.textStyle}>Absent</Text>
                    </View>

                    <View style={[styles.typeView, { marginLeft: 20 }]}>
                        <View style={[styles.tinyCircle, { backgroundColor: lightRed }]}></View>
                        <Text style={styles.textStyle}>Late Entry</Text>
                    </View>

                    <View style={[styles.typeView, { marginLeft: 20 }]}>
                        <View style={[styles.tinyCircle, { backgroundColor: appColor }]}></View>
                        <Text style={styles.textStyle}>Present</Text>
                    </View>
                </View>

                {fetching ?
                    <View style={styles.indicator}>
                        <ActivityIndicator
                            color={appColor}
                            size={'small'}
                        />
                    </View> :
                    emp_Attendance?.length > 0 ?

                        <FlatList
                            data={emp_Attendance}
                            keyExtractor={(item, index) => { return item.empcode }}
                            renderItem={renderItem}
                            style={{ marginBottom: 10, }}
                        />
                        :
                        <View style={styles.indicator}>
                            <Text style={styles.nodata}>No data Found</Text>
                        </View>
                }


            </View>

            <_Calendar
                is_Visible={isDateVisible}
                hideDatePicker={showCalendar}
                onDayPress={onDayPress}
                currentDate={currentDate}
                markedDates={markedDates}
                maxDate={maxDate}
                minDate={minDate}
            />
            <TimePicker
                isVisible={isTimeIn_visible}
                date={time_in}
                onDateChange={onTimeInChange}
                onSelect={handleTimeIn}
                onClose={closeTimeInPicker}
                mode="time"
            />
            <TimePicker
                isVisible={isTimeOut_visible}
                date={time_out}
                onDateChange={onTimeOutChange}
                onSelect={handleTimeOut}
                onClose={closeTimeOutPicker}
                mode="time"
            />
            <BottomTabs
                navigation={navigation}
                jvPress={() => navigation.navigate('journalVoucher')}
                purchasePress={() => navigation.navigate('Purchase')}
                saleInvoicePress={() => navigation.navigate('saleInvoice')}
                leaveApplyPress={() => navigation.navigate('leaveApply')}
            />
            <DialogBox ref={dialogboxRef} />
        </Container>
    )
}

const mapStateToProps = (state) => ({
    user: state.User.userInfo,
})
const mapDispatchToProps = (dispatch) => ({


});

export default connect(mapStateToProps, mapDispatchToProps)(Attendance)
