import React, { useEffect, useRef, useReducer } from "react";
import { View,Platform, } from "react-native";
import { Container, Content, Icon } from "native-base";
import InputField from "../../Components/InputField";
import DatePicker from "../../Components/DatePicker";
import _Calendar from '../../Components/Calendar';
import styles from "./styles";
import AppHeader from "../../Components/AppHeader";
import moment from 'moment'
import DropDown from "../../Components/Dropdown";
import DropdownModal from "../../Components/dropDownModal";
import SaveBtn from "../../Components/SaveBtn";
import CancelBtn from "../../Components/CancelBtn";
import DocPicker from "../../Components/DocPicker";
import DialogBox from 'react-native-dialogbox';
import DocumentPicker from 'react-native-document-picker'
import { NetworkInfo } from "react-native-network-info";
import { SERVER_ERROR, status, __error } from "../../Constants/constValues";
import axios from "axios";
import { baseUrl } from "../../Constants/server";
import {OGP_dd,Product_dd, Location_dd } from '../../redux/actions/Dropdowns'
import { connect } from "react-redux";
import Error from "../../Components/Error";
import { Keyboard } from "react-native";
import BottomTabs from "../../Components/BottomTab";



let filteredOGp = ''
let filteredLocation = ''
let filteredProduct = ''



function InwardGatePass({ navigation,user,saveProduct, saveLocation , saveOGp }) {
    let dialogboxRef = useRef()


    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.access_token}`
            },
            formData: {
                userid: user?.sessions?.userid,
                companyid: user?.sessions?.companyid,
                locationid: user?.sessions?.locationid,
                projectid:user?.sessions.projectid,
            },
            
            ipAddress: '',
            sending: false,

            l_dd: [],
            l_dd_List: [],
            location_id: '',

            p_dd: [],
            p_dd_List: [],
            product_id: '',

            ogp_dd: [],
            ogp_dd_List: [],
            ogp_id: '',
            rate:'',
            rateError:false,
            isdropdownLoading: false,
            voucherNumber: '',
            vnoError:false,
            isCalendar_Visible: false,
            currentDate: '',
            markedDates: {},
            voucherDate: '',
            vdateError:false,
            showOGPList: false,
            ogp: '',
            ogpError:false,
            searchOGP: '',
            location: '',
            locationError:false,
            searchLocation: '',
            product: '',
            productError:false,
            searchProduct: '',
            showLocation: false,
            showProducts: false,
            quantity: '',
            qtyError:false,
            lossQty: '',
            lossQtyError:false,
            gainQty: '',
            gainQtyError:false,
            filePath:'',
            fileType:'',
            errMsg:'',
        }
    )
    const {
        headers,
        formData,
        ipAddress,
        sending,
        rate,
        rateError,
        voucherNumber,
        isdropdownLoading,
        vnoError,
        currentDate,
        markedDates,
        voucherDate,
        vdateError,
        data,
        showOGPList,
        isCalendar_Visible,
        ogp,

        l_dd,
        l_dd_List,
        location_id,

        p_dd,
        p_dd_List,
        product_id,

        ogp_dd,
        ogp_dd_List,
        ogp_id,

        ogpError,
        searchOGP,
        location,
        locationError,
        searchLocation,
        product,
        productError,
        searchProduct,
        showLocation,
        showProducts,
        quantity,
        qtyError,
        gainQty,
        gainQtyError,
        lossQty,
        lossQtyError,
        filePath,
        fileType,
        errMsg

    } = state

    useEffect(() => {

        NetworkInfo.getIPAddress().then(ip => {
            updateState({ ipAddress: ip })
        });
        populateLocation_dd()
        populateProduct_dd()
        populateOGP_dd()

        let d = new Date().getDate()
        let m = new Date().getMonth()+1
        let y = new Date().getFullYear()

        if(d<=9){
          d = '0'+d
        }
        if(m<=9){
            m = '0'+m
        }
        let date =`${y}-${m}-${d}`
        let _show= `${d}-${m}-${y}`

        let selected = {}
        selected[date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        updateState({
            currentDate: date,
            voucherDate:_show,
            markedDates: selected
        })

    }, [])

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', async () => {
            populateLocation_dd()
            populateProduct_dd()
            populateOGP_dd()
            cancelInwardGatePass()
        });
        return unsubscribe;
    }, [])

// console.log('user session',user?.sessions.projectid)

    function alertMsg(msg1, msg2) {
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2],
            btn: {
                text: 'OK',
                callback: () => { },
            },
        })

    }

    function populateOGP_dd() {
        updateState({ isdropdownLoading: true })
        axios.post(`${baseUrl}/reproduction/pending-ogps`, formData, {
            headers: headers
        }).then((res) => {
            const { data } = res
            console.log('OGp dd', data)

            if (data.status === status) {
                /*********** dd Redux **********/
                saveOGp(data.pending_reproduction_ogps)

                let ddValues = []
                if (data.pending_reproduction_ogps?.length > 0) {
                    data.pending_reproduction_ogps.map((item, index) => {
                        ddValues.push(item.vno)
                    })

                    updateState({
                        isdropdownLoading: false,
                        ogp_dd_List: ddValues,// for filter 
                        ogp_dd: data.pending_reproduction_ogps, //work pending
                    })
                } else {
                    updateState({ isdropdownLoading: false })
                }
            } else {
                alertMsg(data.message)
            }
        })
    }


    function populateLocation_dd() {
        updateState({ isdropdownLoading: true })
        axios.post(`${baseUrl}/locations`, formData, {
            headers: headers
        }).then((res) => {
            const { data } = res
            if (data.status === status) {
                /*********** dd Redux **********/
                saveLocation(data.locations)

                let ddValues = []
                if (data.locations?.length > 0) {
                    data.locations.map((item, index) => {
                        ddValues.push(item.vname)
                    })

                    updateState({
                        isdropdownLoading: false,
                        l_dd_List: ddValues,// for filter 
                        l_dd: data.locations,
                    })
                } else {
                    updateState({ isdropdownLoading: false })
                }
            } else {
                alertMsg(data.message)
            }
        })
    }

    function populateProduct_dd() {
        updateState({ isdropdownLoading: true })
        axios.post(`${baseUrl}/inventory/products`, formData, {
            headers: headers
        }).then((res) => {
            const { data } = res
            console.log('product dd', data)
            if (data.status === status) {
                /*********** dd Redux **********/
                saveProduct(data.products)

                let ddValues = []
                if (data.products?.length > 0) {
                    data.products.map((item, index) => {
                        ddValues.push(item.vname)
                    })

                    updateState({
                        isdropdownLoading: false,
                        p_dd_List: ddValues,// for filter 
                        p_dd: data.products,
                    })
                } else {
                    updateState({ isdropdownLoading: false })
                }
            } else {
                alertMsg(data.message)
            }
        })
    }

    function openMenu() { 
        Keyboard.dismiss()
        setTimeout(()=>{
            navigation.openDrawer();
        },500)
    }
    function navigateTo(){ navigation.navigate('IGP_search')}

    async function selectDocument(){
        try {
            const results = await DocumentPicker.pick({
              type: [DocumentPicker.types.images],
            });
            
            updateState({
              filePath: results,
              fileType: results[0].type
            })
            for (const res of results) {
                console.log(
                  res.uri,
                  res.type, // mime type
                  res.name,
                  res.size,
                )
              }
    
          } catch (err) {
            if (DocumentPicker.isCancel(err)) {
              console.log('Document picker cancel')
            } else {
              throw err;
            }
          }
        }
    
       function cancelDocument (){
        updateState({
            filePath:'',
            fileType:''
        })
       }

    function showCalendar() {
        let isVisible = isCalendar_Visible
        updateState({ isCalendar_Visible: !isVisible })
    }

    function closeProductModal() {
        updateState({ showProducts: false })
    }

    function selectProduct(value, index) {
        var FOUND = p_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })
        updateState({
            product: value,
            product_id:FOUND.id,
            productError:false,
            errMsg:'',
            showProducts: false,
            searchProduct: ''
        })
    }
    const handleSearchProduct = (value) => {
        updateState({ searchProduct: value })
    }

    function onDayPress(date) {

        let [y,m,d] = date.dateString.split('-')
        let _show= `${d}-${m}-${y}`
        
        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates: selected,
            voucherDate:_show,
            vdateError:false,
            currentDate:date.dateString,
            isCalendar_Visible:false,
        })
    }

    function selectOGPValue(value, index) {
        var FOUND = ogp_dd.find(function (post, index) {
            if (post.vno == value) {
                return post;
            }
        })
        updateState({
            ogp: value,
            ogp_id:FOUND.id,
            ogpError:false,
            errMsg:'',
            showOGPList: false,
            searchOGP: ''
        })
    }
    const handleSearchOGP = (value) => {
        updateState({ searchOGP: value })
    }

    function selectLocation(value, index) {
        var FOUND = l_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })
        updateState({
            location: value,
            location_id:FOUND.id,
            locationError:false,
            errMsg:'',
            showLocation: false,
            searchLocation: '',
        })
    }
    const handleSearchLocation = (value) => {
        updateState({ searchLocation: value })
    }


    function closeOGPModal() {
        updateState({ showOGPList: false })
    }


    function closeLocationModal() {
        updateState({ showLocation: false })
    }

   async function saveInwardGatePass(){
        let err = false
        if(!voucherDate){
            updateState({vdateError:true})
            err = true
        }
        if(!ogp){
            updateState({ogpError:true})
            err = true
        }
        if(!location){
            updateState({locationError:true})
            err = true
        }
        if(!product){
            updateState({productError:true})
            err = true
        }
        if(!quantity || isNaN(quantity) || quantity < 1){
            updateState({qtyError:true})
            err = true
        }
        if(!lossQty || isNaN(lossQty)){
            updateState({lossQtyError:true})
            err = true
        }
        if(!gainQty || isNaN(gainQty)){
            updateState({gainQtyError:true})
            err = true
        }
        if(!rate || isNaN(rate)){
            updateState({rateError:true})
            err = true
        }
        if(!err){
            //// APi 
            const __headers = {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${user?.access_token}`
            }
            updateState({ sending: true })
            let tempVar = user?.sessions

            let __formData = new FormData()
            __formData.append('vdate', voucherDate)
            __formData.append('productid', product_id)
            __formData.append('productdesc', '')
            __formData.append('qty', quantity)
            __formData.append('vrate',rate)
            __formData.append('ogpid',ogp_id)
            __formData.append('locationid',location_id)
            __formData.append('lossqty',lossQty)
            __formData.append('gainqty',gainQty)
            if(filePath){
            __formData.append('file' ,{
                name: filePath[0]?.name,
                type: filePath[0]?.type,
                uri: Platform.OS === 'ios' ? 
                     filePath[0]?.uri.replace('file://', '')
                     : filePath[0]?.uri,
              })
            }
            __formData.append('companyid', tempVar?.companyid)
            __formData.append('projectid', tempVar?.projectid)
            __formData.append('insertedby', tempVar?.userid)
            __formData.append('insertedip', ipAddress)
            
            console.log('InWardGatePass data send to server', __formData)
           await axios.post(`${baseUrl}/reproduction/igps/create`, __formData, {
                headers: __headers
            }).then((response) => {
                const { data } = response
                console.log('response ', data)
                if (data.status === status) {
                    alertMsg('InWardGate Pass Voucher Saved Successfully')
                    updateState({voucherDate:''})
                    cancelInwardGatePass()

                }else if(data?.status === __error){
                    alertMsg(data?.message)
                    updateState({ sending: false })
                    console.log('error in elseif', data)                  
                }else {
                    updateState({ sending: false })
                    alertMsg('Out of stock', 'Required quantity is not available')
                    console.log('error in else', data)
                }
            }).catch((err) => {
                updateState({ sending: false })
                alertMsg(SERVER_ERROR)
                console.log('err in catch', err)
            })
        }else{
            updateState({ errMsg: 'Please fill required fields' })
        }
    }

   function cancelInwardGatePass(){
    updateState({
        sending: false,
        // voucherDate:'',
        product:'',
        quantity:'',
        rate:'',
        ogp:'',
        location:'',
        lossQty:'',
        gainQty:'',
        filePath:'',
        fileType:'',
        errMsg:'',

        vdateError:false,
        productError:false,
        qtyError:false,
        rateError:false,
        ogpError:false,
        locationError:false,
        lossQtyError:false,
        gainQtyError:false,
    })
    }

    /*********************** custom dd vendor  *************************/
    const filterOGPData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return ogp_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return ogp_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom  dd Product  *************************/
   const filterProductData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return p_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return p_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom  dd location  *************************/
    const filteredLocationData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return l_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return l_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
 /*********************** custom dd location  *************************/
    filteredOGp = filterOGPData(searchOGP)
    filteredProduct = filterProductData(searchProduct)
    filteredLocation = filteredLocationData(searchLocation)


    return (
        <Container>
            <AppHeader
                rightIconName={'search'}
                rightIconType={'EvilIcons'}
                rightBtnPress={navigateTo}
                headingText={'Inward Gate Pass'}
                leftBtnPress={openMenu}
                dashbord={false}
                heading={true}
                search={false}
            />
            <View style={[styles.blueBG]}></View>

            <Content style={styles.container}>
                {/* <InputField
                    label="Voucher Number"
                    placeholder={'Voucher Number'}
                    onChangeText={(v) => updateState({ 
                        voucherNumber: v,
                        vnoError:false,
                        errMsg:'',
                     })}
                    value={voucherNumber}
                    error={vnoError}
                /> */}
                <DatePicker
                    label="Voucher Date"
                    onPress={showCalendar}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={voucherDate}
                />

                <DropDown
                    label="OGP"
                    placeholder={'OGP'}
                    value={ogp}
                    onPress={() => updateState({ showOGPList: true })}
                    error={ogpError}
                />

                <DropDown
                    label="Location"
                    placeholder={'Location'}
                    value={location}
                    onPress={() => updateState({ showLocation: true })}
                    error={locationError}
                />

                <DropDown
                    label="Product"
                    placeholder={'Product'}
                    value={product}
                    onPress={() => updateState({ showProducts: true })}
                    error={productError}
                />

                <InputField
                    label="Quantity"
                    placeholder={'Quantity'}
                    keyboardType={'number-pad'}
                    onChangeText={(v) => updateState({ 
                        quantity: v,
                        qtyError:false,
                        errMsg:'',
                     })}
                    value={quantity}
                    error={qtyError}
                />
                <InputField
                    label="Loss Quantity"
                    placeholder={'Loss Quantity'}
                    keyboardType={'number-pad'}
                    onChangeText={(v) => updateState({ 
                        lossQty: v,
                        lossQtyError:false,
                        errMsg:'',
                     })}
                    value={lossQty}
                    error={lossQtyError}
                />
                <InputField
                    label="Gain Quantity"
                    placeholder={'Gain Quantity'}
                    keyboardType={'number-pad'}
                    onChangeText={(v) => updateState({ 
                        gainQty: v,
                        gainQtyError:false,
                        errMsg:''
                     })}
                    value={gainQty}
                    error={gainQtyError}
                />
                <InputField
                    label="Rate"
                    placeholder={'Rate'}
                    keyboardType={'decimal-pad'}
                    onChangeText={(v) => updateState({ 
                        rate: v,
                        rateError:false,
                        errMsg:''
                     })}
                    value={rate}
                    error={rateError}
                />

                {errMsg ?
                    <Error
                      error={errMsg}
                    /> : null
                }
                {qtyError ?
                    <Error
                      error={'Quantity should be greater then 0'}
                    /> : null
                }
                <View style={styles.btnView}>
                    <SaveBtn
                        onPress={saveInwardGatePass}
                        title={'Save'}
                        saving={sending}
                    />
                    <CancelBtn
                       onPress={cancelInwardGatePass}
                       title={'Cancel'}
                    />
                    <DocPicker
                      onPress={selectDocument}
                      fileType={fileType}
                      filePath={filePath}
                      cancelFile={cancelDocument}
                     />
                </View>
            </Content>



            <_Calendar
                is_Visible={isCalendar_Visible}
                hideDatePicker={showCalendar}
                onDayPress={onDayPress}
                currentDate={currentDate}
                markedDates={markedDates}
            />


            <DropdownModal
                isVisible={showOGPList}
                closeFromModal={closeOGPModal}
                data={filteredOGp}
                onChangeText={handleSearchOGP}
                selectValue={selectOGPValue}
            />

            <DropdownModal
                isVisible={showLocation}
                closeFromModal={closeLocationModal}
                data={filteredLocation}
                onChangeText={handleSearchLocation}
                selectValue={selectLocation}
            />

            <DropdownModal
                isVisible={showProducts}
                closeFromModal={closeProductModal}
                data={filteredProduct}
                onChangeText={handleSearchProduct}
                selectValue={selectProduct}
            />
            <BottomTabs
                navigation={navigation}
                jvPress={()=>navigation.navigate('journalVoucher')}
                purchasePress={()=>navigation.navigate('Purchase')}
                saleInvoicePress={()=>navigation.navigate('saleInvoice')}
                leaveApplyPress={()=>navigation.navigate('leaveApply')}
            />
            <DialogBox ref={dialogboxRef} />
        </Container>
    )

}
const mapStateToProps = (state) => ({
    user: state.User.userInfo,
})
const mapDispatchToProps = (dispatch) => ({
    saveLocation: (v) => dispatch(Location_dd(v)),
    saveProduct: (v) => dispatch(Product_dd(v)),
    saveOGp:(v)=>dispatch(OGP_dd(v))
});
export default connect(mapStateToProps, mapDispatchToProps)(InwardGatePass)

