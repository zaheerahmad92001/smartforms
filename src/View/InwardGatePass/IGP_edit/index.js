import React, { useEffect, useRef, useReducer } from "react";
import { View,Platform } from "react-native";
import { Container, Content, Icon } from "native-base";
import InputField from "../../../Components/InputField";
import DatePicker from "../../../Components/DatePicker";
import _Calendar from '../../../Components/Calendar';
import styles from "../styles";
import AppHeader from "../../../Components/AppHeader";
import moment from 'moment'
import DropDown from "../../../Components/Dropdown";
import DropdownModal from "../../../Components/dropDownModal";
import SaveBtn from "../../../Components/SaveBtn";
import CancelBtn from "../../../Components/CancelBtn";
import DocPicker from "../../../Components/DocPicker";
import DialogBox from 'react-native-dialogbox';
import DocumentPicker from 'react-native-document-picker'
import { NetworkInfo } from "react-native-network-info";
import { SERVER_ERROR, status, __error } from "../../../Constants/constValues";
import axios from "axios";
import { baseUrl } from "../../../Constants/server";
import { connect } from "react-redux";
import Error from "../../../Components/Error";



let filteredOGp = ''
let filteredLocation = ''
let filteredProduct = ''



function InwardGatePass({ navigation,user,route ,loc_dd, product_dd ,Ogp_dd }) {
    let dialogboxRef = useRef()
    let item =  route.params.item


    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.access_token}`
            },
            formData: {
                userid: user?.sessions?.userid,
                companyid: user?.sessions?.companyid,
                locationid: user?.sessions?.locationid,
            },
            
            ipAddress: '',
            sending: false,

            l_dd:[],
            l_dd_List: [],
            location_id:item?.locationid,

            p_dd: [],
            p_dd_List: [],
            product_id:item?.productid,

            ogp_dd: [],
            ogp_dd_List: [],
            ogp_id:item?.ogpid,
            rate:item?.vrate,
            rateError:false,
            isdropdownLoading: false,
            voucherNumber:item?.vno,
            vnoError:false,
            isCalendar_Visible: false,
            currentDate: '',
            markedDates: {},
            voucherDate: '',
            vdateError:false,
            showOGPList: false,
            ogp:item?.ogpvno,
            ogpError:false,
            searchOGP: '',
            location:item?.locationname,
            locationError:false,
            searchLocation: '',
            product:item?.productname,
            productError:false,
            searchProduct: '',
            showLocation: false,
            showProducts: false,
            quantity:Number(item?.qty).toFixed(0),
            qtyError:false,
            lossQty:Number(item?.lossqty).toFixed(0),
            lossQtyError:false,
            gainQty:Number(item?.gainqty).toFixed(0),
            gainQtyError:false,
            filePath:'',
            fileType:'',
            errMsg:'',

            startDay:'' , 
            endDay:''

        }
    )
    const {
        headers,
        formData,
        ipAddress,
        sending,
        rate,
        rateError,
        voucherNumber,
        isdropdownLoading,
        vnoError,
        currentDate,
        markedDates,
        voucherDate,
        vdateError,
        data,
        showOGPList,
        isCalendar_Visible,
        ogp,

        l_dd,
        l_dd_List,
        location_id,

        p_dd,
        p_dd_List,
        product_id,

        ogp_dd,
        ogp_dd_List,
        ogp_id,

        ogpError,
        searchOGP,
        location,
        locationError,
        searchLocation,
        product,
        productError,
        searchProduct,
        showLocation,
        showProducts,
        quantity,
        qtyError,
        gainQty,
        gainQtyError,
        lossQty,
        lossQtyError,
        filePath,
        fileType,
        errMsg,
        startDay , 
        endDay,


    } = state

    useEffect(() => {

        NetworkInfo.getIPAddress().then(ip => {
            updateState({ ipAddress: ip })
        });
        populateLocation_dd()
        populateProduct_dd()
        populateOGP_dd()

        
        let [_d ,_y] = item?.vdate.split(' ')
        let [y,m,d] = _d?.split('-')

        let date =`${y}-${m}-${d}`
        let _show= `${d}-${m}-${y}`

        let fullYear = y
        let fullMonth = m -1
        var my_date = new Date(fullYear, fullMonth);

        var first_date = new Date(my_date.getFullYear(), my_date.getMonth(), 1);
        var last_date = new Date(my_date.getFullYear(), my_date.getMonth() +1, 0);

        let firstDay = moment.utc(first_date).local().format('YYYY-MM-DD')
        let last_day = moment.utc(last_date).local().format('YYYY-MM-DD')

        let selected = {}
        selected[date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        updateState({
            currentDate: date,
            voucherDate:_show,
            markedDates: selected,
            startDay:firstDay,
            endDay:last_day
        })

    }, [])

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', async () => {
            populateLocation_dd()
            populateProduct_dd()
            populateOGP_dd()
        });
        return unsubscribe;
    }, [])

    function alertMsg(msg1, msg2) {
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2],
            btn: {
                text: 'OK',
                callback: () => {navigation.pop() },
            },
        })

    }

    function populateOGP_dd() {
        updateState({ isdropdownLoading: true })
                let ddValues = []
                if (Ogp_dd?.length > 0) {
                    Ogp_dd.map((item, index) => {
                        ddValues.push(item.vname)
                    })

                    updateState({
                        isdropdownLoading: false,
                        ogp_dd_List: ddValues,// for filter 
                        ogp_dd: Ogp_dd, //work pending
                    })
                } else {
                    updateState({ isdropdownLoading: false })
                }
            }


    function populateLocation_dd() {
                let ddValues = []
                if (loc_dd?.length > 0) {
                    loc_dd.map((item, index) => {
                        ddValues.push(item.vname)
                    })

                    updateState({
                        isdropdownLoading: false,
                        l_dd_List: ddValues,// for filter 
                        l_dd:loc_dd,
                    })
                } else {
                    updateState({ isdropdownLoading: false })
                }
            }

    function populateProduct_dd() {
        updateState({ isdropdownLoading: true })
                let ddValues = []
                if (product_dd?.length > 0) {
                    product_dd.map((item, index) => {
                        ddValues.push(item.vname)
                    })

                    updateState({
                        isdropdownLoading: false,
                        p_dd_List: ddValues,// for filter 
                        p_dd:product_dd,
                    })
                } else {
                    updateState({ isdropdownLoading: false })
                }
            }

    function goBack() {navigation.pop()}

    async function selectDocument(){
        try {
            const results = await DocumentPicker.pick({
              type: [DocumentPicker.types.images],
            });
            
            updateState({
              filePath: results,
              fileType: results[0].type
            })
            for (const res of results) {
                console.log(
                  res.uri,
                  res.type, // mime type
                  res.name,
                  res.size,
                )
              }
    
          } catch (err) {
            if (DocumentPicker.isCancel(err)) {
              console.log('Document picker cancel')
            } else {
              throw err;
            }
          }
        }
    
       function cancelDocument (){
        updateState({
            filePath:'',
            fileType:''
        })
       }

    function showCalendar() {
        let isVisible = isCalendar_Visible
        updateState({ isCalendar_Visible: !isVisible })
    }

    function closeProductModal() {
        updateState({ showProducts: false })
    }

    function selectProduct(value, index) {
        var FOUND = p_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })
        updateState({
            product: value,
            product_id:FOUND.id,
            productError:false,
            errMsg:'',
            showProducts: false,
            searchProduct: ''
        })
    }
    const handleSearchProduct = (value) => {
        updateState({ searchProduct: value })
    }

    function onDayPress(date) {

        let [y,m,d] = date.dateString.split('-')
        let _show= `${d}-${m}-${y}`
        
        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates: selected,
            voucherDate:_show,
            currentDate:date.dateString,
            isCalendar_Visible:false,
        })
    }

    function selectOGPValue(value, index) {
        var FOUND = ogp_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })
        updateState({
            ogp: value,
            ogp_id:FOUND.id,
            ogpError:false,
            errMsg:'',
            showOGPList: false,
            searchOGP: ''
        })
    }
    const handleSearchOGP = (value) => {
        updateState({ searchOGP: value })
    }

    function selectLocation(value, index) {
        var FOUND = l_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })
        updateState({
            location: value,
            location_id:FOUND.id,
            locationError:false,
            errMsg:'',
            showLocation: false,
            searchLocation: '',
        })
    }
    const handleSearchLocation = (value) => {
        updateState({ searchLocation: value })
    }


    function closeOGPModal() {
        updateState({ showOGPList: false })
    }


    function closeLocationModal() {
        updateState({ showLocation: false })
    }

   async function saveInwardGatePass(){
        let err = false
        // if(!voucherNumber){
        //     updateState({vnoError:true})
        //     err = true
        // }
        if(!ogp){
            updateState({ogpError:true})
            err = true
        }
        if(!location){
            updateState({locationError:true})
            err = true
        }
        if(!product){
            updateState({productError:true})
            err = true
        }
        if(!quantity || isNaN(quantity) || quantity < 1){
            updateState({qtyError:true})
            err = true
        }
        if(!lossQty || isNaN(lossQty)){
            updateState({lossQtyError:true})
            err = true
        }
        if(!gainQty || isNaN(gainQty)){
            updateState({gainQtyError:true})
            err = true
        }
        if(!rate || isNaN(rate)){
            updateState({rateError:true})
            err = true
        }
        if(!err){
            //// APi 
            const __headers = {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${user?.access_token}`
            }
            updateState({ sending: true })
            let tempVar = user?.sessions


            let __formData = new FormData()
            __formData.append('vdate', voucherDate)
            __formData.append('productid', product_id)
            __formData.append('productdesc', '')
            __formData.append('qty', quantity)
            __formData.append('vrate',rate)
            __formData.append('ogpid',ogp_id)
            __formData.append('locationid',location_id)
            __formData.append('lossqty',lossQty)
            __formData.append('gainqty',gainQty)
            if(filePath){
            __formData.append('file' ,{
                name: filePath[0]?.name,
                type: filePath[0]?.type,
                uri: Platform.OS === 'ios' ? 
                     filePath[0]?.uri.replace('file://', '')
                     : filePath[0]?.uri,
              })
            }
            __formData.append('companyid', tempVar?.companyid)
            __formData.append('projectid', tempVar?.projectid)
            __formData.append('insertedby', tempVar?.userid)
            __formData.append('insertedip', ipAddress)
            
           
            console.log('InWardGatePass data send to server', __formData)
           await axios.post(`${baseUrl}/reproduction/igps/update/${item?.id}`, __formData, {
                headers: __headers
            }).then((response) => {
                const { data } = response
                console.log('response ', data)
                if (data.status === status) {
                    alertMsg('InWardGate Pass Voucher Updated Successfully')
                    updateState({sending: false})
                }else if(data?.status === __error){
                    alertMsg(data?.message)
                    updateState({ sending: false })
                    console.log('error in elseif', data)                  
                }else {
                    updateState({ sending: false })
                    alertMsg('Out of stock', 'Required quantity is not available')
                    console.log('error in else', data)
                }
            }).catch((err) => {
                updateState({ sending: false })
                alertMsg(SERVER_ERROR)
                console.log('err in catch', err)
            })
        }else{
            updateState({ errMsg: 'Please fill required fields' })
        }
    }

   function cancelInwardGatePass(){
    navigation.pop()
    }

    /*********************** custom dd vendor  *************************/
    const filterOGPData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return ogp_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return ogp_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom  dd Product  *************************/
   const filterProductData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return p_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return p_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom  dd location  *************************/
    const filteredLocationData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return l_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return l_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
 /*********************** custom dd location  *************************/
    filteredOGp = filterOGPData(searchOGP)
    filteredProduct = filterProductData(searchProduct)
    filteredLocation = filteredLocationData(searchLocation)


    return (
        <Container>
            <AppHeader
                headingText={'Edit InwardGate Pass'}
                leftIconName={'arrow-back'}
                leftIconType={'Ionicons'}
                leftBtnPress={goBack}
                dashbord={false}
                heading={true}
                search={false}
                edit={true}
            />
            <View style={[styles.blueBG]}></View>

            <Content style={styles.container}>
                <InputField
                    label="Voucher Number"
                    placeholder={'Voucher Number'}
                    editable={false}
                    onChangeText={(v) => updateState({ 
                        voucherNumber: v,
                        vnoError:false,
                        errMsg:'',
                     })}
                    value={voucherNumber}
                    error={vnoError}
                />
                <DatePicker
                    label="Voucher Date"
                    onPress={showCalendar}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={voucherDate}
                />

                <DropDown
                    label="OGP"
                    placeholder={'OGP'}
                    value={ogp}
                    // onPress={() => updateState({ showOGPList: true })}
                    error={ogpError}
                />

                <DropDown
                    label="Location"
                    placeholder={'Location'}
                    value={location}
                    // onPress={() => updateState({ showLocation: true })}
                    error={locationError}
                />

                <DropDown
                    label="Product"
                    placeholder={'Product'}
                    value={product}
                    // onPress={() => updateState({ showProducts: true })}
                    error={productError}
                />

                <InputField
                    label="Quantity"
                    placeholder={'Quantity'}
                    keyboardType={'number-pad'}
                    onChangeText={(v) => updateState({ 
                        quantity: v,
                        qtyError:false,
                        errMsg:'',
                     })}
                    value={quantity}
                    error={qtyError}
                />
                <InputField
                    label="Loss Quantity"
                    placeholder={'Loss Quantity'}
                    keyboardType={'number-pad'}
                    onChangeText={(v) => updateState({ 
                        lossQty: v,
                        lossQtyError:false,
                        errMsg:'',
                     })}
                    value={lossQty}
                    error={lossQtyError}
                />
                <InputField
                    label="Gain Quantity"
                    placeholder={'Gain Quantity'}
                    keyboardType={'number-pad'}
                    onChangeText={(v) => updateState({ 
                        gainQty: v,
                        gainQtyError:false,
                        errMsg:''
                     })}
                    value={gainQty}
                    error={gainQtyError}
                />
                <InputField
                    label="Rate"
                    placeholder={'Rate'}
                    keyboardType={'decimal-pad'}
                    onChangeText={(v) => updateState({ 
                        rate: v,
                        rateError:false,
                        errMsg:''
                     })}
                    value={rate}
                    error={rateError}
                />

                {errMsg ?
                    <Error
                      error={errMsg}
                    /> : null
                }
                {qtyError ?
                    <Error
                      error={'Quantity should be greater then 0'}
                    /> : null
                }

                <View style={styles.btnView}>
                    <SaveBtn
                        onPress={saveInwardGatePass}
                        title={'Save'}
                        saving={sending}
                    />
                    <CancelBtn
                       onPress={cancelInwardGatePass}
                       title={'Cancel'}
                    />
                    <DocPicker
                      onPress={selectDocument}
                      fileType={fileType}
                      filePath={filePath}
                      cancelFile={cancelDocument}
                     />
                </View>
            </Content>



            <_Calendar
                is_Visible={isCalendar_Visible}
                hideDatePicker={showCalendar}
                onDayPress={onDayPress}
                currentDate={currentDate}
                markedDates={markedDates}
                minDate={startDay}
                maxDate={endDay}
            />


            <DropdownModal
                isVisible={showOGPList}
                closeFromModal={closeOGPModal}
                data={filteredOGp}
                onChangeText={handleSearchOGP}
                selectValue={selectOGPValue}
                isLoading={isdropdownLoading}
            />

            <DropdownModal
                isVisible={showLocation}
                closeFromModal={closeLocationModal}
                data={filteredLocation}
                onChangeText={handleSearchLocation}
                selectValue={selectLocation}
                isLoading={isdropdownLoading}
            />

            <DropdownModal
                isVisible={showProducts}
                closeFromModal={closeProductModal}
                data={filteredProduct}
                onChangeText={handleSearchProduct}
                selectValue={selectProduct}
                isLoading={isdropdownLoading}
            />
            <DialogBox ref={dialogboxRef} />
        </Container>
    )

}
const mapStateToProps = (state) => ({
    user: state.User.userInfo,
    loc_dd: state.Dropdown.locationDDs,
    product_dd: state.Dropdown.productDDs,
    Ogp_dd: state.Dropdown.ogpDDs
})
const mapDispatchToProps = (dispatch) => ({
});
export default connect(mapStateToProps, mapDispatchToProps)(InwardGatePass)

