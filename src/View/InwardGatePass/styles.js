import React from "react";
import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import { appColor,white } from "../../Constants/Colors";

export default style = StyleSheet.create({
    container:{
        marginHorizontal:15,
        paddingTop:10,
    },
    blueBG:{
        backgroundColor:appColor,
        height:Platform.OS=='android'? hp(4) : hp(4),
        borderBottomRightRadius:20,
        borderBottomLeftRadius:20,
        
    },
    dateContainer:{
        backgroundColor:appColor,
        borderRadius:5,
      },
      dateText:{
        color:white,
        fontWeight: '500'
      },
      currentDateContainer:{
        backgroundColor:white,
        borderRadius:5,
        borderColor:'#007BFF',
        borderWidth:1
      },
      currentDateText: {
        color:appColor,
        fontWeight: '500'
      },
      btnView:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        marginTop:20,
        marginBottom:30,
      },
})