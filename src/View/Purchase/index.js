import React, { useEffect, useRef, useReducer } from "react";
import { View, Text, PermissionsAndroid,Platform,Keyboard } from "react-native";
import { Container, Content, Icon } from "native-base";
import { logo } from "../../Constants/images";
import InputField from "../../Components/InputField";
import DatePicker from "../../Components/DatePicker";
import _Calendar from '../../Components/Calendar';
import styles from "./styles";
import AppHeader from "../../Components/AppHeader";
import DialogBox from 'react-native-dialogbox';
import DropDown from "../../Components/Dropdown";
import DropdownModal from "../../Components/dropDownModal";
import SaveBtn from "../../Components/SaveBtn";
import CancelBtn from "../../Components/CancelBtn";
import DocPicker from "../../Components/DocPicker";
import DocumentPicker from 'react-native-document-picker'
import Error from "../../Components/Error";
import { NetworkInfo } from "react-native-network-info";
import { SERVER_ERROR, status, STOCK_OUT, STOCK_OUT_MSG, __error } from "../../Constants/constValues";
import axios from "axios";
import { baseUrl } from "../../Constants/server";
import BottomTabs from "../../Components/BottomTab";
import {Vendor_dropdowns ,Bank_dd, Product_dd, Location_dd}from '../../redux/actions/Dropdowns'
import { connect } from "react-redux";
import RNFetchBlob from 'rn-fetch-blob';
import { Alert } from "react-native";



let filteredVendor = ''
let filteredBankAcc = '' 
let filteredLocation = ''
let filteredProduct = ''

function Purchase({ navigation ,user , saveVendors ,saveBank ,saveProduct,saveLocation }) {
    let dialogboxRef = useRef()
    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {

        headers : {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${user?.access_token}`
           },
        formData : {
            userid: user?.sessions?.userid,
            companyid: user?.sessions?.companyid,
            locationid: user?.sessions?.locationid,
        },

            voucherNumber: '',
            vnoError: false,
            isCalendar_Visible: false,
            isCalendar_Visible_due: false,
            ipAddress:'',

            v_dd:[],
            v_dd_List:[],
            vendor_id:'',

            b_dd:[],
            b_dd_List:[],
            bank_id:'',

            p_dd:[],
            p_dd_List:[],
            product_id:'',

            l_dd:[],
            l_dd_List:[],
            location_id:'',

            currentDate: '',
            c_currentDate:'',
            markedDates: {},
            markedDates_due: {},
            voucherDate: '',
            vdateError: false,
            chequeNumber: '',
            chequeNoError: false,
            is_vendor_dd_Visible: false,
            isBank_dd_Visible: false,
            vendor: '',
            vendorError: false,
            searchVendor: '',
            bankAcc: '',
            bankAccError: false,
            searchBank: '',
            dueDate: '',
            dueDateError: false,
            location: '',
            locationError: false,
            product: '',
            productError: false,
            searchProduct: '',
            searchLocation: '',
            is_location_dd_Visible: false,
            is_product_Visible: false,
            filePath: '',
            fileType: '',
            quantity: '',
            qtyError: false,
            rate: '',
            rateError: false,
            errMsg: '',
            isdropdownLoading:false,
            sending:false,

        }
    )
    const {
        headers,
        formData,
        voucherNumber,
        vnoError,
        currentDate,
        c_currentDate,
        markedDates,
        voucherDate,
        vdateError,
        chequeNumber,
        chequeNoError,
        v_dd,
        v_dd_List,
        vendor_id,
        ipAddress,

        p_dd,
        p_dd_List,
        product_id,

        b_dd,
        b_dd_List,
        bank_id,

        l_dd,
        l_dd_List,
        location_id,

        is_vendor_dd_Visible,
        isBank_dd_Visible,
        isCalendar_Visible,
        isCalendar_Visible_due,
        vendor,
        vendorError,
        bankAcc,
        bankAccError,
        dueDate,
        dueDateError,
        markedDates_due,
        location,
        locationError,
        is_location_dd_Visible,
        is_product_Visible,
        product,
        productError,
        searchProduct,
        searchBank,
        searchLocation,
        searchVendor,
        fileType,
        filePath,
        quantity,
        qtyError,
        rate,
        rateError,
        errMsg,
        isdropdownLoading,
        sending,

    } = state

    useEffect(() => {

        NetworkInfo.getIPAddress().then(ip => {
            updateState({ipAddress:ip})
          });

        populateVendor_dd()
        populateBank_dd()
        populateLocation_dd()
        populateProduct_dd()

        if(Platform.OS=='android'){
                requestStoragePermissions()
         }

        let d = new Date().getDate()
        let m = new Date().getMonth()+1
        let y = new Date().getFullYear()

        if(d<=9){
          d = '0'+d
        }
        if(m<=9){
            m = '0'+m
        }
        let date =`${y}-${m}-${d}`
        let _show= `${d}-${m}-${y}`

        let selected = {}
        selected[date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        updateState({
            currentDate: date,
            c_currentDate:date,
            voucherDate:_show,
            dueDate:_show,
            markedDates: selected,
            markedDates_due:selected,
        })

    }, [])

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', async () => {
            populateVendor_dd()
            populateBank_dd()
            populateLocation_dd()
            populateProduct_dd()

            cancelPurchase()  // clear the form
        });
        return unsubscribe;
    },[])

function navigateTo(){ navigation.navigate('Purchase_search')}

async function requestStoragePermissions(){

try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      {
        'title': 'ReactNativeCode Storage Permission',
        'message': 'ReactNativeCode App needs access to your storage to download Photos.'
      }
    )
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
 
    //   Alert.alert("Storage Permission Granted.");
    }
    else {
 
      Alert.alert("Storage Permission Not Granted");
 
    }
  } catch (err) {
    console.warn(err)
  }
}


// const downloadFile = () => {
//     var date = new Date();
//     var image_URL = 'https://github.com/vinzscam/react-native-file-viewer/raw/master/docs/react-native-file-viewer-certificate.pdf';
//     // var image_URL = 'https://reactnativecode.com/wp-content/uploads/2018/02/motorcycle.jpg';
//     var ext = getExtention(image_URL);
//     ext = "." + ext[0];
//     const { config, fs } = RNFetchBlob;
//     let DocumentDir = `${fs.dirs.DownloadDir}`
//     console.log('dir name' , DocumentDir)
//     let options = {
//       fileCache: true,
//       addAndroidDownloads: {
//         useDownloadManager: true,
//         notification: true,
//         path: DocumentDir + "/Purchase.pdf" + Math.floor(date.getTime()
//           + date.getSeconds() / 2) + ext,
//         description: 'PDF File'
//       }
//     }
//     config(options).fetch('GET', image_URL).then((res) => {
//       Alert.alert("Image Downloaded Successfully.");
//     });
//   }

//  const getExtention = (filename) => {
//     return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) :
//       undefined;
//   }


    function alertMsg(msg1 ,msg2){
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2],
            btn: {
                text: 'OK',
                callback: () => { },
            },
        })

    }

   function populateVendor_dd(){
    updateState({ isdropdownLoading: true })
        axios.post(`${baseUrl}/inventory/vendors`, formData, {
            headers: headers
        }).then((res) => {
            const { data } = res
            if (data.status === status) {
                /*********** dd Redux **********/
                saveVendors(data.vendors)

                let ddValues = []
                if (data.vendors?.length > 0) {
                    data.vendors.map((item, index) => {
                        ddValues.push(item.vname)
                    })

                    updateState({
                        isdropdownLoading: false,
                        v_dd_List: ddValues,// for filter 
                        v_dd: data.vendors,
                    })
                } else {
                    updateState({ isdropdownLoading: false })
                }
            } else {
                alertMsg(data.message)
            }
        }) 
    }

    function populateBank_dd(){
        updateState({ isdropdownLoading: true })
            axios.post(`${baseUrl}/banks`, formData, {
                headers: headers
            }).then((res) => {
                const { data } = res
                if (data.status === status) {
                    /*********** dd Redux **********/
                    saveBank(data.banks)
    
                    let ddValues = []
                    if (data.banks?.length > 0) {
                        data.banks.map((item, index) => {
                          ddValues.push(item.vname)
                        })
    
                        updateState({
                            isdropdownLoading: false,
                            b_dd_List: ddValues,// for filter 
                            b_dd: data.banks,
                        })
                    } else {
                        updateState({ isdropdownLoading: false })
                    }
                } else {
                    alertMsg(data.message)
                }
            }) 
        }

        function populateLocation_dd(){
            updateState({ isdropdownLoading: true })
                axios.post(`${baseUrl}/locations`, formData, {
                    headers: headers
                }).then((res) => {
                    const { data } = res
                    if (data.status === status) {
                        /*********** dd Redux **********/
                        saveLocation(data.locations)
        
                        let ddValues = []
                        if (data.locations?.length > 0) {
                            data.locations.map((item, index) => {
                              ddValues.push(item.vname)
                            })
        
                            updateState({
                                isdropdownLoading: false,
                                l_dd_List: ddValues,// for filter 
                                l_dd: data.locations,
                            })
                        } else {
                            updateState({ isdropdownLoading: false })
                        }
                    } else {
                        alertMsg(data.message)
                    }
                }) 
            }

            function populateProduct_dd(){
                updateState({ isdropdownLoading: true })
                    axios.post(`${baseUrl}/inventory/products`, formData, {
                        headers: headers
                    }).then((res) => {
                        const { data } = res
                        console.log('product dd' , data)
                        if (data.status === status) {
                            /*********** dd Redux **********/
                            saveProduct(data.products)
            
                            let ddValues = []
                            if (data.products?.length > 0) {
                                data.products.map((item, index) => {
                                  ddValues.push(item.vname)
                                })
            
                                updateState({
                                    isdropdownLoading: false,
                                    p_dd_List: ddValues,// for filter 
                                    p_dd: data.products,
                                })
                            } else {
                                updateState({ isdropdownLoading: false })
                            }
                        } else {
                            alertMsg(data.message)
                        }
                    }) 
                }
    


    function openMenu() {
        Keyboard.dismiss()
        setTimeout(()=>{
            navigation.openDrawer();
        },500)
    }

    async function selectDocument() {
        try {
            const results = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });

            updateState({
                filePath: results,
                fileType: results[0].type
            })
            for (const res of results) {
                console.log(
                    res.uri,
                    res.type, // mime type
                    res.name,
                    res.size,
                )
            }

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                console.log('Document picker cancel')
            } else {
                throw err;
            }
        }
    }

    function cancelDocument() {
        updateState({
            filePath: '',
            fileType: ''
        })
    }


    function showCalendar() {
        let isVisible = isCalendar_Visible
        updateState({ isCalendar_Visible: !isVisible })
    }

    function showCalendar_due() {
        let isVisible = isCalendar_Visible_due
        updateState({ isCalendar_Visible_due: !isVisible })

    }

    function onDayPress(date) {

        let [y,m,d] = date.dateString.split('-')
        let _show= `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates: selected,
            voucherDate:_show,
            currentDate:date.dateString,
            isCalendar_Visible:false,
            vdateError: false,
            errMsg: '',
        })
    }

    
    function onDayPress_due(date) {

        let [y,m,d] = date.dateString.split('-')
        let _show= `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates_due: selected,
            dueDate:_show,
            c_currentDate:date.dateString,
            isCalendar_Visible_due:false,
            dueDateError: false,
            errMsg: '',
        })
    }

    
    function selectValue_Vendor(value, index) {
        var FOUND = v_dd.find(function (post, index) {
            if (post.vname == value) {
                  return post;
            }
        })
        updateState({
            vendor: value,
            vendor_id:FOUND.id,
            is_vendor_dd_Visible: false,
            searchVendor: '',
            vendorError: false,
            errMsg: '',

        })
    }
    const handleSearchVendor = (value) => {
        updateState({ searchVendor: value })
    }

    function selectValue_location(value, index) {
        var FOUND = l_dd.find(function (post, index) {
            if (post.vname == value) {
                  return post;
            }
        })
        updateState({
            location: value,
            location_id:FOUND.id,
            is_location_dd_Visible: false,
            searchLocation: '',
            locationError: false,
            errMsg: '',
        })
    }

    const handleSearchLocation = (value) => {
        updateState({ searchLocation: value })
    }

    function selectValue_product(value, index) {
        var FOUND = p_dd.find(function (post, index) {
            if (post.vname == value) {
                  return post;
            }
        })
        updateState({
            product: value,
            product_id:FOUND.id,
            is_product_Visible: false,
            searchProduct: '',
            productError: false,
            errMsg: '',

        })
    }
    const handleSearchProduct = (value) => {
        updateState({ searchProduct: value })
    }


    function selectBank(value, index) {
        var FOUND = b_dd.find(function (post, index) {
            if (post.vname == value) {
                  return post;
            }
        })
        updateState({
            bankAcc: value,
            bank_id:FOUND.id,
            isBank_dd_Visible: false,
            searchBank: '',
            bankAccError: false,
            errMsg: '',

        })
    }

    const handleSearchBank = (value) => {
        updateState({ searchBank: value })
    }


    function closeVendorModal() {
        updateState({ is_vendor_dd_Visible: false })
    }
    function closeBankModal() {
        updateState({ isBank_dd_Visible: false })
    }

    function closeLocationModal() {
        updateState({ is_location_dd_Visible: false })
    }

    function closeProdutModal() {
        updateState({ is_product_Visible: false })
    }

  async function savePurchases() {

        let err = false
        if (!voucherDate) {
            updateState({ vdateError: true, })
            err = true
        }
        if (!vendor) {
            updateState({ vendorError: true, })
            err = true
        }
        if (!bankAcc) {
            updateState({ bankAccError: true, })
            err = true
        }
        if (!chequeNumber) {
            updateState({ chequeNoError: true, })
            err = true
        }
        if (!dueDate) {
            updateState({ dueDateError: true, })
            err = true
        }
        if (!location) {
            updateState({ locationError: true, })
            err = true
        }
        if (!product) {
            updateState({ productError: true, })
            err = true
        }
        if (!quantity || isNaN(quantity) || quantity < 1 ) {
            updateState({ qtyError: true, })
            err = true
        }
        if (!rate || isNaN(rate) ) {
            updateState({ rateError: true, })
            err = true
        }
        if (!err) {
            // api 
            const __headers = {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${user?.access_token}`
            }
            updateState({sending:true})
            let tempVar =user?.sessions 

            let formData = new FormData()
            
              formData.append('vdate',voucherDate)
              formData.append('vendorid',vendor_id)
              formData.append('bankid',bank_id)
              formData.append('chequeno',chequeNumber)
              formData.append('chequedate',dueDate)
              if(filePath){
              formData.append('file' ,{
                name: filePath[0]?.name,
                type: filePath[0]?.type,
                uri: Platform.OS === 'ios' ? 
                     filePath[0]?.uri.replace('file://', '')
                     : filePath[0]?.uri,
              })
            }
              formData.append('companyid',tempVar?.companyid)
              formData.append('locationid',location_id)
              formData.append('projectid',tempVar?.projectid)
              formData.append('insertedby',tempVar?.userid)
              formData.append('insertedip',ipAddress)
              formData.append('productid',product_id)
              formData.append('qty',quantity) 
              formData.append('vrate',rate)
    
            console.log('sending data to server' , formData)
      await axios.post(`${baseUrl}/inventory/grnbs/create`,formData,{
            headers: __headers
        }).then((response)=>{
            const {data} = response
            if(data.status===status){
                alertMsg('Purchase Voucher Saved Successfully')
                updateState({
                    voucherDate:'',
                    dueDate:'',
                })
                cancelPurchase()

            }else if(data?.status===__error){
              updateState({sending:false})
              alertMsg(data?.message)
              console.log('error in else if ' , data)
            }else{
                updateState({sending:false})
                alertMsg(SERVER_ERROR)
                console.log('error in else if ' , data)
            }
        }).catch((err)=>{
            updateState({sending:false})
            alertMsg(SERVER_ERROR)
            console.log('error in catch ' , err)
        })

        } else {
            updateState({ errMsg: 'Please fill required fields' })
        }

    }

    function cancelPurchase(){
        updateState({
            sending:false,
            chequeNumber:'',
            // voucherDate:'',
            // dueDate:'',
            voucherNumber:'',
            vendor:'',
            bankAcc:'',
            location:'',
            product:'',
            quantity:'',
            rate:'',
            filePath:'',
            fileType:'',
            errMsg:'',

            chequeNoError:false,
            chequeNoError:false,
            vdateError:false,
            dueDateError:false,
            vendorError:false,
            bankAccError:false,
            locationError:false,
            productError:false,
            qtyError:false,
            rateError:false,

            })
       }



    /*********************** custom dd vendor  *************************/
    const filterVendorData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return v_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return v_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom dd vendor  *************************/
    const filterProductData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return p_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return p_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom dd product  *************************/
    const filteredBankData = (query) => {
    
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return b_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return b_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
/*********************** custom dd bank  *************************/
    const filteredLocationData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return l_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return l_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
/*********************** custom dd location  *************************/ 
    filteredVendor = filterVendorData(searchVendor)
    filteredProduct = filterProductData(searchProduct)
    filteredBankAcc = filteredBankData(searchBank)
    filteredLocation = filteredLocationData(searchLocation)

    return (
        <Container>
            <AppHeader
                rightIconName={'search'}
                rightIconType={'EvilIcons'}
                rightBtnPress={navigateTo}
                headingText={'Purchase'}
                leftBtnPress={openMenu}
                dashbord={false}
                heading={true}
                search={false}
            />
            <View style={[styles.blueBG]}></View>

            <Content style={styles.container}>
                {/* <InputField
                    label="Voucher Number"
                    placeholder={'Voucher Number'}
                    onChangeText={(v) => updateState({
                        voucherNumber: v,
                        vnoError: false,
                        errMsg: '',
                    })}
                    value={voucherNumber}
                    error={vnoError}

                /> */}
                <DatePicker
                    label="Voucher Date"
                    onPress={showCalendar}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={voucherDate}
                    error={vdateError}
                />

                <DropDown
                    label="Vendor"
                    placeholder={'Vendor'}
                    value={vendor}
                    onPress={() => updateState({ is_vendor_dd_Visible: true })}
                    error={vendorError}
                />

                <DropDown
                    label="Bank Account"
                    placeholder={'Bank Account'}
                    value={bankAcc}
                    onPress={() => updateState({ isBank_dd_Visible: true })}
                    error={bankAccError}
                />

                <InputField
                    label="Cheque Number"
                    placeholder={'Cheque Number'}
                    onChangeText={(v) => updateState({
                        chequeNumber: v,
                        chequeNoError: false,
                        errMsg: '',
                    })}
                    value={chequeNumber}
                    error={chequeNoError}
                />
                <DatePicker
                    label="Cheque Date"
                    onPress={showCalendar_due}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={dueDate}
                    error={dueDateError}
                />

                <DropDown
                    label="Location"
                    placeholder={'Location'}
                    value={location}
                    onPress={() => updateState({ is_location_dd_Visible: true })}
                    error={locationError}
                />
                <DropDown
                    label="Product"
                    placeholder={'Product'}
                    value={product}
                    onPress={() => updateState({ is_product_Visible: true })}
                    error={productError}
                />
                <InputField
                    label="Quantity"
                    placeholder={'Quantity'}
                    keyboardType={'number-pad'}
                    onChangeText={(v) => updateState({
                        quantity: v,
                        qtyError: false,
                        errMsg: '',
                    })}
                    value={quantity}
                    error={qtyError}

                />
                <InputField
                    label="Rate"
                    placeholder={'Rate'}
                    keyboardType={'decimal-pad'}
                    onChangeText={(v) => updateState({
                        rate: v,
                        rateError: false,
                        errMsg: ''
                    })}
                    value={rate}
                    error={rateError}
                />


                {errMsg ?
                    <Error
                      error={errMsg}
                    /> : null
                }
                {qtyError ?
                    <Error
                        error={'Quantity shoud be greater than 0'}
                    /> : null}

                <View style={styles.btnView}>
                    <SaveBtn
                        onPress={savePurchases}
                        title={'Save'}
                        saving ={sending}
                    />
                    <CancelBtn
                       onPress={cancelPurchase}
                        title={'Cancel'}
                    />
                    <DocPicker
                        onPress={selectDocument}
                        fileType={fileType}
                        filePath={filePath}
                        cancelFile={cancelDocument}
                    />
                </View>

            </Content>




            <_Calendar
                is_Visible={isCalendar_Visible}
                hideDatePicker={showCalendar}
                onDayPress={onDayPress}
                currentDate={currentDate}
                markedDates={markedDates}
            />
            <_Calendar
                is_Visible={isCalendar_Visible_due}
                hideDatePicker={showCalendar_due}
                onDayPress={onDayPress_due}
                currentDate={c_currentDate}
                markedDates={markedDates_due}
            />
            <DropdownModal
                isVisible={is_vendor_dd_Visible}
                closeFromModal={closeVendorModal}
                data={filteredVendor}
                onChangeText={handleSearchVendor}
                selectValue={selectValue_Vendor}
                isLoading ={isdropdownLoading}
            />

            <DropdownModal
                isVisible={is_location_dd_Visible}
                closeFromModal={closeLocationModal}
                data={filteredLocation}
                onChangeText={handleSearchLocation}
                selectValue={selectValue_location}
                isLoading ={isdropdownLoading}

            />

            <DropdownModal
                isVisible={isBank_dd_Visible}
                closeFromModal={closeBankModal}
                data={filteredBankAcc}
                onChangeText={handleSearchBank}
                selectValue={selectBank}
                isLoading ={isdropdownLoading}

            />
            <DropdownModal
                isVisible={is_product_Visible}
                closeFromModal={closeProdutModal}
                data={filteredProduct}
                onChangeText={handleSearchProduct}
                selectValue={selectValue_product}
                isLoading ={isdropdownLoading}
            />
            <BottomTabs
                navigation={navigation}
                jvPress={()=>navigation.navigate('journalVoucher')}
                purchasePress={()=>navigation.navigate('Purchase')}
                saleInvoicePress={()=>navigation.navigate('saleInvoice')}
                leaveApplyPress={()=>navigation.navigate('leaveApply')}
            />

          <DialogBox ref={dialogboxRef}/>
        </Container>
    )
}

const mapStateToProps = (state) => ({
    user: state.User.userInfo,
})
const mapDispatchToProps = (dispatch) => ({
    saveVendors:(v)=>dispatch(Vendor_dropdowns(v)),
    saveBank:(v)=>dispatch(Bank_dd(v)),
    saveLocation:(v)=>dispatch(Location_dd(v)),
    saveProduct:(v)=>dispatch(Product_dd(v)),
});
export default connect(mapStateToProps,mapDispatchToProps)(Purchase)

