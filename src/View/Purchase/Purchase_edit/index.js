import React, { useEffect, useRef, useReducer } from "react";
import { View, Text, Pressable,PermissionsAndroid,Platform ,Alert} from "react-native";
import { Container, Content, Icon } from "native-base";
import { logo } from "../../../Constants/images";
import InputField from "../../../Components/InputField";
import DatePicker from "../../../Components/DatePicker";
import _Calendar from '../../../Components/Calendar';
import styles from "../styles";
import AppHeader from "../../../Components/AppHeader";
import DialogBox from 'react-native-dialogbox';
import DropDown from "../../../Components/Dropdown";
import DropdownModal from "../../../Components/dropDownModal";
import SaveBtn from "../../../Components/SaveBtn";
import CancelBtn from "../../../Components/CancelBtn";
import DocPicker from "../../../Components/DocPicker";
import DocumentPicker from 'react-native-document-picker'
import Error from "../../../Components/Error";
import { NetworkInfo } from "react-native-network-info";
import { SERVER_ERROR, status, STOCK_OUT, STOCK_OUT_MSG, __error } from "../../../Constants/constValues";
import axios from "axios";
import { baseUrl } from "../../../Constants/server";
import { connect } from "react-redux";
import RNFetchBlob from 'rn-fetch-blob';
import FileViewer from 'react-native-file-viewer';
import RNFS from 'react-native-fs';
import moment from 'moment'




let filteredVendor = ''
let filteredBankAcc = ''
let filteredLocation = ''
let filteredProduct = ''

function Purchase({ navigation, user, route, vendor_dd, bank_dd, loc_dd, product_dd }) {
    let dialogboxRef = useRef()
    let item =  route.params.item

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {

            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.access_token}`
            },
            formData: {
                userid: user?.sessions?.userid,
                companyid: user?.sessions?.companyid,
                locationid: user?.sessions?.locationid,
            },

            voucherNumber:item?.vno,
            vnoError: false,
            isCalendar_Visible: false,
            isCalendar_Visible_due: false,
            fileDonwloading:false,
            ipAddress: '',

            v_dd: [],
            v_dd_List: [],
            vendor_id:item?.vendorid,

            b_dd: [],
            b_dd_List: [],

            p_dd: [],
            p_dd_List: [],
            product_id:item?.productid,

            l_dd: [],
            l_dd_List: [],
            location_id:item?.locationid,

            currentDate: '',
            c_currentDate:'',
            markedDates: {},
            markedDates_due: {},
            voucherDate: '',
            vdateError: false,
            chequeNumber:item?.chequeno,
            chequeNoError: false,
            is_vendor_dd_Visible: false,
            isBank_dd_Visible: false,
            vendor:item?.vendorname,
            vendorError: false,
            searchVendor: '',
            bankAcc:item?.bankaccountname,
            bank_id:item?.bankid,

            bankAccError: false,
            searchBank: '',
            dueDate: '',
            dueDateError: false,
            location:item?.locationname,
            locationError: false,
            product:item?.productname,
            productError: false,
            searchProduct: '',
            searchLocation: '',
            is_location_dd_Visible: false,
            is_product_Visible: false,
            filePath: '',
            fileType: '',
            quantity:Number(item?.qty).toFixed(0),
            qtyError: false,
            rate:item?.vrate,
            rateError: false,
            errMsg: '',
            isdropdownLoading: false,
            sending: false,

            c_startDay:'' , 
            c_endDay:'',
            startDay:'' , 
            endDay:'' 

        }
    )
    const {
        headers,
        formData,
        voucherNumber,
        vnoError,
        currentDate,
        c_currentDate,
        markedDates,
        voucherDate,
        vdateError,
        chequeNumber,
        chequeNoError,
        fileDonwloading,
        v_dd,
        v_dd_List,
        vendor_id,
        ipAddress,

        p_dd,
        p_dd_List,
        product_id,

        b_dd,
        b_dd_List,
        bank_id,

        l_dd,
        l_dd_List,
        location_id,

        is_vendor_dd_Visible,
        isBank_dd_Visible,
        isCalendar_Visible,
        isCalendar_Visible_due,
        vendor,
        vendorError,
        bankAcc,
        bankAccError,
        dueDate,
        dueDateError,
        markedDates_due,
        location,
        locationError,
        is_location_dd_Visible,
        is_product_Visible,
        product,
        productError,
        searchProduct,
        searchBank,
        searchLocation,
        searchVendor,
        fileType,
        filePath,
        quantity,
        qtyError,
        rate,
        rateError,
        errMsg,
        isdropdownLoading,
        sending,

        c_startDay , 
        c_endDay,
        startDay , 
        endDay

    } = state
    // console.log('type of ' , Number(item?.amount).toFixed(0))

    useEffect(() => {

        NetworkInfo.getIPAddress().then(ip => {
            updateState({ ipAddress: ip })
        });

        populateVendor_dd()
        populateBank_dd()
        populateLocation_dd()
        populateProduct_dd()


        let [_d ,_y] = item?.vdate.split(' ')
        let [y,m,d] = _d?.split('-')
        
        let date = `${y}-${m}-${d}`
        let _show = `${d}-${m}-${y}`

        let fullYear = y
        let fullMonth = m -1
        var my_date = new Date(fullYear, fullMonth);

        var first_date = new Date(my_date.getFullYear(), my_date.getMonth(), 1);
        var last_date = new Date(my_date.getFullYear(), my_date.getMonth() +1, 0);

        let _first = moment.utc(first_date).local().format('YYYY-MM-DD HH:mm:ss')
        let [firstDay, _time] = _first.split(' ')

        let _last = moment.utc(last_date).local().format('YYYY-MM-DD HH:mm:ss')
        let [last_day, l_time] = _last.split(' ')


        let [_cd ,_cy] = item?.chequedate.split(' ')
        let [cy,cm,cd] = _cd?.split('-')

        let c_date =`${cy}-${cm}-${cd}`
        let c_show = `${cd}-${cm}-${cy}`

        let c_fullYear = cy
        let c_fullMonth = cm - 1
        var c_my_date = new Date(c_fullYear, c_fullMonth);
        var c_first_date = new Date(c_my_date.getFullYear(), c_my_date.getMonth(), 1);
        var c_last_date = new Date(c_my_date.getFullYear(), c_my_date.getMonth() +1, 0);


        let c_first = moment.utc(c_first_date).local().format('YYYY-MM-DD HH:mm:ss')
        let [c_firstDay, c_time] = c_first.split(' ')

        let c_last = moment.utc(c_last_date).local().format('YYYY-MM-DD HH:mm:ss')
        let [c_last_day, c_l_time] = c_last.split(' ')



        let selected = {}
        selected[date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        let c_selected = {}
        c_selected[c_date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        updateState({
            currentDate: date,
            c_currentDate :c_date,
            voucherDate: _show,
            dueDate: c_show,
            markedDates: selected,
            markedDates_due: c_selected,

            c_startDay:c_firstDay,
            c_endDay:c_last_day,
            startDay:firstDay,
            endDay:last_day
        })

    }, [])

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', async () => {
            populateVendor_dd()
            populateBank_dd()
            populateLocation_dd()
            populateProduct_dd()

            if(Platform.OS=='android'){
                requestStoragePermissions()
         }

        });
        return unsubscribe;
    }, [])


    function alertMsg(msg1, msg2) {
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2],
            btn: {
                text: 'OK',
                callback: () => {navigation.pop()},
            },
        })

    }

    function populateVendor_dd() {
        updateState({ isdropdownLoading: true })

        let ddValues = []
        if (vendor_dd?.length > 0) {
            vendor_dd.map((item, index) => {
                ddValues.push(item.vname)
            })

            updateState({
                isdropdownLoading: false,
                v_dd_List: ddValues,// for filter 
                v_dd: vendor_dd,
            })
        } else {
            updateState({ isdropdownLoading: false })
        }
    }

    function populateBank_dd() {
        updateState({ isdropdownLoading: true })
                let ddValues = []
                if (bank_dd?.length > 0) {
                    bank_dd.map((item, index) => {
                        ddValues.push(item.vname)
                    })

                    updateState({
                        isdropdownLoading: false,
                        b_dd_List: ddValues,// for filter 
                        b_dd:bank_dd,
                    })
                } else {
                    updateState({ isdropdownLoading: false })
                }
            }


    function populateLocation_dd() {
        updateState({ isdropdownLoading: true })

                let ddValues = []
                if (loc_dd?.length > 0) {
                    loc_dd.map((item, index) => {
                        ddValues.push(item.vname)
                    })

                    updateState({
                        isdropdownLoading: false,
                        l_dd_List: ddValues,// for filter 
                        l_dd:loc_dd,
                    })
                } else {
                    updateState({ isdropdownLoading: false })
                }
            }

    function populateProduct_dd() {
        updateState({ isdropdownLoading: true })

                let ddValues = []
                if (product_dd?.length > 0) {
                    product_dd.map((item, index) => {
                        ddValues.push(item.vname)
                    })

                    updateState({
                        isdropdownLoading: false,
                        p_dd_List: ddValues,// for filter 
                        p_dd: product_dd,
                    })
                } else {
                    updateState({ isdropdownLoading: false })
                }
            }



    function goBack() {navigation.pop()}

    async function selectDocument() {
        try {
            const results = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });

            updateState({
                filePath: results,
                fileType: results[0].type
            })
            for (const res of results) {
                console.log(
                    res.uri,
                    res.type, // mime type
                    res.name,
                    res.size,
                )
            }

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                console.log('Document picker cancel')
            } else {
                throw err;
            }
        }
    }

    function cancelDocument() {
        updateState({
            filePath: '',
            fileType: ''
        })
    }

    async function requestStoragePermissions(){

        try {
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
              {
                'title': 'Storage Permission',
                'message': 'SMART FORMS App needs access to your storage to download File.'
              }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
         
            //   Alert.alert("Storage Permission Granted.");
            }
            else {
         
              Alert.alert("Storage Permission Not Granted");
         
            }
          } catch (err) {
            console.warn(err)
          }
        }

function getFile(){
    let tempVar = user?.sessions

    let comp_id =  tempVar?.companyid
    let u_id =  tempVar?.userid
    let proj_id = tempVar?.projectid

    let url = `${baseUrl}/inventory/reports/grnb-document-view/${item.id}?userid=${u_id}&companyid=${comp_id}&projectid=${proj_id}`
    // console.log('url', url,{ headers:headers})
    axios.get(url,{headers:headers}).then((response)=>{
    //   console.log('file response',res)
    const {data} = response
    if(data.status===status){
        requestDownloadFile(data?.url)
        
    }else{
        console.log('error in file loading',data)
    }
    }).catch((err)=>{
        console.log('error while getting file ',  err)
    })
}




    const requestDownloadFile = (file) => {
        updateState({fileDonwloading:true})
        var date = new Date();
        let file_URL = file
        // var file_URL = 'https://github.com/vinzscam/react-native-file-viewer/raw/master/docs/react-native-file-viewer-certificate.pdf';
        var ext = getExtention(file_URL);
        ext = "." + ext[0];
        const { config, fs } = RNFetchBlob;
        let DocumentDir = `${fs.dirs.DownloadDir}`
        console.log('dir name' , DocumentDir)
        let options = {
          fileCache: true,
          addAndroidDownloads: {
            useDownloadManager: true,
            notification: true,
            path: DocumentDir + "/Purchase.pdf" + Math.floor(date.getTime()
              + date.getSeconds() / 2) + ext,
            description: 'PDF File'
          }
        }
        config(options).fetch('GET', file_URL).then((res) => {
            updateState({fileDonwloading:false})
            dialogboxRef.current?.confirm({
                title: 'Success!',
                content: ['File downloaded in Document directory' ,'Open File'],
                ok: {
                    text: 'Yes',
                    style: styles.yesBtnStyle,
                    callback: () => {showDocument(file_URL) },
                },
                cancel: {
                    text: 'No',
                    style: styles.noBtnStyle,
                    callback: () => { },
                },
            });

        });
      }
    
     const getExtention = (filename) => {
        return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) :
          undefined;
      }

      showDocument = (uri,) => {
        const localFile = `${RNFS.DocumentDirectoryPath}/Document.pdf`;
        const options = {
            fromUrl: uri,
            toFile: localFile
        };
        RNFS.downloadFile(options).promise
            .then(() => FileViewer.open(localFile))
            .then((res) => {
                console.log('here is downlaod response', res)
            })
            .catch(error => {
                console.log('here is issue', error)
            });
    }


    function showCalendar() {
        let isVisible = isCalendar_Visible
        updateState({ isCalendar_Visible: !isVisible })
    }

    function showCalendar_due() {
        let isVisible = isCalendar_Visible_due
        updateState({ isCalendar_Visible_due: !isVisible })

    }

    function onDayPress(date) {

        let [y, m, d] = date.dateString.split('-')
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates: selected,
            voucherDate: _show,
            isCalendar_Visible: false,
            vdateError: false,
            errMsg: '',
        })
    }


    function onDayPress_due(date) {

        let [y, m, d] = date.dateString.split('-')
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates_due: selected,
            dueDate: _show,
            isCalendar_Visible_due: false,
            dueDateError: false,
            errMsg: '',
        })
    }


    function selectValue_Vendor(value, index) {
        var FOUND = v_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })
        updateState({
            vendor: value,
            vendor_id: FOUND.id,
            is_vendor_dd_Visible: false,
            searchVendor: '',
            vendorError: false,
            errMsg: '',

        })
    }
    const handleSearchVendor = (value) => {
        updateState({ searchVendor: value })
    }

    function selectValue_location(value, index) {
        var FOUND = l_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })
        updateState({
            location: value,
            location_id: FOUND.id,
            is_location_dd_Visible: false,
            searchLocation: '',
            locationError: false,
            errMsg: '',
        })
    }

    const handleSearchLocation = (value) => {
        updateState({ searchLocation: value })
    }

    function selectValue_product(value, index) {
        var FOUND = p_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })
        updateState({
            product: value,
            product_id: FOUND.id,
            is_product_Visible: false,
            searchProduct: '',
            productError: false,
            errMsg: '',

        })
    }
    const handleSearchProduct = (value) => {
        updateState({ searchProduct: value })
    }


    function selectBank(value, index) {
        var FOUND = b_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })
        updateState({
            bankAcc: value,
            bank_id: FOUND.id,
            isBank_dd_Visible: false,
            searchBank: '',
            bankAccError: false,
            errMsg: '',

        })
    }

    const handleSearchBank = (value) => {
        updateState({ searchBank: value })
    }


    function closeVendorModal() {
        updateState({ is_vendor_dd_Visible: false })
    }
    function closeBankModal() {
        updateState({ isBank_dd_Visible: false })
    }

    function closeLocationModal() {
        updateState({ is_location_dd_Visible: false })
    }

    function closeProdutModal() {
        updateState({ is_product_Visible: false })
    }

  async  function savePurchases() {
        let err = false
        if (!voucherDate) {
            updateState({ vdateError: true, })
            err = true
        }
        if (!vendor) {
            updateState({ vendorError: true, })
            err = true
        }
        if (!bankAcc) {
            updateState({ bankAccError: true, })
            err = true
        }
        if (!chequeNumber) {
            updateState({ chequeNoError: true, })
            err = true
        }
        if (!dueDate) {
            updateState({ dueDateError: true, })
            err = true
        }
        if (!location) {
            updateState({ locationError: true, })
            err = true
        }
        if (!product) {
            updateState({ productError: true, })
            err = true
        }
        if (!quantity || isNaN(quantity) || quantity < 1) {
            updateState({ qtyError: true, })
            err = true
        }
        if (!rate || isNaN(rate)) {
            updateState({ rateError: true, })
            err = true
        }
        if (!err) {
            // api 
            updateState({ sending: true })
            let tempVar = user?.sessions

            const __headers = {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${user?.access_token}`
            }

            let formData = new FormData()
            
              formData.append('vdate',voucherDate)
              formData.append('vendorid',vendor_id)
              formData.append('bankid',bank_id)
              formData.append('chequeno',chequeNumber)
              formData.append('chequedate',dueDate)
              if(filePath){
              formData.append('file' ,{
                name: filePath[0]?.name,
                type: filePath[0]?.type,
                uri: Platform.OS === 'ios' ? 
                     filePath[0]?.uri.replace('file://', '')
                     : filePath[0]?.uri,
              })
            }
              formData.append('companyid',tempVar?.companyid)
              formData.append('locationid',location_id)
              formData.append('projectid',tempVar?.projectid)
              formData.append('insertedby',tempVar?.userid)
              formData.append('insertedip',ipAddress)
              formData.append('productid',product_id)
              formData.append('qty',quantity) 
              formData.append('vrate',rate)

            // let formData = {
            //     vdate: voucherDate,
            //     vendorid: vendor_id,
            //     bankid: bank_id,
            //     chequeno: chequeNumber,
            //     chequedate: dueDate,
            //     companyid: tempVar?.companyid,
            //     locationid: location_id,
            //     projectid: tempVar?.projectid,
            //     insertedby: tempVar?.userid,
            //     insertedip: ipAddress,
            //     productid: product_id,
            //     qty: quantity,
            //     vrate: rate,
            // }
            console.log('sending data to server', formData)
            
         await  axios.post(`${baseUrl}/inventory/grnbs/update/${item.id}`, formData, {
                headers: headers
            }).then((response) => {
                const { data } = response
                if (data.status === status) {
                    alertMsg('Purchase Voucher Updated Successfully')
                    
                } else if (data?.status === __error) {
                    updateState({ sending: false })
                    alertMsg(data?.message)
                    console.log('error in else if ', data)
                } else {
                    updateState({ sending: false })
                    alertMsg(SERVER_ERROR)
                    console.log('error in else if ', data)
                }
            }).catch((err) => {
                updateState({ sending: false })
                alertMsg(SERVER_ERROR)
                console.log('error in catch ', err)
            })

        } else {
            updateState({ errMsg: 'Please fill required fields' })
        }

    }

    function cancelPurchase() {
        navigation.pop()
    }



    /*********************** custom dd vendor  *************************/
    const filterVendorData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return v_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return v_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom dd vendor  *************************/
    const filterProductData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return p_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return p_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom dd product  *************************/
    const filteredBankData = (query) => {

        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return b_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return b_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom dd bank  *************************/
    const filteredLocationData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return l_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return l_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom dd location  *************************/
    filteredVendor = filterVendorData(searchVendor)
    filteredProduct = filterProductData(searchProduct)
    filteredBankAcc = filteredBankData(searchBank)
    filteredLocation = filteredLocationData(searchLocation)

    return (
        <Container>
            <AppHeader
                headingText={'Edit Purchase'}
                leftIconName={'arrow-back'}
                leftIconType={'Ionicons'}
                leftBtnPress={goBack}
                dashbord={false}
                heading={true}
                search={false}
                edit={true}
            />
            <View style={[styles.blueBG]}></View>

            <Content style={styles.container}>
                <InputField
                    label="Voucher Number"
                    placeholder={'Voucher Number'}
                    editable={false}
                    onChangeText={(v) => updateState({
                        voucherNumber: v,
                        vnoError: false,
                        errMsg: '',
                    })}
                    value={voucherNumber}
                    error={vnoError}

                />
                <DatePicker
                    label="Voucher Date"
                    onPress={showCalendar}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={voucherDate}
                    error={vdateError}
                />

                <DropDown
                    label="Vendor"
                    placeholder={'Vendor'}
                    value={vendor}
                    // onPress={() => updateState({ is_vendor_dd_Visible: true })}
                    error={vendorError}
                />

                <DropDown
                    label="Bank Account"
                    placeholder={'Bank Account'}
                    value={bankAcc}
                    // onPress={() => updateState({ isBank_dd_Visible: true })}
                    error={bankAccError}
                />

                <InputField
                    label="Cheque Number"
                    placeholder={'Cheque Number'}
                    onChangeText={(v) => updateState({
                        chequeNumber: v,
                        chequeNoError: false,
                        errMsg: '',
                    })}
                    value={chequeNumber}
                    error={chequeNoError}
                />
                <DatePicker
                    label="Cheque Date"
                    onPress={showCalendar_due}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={dueDate}
                    error={dueDateError}
                />

                <DropDown
                    label="Location"
                    placeholder={'Location'}
                    value={location}
                    // onPress={() => updateState({ is_location_dd_Visible: true })}
                    error={locationError}
                />
                <DropDown
                    label="Product"
                    placeholder={'Product'}
                    value={product}
                    // onPress={() => updateState({ is_product_Visible: true })}
                    error={productError}
                />
                <InputField
                    label="Quantity"
                    placeholder={'Quantity'}
                    keyboardType={'number-pad'}
                    onChangeText={(v) => updateState({
                        quantity: v,
                        qtyError: false,
                        errMsg: '',
                    })}
                    value={quantity}
                    error={qtyError}

                />
                <InputField
                    label="Rate"
                    placeholder={'Rate'}
                    keyboardType={'decimal-pad'}
                    onChangeText={(v) => updateState({
                        rate: v,
                        rateError: false,
                        errMsg: ''
                    })}
                    value={rate}
                    error={rateError}
                />


                {errMsg ?
                    <Error
                        error={errMsg}
                    /> : null
                }

                {qtyError ?
                    <Error
                        error={'Quantity shoud be greater than 0'}
                    /> : null}

                <View style={styles.btnView}>
                    <SaveBtn
                        onPress={savePurchases}
                        title={'Save'}
                        saving={sending}
                    />
                    <CancelBtn
                        onPress={cancelPurchase}
                        title={'Cancel'}
                    />
                    <DocPicker
                        onPress={selectDocument}
                        fileType={fileType}
                        filePath={filePath}
                        cancelFile={cancelDocument}
                    />
                </View>

            <Pressable
                  onPress={getFile}
                  style={styles.pdfBtn}
                  disabled={fileDonwloading}
                >
                <View style={{flexDirection:'row',alignItems:'center'}}>
                    {fileDonwloading?
                    <Text style={styles.titleStyle}>Downloading...</Text>
                    :
                    <Text style={styles.titleStyle}>Download PDF File</Text>
                  }
                  {fileDonwloading? null :
                    <Icon
                    name={'file-check-outline'}
                    type={'MaterialCommunityIcons'}
                    style={{fontSize:20 ,marginLeft:20}}
                    />
                  }
              </View>
          </Pressable>

            </Content>




            <_Calendar
                is_Visible={isCalendar_Visible}
                hideDatePicker={showCalendar}
                onDayPress={onDayPress}
                currentDate={currentDate}
                markedDates={markedDates}
                minDate={startDay}
                maxDate={endDay}
            />
            <_Calendar
                is_Visible={isCalendar_Visible_due}
                hideDatePicker={showCalendar_due}
                onDayPress={onDayPress_due}
                currentDate={c_currentDate}
                markedDates={markedDates_due}
                minDate={c_startDay}
                maxDate={c_endDay}
            />
            <DropdownModal
                isVisible={is_vendor_dd_Visible}
                closeFromModal={closeVendorModal}
                data={filteredVendor}
                onChangeText={handleSearchVendor}
                selectValue={selectValue_Vendor}
            />

            <DropdownModal
                isVisible={is_location_dd_Visible}
                closeFromModal={closeLocationModal}
                data={filteredLocation}
                onChangeText={handleSearchLocation}
                selectValue={selectValue_location}
            />

            <DropdownModal
                isVisible={isBank_dd_Visible}
                closeFromModal={closeBankModal}
                data={filteredBankAcc}
                onChangeText={handleSearchBank}
                selectValue={selectBank}
            />
            <DropdownModal
                isVisible={is_product_Visible}
                closeFromModal={closeProdutModal}
                data={filteredProduct}
                onChangeText={handleSearchProduct}
                selectValue={selectValue_product}
            />

            <DialogBox ref={dialogboxRef} />
        </Container>
    )
}

const mapStateToProps = (state) => ({
    user: state.User.userInfo,
    vendor_dd: state.Dropdown.vendorDDs,
    bank_dd: state.Dropdown.bankDDs,
    loc_dd: state.Dropdown.locationDDs,
    product_dd: state.Dropdown.productDDs,


})
const mapDispatchToProps = (dispatch) => ({

});
export default connect(mapStateToProps, null)(Purchase)

