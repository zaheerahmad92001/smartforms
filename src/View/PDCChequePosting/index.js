import React, { useEffect, useRef, useReducer } from 'react'
import { View, Text } from 'react-native'
import { Container, Content, Icon, Item } from "native-base";
import AppHeader from "../../Components/AppHeader";
import SaveBtn from "../../Components/SaveBtn";
import CancelBtn from "../../Components/CancelBtn";
import styles from './styles'
import DropdownModal from '../../Components/dropDownModal';
import DropDown from '../../Components/Dropdown';
import _Calendar from '../../Components/Calendar';
import DatePicker from "../../Components/DatePicker";
import InputField from '../../Components/InputField';
import DocPicker from "../../Components/DocPicker";
import DocumentPicker from 'react-native-document-picker'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { NetworkInfo } from "react-native-network-info";
import { status } from "../../Constants/constValues";
import axios from "axios";
import { baseUrl } from "../../Constants/server";
import { appColor } from "../../Constants/Colors";
import Error from '../../Components/Error';
import { smallText } from '../../Constants/FontSize';
import PDC_Detail from '../../Components/PDC_Detail';
import { FlatList } from 'react-native-gesture-handler';
import DialogBox from 'react-native-dialogbox';
import { connect } from "react-redux";
import { Keyboard } from 'react-native';

let _data = ['aaaa', 'bbbb', 'cccc', 'dddd', 'eeee','aaaa', 'bbbb', 'cccc', 'dddd', 'eeee']
let filteredVType = ''


function PDCChequPosting({ navigation ,user ,saveEmployee}) {

     let dialogboxRef = useRef()

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.access_token}`
            },

            vType: '',
            vTypeError:'',
            is_vType_Visible: false,
            searchVType:'',
            data:_data,
            markedDates:{},
            postinDate:'',
            currentDate: new Date(),
            isCalendarVisible: false,
            filePath: '',
            fileType: '',
            errMsg: '',

        }
    )

    const {
        headers,

        markedDates,
        searchVType,
        vType, 
        vTypeError,
        is_vType_Visible,
        postinDate,
        data,
        currentDate,
        isCalendarVisible,
        fileType,
        filePath,
        errMsg,

    } = state

    useEffect(() => {

        let d = new Date().getDate()
        let m = new Date().getMonth()+1
        let y = new Date().getFullYear()

        if(d<=9){
          d = '0'+d
        }
        if(m<=9){
            m = '0'+m
        }
        let date =`${y}-${m}-${d}`
        let _show= `${d}-${m}-${y}`

        let selected = {}
        selected[date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        updateState({
            postinDate:_show,
            markedDates:selected,
        })
    }, [])

      function alertMsg(msg1 ,msg2){
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2],
            btn: {
                text: 'OK',
                callback: () => { },
            },
        })
    }

    function openMenu() {
        Keyboard.dismiss()
        setTimeout(()=>{
            navigation.openDrawer();
        },500)
    }

    async function selectDocument() {
        try {
            const results = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });

            updateState({
                filePath: results,
                fileType: results[0].type
            })
            for (const res of results) {
                console.log(
                    res.uri,
                    res.type, // mime type
                    res.name,
                    res.size,
                )
            }

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                console.log('Document picker cancel')
            } else {
                throw err;
            }
        }
    }

    function cancelDocument() {
        updateState({
            filePath: '',
            fileType: ''
        })
    }


    function showCalendar() {
        let isVisible = isCalendarVisible
        updateState({ isCalendarVisible: !isVisible })
    }
    
    function onDayPress(date) {

        let [y,m,d] = date.dateString.split('-')
        let _show= `${d}-${m}-${y}`
        
        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates: selected,
            postinDate:_show,
            isCalendarVisible:false,
        })
    }
    

    

    function closeVTypeModal() {
        updateState({ is_vType_Visible: false })
    }
    const handleVTypeSearch = (value) => {
        updateState({ searchVType: value })
    }
    function selectVType(value, index) {
        updateState({
            vType:value,
            vTypeError: false,
            is_vType_Visible: false,
            searchVType: '',
            errMsg:'',
        })
    }

    

    function saveClosingMonthDetail(){
        let err = false
        if(!vType){
            updateState({vTypeError:true})
            err = true  
        }
    
          if(!err){
              //api 
          }
          else{
            updateState({ errMsg: 'Please fill required fields' })
          }
    }


    const renderItem=({item, index})=>{
        return(
            <PDC_Detail
             item={item}
             index={index}
            />
        )
    }
    /*********************** custom  dd Bank  *************************/
    const filteredVTypeData = (query) => {
        const { data } = state
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return data
            }
            const regex = new RegExp([query.trim()], 'i')
            return data.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }

/*********************** custom dd vendor  *************************/
    filteredVType  = filteredVTypeData(searchVType)

    return(
        <Container>
            <AppHeader
                rightIconName={'search'}
                rightIconType={'EvilIcons'}
                // rightBtnPress={navigateTo}
                headingText={'PDC Cheque Posting'}
                leftBtnPress={openMenu}
                dashbord={false}
                heading={true}
                search={false}
            />
            <View style={[styles.blueBG]}></View>
            <View style={styles.container}>
                 <DatePicker
                     label="Posting Date"
                     onPress={showCalendar}
                     iconName={'calendar-month-outline'}
                     iconType={'MaterialCommunityIcons'}
                     date={postinDate}
                    />
                <DropDown
                    label="VType"
                    placeholder={'VType'}
                    value={vType}
                    onPress={() => updateState({is_vType_Visible: true })}
                    error={vTypeError}
                />

                <SaveBtn
                    title={'Fetch Record'}
                    buttonContainer={{ top:5, width: wp(30), alignSelf:'flex-end' }}
                    textStyle={{ fontSize: smallText }}
                    />
               
                
                <FlatList
                 data={data}
                 keyExtractor={(item)=>item.id}
                 renderItem={renderItem}
                 style={{marginTop:10,}}
                />
               
                {errMsg ?
                    <Error
                      error={errMsg}
                    /> : null
                }
                
            </View>
            <View style={styles.btnView}>
                    <SaveBtn
                       onPress={saveClosingMonthDetail}
                        title={'Save'}
                    />
                    <CancelBtn
                        title={'Cancel'}
                    />
                    <DocPicker
                        onPress={selectDocument}
                        fileType={fileType}
                        filePath={filePath}
                        cancelFile={cancelDocument}
                    />
                </View>
           
            <_Calendar
                is_Visible={isCalendarVisible}
                hideDatePicker={showCalendar}
                onDayPress={onDayPress}
                currentDate={currentDate}
                markedDates={markedDates}
            />
           
            <DropdownModal
                isVisible={is_vType_Visible}
                closeFromModal={closeVTypeModal}
                data={filteredVType}
                onChangeText={handleVTypeSearch}
                selectValue={selectVType}
            />
        </Container>
    )
}

const mapStateToProps = (state) => ({
    user: state.User.userInfo,
})
const mapDispatchToProps = (dispatch) => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(PDCChequPosting)

