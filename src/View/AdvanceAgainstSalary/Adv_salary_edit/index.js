import React, { useEffect, useRef, useReducer } from 'react'
import { View, Text } from 'react-native'
import { Container, Content, Icon, Item } from "native-base";
import AppHeader from "../../../Components/AppHeader";
import SaveBtn from "../../../Components/SaveBtn";
import CancelBtn from "../../../Components/CancelBtn";
import styles from '../styles'
import DropdownModal from '../../../Components/dropDownModal';
import DropDown from '../../../Components/Dropdown';
import _Calendar from '../../../Components/Calendar';
import DatePicker from "../../../Components/DatePicker";
import InputField from '../../../Components/InputField';
import DocPicker from "../../../Components/DocPicker";
import DocumentPicker from 'react-native-document-picker'
import DialogBox from 'react-native-dialogbox';
import { NetworkInfo } from "react-native-network-info";
import { SERVER_ERROR, status, __error } from "../../../Constants/constValues";
import axios from "axios";
import { Employee_dd, Bank_dd } from '../../../redux/actions/Dropdowns'
import { baseUrl } from "../../../Constants/server";
import Error from '../../../Components/Error';
import { connect } from "react-redux";
import moment from 'moment'


let filteredBankAcc = ''
let filteredEmployee = ''


function AdvanceSalary_edit({ navigation, user,route, emp_dd, bank_dd }) {
   
    let item = route.params.item
    let dialogboxRef = useRef()

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.access_token}`
            },
            showEmployeeCodeList: false,
            isCalendar_Visible_cheque: false,
            showBankList: false,
            voucherNumber:item?.vno,
            bankAccError: false,
            searchBank: '',
            bankAcc:item?.bankname,
            chequeNumber:item?.chequeno,
            chequeNoError,
            markedDates: {},
            markedDates_cheque: {},
            chequeDate: '',
            c_currentDate: '',
            markedDatesTo: {},

            emp_id:item?.employeeid,
            emp_name:item?.employeename,
            employee_list: [],
            employee_dd: [],

            b_dd: [],
            b_dd_List: [],
            bank_id:item?.accountid,

            employeeCode: '',
            employeeCodeError: false,
            searchEmployeeCode: '',
            amount:item?.amount,
            amountError: false,
            currentDate: new Date(),
            entryDate: '',
            isCalendarVisible: false,
            filePath: '',
            fileType: '',
            errMsg: '',
            sending: false,
            ipAddress: '',
            isdropdownLoading: false,
            last_date: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0),

            c_startDay:'' , 
            c_endDay:'',
            startDay:'' , 
            endDay:''

        }
    )

    const {
        headers,
        markedDates,
        showBankList,
        searchBank,
        showEmployeeCodeList,
        isCalendar_Visible_cheque,
        voucherNumber,

        emp_id,
        emp_name,
        employee_list,
        employee_dd,

        b_dd,
        b_dd_List,
        bank_id,

        employeeCode,
        employeeCodeError,
        searchEmployeeCode,
        bankAcc,
        bankAccError,
        chequeNumber,
        chequeNoError,
        chequeDate,
        c_currentDate,
        markedDates_cheque,
        amount,
        amountError,
        currentDate,
        entryDate,
        isCalendarVisible,
        fileType,
        filePath,
        errMsg,
        sending,
        ipAddress,
        last_date,
        isdropdownLoading,
        c_startDay , 
        c_endDay,
        startDay , 
        endDay

    } = state

    useEffect(() => {

        NetworkInfo.getIPAddress().then(ip => {
            updateState({ ipAddress: ip })
        });

        load_Employee()
        populateBank_dd()


        let [_d,_t] = item?.chequedate.split(' ')
        let [y,m,d] = _d?.split('-')
 
        let c_date = `${y}-${m}-${d}`
        let c_show = `${d}-${m}-${y}`

        let c_fullYear = y
        let c_fullMonth = m - 1
        var c_my_date = new Date(c_fullYear, c_fullMonth);
        var c_first_date = new Date(c_my_date.getFullYear(), c_my_date.getMonth(), 1);
        var c_last_date = new Date(c_my_date.getFullYear(), c_my_date.getMonth() +1, 0);


        let c_firstDay = moment.utc(c_first_date).local().format('YYYY-MM-DD')
        let c_last_day = moment.utc(c_last_date).local().format('YYYY-MM-DD')




        let [e_d,e_t] = item?.posteddate.split(' ')
        let [e_y,e_m,e_dd] = e_d?.split('-')
 
        let e_date = `${e_y}-${e_m}-${e_dd}`
        let e_show = `${e_dd}-${e_m}-${e_y}`

        let fullYear = e_y
        let fullMonth = e_m -1
        var my_date = new Date(fullYear, fullMonth);

        var first_date = new Date(my_date.getFullYear(), my_date.getMonth(), 1);
        var last_date = new Date(my_date.getFullYear(), my_date.getMonth() +1, 0);

        let firstDay = moment.utc(first_date).local().format('YYYY-MM-DD')
        let last_day = moment.utc(last_date).local().format('YYYY-MM-DD')
        

        let c_selected = {}
       c_selected[c_date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }

        let e_selected = {}
       e_selected[e_date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }


        updateState({
            entryDate: e_show,
            chequeDate: c_show,
            markedDates: e_selected,
            currentDate: e_date,
            c_currentDate: c_date,
            markedDates_cheque: c_selected,

            c_startDay:c_firstDay,
            c_endDay:c_last_day,
            startDay:firstDay,
            endDay:last_day
        })
    }, [])

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', async () => {
            load_Employee()
            populateBank_dd()

        });
        return unsubscribe;
    }, [])

    function alertMsg(msg1, msg2) {
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2],
            btn: {
                text: 'OK',
                callback: () => { },
            },
        })

    }

    function load_Employee() {
        updateState({ isdropdownLoading: true })

            if (emp_dd?.length > 0) {
                let ddValue = []
                emp_dd.map((item, index) => {
                    ddValue.push(item.employeename)
                })
                updateState({
                    employee_list: ddValue,
                    employee_dd: emp_dd
                })
            }
    }

    function populateBank_dd() {                
                let ddValues = []
                if (bank_dd?.length > 0) {
                    bank_dd.map((item, index) => {
                        ddValues.push(item.vname)
                    })

                    updateState({
                        isdropdownLoading: false,
                        b_dd_List: ddValues,// for filter 
                        b_dd:bank_dd,
                    })
                }
            }  

    function goBack() {navigation.pop()}


    async function selectDocument() {
        try {
            const results = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });

            updateState({
                filePath: results,
                fileType: results[0].type
            })
            for (const res of results) {
                console.log(
                    res.uri,
                    res.type, // mime type
                    res.name,
                    res.size,
                )
            }

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                console.log('Document picker cancel')
            } else {
                throw err;
            }
        }
    }

    function cancelDocument() {
        updateState({
            filePath: '',
            fileType: ''
        })
    }

    function closeEmployeeCodeList() {
        updateState({ showEmployeeCodeList: false })
    }
    const handleSearchEmployeeCode = (value) => {
        updateState({ searchEmployeeCode: value })
    }
    function selectCode(value, index) {
        var FOUND = employee_dd.find(function (post, index) {
            if (post.employeename == value) {
                return post;
            }
        })
        updateState({
            emp_id: FOUND.id,
            emp_name: value,
            errMsg: '',
            employeeCodeError: false,
            showEmployeeCodeList: false,
            searchEmployeeCode: ''
        })
    }


    function showCalendar() {
        let isVisible = isCalendarVisible
        updateState({ isCalendarVisible: !isVisible })
    }

    function onDayPress(date) {

        let [y, m, d] = date.dateString.split('-')
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            // chequeDate:_show,
            entryDate: _show,
            markedDates: selected,
            currentDate: date.dateString,
            isCalendarVisible: false,
        })
    }
    function closeBankModal() {
        updateState({ showBankList: false })
    }
    const handleSearchBank = (value) => {
        updateState({ searchBank: value })
    }

    function selectBank(value, index) {
        var FOUND = b_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })
        updateState({
            bankAcc: value,
            bank_id: FOUND.id,
            showBankList: false,
            searchBank: '',
            bankAccError: false,
            errMsg: '',

        })
    }

    function showCalendar_cheque() {
        let isVisible = isCalendar_Visible_cheque
        updateState({ isCalendar_Visible_cheque: !isVisible })

    }
    function onDayPress_cheque(date) {

        let [y, m, d] = date.dateString.split('-')
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates_cheque: selected,
            chequeDate: _show,
            isCalendar_Visible_cheque: false,
            errMsg: '',
            c_currentDate: date.dateString
        })
    }

   async function saveAdvanceSalary() {
        let err = false
        if (!emp_name) {
            updateState({ employeeCodeError: true })
            err = true
        }
        if (!bankAcc) {
            updateState({ bankAccError: true })
            err = true
        }
        if (!chequeNumber) {
            updateState({ chequeNoError: true })
            err = true
        }
        if (!amount || isNaN(amount) || amount < 1) {
            updateState({ amountError: true })
            err = true
        }
        if (!err) {
            //api 
            const __headers = {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${user?.access_token}`
            } 
            updateState({ sending: true })
            let tempVar = user?.sessions

            let formData =  new FormData()
                formData.append('employeeid', 1)
                formData.append('amount', amount)
                formData.append('accountid',bank_id)
                formData.append('chequeno', chequeNumber)
                formData.append('chequedate', chequeDate)
                formData.append('datein', entryDate)
                formData.append('companyid', tempVar?.companyid)
                formData.append('locationid',tempVar.locationid)
                formData.append('insertedby',tempVar?.userid)
                formData.append('updatedby',tempVar?.userid)
                formData.append('insertedip', ipAddress)
                formData.append('updatedip',ipAddress)
                if(filePath){
                formData.append('file' ,{
                    name: filePath[0]?.name,
                    type: filePath[0]?.type,
                    uri: Platform.OS === 'ios' ? 
                         filePath[0]?.uri.replace('file://', '')
                         : filePath[0]?.uri,
                  })
                }

            // let formData = {
            //     allowdedid: 1,
            //     employeeid:emp_id,
            //     refno:'',
            //     qty: 1,
            //     amount: amount,
            //     accountid:bank_id,
            //     chequeno: chequeNumber,
            //     chequedate: chequeDate,
            //     datein: entryDate,
            //     remarks: 'testing',
            //     companyid: tempVar?.companyid,
            //     locationid:tempVar.locationid,
            //     insertedby: tempVar?.userid,
            //     updatedby: tempVar?.userid,
            //     insertedip: ipAddress,
            //     updatedip: ipAddress,
            // }

            console.log('sending data to server', formData)
           await axios.post(`${baseUrl}/attendance/sal-advance/update/${item?.id}`, formData, {
                headers: __headers
            }).then((response) => {
                const { data } = response
                console.log('advance salary response', data)
                if (data.status === status) {
                    alertMsg('Success!')
                    updateState({sending: false,})
                } else if (data?.status === __error) {
                    updateState({ sending: false })
                    alertMsg(data?.message)
                    console.log('error in else if ', data)
                } else {
                    updateState({ sending: false })
                    alertMsg(SERVER_ERROR)
                    console.log('error in else if ', data)
                }
            }).catch((err) => {
                updateState({ sending: false })
                alertMsg(SERVER_ERROR)
                console.log('error in catch ', err)
            })

        }
        else {
            updateState({ errMsg: 'Please fill required fields' })
        }
    }

   function cancelAdvance(){
       navigation.pop()
       updateState({sending:false,})
   }

    /*********************** custom  dd Bank  *************************/
    const filteredBankData = (query) => {

        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return b_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return b_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }


    /*********************** custom dd vendor  *************************/
    const filterEmployeeData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return employee_list
            }
            const regex = new RegExp([query.trim()], 'i')
            return employee_list.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom dd vendor  *************************/
    filteredBankAcc = filteredBankData(searchBank)
    filteredEmployee = filterEmployeeData(searchEmployeeCode)


    return (
        <Container>
            <AppHeader
                headingText={'Edit Advances'}
                leftIconName={'arrow-back'}
                leftIconType={'Ionicons'}
                leftBtnPress={goBack}
                dashbord={false}
                heading={true}
                search={false}
                edit={true}
            />
            <View style={[styles.blueBG]}></View>
            <Content style={styles.container}>
            <InputField
                    label="Voucher Number"
                    placeholder={'Voucher Number'}
                    editable={false}
                    onChangeText={(v) => updateState({ 
                        voucherNumber: v,
                        vnoError:false,
                        errMsg:'',
                     })}
                    value={voucherNumber}
                    // error={vnoError}
                />
                <DropDown
                    label="Employee"
                    placeholder={'Employee'}
                    value={emp_name}
                    onPress={() => updateState({ showEmployeeCodeList: true })}
                    error={employeeCodeError}
                />
                <DatePicker
                    label="Entry Date"
                    onPress={showCalendar}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={entryDate}
                />
                <DropDown
                    label="Bank"
                    placeholder={'Bank'}
                    value={bankAcc}
                    onPress={() => updateState({ showBankList: true })}
                    error={bankAccError}
                />
                <InputField
                    label="Cheque Number"
                    placeholder={'Cheque Number'}
                    onChangeText={(v) => updateState({
                        chequeNumber: v,
                        chequeNoError: false,
                        errMsg: '',
                    })}
                    value={chequeNumber}
                    error={chequeNoError}
                />
                <DatePicker
                    label="Cheque Date"
                    onPress={showCalendar_cheque}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={chequeDate}
                />
                <InputField
                    label="Amount"
                    placeholder={'Amount'}
                    keyboardType={'decimal-pad'}
                    onChangeText={(v) => updateState({
                        amount: v,
                        amountError: false,
                        errMsg: '',
                    })}
                    value={amount}
                    error={amountError}
                />
                {errMsg ?
                    <Error
                        error={errMsg}
                    /> : null
                }
                <View style={styles.btnView}>
                    <SaveBtn
                        onPress={saveAdvanceSalary}
                        title={'Save'}
                        saving={sending}
                    />
                    <CancelBtn
                        title={'Cancel'}
                        onPress={cancelAdvance}
                    />
                    <DocPicker
                        onPress={selectDocument}
                        fileType={fileType}
                        filePath={filePath}
                        cancelFile={cancelDocument}
                    />
                </View>
            </Content>

            <DropdownModal
                isVisible={showEmployeeCodeList}
                closeFromModal={closeEmployeeCodeList}
                data={filteredEmployee}
                onChangeText={handleSearchEmployeeCode}
                selectValue={selectCode}
            />
            <_Calendar
                is_Visible={isCalendarVisible}
                hideDatePicker={showCalendar}
                onDayPress={onDayPress}
                currentDate={currentDate}
                markedDates={markedDates}
                maxDate={last_date}
                minDate={startDay}
                maxDate={endDay}

            />
            <_Calendar
                is_Visible={isCalendar_Visible_cheque}
                hideDatePicker={showCalendar_cheque}
                onDayPress={onDayPress_cheque}
                currentDate={c_currentDate}
                markedDates={markedDates_cheque}
                maxDate={last_date}
                minDate={c_startDay}
                maxDate={c_endDay}
            />

            <DropdownModal
                isVisible={showBankList}
                closeFromModal={closeBankModal}
                data={filteredBankAcc}
                onChangeText={handleSearchBank}
                selectValue={selectBank}
            />
            <DialogBox ref={dialogboxRef} />
        </Container>
    )
}

const mapStateToProps = (state) => ({
    user: state.User.userInfo,
    emp_dd: state.Dropdown.employeeDDs,
    bank_dd: state.Dropdown.bankDDs,
})
const mapDispatchToProps = (dispatch) => ({
    // saveEmployee: (d) => dispatch(Employee_dd(d)),
    // saveBank: (v) => dispatch(Bank_dd(v)),

});

export default connect(mapStateToProps, mapDispatchToProps)(AdvanceSalary_edit)

