import { Platform } from 'react-native';
import { StyleSheet } from 'react-native'
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { appColor, black, danger, darkGrey, lightYellow, white } from "../../Constants/Colors";
import { LatoBlack, LatoRegular } from "../../Constants/Fonts";
import { largeText, mediumText } from "../../Constants/FontSize";

export default styles = StyleSheet.create({
    container: {
        marginHorizontal: 15,
        paddingTop: 15,
    },
    blueBG: {
        backgroundColor: appColor,
        height: Platform.OS == 'android' ? hp(4) : hp(4),
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
    },
    category: {
        color: darkGrey,
        fontFamily: LatoBlack,
        fontSize: mediumText,

    },
    btnView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 20,
        marginBottom: 30,
    },
    dateView:{
        flexDirection:'row',
        justifyContent:'space-between',
        // justifyContent:'center',
        // alignItems:'center',
        flex:1
    },
    currentDateContainer:{
        backgroundColor:white,
        borderRadius:5,
        borderColor:'#007BFF',
        borderWidth:1
      },
      currentDateText: {
        color:appColor,
        fontWeight: '500'
      },
      dateText:{
        color:white,
        fontWeight: '500'
      },
      dateContainer:{
        backgroundColor:appColor,
        borderRadius:5,
      },

})