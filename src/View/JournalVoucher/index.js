import React, { useEffect, useRef, useReducer } from "react";
import { View,Platform,Keyboard} from "react-native";
import { Container, Content, Icon } from "native-base";
import InputField from "../../Components/InputField";
import DatePicker from "../../Components/DatePicker";
import _Calendar from '../../Components/Calendar';
import styles from "./styles";
import AppHeader from "../../Components/AppHeader";
import DropDown from "../../Components/Dropdown";
import DropdownModal from "../../Components/dropDownModal";
import { connect } from 'react-redux'
import{Jv_To_dropdown}from '../../redux/actions/Dropdowns'
import BottomTabs from "../../Components/BottomTab";
import SaveBtn from "../../Components/SaveBtn";
import CancelBtn from "../../Components/CancelBtn";
import DocPicker from "../../Components/DocPicker";
import DocumentPicker from 'react-native-document-picker'
import DialogBox from 'react-native-dialogbox';
import Error from "../../Components/Error";
import { NetworkInfo } from "react-native-network-info";
import { INVALID_VALUE, SERVER_ERROR, status } from "../../Constants/constValues";
import axios from "axios";
import { baseUrl } from "../../Constants/server";


let filteredFrom = ''
let filteredTo = ''


function JournalVoucher({ navigation ,user , saveDropdown,saveJVdd }) {

    let dialogboxRef = useRef()

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            voucherNumber: '',
            vnoError: false,

            isCalendar_Visible: false,
            isChequeCalendar_Visible:false,
            currentDate: '',
            cheq_currentDate:'',
            markedDates: {},
            markedDates_cheque: {},
            voucherDate: '',
            chequeDate:'',
            c_date_err:false,
            dropdownList:[],
            dropdowns:[],
            toDropdown:[],
            toDropdownList:[],
            vdateError: false,
            chequeNumber: '',
            chequeNoError: false,
            is_from_dd_Visible: false,
            value_from: '',
            from_id:'',
            vfromError: false,
            searchV_from: '',
            vtoError: false,
            value_to: '',
            to_id:'',
            searchV_to: '',
            is_to_dd_Visible: false,
            amount: '',
            amountError: false,
            filePath: '',
            fileType: '',
            errMsg: '',
            isdropdownLoading : false,
            ipAddress:'',
            sending:false,

        }
    )
    const {
        voucherNumber,
        vnoError,
        isCalendar_Visible,
        isChequeCalendar_Visible,
        currentDate,
        cheq_currentDate,
        markedDates,
        markedDates_cheque,
        voucherDate,
        vdateError,
        c_date_err,
        chequeNumber,
        chequeDate,
        chequeNoError,
        is_from_dd_Visible,
        dropdownList,
        dropdowns,
        toDropdownList,
        toDropdown,
        value_from,
        from_id,
        vfromError,
        searchV_from,
        value_to,
        to_id,
        vtoError,
        searchV_to,
        is_to_dd_Visible,
        amount,
        amountError,
        fileType,
        filePath,
        errMsg,
        ipAddress,
        sending,
        isdropdownLoading,

    } = state

    useEffect(() => {

        NetworkInfo.getIPAddress().then(ip => {
            updateState({ipAddress:ip})
          });

        populateToDropdown()

        let d = new Date().getDate()
        let m = new Date().getMonth()+1
        let y = new Date().getFullYear()

        if(d<=9){
          d = '0'+d
        }
        if(m<=9){
            m = '0'+m
        }
        let date =`${y}-${m}-${d}`
        let _show= `${d}-${m}-${y}`

        let selected = {}
        selected[date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        updateState({
            currentDate: date,
            cheq_currentDate:date,
            voucherDate:_show,
            chequeDate:_show,
            markedDates: selected,
            markedDates_cheque:selected,
        })

        const unsubscribe = navigation.addListener('focus', async () => {
            populateToDropdown()
            cancelJournalVoucher()
        });
     return unsubscribe;
    }, [])


    
    // function populateDropdown() {
    //     updateState({ isdropdownLoading: true })
    //     const headers = {
    //         'Content-Type': 'application/json',
    //         'Authorization': `Bearer ${user?.access_token}`
    //     }

    //     let tempVar = user?.sessions

    //     let formData = {
    //         userid: tempVar?.userid,
    //         companyid: tempVar?.companyid,
    //         locationid: tempVar?.locationid,
    //         accountgroupcode: 'B'
    //     }

    //     axios.post(`${baseUrl}/account/group-details`, formData, {
    //         headers: headers
    //     }).then((res) => {
    //         const { data } = res
    //         if (data.status === status) {
    //             /*********** dd Redux **********/
    //             saveDropdown(data.group_details)

    //             let ddValues = []
    //             if (data.group_details?.length > 0) {
    //                 data.group_details.map((item, index) => {
    //                     ddValues.push(item.vname)
    //                 })

    //                 updateState({
    //                     isdropdownLoading: false,
    //                     dropdownList: ddValues,// for filter 
    //                     dropdowns: data.group_details,
    //                 })
    //             } else {
    //                 updateState({ isdropdownLoading: false })
    //             }
    //         } else {
    //             alertMsg(data.message)
    //         }

    //         // console.log('dropdown response' ,data)
    //     })
    //     /******** From dropdown logic end  *****/
    // }


   function populateToDropdown(){
    updateState({isdropdownLoading:true})
    let tempVar =user?.sessions 
    const headers = {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${user?.access_token}`
        }

    let formData = {
        userid:tempVar?.userid,
        companyid:tempVar?.companyid,
        locationid:tempVar?.locationid,
        islevelnbr:1
        }

    axios.post(`${baseUrl}/account/coa`,formData,{
        headers: headers
        }).then((response)=>{
          const {data} = response
          if(data.status===status){
              let dd = []
              data?.coa.map((item,index)=>{
                dd.push(item.vname)
              })
        /*********** Redux  *****/
           saveJVdd(data.coa)
          
              updateState({
                  toDropdownList:dd,
                  toDropdown:data.coa,
                  isdropdownLoading:false
                })
              
          }else{
            alertMsg(data?.message)
          }

        }).catch((err)=>{
            alertMsg(SERVER_ERROR)
        })
   }
   function alertMsg(msg1 ,msg2){
    dialogboxRef.current.tip({
        title: 'Message',
        content: [
            msg1,
            msg2],
        btn: {
            text: 'OK',
            callback: () => { },
        },
    })
}


    // function goBack() { navigation.pop() }

    function openMenu() {
        Keyboard.dismiss()
        setTimeout(()=>{
            navigation.openDrawer();
        },500)
    }

    async function selectDocument() {
        try {
            const results = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });

            updateState({
                filePath: results,
                fileType: results[0].type
            })
            for (const res of results) {
                console.log(
                    res.uri,
                    res.type, // mime type
                    res.name,
                    res.size,
                )
            }

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                console.log('Document picker cancel')
            } else {
                throw err;
            }
        }
    }

    function cancelDocument() {
        updateState({
            filePath: '',
            fileType: ''
        })
    }

    function showCalendar() {
        let isVisible = isCalendar_Visible
        updateState({ isCalendar_Visible: !isVisible })
    }

    function onDayPress(date) {
        let [y,m,d] = date.dateString.split('-')
        let _show= `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates: selected,
            currentDate:date.dateString,
            voucherDate:_show,
            isCalendar_Visible:false,
            vdateError: false,
            errMsg: '',
        })
    }


    function show_chequeDate_calendar() {
        let isVisible = isChequeCalendar_Visible
        updateState({ isChequeCalendar_Visible: !isVisible })
    }

    function onDayPress_chequeDate(date) {
        let [y,m,d] = date.dateString.split('-')
        let _show= `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates_cheque: selected,
            cheq_currentDate:date.dateString,
            chequeDate:_show,
            c_date_err:false,
            errMsg: '',
            isChequeCalendar_Visible:false,
        })
    }



    function closeFromModal() {
        updateState({ is_from_dd_Visible: false })
    }

    function selectValue_from(value, index) {
            var FOUND = toDropdown.find(function (post, index) {
            if (post.vname == value){
                updateState({from_id: post.id})
            }
          })

        updateState({
            value_from: value,
            is_from_dd_Visible: false,
            searchV_from: '',
            vfromError: false,
            errMsg: '',
        })
    }

    const handleSearchValue_from = (value) => {
        updateState({ searchV_from: value })
    }

    function selectValue_to(value, index) {
        var FOUND = toDropdown.find(function (post, index) {
            if (post.vname == value){
                updateState({to_id: post.id})
            }
          })

        updateState({
            value_to: value,
            is_to_dd_Visible: false,
            searchV_to: '',
            vtoError: false,
            errMsg: '',
        })
    }

    const handleSearchValue_to = (value) => { updateState({ searchV_to: value })}

    function closeToModal() { updateState({ is_to_dd_Visible: false })}

    function navigateTo() {navigation.navigate('Jv_search')}

   async function saveJournalVoucher() {
        let err = false
        let _equal = ''
        if (value_from) {
            _equal = value_from.localeCompare(value_to);
        }
        if (!voucherDate) {
            updateState({ vdateError: true, })
            err = true
        }
        if(!chequeDate){
            updateState({c_date_err:true})
            err = true
        }
        if (!chequeNumber) {
            updateState({ chequeNoError: true, })
            err = true
        }
        if (!value_from) {
            updateState({ vfromError: true, })
            err = true
        }
        if (!value_to) {
            updateState({ vtoError: true })
            err = true
        }

        if (_equal === 0) {
            updateState({ vtoError: true, vfromError: true })
            alertMsg(INVALID_VALUE)
            err = true
        }

        if (!amount || isNaN(amount) || amount < 1) {
            updateState({ amountError: true })
            err = true
        }
        if (!err) {
         updateState({sending:true})
        /******* API call  */ 
        let tempVar =user?.sessions 

        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'multipart/form-data',
            'Authorization': `Bearer ${user?.access_token}`
        }

           let formData = new FormData()

           formData.append('vdate',voucherDate)
           formData.append('chequeno',chequeNumber)
           formData.append('chequedate',chequeDate)
           formData.append('draccountid',from_id)
           formData.append('craccountid',to_id)
           formData.append('amount',amount)
           if(filePath){
           formData.append('file' ,{
            name: filePath[0]?.name,
            type: filePath[0]?.type,
            uri: Platform.OS === 'ios' ? 
                 filePath[0]?.uri.replace('file://', '')
                 : filePath[0]?.uri,
          })
        }
           formData.append('companyid',tempVar?.companyid)
           formData.append('locationid',tempVar?.locationid)
           formData.append('projectid',tempVar?.projectid)
           formData.append('insertedby',tempVar?.userid)
           formData.append('insertedip',ipAddress)

       await axios.post(`${baseUrl}/account/voucher-main-jv/create`,formData,{
            headers: headers
          }).then((response)=>{
            const {data} = response
            if(data.status===status){
                console.log('jv inserted ' , data)
                alertMsg('Journal Voucher Saved Successfully')

                 updateState({ 
                 voucherDate:'',
                 chequeDate:'',
                })
                 cancelJournalVoucher()

            }else{
                updateState({sending:false})
                console.log('error in jv ' , data )
                alertMsg(data?.message)
            }
          }).catch((error)=>{
            updateState({sending:false})
            console.log('error in jv ' , error )
            alertMsg(SERVER_ERROR)
          })

        
        } else {
            updateState({ errMsg: 'Please fill required fields' })
        }
    }

    function cancelJournalVoucher(){
    updateState({
        sending: false,
        voucherNumber: '',
        // voucherDate:'',
        // chequeDate:'',
        chequeNumber: '',
        value_from: '',
        from_id: '',
        value_to: '',
        to_id: '',
        amount: '',
        filePath:'',
        fileType:'',

        vdateError:false,
        c_date_err:false,
        vfromError:false,
        vtoError:false,
        amountError:false,
        chequeNoError:false,
    })
}


    /*********************** custom  dd from  *************************/
    const filterFromData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return toDropdownList
                // return dropdownList
            }
            const regex = new RegExp([query.trim()], 'i')
            return toDropdownList.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }


    /*********************** custom  dd To  *************************/
    const filter_To_Data = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return toDropdownList
            }
            const regex = new RegExp([query.trim()], 'i')
            return toDropdownList.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom dd To  *************************/
    filteredFrom = filterFromData(searchV_from)
    filteredTo = filter_To_Data(searchV_to)


    return (
        <Container>
            <AppHeader
                rightIconName={'search'}
                rightIconType={'EvilIcons'}
                rightBtnPress={navigateTo}
                headingText={'Journal Voucher'}
                leftBtnPress={openMenu}
                dashbord={false}
                heading={true}
                search={false}
            />
            <View style={[styles.blueBG]}></View>

            <Content style={styles.container}>
                {/* <InputField
                    label="Voucher Number"
                    placeholder={'Voucher Number'}
                    onChangeText={(v) => updateState({
                        voucherNumber: v,
                        vnoError: false,
                        errMsg: '',
                    })}
                    value={voucherNumber}
                    error={vnoError}
                /> */}
                <DatePicker
                    label="Voucher Date"
                    onPress={showCalendar}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={voucherDate}
                    error={vdateError}
                />
                <InputField
                    label="Cheque Number"
                    placeholder={'Cheque Number'}
                    onChangeText={(v) => updateState({
                        chequeNumber: v,
                        chequeNoError: '',
                        errMsg: '',
                    })}
                    value={chequeNumber}
                    error={chequeNoError}
                />
                <DatePicker
                    label="Cheque Date"
                    onPress={show_chequeDate_calendar}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={chequeDate}
                    error={c_date_err}
                />

                <DropDown
                    label="From"
                    placeholder={'From'}
                    value={value_from}
                    onPress={() => updateState({ is_from_dd_Visible: true })}
                    error={vfromError}
                />
                <DropDown
                    label="To"
                    placeholder={'To'}
                    value={value_to}
                    onPress={() => updateState({ is_to_dd_Visible: true })}
                    error={vtoError}
                />
                <InputField
                    label="Amount"
                    placeholder={'amount'}
                    keyboardType={'decimal-pad'}
                    onChangeText={(v) => updateState({
                        amount: v,
                        amountError: false,
                        errMsg: '',
                    })}
                    value={amount}
                    error={amountError}
                />

                {errMsg ?
                    <Error
                      error={errMsg}
                    /> : null}

               {amountError ?
                    <Error
                        error={'Amount shoud be greater than 0'}
                    /> : null}


                <View style={styles.btnView}>
                    
                    <SaveBtn
                        onPress={saveJournalVoucher}
                        title={'Save'}
                        saving ={sending}/>

                    <CancelBtn
                        onPress={cancelJournalVoucher}
                        title={'Cancel'}
                    />
                    <DocPicker
                        onPress={selectDocument}
                        fileType={fileType}
                        filePath={filePath}
                        cancelFile={cancelDocument}
                    />
                </View>
            </Content>


            <_Calendar
                is_Visible={isCalendar_Visible}
                hideDatePicker={showCalendar}
                onDayPress={onDayPress}
                currentDate={currentDate}
                markedDates={markedDates}
            />

            <_Calendar
                is_Visible={isChequeCalendar_Visible}
                hideDatePicker={show_chequeDate_calendar}
                onDayPress={onDayPress_chequeDate}
                currentDate={cheq_currentDate}
                markedDates={markedDates_cheque}
            />

            <DropdownModal
                isVisible={is_from_dd_Visible}
                closeFromModal={closeFromModal}
                data={filteredFrom}
                onChangeText={handleSearchValue_from}
                selectValue={selectValue_from}
            />
            <DropdownModal
                isVisible={is_to_dd_Visible}
                closeFromModal={closeToModal}
                data={filteredTo}
                onChangeText={handleSearchValue_to}
                selectValue={selectValue_to}
                isLoading={isdropdownLoading}
            />
            <BottomTabs
                navigation={navigation}
                jvPress={()=>navigation.navigate('journalVoucher')}
                purchasePress={()=>navigation.navigate('Purchase')}
                saleInvoicePress={()=>navigation.navigate('saleInvoice')}
                leaveApplyPress={()=>navigation.navigate('leaveApply')}
            />
            <DialogBox ref={dialogboxRef} />
        </Container>
    )
}

const mapStateToProps = (state) => ({
    user: state.User.userInfo,
    dropdowns: state.Dropdown.FromToDD /// acc transfer dd 
})
const mapDispatchToProps = (dispatch) => ({
    saveJVdd:(jv)=>dispatch(Jv_To_dropdown(jv))

});

export default connect(mapStateToProps, mapDispatchToProps)(JournalVoucher)