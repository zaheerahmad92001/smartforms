import React, { useEffect, useRef, useReducer } from 'react'
import { View, Text } from 'react-native'
import { Container, Content, Icon, Item } from "native-base";
import AppHeader from "../../../Components/AppHeader";
import SaveBtn from "../../../Components/SaveBtn";
import CancelBtn from "../../../Components/CancelBtn";
import styles from '../styles'
import DropdownModal from '../../../Components/dropDownModal';
import DropDown from '../../../Components/Dropdown';
import _Calendar from '../../../Components/Calendar';
import DatePicker from "../../../Components/DatePicker";
import InputField from '../../../Components/InputField';
import DocPicker from "../../../Components/DocPicker";
import DocumentPicker from 'react-native-document-picker'
import DialogBox from 'react-native-dialogbox';
import { NetworkInfo } from "react-native-network-info";
import { SERVER_ERROR, status, __error } from "../../../Constants/constValues";
import axios from "axios";
import { baseUrl } from "../../../Constants/server";
import Error from '../../../Components/Error';
import { connect } from "react-redux";
import moment from 'moment'


let filteredBankAcc = ''
let filteredEmployee = ''
let filteredVType = ''


function ClosingMonth_edit({ navigation,user ,route, emp_dd, bank_dd }) {

    let item = route.params.item
    let dialogboxRef = useRef()

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.access_token}`
            },
            emp_id:item?.employeeid,
            emp_name:item?.employeename,
            employee_list:[],
            employee_dd:[],


            b_dd:[],
            b_dd_List:[],
            bank_id:item?.bankid,

            voucherNumber:item?.vno,
            vnoError:false,
            vType: '',
            vTypeError:'',
            showEmployeeCodeList: false,
            is_vType_Visible: false,
            showBankList: false,
            bankAccError:false,
            searchBank:'',
            searchVType:'',
            bankAcc:item?.bankname,
            chequeNumber:item?.chequeno,
            chequeNoError,
            chequeDate:'',
            chequeDateError:'',
            markedDates:{},
            c_markedDates:{},
            employeeCodeError:false,
            searchEmployeeCode: '',
            amount:item?.chequeamount,
            amountError:false,
            currentDate: new Date(),
            c_currentDate : new Date(),
            isCalendarVisible: false,
            isCalendarVisible_cheque:false,

            filePath: '',
            fileType: '',
            errMsg: '',
            isdropdownLoading:false,
            sending:false,
            ipAddress:'',

            c_startDay:'' , 
            c_endDay:'',
            startDay:'' , 
            endDay:'',
        }
    )

    const {
        headers,
        voucherNumber,
        vnoError,
        emp_id,
        emp_name,
        employee_list,
        employee_dd,

        b_dd,
        b_dd_List,
        bank_id,

        markedDates,
        c_markedDates,
        showBankList,
        searchBank,
        searchVType,
        showEmployeeCodeList,
        vType, 
        vTypeError,
        is_vType_Visible,
        employeeCode,
        employeeCodeError,
        searchEmployeeCode,
        bankAcc,
        bankAccError,
        chequeNumber,
        chequeNoError,
        amount,
        amountError,
        currentDate,
        entryDate,
        isCalendarVisible,
        fileType,
        filePath,
        errMsg,
        isdropdownLoading,
        sending,
        ipAddress,
        chequeDate,
        chequeDateError,
        c_currentDate,
        isCalendarVisible_cheque,

        c_startDay , 
        c_endDay,
        startDay , 
        endDay


    } = state

    useEffect(() => {

        NetworkInfo.getIPAddress().then(ip => {
            updateState({ ipAddress: ip })
        });

        load_Employee()
        populateBank_dd()


        let [_d,_t] = item?.chequedate.split(' ')
        let [y,m,d] = _d?.split('-')
 
        let c_date = `${y}-${m}-${d}`
        let c_show = `${d}-${m}-${y}`

        let c_fullYear = y
        let c_fullMonth = m - 1
        var c_my_date = new Date(c_fullYear, c_fullMonth);
        var c_first_date = new Date(c_my_date.getFullYear(), c_my_date.getMonth(), 1);
        var c_last_date = new Date(c_my_date.getFullYear(), c_my_date.getMonth() +1, 0);

        let c_firstDay = moment.utc(c_first_date).local().format('YYYY-MM-DD')
        let c_last_day = moment.utc(c_last_date).local().format('YYYY-MM-DD')

    
        let [e_d,e_t] = item?.vdate.split(' ')
        let [_y,_m,_dd] = e_d?.split('-')

        let date = `${_y}-${_m}-${_dd}`
        let _show = `${_dd}-${_m}-${_y}`

        let fullYear = _y
        let fullMonth = _m -1
        var my_date = new Date(fullYear, fullMonth);

        var first_date = new Date(my_date.getFullYear(), my_date.getMonth(), 1);
        var last_date = new Date(my_date.getFullYear(), my_date.getMonth() +1, 0);

        let firstDay = moment.utc(first_date).local().format('YYYY-MM-DD')
        let last_day = moment.utc(last_date).local().format('YYYY-MM-DD')
    

        let c_selected = {}
        c_selected[c_date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        let selected = {}
        selected[date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
       
        
        updateState({
            entryDate:_show,
            chequeDate:c_show,
            markedDates:selected,
            c_markedDates:c_selected,

            c_startDay:c_firstDay,
            c_endDay:c_last_day,
            startDay:firstDay,
            endDay:last_day
        })


    }, [])


    function populateBank_dd(){
            let ddValues = []
            if (bank_dd?.length > 0) {
                bank_dd.map((item, index) => {
                    ddValues.push(item.vname)
                })

                updateState({
                    isdropdownLoading: false,
                    b_dd_List: ddValues,// for filter 
                    b_dd:bank_dd,
                })
            } 
        }
        

    function load_Employee(){
    if(emp_dd?.length >0){
               let ddValue=[]
               emp_dd.map((item,index)=>{
                   ddValue.push(item.employeename)
               })
               updateState({
                   employee_list:ddValue,
                   employee_dd:emp_dd
              })
           }
        }
      

    function goBack(){ navigation.pop()}
   
    async function selectDocument() {
        try {
            const results = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });

            updateState({
                filePath: results,
                fileType: results[0].type
            })
            for (const res of results) {
                console.log(
                    res.uri,
                    res.type, // mime type
                    res.name,
                    res.size,
                )
            }

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                console.log('Document picker cancel')
            } else {
                throw err;
            }
        }
    }

    function cancelDocument() {
        updateState({
            filePath: '',
            fileType: ''
        })
    }

    function closeEmployeeCodeList() {
        updateState({ showEmployeeCodeList: false })
    }
    const handleSearchEmployeeCode = (value) => {
        updateState({ searchEmployeeCode: value })
    }
    function selectCode(value, index) {
        var FOUND = employee_dd.find(function (post, index) {
            if (post.employeename == value) {
                return post;
            }
        })
        updateState({
            emp_id:FOUND.id,
            emp_name:value,
            errMsg:'',
            employeeCodeError:false,
            showEmployeeCodeList: false,
            searchEmployeeCode: ''
        })
    }


    function showCalendar() {
        let isVisible = isCalendarVisible
        updateState({ isCalendarVisible: !isVisible })
    }
    
    function onDayPress(date) {

        let [y,m,d] = date.dateString.split('-')
        let _show= `${d}-${m}-${y}`
        
        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates: selected,
            entryDate:_show,
            entryDateError:false,
            isCalendarVisible:false,
        })
    }

    function showCalendar_cheque() {
        let isVisible = isCalendarVisible_cheque
        updateState({ isCalendarVisible_cheque: !isVisible })
    }
    
    function onDayPress_cheque(date) {

        let [y,m,d] = date.dateString.split('-')
        let _show= `${d}-${m}-${y}`
        
        let selected = {}
        selected[c_currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            c_markedDates: selected,
            chequeDate:_show,
            chequeDateError:false,
            isCalendarVisible_cheque:false,
        })
    }


    function closeBankModal() {
        updateState({ showBankList: false })
    }
    const handleSearchBank = (value) => {
        updateState({ searchBank: value })
    }

    function selectBank(value, index) {
        var FOUND = b_dd.find(function (post, index) {
            if (post.vname == value) {
                  return post;
            }
        })
        updateState({
            bankAcc: value,
            bank_id:FOUND.id,
            showBankList: false,
            searchBank: '',
            bankAccError: false,
            errMsg: '',

        })
    }

    function closeVTypeModal() {
        updateState({ is_vType_Visible: false })
    }
    const handleVTypeSearch = (value) => {
        updateState({ searchVType: value })
    }
    function selectVType(value, index) {
        updateState({
            vType:value,
            vTypeError: false,
            is_vType_Visible: false,
            searchVType: '',
            errMsg:'',
        })
    }

    function alertMsg(msg1 ,msg2){
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2],
            btn: {
                text: 'OK',
                callback: () => { },
            },
        })
    }

    function saveClosingMonthDetail(){
        let err = false
        // if(!vType){
        //     updateState({vTypeError:true})
        //     err = true  
        // }
        if(!emp_name){
          updateState({employeeCodeError:true})
          err = true
        }
        if(!bankAcc){
            updateState({bankAccError:true})
            err = true
          }
          if(!chequeNumber){
            updateState({chequeNoError:true})
            err = true
          }
          if(!amount || isNaN(amount) || amount < 1 ){
            updateState({amountError:true})
            err = true
          }
          if(!err){
              //api 
              const __headers = {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${user?.access_token}`
            }
            updateState({sending:true})
            let tempVar =user?.sessions 

            let formData= new FormData()
             formData.append('bankid',bank_id)
             formData.append('employeeid',emp_id)
             formData.append('chequeno',chequeNumber)
             formData.append('chequedate',chequeDate)
             formData.append('vdate',entryDate)
             formData.append('vname','closing cheque')
             formData.append('vtype','Employee Salary')
             formData.append('chequeamount',amount)
             formData.append('userid',tempVar?.userid)
             formData.append('companyid',tempVar?.companyid)
             formData.append('locationid',tempVar?.locationid) 
             formData.append('insertedby',tempVar?.userid)
             formData.append('updatedby',tempVar?.userid)
             formData.append('insertedip',ipAddress)
             formData.append('updatedip', ipAddress)
             if(filePath){
             formData.append('file' ,{
                name: filePath[0]?.name,
                type: filePath[0]?.type,
                uri: Platform.OS === 'ios' ? 
                     filePath[0]?.uri.replace('file://', '')
                     : filePath[0]?.uri,
              })
            }
            
        console.log('sending data to server' , formData)
        axios.post(`${baseUrl}/attendance/closing-month-cheque/update/${item?.id}`,formData,{
            headers: __headers
        }).then((response)=>{
            const {data} = response
            if(data.status===status){
                alertMsg('Closing Month Detail Update Successfully!')
                updateState({
                    sending:false,
                    bankAcc:'',
                    emp_name:'',
                    chequeNumber:'',
                    amount:'',
                    
                })
            }else if(data?.status===__error){
              updateState({sending:false})
              alertMsg(data?.message)
              console.log('error in else if ' , data)
            }else{
                updateState({sending:false})
                alertMsg(SERVER_ERROR)
                console.log('error in else  ' , data)
            }
        }).catch((err)=>{
            updateState({sending:false})
            alertMsg(SERVER_ERROR)
            console.log('error in catch ' , err)
        })

        }
          else{
            updateState({ errMsg: 'Please fill required fields' })
          }
    }

   function  cancelClosingMonthDetail(){
       updateState({
           sending:false,
           bankAcc:'',
           emp_name:'',
           chequeNumber:'',
           amount:'',


       })
   }

    /*********************** custom  dd Bank  *************************/
    const filteredVTypeData = (query) => {
        const { data } = state
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return data
            }
            const regex = new RegExp([query.trim()], 'i')
            return data.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }


/*********************** custom  dd Bank  *************************/
const filteredBankData = (query) => {
    
    if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
        if (query === '') {
            return b_dd_List
        }
        const regex = new RegExp([query.trim()], 'i')
        return b_dd_List.filter((c) => c.search(regex) >= 0)
    } else {
        console.log('invalid query', query)
    }
}

    /*********************** custom dd vendor  *************************/
    const filterEmployeeData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return employee_list
            }
            const regex = new RegExp([query.trim()], 'i')
            return employee_list.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
/*********************** custom dd vendor  *************************/
    filteredBankAcc = filteredBankData(searchBank)
    filteredEmployee = filterEmployeeData(searchEmployeeCode)
    filteredVType  = filteredVTypeData(searchVType)

    return(
        <Container>
            <AppHeader
                headingText={'Edit Closing Month Detail'}
                leftIconName={'arrow-back'}
                leftIconType={'Ionicons'}
                leftBtnPress={goBack}
                dashbord={false}
                heading={true}
                search={false}
                edit={true}
            />
            <View style={[styles.blueBG]}></View>
            <Content style={styles.container}>
            {/* <DropDown
                    label="VType"
                    placeholder={'VType'}
                    value={vType}
                    onPress={() => updateState({is_vType_Visible: true })}
                    error={vTypeError}
                /> */}

             <InputField
                    label="Voucher Number"
                    placeholder={'Voucher Number'}
                    editable={false}
                    onChangeText={(v) => updateState({ 
                        voucherNumber: v,
                        vnoError:false,
                        errMsg:'',
                     })}
                    value={voucherNumber}
                    error={vnoError}
                />

                <DropDown
                    label="Bank"
                    placeholder={'Bank'}
                    value={bankAcc}
                    // onPress={() => updateState({ showBankList: true })}
                    error={bankAccError}
                />
                <InputField
                    label="Cheque Number"
                    placeholder={'Cheque Number'}
                    onChangeText={(v) => updateState({
                        chequeNumber: v,
                        chequeNoError: false,
                        errMsg: '',
                    })}
                    value={chequeNumber}
                    error={chequeNoError}
                />
                <DatePicker
                     label="Cheque Date"
                     onPress={showCalendar_cheque}
                     iconName={'calendar-month-outline'}
                     iconType={'MaterialCommunityIcons'}
                     date={chequeDate}
                     error = {chequeDateError}
                    />
                 <DatePicker
                     label="Entry Date"
                     onPress={showCalendar}
                     iconName={'calendar-month-outline'}
                     iconType={'MaterialCommunityIcons'}
                     date={entryDate}
                    />
               
                <DropDown
                    label="Employee Code"
                    placeholder={'Employee Code'}
                    value={emp_name}
                    onPress={() => updateState({ showEmployeeCodeList: true })}
                    error={employeeCodeError}
                />
               
                <InputField
                    label="Amount"
                    placeholder={'Amount'}
                    keyboardType={'decimal-pad'}
                    onChangeText={(v) => updateState({
                        amount: v,
                        amountError: false,
                        errMsg: '',
                    })}
                    value={amount}
                    error={amountError}
                />
                {errMsg ?
                    <Error
                      error={errMsg}
                    /> : null
                }
                {amountError ?
                    <Error
                      error={'Amount shoul be greater then 0'}
                    /> : null
                }
                <View style={styles.btnView}>
                    <SaveBtn
                       onPress={saveClosingMonthDetail}
                        title={'Save'}
                        saving={sending}
                    />
                    <CancelBtn
                        title={'Cancel'}
                        onPress={cancelClosingMonthDetail}
                    />
                    <DocPicker
                        onPress={selectDocument}
                        fileType={fileType}
                        filePath={filePath}
                        cancelFile={cancelDocument}
                    />
                </View>
            </Content>

            <DropdownModal
                isVisible={showEmployeeCodeList}
                closeFromModal={closeEmployeeCodeList}
                data={filteredEmployee}
                onChangeText={handleSearchEmployeeCode}
                selectValue={selectCode}
            />
            <_Calendar
                is_Visible={isCalendarVisible}
                hideDatePicker={showCalendar}
                onDayPress={onDayPress}
                currentDate={currentDate}
                markedDates={markedDates}
                minDate={startDay}
                maxDate={endDay}
            />
            <_Calendar
                is_Visible={isCalendarVisible_cheque}
                hideDatePicker={showCalendar_cheque}
                onDayPress={onDayPress_cheque}
                currentDate={c_currentDate}
                markedDates={c_markedDates}
                minDate={c_startDay}
                maxDate={c_endDay}
            />
            <DropdownModal
                isVisible={showBankList}
                closeFromModal={closeBankModal}
                data={filteredBankAcc}
                onChangeText={handleSearchBank}
                selectValue={selectBank}
                isLoading={isdropdownLoading}
            />
            <DropdownModal
                isVisible={is_vType_Visible}
                closeFromModal={closeVTypeModal}
                data={filteredVType}
                onChangeText={handleVTypeSearch}
                selectValue={selectVType}
                isLoading={isdropdownLoading}
            />

          <DialogBox ref={dialogboxRef}/>
        </Container>
    )
}


const mapStateToProps = (state) => ({
    user: state.User.userInfo,
    emp_dd: state.Dropdown.employeeDDs,
    bank_dd: state.Dropdown.bankDDs,
})
const mapDispatchToProps = (dispatch) => ({
    
});

export default connect(mapStateToProps, mapDispatchToProps)(ClosingMonth_edit)

