import React, { useEffect, useRef, useReducer } from 'react'
import { View, Text } from 'react-native'
import { Container, Content, Icon, Item } from "native-base";
import AppHeader from "../../Components/AppHeader";
import SaveBtn from "../../Components/SaveBtn";
import CancelBtn from "../../Components/CancelBtn";
import styles from './styles'
import DropdownModal from '../../Components/dropDownModal';
import DropDown from '../../Components/Dropdown';
import _Calendar from '../../Components/Calendar';
import DatePicker from "../../Components/DatePicker";
import InputField from '../../Components/InputField';
import DocPicker from "../../Components/DocPicker";
import DocumentPicker from 'react-native-document-picker'
import DialogBox from 'react-native-dialogbox';
import { NetworkInfo } from "react-native-network-info";
import { SERVER_ERROR, status, __error } from "../../Constants/constValues";
import axios from "axios";
import { baseUrl } from "../../Constants/server";
import Error from '../../Components/Error';
import {Employee_dd ,Bank_dd } from '../../redux/actions/Dropdowns'
import { connect } from "react-redux";
import { Keyboard } from 'react-native';
import BottomTabs from '../../Components/BottomTab';

let _data = ['aaaa', 'bbbb', 'cccc', 'dddd', 'eeee']
let filteredBankAcc = ''
let filteredEmployee = ''
let filteredVType = ''


function ClosingMonthDetail({ navigation,user ,saveEmployee ,saveBank }) {
    let dialogboxRef = useRef()

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.access_token}`
            },
            emp_id: '',
            emp_name:'',
            employee_list:[],
            employee_dd:[],


            b_dd:[],
            b_dd_List:[],
            bank_id:'',

            selectedIndex: '',
            vType: '',
            vTypeError:'',
            entryDate:'',
            entryDateError:false,
            showEmployeeCodeList: false,
            is_vType_Visible: false,
            showBankList: false,
            bankAccError:false,
            searchBank:'',
            searchVType:'',
            bankAcc:'',
            chequeDate:'',
            chequeDateError:'',
            chequeNumber,
            chequeNoError,
            markedDates:{},
            c_markedDates:{},
            employeeCodeError:false,
            searchEmployeeCode: '',
            data: _data,
            amount:'',
            amountError:false,
            currentDate: new Date(),
            c_currentDate: new Date(),
            isCalendarVisible: false,
            isCalendarVisible_cheque:false,
            filePath: '',
            fileType: '',
            errMsg: '',
            isdropdownLoading:false,
            sending:false,
            ipAddress:'',


        }
    )

    const {
        headers,
        emp_id,
        emp_name,
        employee_list,
        employee_dd,

        b_dd,
        b_dd_List,
        bank_id,

        markedDates,
        c_markedDates,
        showBankList,
        searchBank,
        searchVType,
        showEmployeeCodeList,
        vType, 
        vTypeError,
        is_vType_Visible,
        employeeCode,
        employeeCodeError,
        searchEmployeeCode,
        bankAcc,
        bankAccError,
        chequeDate,
        chequeDateError,
        chequeNumber,
        chequeNoError,
        amount,
        amountError,
        data,
        currentDate,
        c_currentDate,
        entryDate,
        entryDateError,
        isCalendarVisible,
        isCalendarVisible_cheque,
        fileType,
        filePath,
        errMsg,
        isdropdownLoading,
        sending,
        ipAddress


    } = state

    useEffect(() => {

        NetworkInfo.getIPAddress().then(ip => {
            updateState({ ipAddress: ip })
        });

        load_Employee()
        populateBank_dd()


        let d = new Date().getDate()
        let m = new Date().getMonth()+1
        let y = new Date().getFullYear()

        if(d<=9){
          d = '0'+d
        }
        if(m<=9){
            m = '0'+m
        }
        let date =`${y}-${m}-${d}`
        let _show= `${d}-${m}-${y}`

        let selected = {}
        selected[date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }

        let c_selected = {}
        c_selected[date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        updateState({
            entryDate:_show,
            chequeDate:_show,
            markedDates:selected,
            c_markedDates:c_selected,
            
        })
        const unsubscribe = navigation.addListener('focus', async () => {
            cancelClosingMonthDetail()
        });
        return unsubscribe;


    }, [])


    function populateBank_dd(){
       let formData = {
            userid: user?.sessions?.userid,
            companyid: user?.sessions?.companyid,
            locationid: user?.sessions?.locationid,
        }
        updateState({ isdropdownLoading: true })
            axios.post(`${baseUrl}/banks`, formData, {
                headers: headers
            }).then((res) => {
                const { data } = res
                if (data.status === status) {
                    /*********** dd Redux **********/
                    saveBank(data.banks)
    
                    let ddValues = []
                    if (data.banks?.length > 0) {
                        data.banks.map((item, index) => {
                          ddValues.push(item.vname)
                        })
    
                        updateState({
                            isdropdownLoading: false,
                            b_dd_List: ddValues,// for filter 
                            b_dd: data.banks,
                        })
                    } else {
                        updateState({ isdropdownLoading: false })
                    }
                } else {
                    alertMsg(data.message)
                }
            }) 
        }


    function load_Employee(){
        updateState({isdropdownLoading:true})
    
        let tempVar = user?.sessions
        let formData = {
            userid: tempVar?.userid,
            companyid: tempVar?.companyid,
            locationid: tempVar?.locationid,
            isactive:1
        }
        axios.post(`${baseUrl}/attendance/employees`, formData, {
           headers: headers
       }).then((response)=>{
           const {data} = response
           if(data.status===status){
    
         /********* Redux  *********/
             saveEmployee(data.employees)
    
               let ddValue=[]
               data.employees.map((item,index)=>{
                   ddValue.push(item.employeename)
               })
               updateState({
                   employee_list:ddValue,
                   employee_dd:data.employees
              })
           }else{
            updateState({isdropdownLoading:false})
            alertMsg(SERVER_ERROR)
            console.log('error in else' ,)
           }
       }).catch((err)=>{
           updateState({isdropdownLoading:false})
            alertMsg(SERVER_ERROR)
            console.log('error in catch' , err)
       })
      }

    function openMenu() {
        Keyboard.dismiss()
        setTimeout(()=>{
            navigation.openDrawer();
        },500)
    }
   function navigateTo (){navigation.navigate('closingMonth_search')}


    async function selectDocument() {
        try {
            const results = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });

            updateState({
                filePath: results,
                fileType: results[0].type
            })
            for (const res of results) {
                console.log(
                    res.uri,
                    res.type, // mime type
                    res.name,
                    res.size,
                )
            }

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                console.log('Document picker cancel')
            } else {
                throw err;
            }
        }
    }

    function cancelDocument() {
        updateState({
            filePath: '',
            fileType: ''
        })
    }

    function closeEmployeeCodeList() {
        updateState({ showEmployeeCodeList: false })
    }
    const handleSearchEmployeeCode = (value) => {
        updateState({ searchEmployeeCode: value })
    }
    function selectCode(value, index) {
        var FOUND = employee_dd.find(function (post, index) {
            if (post.employeename == value) {
                return post;
            }
        })
        updateState({
            emp_id:FOUND.id,
            emp_name:value,
            errMsg:'',
            employeeCodeError:false,
            showEmployeeCodeList: false,
            searchEmployeeCode: ''
        })
    }


    function showCalendar() {
        let isVisible = isCalendarVisible
        updateState({ isCalendarVisible: !isVisible })
    }
    
    function onDayPress(date) {

        let [y,m,d] = date.dateString.split('-')
        let _show= `${d}-${m}-${y}`
        
        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates: selected,
            entryDate:_show,
            entryDateError:false,
            isCalendarVisible:false,
        })
    }


    function showCalendar_cheque() {
        let isVisible = isCalendarVisible_cheque
        updateState({ isCalendarVisible_cheque: !isVisible })
    }
    
    function onDayPress_cheque(date) {

        let [y,m,d] = date.dateString.split('-')
        let _show= `${d}-${m}-${y}`
        
        let selected = {}
        selected[c_currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            c_markedDates: selected,
            chequeDate:_show,
            chequeDateError:false,
            isCalendarVisible_cheque:false,
        })
    }



    function closeBankModal() {
        updateState({ showBankList: false })
    }
    const handleSearchBank = (value) => {
        updateState({ searchBank: value })
    }

    function selectBank(value, index) {
        var FOUND = b_dd.find(function (post, index) {
            if (post.vname == value) {
                  return post;
            }
        })
        updateState({
            bankAcc: value,
            bank_id:FOUND.id,
            showBankList: false,
            searchBank: '',
            bankAccError: false,
            errMsg: '',

        })
    }

    function closeVTypeModal() {
        updateState({ is_vType_Visible: false })
    }
    const handleVTypeSearch = (value) => {
        updateState({ searchVType: value })
    }
    function selectVType(value, index) {
        updateState({
            vType:value,
            vTypeError: false,
            is_vType_Visible: false,
            searchVType: '',
            errMsg:'',
        })
    }

    function alertMsg(msg1 ,msg2){
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2],
            btn: {
                text: 'OK',
                callback: () => { },
            },
        })
    }

   async function saveClosingMonthDetail(){
        let err = false
        // if(!vType){
        //     updateState({vTypeError:true})
        //     err = true  
        // }
        if(!entryDate){
            updateState({entryDateError:true})
            err = true
        }
        if(!chequeDate){
            updateState({chequeDateError:true})
            err = true  
        }
        if(!emp_name){
          updateState({employeeCodeError:true})
          err = true
        }
        if(!bankAcc){
            updateState({bankAccError:true})
            err = true
          }
          if(!chequeNumber){
            updateState({chequeNoError:true})
            err = true
          }
          if(!amount || isNaN(amount) || amount < 1){
            updateState({amountError:true})
            err = true
          }
          if(!err){
              //api 
              const __headers = {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${user?.access_token}`
            }

          updateState({sending:true})
         let tempVar =user?.sessions 

        let formData= new FormData()
         formData.append('bankid',bank_id)
         formData.append('employeeid',emp_id)
         formData.append('chequeno',chequeNumber)
         formData.append('chequedate',chequeDate)
         formData.append('vdate',entryDate)
         formData.append('vname','closing cheque')
         formData.append('vtype','Employee Salary')
         formData.append('chequeamount',amount)
         formData.append('userid',tempVar?.userid)
         formData.append('companyid',tempVar?.companyid)
         formData.append('locationid',tempVar?.locationid) 
         formData.append('insertedby',tempVar?.userid)
         formData.append('updatedby',tempVar?.userid)
         formData.append('insertedip',ipAddress)
         formData.append('updatedip', ipAddress)
         if(filePath){
         formData.append('file' ,{
            name: filePath[0]?.name,
            type: filePath[0]?.type,
            uri: Platform.OS === 'ios' ? 
                 filePath[0]?.uri.replace('file://', '')
                 : filePath[0]?.uri,
          })
        }
        
        console.log('sending data to server' , formData)
      await axios.post(`${baseUrl}/attendance/closing-month-cheque/create`,formData,{
            headers: __headers
        }).then((response)=>{
            const {data} = response
            if(data.status===status){
                alertMsg('Closing Month Detail Save Successfully!')
                
                updateState({
                    entryDate:'',
                    chequeDate:'',
                })
                cancelClosingMonthDetail()

            }else if(data?.status===__error){
              updateState({sending:false})
              alertMsg(data?.message)
              console.log('error in else if ' , data)
            }else{
                updateState({sending:false})
                alertMsg(SERVER_ERROR)
                console.log('error in else  ' , data)
            }
        }).catch((err)=>{
            updateState({sending:false})
            alertMsg(SERVER_ERROR)
            console.log('error in catch ' , err)
        })

        }
          else{
            updateState({ errMsg: 'Please fill required fields' })
          }
    }

   function  cancelClosingMonthDetail(){
    updateState({
        sending:false,
        entryDate:'',
        chequeDate:'',
        bankAcc:'',
        emp_name:'',
        chequeNumber:'',
        amount:'',
        filePath:'',
        fileType:'',
        errMsg:'',

        chequeDateError:false,
        entryDateError:false,
        bankAccError:false,
        chequeNoError:false,
        amountError:false,
        employeeCodeError:false
    
    })
   }

    /*********************** custom  dd Bank  *************************/
    const filteredVTypeData = (query) => {
        const { data } = state
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return data
            }
            const regex = new RegExp([query.trim()], 'i')
            return data.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }


/*********************** custom  dd Bank  *************************/
const filteredBankData = (query) => {
    
    if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
        if (query === '') {
            return b_dd_List
        }
        const regex = new RegExp([query.trim()], 'i')
        return b_dd_List.filter((c) => c.search(regex) >= 0)
    } else {
        console.log('invalid query', query)
    }
}

    /*********************** custom dd vendor  *************************/
    const filterEmployeeData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return employee_list
            }
            const regex = new RegExp([query.trim()], 'i')
            return employee_list.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
/*********************** custom dd vendor  *************************/
    filteredBankAcc = filteredBankData(searchBank)
    filteredEmployee = filterEmployeeData(searchEmployeeCode)
    filteredVType  = filteredVTypeData(searchVType)

    return(
        <Container>
            <AppHeader
                rightIconName={'search'}
                rightIconType={'EvilIcons'}
                rightBtnPress={navigateTo}
                headingText={'Closing Month Detail'}
                leftBtnPress={openMenu}
                dashbord={false}
                heading={true}
                search={false}
            />
            <View style={[styles.blueBG]}></View>
            <Content style={styles.container}>
            {/* <DropDown
                    label="VType"
                    placeholder={'VType'}
                    value={vType}
                    onPress={() => updateState({is_vType_Visible: true })}
                    error={vTypeError}
                /> */}
                <DropDown
                    label="Bank"
                    placeholder={'Bank'}
                    value={bankAcc}
                    onPress={() => updateState({ showBankList: true })}
                    error={bankAccError}
                />
                <InputField
                    label="Cheque Number"
                    placeholder={'Cheque Number'}
                    onChangeText={(v) => updateState({
                        chequeNumber: v,
                        chequeNoError: false,
                        errMsg: '',
                    })}
                    value={chequeNumber}
                    error={chequeNoError}
                />

                 <DatePicker
                     label="Cheque Date"
                     onPress={showCalendar_cheque}
                     iconName={'calendar-month-outline'}
                     iconType={'MaterialCommunityIcons'}
                     date={chequeDate}
                     error = {chequeDateError}
                    />

                 <DatePicker
                     label="Entry Date"
                     onPress={showCalendar}
                     iconName={'calendar-month-outline'}
                     iconType={'MaterialCommunityIcons'}
                     date={entryDate}
                     error = {entryDateError}
                    />

               
                <DropDown
                    label="Employee Code"
                    placeholder={'Employee Code'}
                    value={emp_name}
                    onPress={() => updateState({ showEmployeeCodeList: true })}
                    error={employeeCodeError}
                />
               
                <InputField
                    label="Amount"
                    placeholder={'Amount'}
                    keyboardType={'decimal-pad'}
                    onChangeText={(v) => updateState({
                        amount: v,
                        amountError: false,
                        errMsg: '',
                    })}
                    value={amount}
                    error={amountError}
                />
                {errMsg ?
                    <Error
                      error={errMsg}
                    /> : null
                }
                {amountError ?
                    <Error
                      error={'Amount shoul be greater then 0'}
                    /> : null
                }
                <View style={styles.btnView}>
                    <SaveBtn
                       onPress={saveClosingMonthDetail}
                        title={'Save'}
                        saving={sending}
                    />
                    <CancelBtn
                        title={'Cancel'}
                        onPress={cancelClosingMonthDetail}
                    />
                    <DocPicker
                        onPress={selectDocument}
                        fileType={fileType}
                        filePath={filePath}
                        cancelFile={cancelDocument}
                    />
                </View>
            </Content>

            <DropdownModal
                isVisible={showEmployeeCodeList}
                closeFromModal={closeEmployeeCodeList}
                data={filteredEmployee}
                onChangeText={handleSearchEmployeeCode}
                selectValue={selectCode}
            />
            <_Calendar
                is_Visible={isCalendarVisible}
                hideDatePicker={showCalendar}
                onDayPress={onDayPress}
                currentDate={currentDate}
                markedDates={markedDates}
            />
            <_Calendar
                is_Visible={isCalendarVisible_cheque}
                hideDatePicker={showCalendar_cheque}
                onDayPress={onDayPress_cheque}
                currentDate={c_currentDate}
                markedDates={c_markedDates}
            />
            <DropdownModal
                isVisible={showBankList}
                closeFromModal={closeBankModal}
                data={filteredBankAcc}
                onChangeText={handleSearchBank}
                selectValue={selectBank}
                isLoading={isdropdownLoading}
            />
            <DropdownModal
                isVisible={is_vType_Visible}
                closeFromModal={closeVTypeModal}
                data={filteredVType}
                onChangeText={handleVTypeSearch}
                selectValue={selectVType}
                isLoading={isdropdownLoading}
            />
          <BottomTabs
                navigation={navigation}
                jvPress={()=>navigation.navigate('journalVoucher')}
                purchasePress={()=>navigation.navigate('Purchase')}
                saleInvoicePress={()=>navigation.navigate('saleInvoice')}
                leaveApplyPress={()=>navigation.navigate('leaveApply')}
            />
          <DialogBox ref={dialogboxRef}/>
        </Container>
    )
}


const mapStateToProps = (state) => ({
    user: state.User.userInfo,
})
const mapDispatchToProps = (dispatch) => ({
    saveEmployee:(d)=>dispatch(Employee_dd(d)),
    saveBank:(v)=>dispatch(Bank_dd(v)),

});

export default connect(mapStateToProps, mapDispatchToProps)(ClosingMonthDetail)

