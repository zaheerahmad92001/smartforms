import { Platform, StyleSheet } from 'react-native';
import { appColor, white ,lightGrey, red, grey, black, lightBlack, borderColor, jvcolor, offWhite } from '../../Constants/Colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { largeText, mediumText, xxlText,xlText } from '../../Constants/FontSize';
import { LatoBlack, LatoBold, LatoLight, LatoRegular } from '../../Constants/Fonts';
import { darkSky } from "../../Constants/Colors";

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:white,
    },
    blueBG:{
        backgroundColor:appColor,
        height:hp(23),
        // top:-2,
        zIndex:-1
        // ...StyleSheet.absoluteFillObject,
    },
    overlapView:{
        position: 'absolute',
        height: hp(100),
        width: wp(100),
        zIndex:-1, // overlap android -1 while ios ok with 1
    },
    whiteBox:{
        backgroundColor:white,
        paddingTop:25,
        paddingBottom:10,
        paddingHorizontal:20,
        borderRadius:20,
        marginHorizontal:15,
        marginTop:hp(15),
        // marginTop:hp(-16),
        elevation:1,
        shadowColor:'#000',
        shadowOffset:{height:0,width:0},
        shadowOpacity:0.5,
        shadowRadius:0,
    },

    totalBalance:{
        color:lightBlack,
        fontFamily:LatoRegular,
        fontSize:mediumText,
        fontStyle:'normal',
        fontWeight:'400',
    },
    balance:{
        color:lightBlack,
        fontFamily:LatoRegular,
        fontWeight:'700',
        fontSize:xlText,
        // marginTop:5,
    },
    divider:{
      borderWidth:0.5,
      borderColor:borderColor,
      marginTop:10,
      marginBottom:10,
    },
    btnContainer:{
     flexDirection:'row',
     justifyContent:'space-between',
     alignItems:'center',
     marginTop:10,
     marginBottom:10,
    },
    content:{
        marginHorizontal:15,
        marginTop:20,

    },
    heading:{
        fontSize:mediumText,
        fontWeight:'600',
        fontFamily:LatoRegular,
        color:lightBlack,
    },
    statistics:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginTop:10,
    },
    
    
    
    
    
})
export default styles