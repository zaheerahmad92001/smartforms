import React, { useEffect, useRef, useReducer } from "react";
import { View, Text, Image, TouchableOpacity, TouchableWithoutFeedback, Platform, Pressable, } from "react-native";
import { Container, Content } from "native-base";
import { journal, expense, accTransfer, logo } from "../../Constants/images";
import styles from './styles'
import AppHeader from "../../Components/AppHeader";
import { Divider } from 'react-native-elements';
import _Pressable from "../../Components/Pressable";
import { danger, jvcolor, lightRed, yellow, lightYellow } from "../../Constants/Colors";
import { ScrollView } from "react-native-gesture-handler";
import { heightPercentageToDP, widthPercentageToDP } from "react-native-responsive-screen";
import Statistics from "../../Components/StatisticsBox";

function Dashboard({navigation}) {
  
  useEffect(() => {
  });

function navigateTo(routeName) {
    navigation.navigate(routeName)
  }
function openMenu() {
    navigation.openDrawer();
}

  return (
    <Container style={styles.container}>
      
      <AppHeader
        // leftIconName={'align-left'}
        // leftIconType={'Feather'}
        leftBtnPress={openMenu}
        logo={logo}
        text1={'SMART'}
        text2={'FORMS'}
        dashboard={true}
        edit={true}
        // rightIconName={'search'}
        // rightIconType={'EvilIcons'}
        rightBtnPress={()=>navigateTo('search')}
      />
      <View style={[styles.blueBG]}></View>

      <View style={styles.overlapView}>
        <Content
        style={{flex:1}}
        >
          <View style={[styles.whiteBox]}>
            <Text style={styles.totalBalance}>Total Balance</Text>
            <Text style={styles.balance}>RS 12000000.00</Text>

            <Divider
              orientation="horizontal"
              style={styles.divider} />

            <View style={styles.btnContainer}>
              <_Pressable
                onPress={()=>navigateTo('journalVoucher')}
                img={journal}
                title={'Journal Voucher'}
                container={{ backgroundColor: jvcolor }}
              />
              <_Pressable
                onPress={()=>navigateTo('expenses')}
                img={expense}
                title={'Expense'}
                container={{ backgroundColor: danger }}
              />
              <_Pressable
                onPress={()=>navigateTo('accountTransfer')}
                img={accTransfer}
                title={'Account Transfer'}
                container={{ backgroundColor: yellow }}
              />
            </View>
          </View>

          <View style={styles.content}>
            <Text style={styles.heading}>Today :</Text>
            <View style={styles.statistics}>
              <Statistics
                heading={'Income'}
                amount={'150000'}
                style={{ color: lightRed }}
              />
              <Statistics
                heading={'Expenses'}
                amount={'150000'}
                style={{ color: lightYellow }}
              />
            </View>
            <View style={[styles.statistics, { marginTop: 20 }]}>
              <Statistics
                heading={'Total Bill'}
                amount={'150000'}
                style={{ color: lightRed }}
              />
              <Statistics
                heading={'Savings'}
                amount={'7000'}
                style={{ color: lightYellow }}
              />
            </View>


            <Text style={[styles.heading, { marginTop: 20 }]}>Weekly :</Text>
            <View style={[styles.statistics,{marginBottom:20}]}>
              <Statistics
                heading={'Income'}
                amount={'150000'}
                style={{ color: lightRed }}
              />
              <Statistics
                heading={'Expenses'}
                amount={'150000'}
                style={{ color: lightYellow }}
              />
            </View>

          </View>
        </Content>
      </View>

    </Container>
  )
}
export default Dashboard;