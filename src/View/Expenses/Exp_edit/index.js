import React, { useEffect, useRef, useReducer } from "react";
import { View, Platform } from "react-native";
import { Container, Content, Icon } from "native-base";
import InputField from "../../../Components/InputField";
import DatePicker from "../../../Components/DatePicker";
import _Calendar from '../../../Components/Calendar';
import styles from "../styles";
import AppHeader from "../../../Components/AppHeader";
import DropDown from "../../../Components/Dropdown";
import DropdownModal from "../../../Components/dropDownModal";
import SaveBtn from "../../../Components/SaveBtn";
import CancelBtn from "../../../Components/CancelBtn";
import DocPicker from "../../../Components/DocPicker";
import DocumentPicker from 'react-native-document-picker'
import DialogBox from 'react-native-dialogbox';
import Error from "../../../Components/Error";
import { NetworkInfo } from "react-native-network-info";
import { INVALID_VALUE, status, TO_FROM_VALUE } from "../../../Constants/constValues";
import axios from "axios";
import { baseUrl } from "../../../Constants/server";
import { connect } from "react-redux";
import moment from 'moment'

let filteredFrom = ''
let filteredTo = ''


function Exp_edit({ navigation, user,route, from_dd,to_dd }) {

    let dialogboxRef = useRef()
    let item =  route.params.item

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            voucherNumber:item?.vno,
            vnoError: false,
            isCalendar_Visible: false,
            isChequeCalendar_Visible: false,
            currentDate: '',
            c_currentDate:'',
            markedDates: {},
            markedDates_cheque: {},
            dropdownList_from:[],
            dropdowns_from:[],
            dropdown_ex: [],
            dropdownList_ex: [],
            voucherDate: '',
            vdateError: false,
            chequeNumber:item?.chequeno,
            chequeNoError: false,
            chequeDate: '',
            is_from_dd_Visible: false,

            value_from:item?.bankname,
            from_id:item?.bankid,
            vfromError: false,
            searchV_from: '',
            value_to:item?.expname,
            to_id:item?.expid,
            vtoError: false,
            searchV_to: '',
            is_to_dd_Visible: false,
            amount:item?.amount,
            amountError: false,
            filePath: '',
            fileType: '',
            errMsg: '',
            isdropdownLoading: false,
            ipAddress: '',
            sending: false,

            c_startDay:'' , 
            c_endDay:'',
            startDay:'' , 
            endDay:''

        }
    )
    const {
        voucherNumber,
        vnoError,
        chequeDate,
        isCalendar_Visible,
        isChequeCalendar_Visible,
        currentDate,
        c_currentDate,
        markedDates,
        markedDates_cheque,
        dropdownList_from,
        dropdowns_from,
        dropdown_ex,
        dropdownList_ex,
        voucherDate,
        vdateError,
        chequeNumber,
        chequeNoError,
        is_from_dd_Visible,
        data,
        value_from,
        from_id,
        vfromError,
        searchV_from,
        value_to,
        to_id,
        vtoError,
        searchV_to,
        is_to_dd_Visible,
        amount,
        amountError,
        fileType,
        filePath,
        errMsg,
        isdropdownLoading,
        ipAddress,
        sending,

        c_startDay , 
        c_endDay,
        startDay , 
        endDay

    } = state

    useEffect(() => {

        NetworkInfo.getIPAddress().then(ip => {
            updateState({ ipAddress: ip })
        });

        populate_From_Dropdown()
        populate_To_Dropdown()

        let [_d ,_y] = item?.vdate.split(' ')
        let [y,m,d] = _d?.split('-')

        let date = `${y}-${m}-${d}`
        let _show = `${d}-${m}-${y}`

        let fullYear = y
        let fullMonth = m -1
        var my_date = new Date(fullYear, fullMonth);

        var first_date = new Date(my_date.getFullYear(), my_date.getMonth(), 1);
        var last_date = new Date(my_date.getFullYear(), my_date.getMonth() +1, 0);

        let _first = moment.utc(first_date).local().format('YYYY-MM-DD HH:mm:ss')
        let [firstDay, _time] = _first.split(' ')

        let _last = moment.utc(last_date).local().format('YYYY-MM-DD HH:mm:ss')
        let [last_day, l_time] = _last.split(' ')


        let [_cd ,_cy] = item?.chequedate.split(' ')
        let [cy,cm,cd] = _cd?.split('-')
          
        let c_date =`${cy}-${cm}-${cd}`
        let c_show = `${cd}-${cm}-${cy}`

        let c_fullYear = cy
        let c_fullMonth = cm - 1
        var c_my_date = new Date(c_fullYear, c_fullMonth);
        var c_first_date = new Date(c_my_date.getFullYear(), c_my_date.getMonth(), 1);
        var c_last_date = new Date(c_my_date.getFullYear(), c_my_date.getMonth() +1, 0);

        let c_first = moment.utc(c_first_date).local().format('YYYY-MM-DD HH:mm:ss')
        let [c_firstDay, c_time] = c_first.split(' ')

        let c_last = moment.utc(c_last_date).local().format('YYYY-MM-DD HH:mm:ss')
        let [c_last_day, c_l_time] = c_last.split(' ')

        let selected = {}
        selected[date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }

        let c_selected = {}
        c_selected[c_date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        updateState({
            currentDate: date,
            c_currentDate:c_date,
            voucherDate: _show,
            chequeDate: c_show,
            markedDates: selected,
            markedDates_cheque: c_selected,

            c_startDay:c_firstDay,
            c_endDay:c_last_day,
            startDay:firstDay,
            endDay:last_day
        })
        const unsubscribe = navigation.addListener('focus', async () => {
            populate_To_Dropdown()
            populate_From_Dropdown()
        });
        return unsubscribe;

    }, [])

    function goBack() { navigation.pop() }

    function populate_From_Dropdown() {
        updateState({ isdropdownLoading: true })

                let ddValues = []
                if (from_dd?.length > 0) {
                    from_dd.map((item, index) => {
                        ddValues.push(item.vname)
                    })

                    updateState({
                        isdropdownLoading: false,
                        dropdownList_from: ddValues,// for filter 
                        dropdowns_from:from_dd,
                    })
                } else {
                    updateState({ isdropdownLoading: false })
                }
            }



    function populate_To_Dropdown() {

            let dd = []
            to_dd.map((item, index) => {
                dd.push(item.vname)
            })

            updateState({
                dropdownList_ex: dd,
                dropdown_ex:to_dd,
                isdropdownLoading: false
            })

            }


    function alertMsg(msg1, msg2) {
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2],
            btn: {
                text: 'OK',
                callback: () => {navigation.pop()},
            },
        })
    }


    async function selectDocument() {
        try {
            const results = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });

            updateState({
                filePath: results,
                fileType: results[0].type
            })
            for (const res of results) {
                console.log(
                    res.uri,
                    res.type, // mime type
                    res.name,
                    res.size,
                )
            }

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                console.log('Document picker cancel')
            } else {
                throw err;
            }
        }
    }

    function cancelDocument() {
        updateState({
            filePath: '',
            fileType: ''
        })
    }

    function showCalendar() {
        let isVisible = isCalendar_Visible
        updateState({ isCalendar_Visible: !isVisible })
    }

    function onDayPress(date) {

        let [y, m, d] = date.dateString.split('-')
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates: selected,
            voucherDate: _show,
            isCalendar_Visible: false,
            vdateError: false,
            errMsg: '',
        })
    }


    function show_chequeDate_calendar() {
        let isVisible = isChequeCalendar_Visible
        updateState({ isChequeCalendar_Visible: !isVisible })
    }
    function onDayPress_chequeDate(date) {
        let [y, m, d] = date.dateString.split('-')
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates_cheque: selected,
            chequeDate: _show,
            errMsg: '',
            isChequeCalendar_Visible: false,
        })
    }

    function closeFromModal() {
        updateState({ is_from_dd_Visible: false })
    }

    function selectValue_from(value, index) {
        var FOUND = dropdowns_from.find(function (post, index) {
            if (post.vname == value) {
                updateState({ from_id: post.id })
            }
        })
        updateState({
            value_from: value,
            is_from_dd_Visible: false,
            searchV_from: '',
            vfromError: false,
            errMsg: '',
        })
    }

    const handleSearchValue_from = (value) => {
        updateState({ searchV_from: value })
    }

    function selectValue_to(value, index) {
        var FOUND = dropdown_ex.find(function (post, index) {
            if (post.vname == value) {
                updateState({ to_id: post.id })
            }
        })
        updateState({
            value_to: value,
            is_to_dd_Visible: false,
            searchV_to: '',
            vtoError: false,
            errMsg: ''
        })
    }

    const handleSearchValue_to = (value) => {
        updateState({ searchV_to: value })
    }
    function closeToModal() {
        updateState({ is_to_dd_Visible: false })
    }


   async function saveExpenses() {
        let err = false
        let _equal = ''
        if (value_from) {
            _equal = value_from.localeCompare(value_to);
        }

        if (!voucherDate) {
            updateState({ vdateError: true, })
            err = true
        }
        if (!chequeNumber) {
            updateState({ chequeNoError: true, })
            err = true
        }
        if (!value_from) {
            updateState({ vfromError: true, })
            err = true
        }
        if (!value_to) {
            updateState({ vtoError: true })
            err = true
        }

        if (_equal === 0) {
            updateState({ vtoError: true, vfromError: true })
            alertMsg(
               INVALID_VALUE,
               TO_FROM_VALUE
            )
            err = true
        }

        if (!amount || isNaN(amount) || amount < 1 ) {
            updateState({ amountError: true })
            err = true
        }
        if (!err) {
            updateState({ sending: true })
            /******* API call  */
            let tempVar = user?.sessions

            const headers = {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${user?.access_token}`
            }

            let formData = new FormData()

             formData.append('vdate',voucherDate)
             formData.append('chequeno',chequeNumber)
             formData.append('chequedate',chequeDate)
             formData.append('daccountid',from_id)
             formData.append('accountid',to_id)
             formData.append('amount',amount)
             if(filePath){
             formData.append('file' ,{
                name: filePath[0]?.name,
                type: filePath[0]?.type,
                uri: Platform.OS === 'ios' ? 
                     filePath[0]?.uri.replace('file://', '')
                     : filePath[0]?.uri,
              })
            }
             formData.append('companyid',tempVar?.companyid)
             formData.append('locationid',tempVar?.locationid)
             formData.append('projectid',tempVar?.projectid)
             formData.append('insertedby', tempVar?.userid)
             formData.append('insertedip', ipAddress)
            

           await axios.post(`${baseUrl}/account/voucher-main-exps/update/${item.id}`, formData, {
                headers: headers
            }).then((response) => {
                const { data } = response
                if (data.status === status) {
                    console.log('expenses inserted ', data)
                    alertMsg('Expense Voucher  Update Sccessfully')
                } else {
                    updateState({ sending: false })
                    alertMsg(data?.message)
                    console.log('error in else ', data)
                }
            }).catch((error) => {
                updateState({ sending: false })
                alertMsg('Internal Server Error')
            })

        } else {
            updateState({ errMsg: 'Please fill required fields' })
        }
    }

    function cancelExpenses() {
       navigation.pop()
    }




    /*********************** custom  dd from  *************************/
    const filterFromData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return dropdownList_from
                // return dropdownList_ex
            }
            const regex = new RegExp([query.trim()], 'i')
            return dropdownList_from.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom  dd To  *************************/
    const filter_To_Data = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return dropdownList_ex
            }
            const regex = new RegExp([query.trim()], 'i')
            return dropdownList_ex.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom dd To  *************************/
    filteredFrom = filterFromData(searchV_from)
    filteredTo = filter_To_Data(searchV_to)


    return (
        <Container>
            <AppHeader
                
                headingText={'Edit Expenses'}
                leftIconName={'arrow-back'}
                leftIconType={'Ionicons'}
                leftBtnPress={goBack}
                dashbord={false}
                heading={true}
                search={false}
                edit={true}
            />
            <View style={[styles.blueBG]}></View>

            <Content style={styles.container}>
                <InputField
                    label="Voucher Number"
                    placeholder={'Voucher Number'}
                    editable={false}
                    onChangeText={(v) => updateState({ 
                        voucherNumber: v,
                        vnoError:false,
                        errMsg:'',
                     })}
                    value={voucherNumber}
                    error={vnoError}
                />
                <DatePicker
                    label="Voucher Date"
                    onPress={showCalendar}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={voucherDate}
                    error={vdateError}

                />
                <InputField
                    label="Cheque Number"
                    placeholder={'Cheque Number'}
                    onChangeText={(v) => updateState({
                        chequeNumber: v,
                        chequeNoError: false,
                        errMsg: '',
                    })}
                    value={chequeNumber}
                    error={chequeNoError}
                />
                <DatePicker
                    label="Cheque Date"
                    onPress={show_chequeDate_calendar}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={chequeDate}
                />

                <DropDown
                    label="From"
                    placeholder={'From'}
                    value={value_from}
                    // onPress={() => updateState({ is_from_dd_Visible: true })}
                    error={vfromError}
                />
                <DropDown
                    label="To"
                    placeholder={'To'}
                    value={value_to}
                    // onPress={() => updateState({ is_to_dd_Visible: true })}
                    error={vtoError}
                />
                <InputField
                    label="Amount"
                    placeholder={'amount'}
                    keyboardType={'decimal-pad'}
                    onChangeText={(v) => updateState({
                        amount: v,
                        amountError: false,
                        errMsg: '',
                    })}
                    value={amount}
                    error={amountError}
                />

                {errMsg ?
                    <Error
                        error={errMsg}
                    /> : null
                }

                {amountError ?
                    <Error
                        error={'Amount shoud be greater than 0'}
                    /> : null}

                <View style={styles.btnView}>
                    <SaveBtn
                        onPress={saveExpenses}
                        title={'Save'}
                        saving={sending} />

                    <CancelBtn
                        onPress={cancelExpenses}
                        title={'Cancel'}
                    />
                    <DocPicker
                        onPress={selectDocument}
                        fileType={fileType}
                        filePath={filePath}
                        cancelFile={cancelDocument}
                    />
                </View>
            </Content>




            <_Calendar
                is_Visible={isCalendar_Visible}
                hideDatePicker={showCalendar}
                onDayPress={onDayPress}
                currentDate={currentDate}
                markedDates={markedDates}
                minDate={startDay}
                maxDate={endDay}
            />
            <_Calendar
                is_Visible={isChequeCalendar_Visible}
                hideDatePicker={show_chequeDate_calendar}
                onDayPress={onDayPress_chequeDate}
                currentDate={c_currentDate}
                markedDates={markedDates_cheque}
                minDate={c_startDay}
                maxDate={c_endDay}
            />

            <DropdownModal
                isVisible={is_from_dd_Visible}
                closeFromModal={closeFromModal}
                data={filteredFrom}
                onChangeText={handleSearchValue_from}
                selectValue={selectValue_from}
            />
            <DropdownModal
                isVisible={is_to_dd_Visible}
                closeFromModal={closeToModal}
                data={filteredTo}
                onChangeText={handleSearchValue_to}
                selectValue={selectValue_to}
            />
            <DialogBox ref={dialogboxRef} />
        </Container>
    )
}

const mapStateToProps = (state) => ({
    user: state.User.userInfo,
    from_dd:state.Dropdown.FromToDD, 
    to_dd:state.Dropdown.expDDs,
})
const mapDispatchToProps = (dispatch) => ({

});

export default connect(mapStateToProps,null)(Exp_edit)