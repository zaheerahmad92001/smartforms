import { Platform, StyleSheet } from 'react-native';
import { appColor, white } from '../../Constants/Colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { largeText, mediumText } from '../../Constants/FontSize';
import { LatoBlack, LatoBold, LatoRegular } from '../../Constants/Fonts';
export default styles = StyleSheet.create({
    wraper: {
        flex: 1,
        backgroundColor: appColor
    },
    slide: {
        flex: 1
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    skipBtn: {
        marginTop:Platform.OS=='ios'? hp(10):hp(8),
        alignSelf: 'flex-end',
        marginRight: wp(12),
    },
    arrow: {
        backgroundColor: white,
        height: 18,
        width: 18,
        borderRadius: 18 / 2,
        alignItems: 'center',
        justifyContent: 'center'
    },
    arrowStyle: {
        color: appColor,
        fontSize: 15,
    },
    textStyle: {
        color: white,
        fontSize: mediumText,
        fontFamily: LatoBlack,
        fontWeight: '700',
        marginRight: 5,
        marginTop: -3
    },
    logoStyle: {
        width: 150,
        height: 150,
    },
    middleContent: {
        marginTop: hp(4),
        justifyContent: 'center',
        alignItems: 'center',
    },
    appName: {
        marginTop: -10,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    firstName: {
        color: white,
        fontSize: 15,
        fontFamily: LatoBlack,
        fontWeight: '900',
        marginRight: 5,
    },
    lastName: {
        color: white,
        fontSize: mediumText,
        fontFamily: LatoRegular,
        fontWeight: '500',
    },
    infoText: {
        color: white,
        fontSize: largeText,
        fontFamily: LatoRegular,
        fontWeight: '500',
    },
    bottomView: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        position: 'absolute',
        bottom: hp(15),
    },
    activeDotStyle: {
        // backgroundColor: 'rgba(0, 0, 0, .2)',
        borderColor: white,
        borderWidth: 1,
    },
    inActiveDotStyle: {
        backgroundColor: white,
        width: 5,
        height: 5,
        borderRadius: 5 / 2,
    }

})