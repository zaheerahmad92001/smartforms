import React,{useEffect , useReducer , useRef } from "react";
import AppIntroSlider from 'react-native-app-intro-slider';
import { View,Text,Image, TouchableWithoutFeedback } from "react-native";
import {ON_BOARDING}from '../../Constants/constValues'
import { StackActions } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Icon} from "native-base";  
import { slides } from "../../Constants/constValues";
import styles from "./styles";

function OnBoarding({navigation}){

const [state , updateState]  = useReducer(
    (state , newState)=> ({...state, ...newState}),
    {
        showRealApp:false
    }
)
const {showRealApp} = state


   function _onDone () {
        updateState({ showRealApp: true });
      }

   function renderDoneButton () {
        return (
          <View></View>
        );
      };

async function onSkip() {
  await AsyncStorage.setItem(ON_BOARDING,'true')
   navigation.dispatch(StackActions.replace('login'));
        // navigation.navigate('login') 
     } 

   function _renderItem  ({ item }) {
        return (
          <View style={styles.slide}>
              <View style={styles.skipBtn}>
              {/* {item.key !=2? */}
                  <TouchableWithoutFeedback
                   onPress={onSkip}
                  >
                      <View style={styles.row}>
                          <Text style={styles.textStyle} >Skip</Text>
                          <View style={styles.arrow}>
                            <Icon
                              name={'arrow-forward-sharp'}
                              type={'Ionicons'}
                              style={styles.arrowStyle}
                            />
                          </View>
                      </View>
                  </TouchableWithoutFeedback>
                  {/* : null} */}
              </View>
              <View style={styles.middleContent}>
                  <Image
                    resizeMode={'contain'}
                    source={item.image}
                    style={styles.logoStyle}
                  />
                  <View style={styles.appName}>
                      <Text style={styles.firstName}>{item.appNameText1}</Text>
                      <Text style={styles.lastName}>{item.appNameText2}</Text>
                  </View>
              </View>
            <View style={styles.bottomView}>
                 <Text style={styles.infoText}>{item.infoText1}</Text>
                 <Text style={styles.infoText}>{item.infoText2}</Text>
            </View>
             
          </View>
        );
      }

    return(
        <View style={styles.wraper}>
        <AppIntroSlider
          renderItem={_renderItem} 
          data={slides} 
          onDone={_onDone}
          renderDoneButton={renderDoneButton}
          renderNextButton={renderDoneButton}
          activeDotStyle={styles.activeDotStyle}
          dotStyle={styles.inActiveDotStyle}
          />
        </View>
    )
}
export default OnBoarding