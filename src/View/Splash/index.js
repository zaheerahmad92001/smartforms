import React, { useEffect, useRef, useReducer } from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import { connect } from 'react-redux'
import { loginUser } from '../../redux/actions/User'
import { appColor, white } from "../../Constants/Colors";
import { logo } from '../../Constants/images';
import { LatoBlack, LatoRegular } from '../../Constants/Fonts';
import { mediumText } from '../../Constants/FontSize';
import {USER ,ON_BOARDING}from '../../Constants/constValues'
import { StackActions } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Platform } from 'react-native';
import { PermissionsAndroid } from 'react-native';



function Splash({ navigation ,saveUser ,user }) {
    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {}
    )
    const {
    } = state

    useEffect(() => {
      runApp()
      requestPermission()
    }, [])


    const runApp = async()=>{
        let user = await AsyncStorage.getItem(USER)
        console.log('User' , JSON.parse(user))
        let onboarding  = await AsyncStorage.getItem(ON_BOARDING)
        console.log('onBoarding' , onboarding)

        setTimeout(() => {
            let item = JSON.parse(user)
            if(item){
                saveUser(item)
             navigation.dispatch(StackActions.replace('MyDrawer'));
            }else if(onboarding=='true'){
              navigation.dispatch(StackActions.replace('login'));    
            }
            else{
             navigation.dispatch(StackActions.replace('Onboarding'));
            }
        },2000);
    }

  async function requestPermission(){
      if(Platform.OS=='android'){
        try {
            const granted = await PermissionsAndroid.requestMultiple(
               [PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE],
               [PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE],
              {
                title: 'External Storage Write Permission',
                message: 'App needs access to Storage data',
              },
            );
            return granted === PermissionsAndroid.RESULTS.GRANTED;
          } catch (err) {
            alert('Write permission ', err);
            return false;
          }
        } else {
          return true;
        }
      }
    
  
    return (
        <View style={styles.container}>
            <Image
                resizeMode={'contain'}
                source={logo}
                style={styles.logoStyle}
            />
            <View style={styles.appName}>
                <Text style={styles.firstName}>{'SMART'}</Text>
                <Text style={styles.lastName}>{'FORMS'}</Text>
          </View>
        </View>
    )
}
const mapStateToProps = (state) => ({
    user: state.User
  })
  const mapDispatchToProps = (dispatch) => ({
    saveUser: (user) => dispatch(loginUser(user)),
  });
export default connect(mapStateToProps ,mapDispatchToProps)(Splash)


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: appColor
    },
    logoStyle: {
        width: 150,
        height: 150,
    },
    appName: {
        marginTop: -10,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    firstName: {
        color: white,
        fontSize: 15,
        fontFamily: LatoBlack,
        fontWeight: '900',
        marginRight: 5,
    },
    lastName: {
        color: white,
        fontSize: mediumText,
        fontFamily: LatoRegular,
        fontWeight: '500',
    },

})