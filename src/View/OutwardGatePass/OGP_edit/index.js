import React, { useEffect, useRef, useReducer } from "react";
import { View, Platform } from "react-native";
import { Container, Content, Icon } from "native-base";
import InputField from "../../../Components/InputField";
import DatePicker from "../../../Components/DatePicker";
import _Calendar from '../../../Components/Calendar';
import styles from "../styles";
import AppHeader from "../../../Components/AppHeader";
import moment from 'moment'
import DropDown from "../../../Components/Dropdown";
import DialogBox from 'react-native-dialogbox';
import DropdownModal from "../../../Components/dropDownModal";
import SaveBtn from "../../../Components/SaveBtn";
import CancelBtn from "../../../Components/CancelBtn";
import DocPicker from "../../../Components/DocPicker";
import TimePickerField from "../../../Components/TimePickerField";
import TimePicker from "../../../Components/TimePicker";
import DocumentPicker from 'react-native-document-picker'
import { NetworkInfo } from "react-native-network-info";
import { SERVER_ERROR, status, __error } from "../../../Constants/constValues";
import axios from "axios";
import { baseUrl } from "../../../Constants/server";
import { connect } from "react-redux";
import Error from "../../../Components/Error";


let filteredVendro = ''
let filteredLocation = ''
let filteredProduct = ''


function OGP_edit({ navigation, user,route, vendor_dd, product_dd, loc_dd }) {
    let dialogboxRef = useRef()
    let item =  route.params.item


    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.access_token}`
            },
            formData: {
                userid: user?.sessions?.userid,
                companyid: user?.sessions?.companyid,
                locationid: user?.sessions?.locationid,
            },

            ipAddress: '',
            sending: false,
            l_dd: [],
            l_dd_List: [],
            location_id:item?.locationid,

            p_dd: [],
            p_dd_List: [],
            product_id:item?.productid,

            v_dd: [],
            v_dd_List: [],
            vendor_id:item?.vendorid,

            voucherNumber:item?.vno,
            vnoError: false,
            isCalendar_Visible: false,
            currentDate: '',
            markedDates: {},
            voucherDate: '',
            vdateError: false,
            isdropdownLoading: false,
            showVendorList: false,
            vendor:item?.vendorname,
            vendorError: false,
            OGPT:item?.outwardtime,
            searchVendor: '',
            location:item?.locationname,
            locationError: false,
            searchLocation: '',
            product:item?.productname,
            productError: false,
            searchProduct: '',
            showLocation: false,
            showProducts: false,
            quantity:Number(item?.qty).toFixed(0),
            qtyError: false,
            driverName:item?.drivername,
            driverError: false,
            vehicleNo:item?.vehicleno,
            vehicleNoError: false,
            time: new Date(),
            selectedTime: '',
            
            selectedTimeError: false,
            showTimePicker: false,
            filePath: '',
            fileType: '',
            errMsg: '',

            startDay:'' , 
            endDay:''
        }
    )
    const {
        headers,
        formData,
        voucherNumber,
        OGPT,
        ipAddress,
        sending,
        isdropdownLoading,
        vnoError,
        currentDate,
        markedDates,
        voucherDate,
        vdateError,
        showVendorList,
        isCalendar_Visible,
        vendor,

        l_dd,
        l_dd_List,
        location_id,

        p_dd,
        p_dd_List,
        product_id,
        v_dd,
        v_dd_List,
        vendor_id,

        vendorError,
        searchVendor,
        location,
        locationError,
        searchLocation,
        product,
        productError,
        searchProduct,
        showLocation,
        showProducts,
        quantity,
        qtyError,
        vehicleNo,
        vehicleNoError,
        driverName,
        driverError,
        time,
        selectedTime,
        OGTP,
        selectedTimeError,
        showTimePicker,
        filePath,
        fileType,
        errMsg,

        startDay, 
        endDay,

    } = state

    useEffect(() => {

        NetworkInfo.getIPAddress().then(ip => {
            updateState({ ipAddress: ip })
        });

        populateVendor_dd()
        populateLocation_dd()
        populateProduct_dd()

        let [hh, mm, ss] = item?.outwardtime.split(':')

        var format = hh >= 12 ? 'PM' : 'AM';
        hh = hh % 12;
        hh = hh ? hh : 12;
        mm = mm
        let Time = `${hh}:${mm} ${format}`



        let [_d ,_y] = item?.vdate.split(' ')
        let [y,m,d] = _d?.split('-')

        let date = `${y}-${m}-${d}`
        let _show = `${d}-${m}-${y}`

        let fullYear = y
        let fullMonth = m -1
        var my_date = new Date(fullYear, fullMonth);

        var first_date = new Date(my_date.getFullYear(), my_date.getMonth(), 1);
        var last_date = new Date(my_date.getFullYear(), my_date.getMonth() +1, 0);

        let firstDay = moment.utc(first_date).local().format('YYYY-MM-DD')
        let last_day = moment.utc(last_date).local().format('YYYY-MM-DD')

        let selected = {}
        selected[date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        updateState({
            currentDate: date,
            voucherDate: _show,
            markedDates: selected,
            selectedTime:Time ,
            startDay:firstDay,
            endDay:last_day
        })

    }, [])

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', async () => {
            populateVendor_dd()
            populateLocation_dd()
            populateProduct_dd()
        });
        return unsubscribe;
    }, [])

    function goBack() {navigation.pop()}


    function alertMsg(msg1, msg2) {
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2],
            btn: {
                text: 'OK',
                callback: () => {navigation.pop()},
            },
        })

    }

    function populateVendor_dd() {
        updateState({ isdropdownLoading: true })

                let ddValues = []
                if (vendor_dd?.length > 0) {
                    vendor_dd.map((item, index) => {
                        ddValues.push(item.vname)
                    })

                    updateState({
                        isdropdownLoading: false,
                        v_dd_List: ddValues,// for filter 
                        v_dd:vendor_dd,
                    })
                } else {
                    updateState({ isdropdownLoading: false })
                }
            }

    function populateLocation_dd() {
        updateState({ isdropdownLoading: true })

                let ddValues = []
                if (loc_dd?.length > 0) {
                    loc_dd?.map((item, index) => {
                        ddValues.push(item.vname)
                    })

                    updateState({
                        isdropdownLoading: false,
                        l_dd_List: ddValues,// for filter 
                        l_dd:loc_dd,
                    })
                } else {
                    updateState({ isdropdownLoading: false })
                }
            }

    function populateProduct_dd() {
        updateState({ isdropdownLoading: true })

                let ddValues = []
                if (product_dd?.length > 0) {
                    product_dd.map((item, index) => {
                        ddValues.push(item.vname)
                    })

                    updateState({
                        isdropdownLoading: false,
                        p_dd_List: ddValues,// for filter 
                        p_dd:product_dd,
                    })
                } else {
                    updateState({ isdropdownLoading: false })
                }
            }


    async function selectDocument() {
        try {
            const results = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });

            updateState({
                filePath: results,
                fileType: results[0].type
            })
            for (const res of results) {
                console.log(
                    res.uri,
                    res.type, // mime type
                    res.name,
                    res.size,
                )
            }

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                console.log('Document picker cancel')
            } else {
                throw err;
            }
        }
    }

    function cancelDocument() {
        updateState({
            filePath: '',
            fileType: ''
        })
    }

    function showCalendar() {
        let isVisible = isCalendar_Visible
        updateState({ isCalendar_Visible: !isVisible })
    }


    function closeProductModal() {
        updateState({ showProducts: false })
    }

    function selectProduct(value, index) {
        var FOUND = p_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })
        updateState({
            product: value,
            product_id: FOUND.id,
            productError: false,
            errMsg: '',
            showProducts: false,
            searchProduct: ''
        })
    }
    const handleSearchProduct = (value) => {
        updateState({ searchProduct: value })
    }


    function onDayPress(date) {

        let [y, m, d] = date.dateString.split('-')
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates: selected,
            currentDate:date.dateString,
            voucherDate: _show,
            isCalendar_Visible: false,
        })
    }

    function handleTime() {
        if (time) {
            let dateTime = moment.utc(time).local().format('YYYY-MM-DD: HH:mm:ss')
            let [date, _time] = dateTime.split(' ')
            console.log('date', date, 'time ', _time)

            let [h, m, s] = _time.split(':')

            var format = h >= 12 ? 'PM' : 'AM';
            h = h % 12;
            h = h ? h : 12;
            m = m
            let Time = `${h}:${m} ${format}`
            updateState({
                selectedTime: Time,
                OGPT:_time,
                showTimePicker: false,
                selectedTimeError: false,
                errMsg: '',
            })
            console.log('sening to DB', time)
        }
    }

    function closeTimePicker() {
        updateState({ showTimePicker: false })
    }

    function onTimeChange(time) {
        updateState({ time: time })
    }

    function selectVendor(value, index) {
        var FOUND = v_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })
        updateState({
            vendor: value,
            vendor_id: FOUND.id,
            vendorError: false,
            errMsg: '',
            showVendorList: false,
            searchVendor: '',
        })
    }
    const handleSearchVendor = (value) => {
        updateState({ searchVendor: value })
    }

    function selectLocation(value, index) {
        var FOUND = l_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })
        updateState({
            location: value,
            location_id: FOUND.id,
            locationError: false,
            errMsg: '',
            showLocation: false,
            searchLocation: '',
        })
    }
    const handleSearchLocation = (value) => {
        updateState({ searchLocation: value })
    }



    function closeVendorModal() {
        updateState({ showVendorList: false })
    }


    function closeLocationModal() {
        updateState({ showLocation: false })
    }

   async function saveOutwardGatePass() {
        let err = false
        
        if (!vendor) {
            updateState({ vendorError: true })
            err = true
        }
        if (!location) {
            updateState({ locationError: true })
            err = true
        }
        if (!product) {
            updateState({ productError: true })
            err = true
        }
        if (!quantity || isNaN(quantity) || quantity < 1 ) {
            updateState({ qtyError: true })
            err = true
        }
        if (!driverName) {
            updateState({ driverError: true })
            err = true
        }
        if (!vehicleNo) {
            updateState({ vehicleNoError: true })
            err = true
        }
        if (!selectedTime) {
            updateState({ selectedTimeError: true })
            err = true
        }
        if (!err) {
            // Api
            const __headers = {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${user?.access_token}`
            }

            updateState({ sending: true })
            let tempVar = user?.sessions
            let __formData =  new FormData()
            __formData.append('vdate', voucherDate)
            __formData.append('vendorid', vendor_id)
            __formData.append('productid', product_id)
            __formData.append('productdesc', '')
            __formData.append('qty', quantity)
            __formData.append('drivername',driverName)
            __formData.append('vehicleno', vehicleNo)
            __formData.append('outwardtime', OGPT)
            __formData.append('companyid',tempVar?.companyid)
            __formData.append('locationid', location_id)
            __formData.append('projectid', tempVar?.projectid)
            __formData.append('insertedby', tempVar?.userid)
            __formData.append('insertedip', ipAddress)
            if(filePath){
            __formData.append('file' ,{
                name: filePath[0]?.name,
                type: filePath[0]?.type,
                uri: Platform.OS === 'ios' ? 
                     filePath[0]?.uri.replace('file://', '')
                     : filePath[0]?.uri,
              })
            }

            console.log('OutWardGatePass data send to server', __formData)
          await axios.post(`${baseUrl}/reproduction/ogps/update/${item?.id}`, __formData, {
                headers: __headers
            }).then((response) => {
                const { data } = response
                console.log('response ', data)
                if (data.status === status) {
                    alertMsg('OutWardGate Pass Voucher Saved Successfully')
                    updateState({
                        sending: false,
                    })
                }else if(data?.status === __error){
                    alertMsg(data?.message)
                    updateState({ sending: false })
                    console.log('error in elseif', data)                  
                }else {
                    updateState({ sending: false })
                    alertMsg('Out of stock', 'Required quantity is not available')
                    console.log('error in else', data)
                }
            }).catch((err) => {
                updateState({ sending: false })
                alertMsg(SERVER_ERROR)
                console.log('err in catch', err)
            })
        } else {
            updateState({ errMsg: 'Please fill required fields' })
        }
    }

    function cancelOutwardGatePass() {
        updateState({sending: false,})
        navigation.pop()
    }

    /*********************** custom dd vendor  *************************/
    const filterVendorData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return v_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return v_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom  dd Product  *************************/
    const filterProductData = (query) => {

        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return p_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return p_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }

    /*********************** custom  dd location  *************************/
    const filteredLocationData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return l_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return l_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom dd location  *************************/
    filteredVendro = filterVendorData(searchVendor)
    filteredProduct = filterProductData(searchProduct)
    filteredLocation = filteredLocationData(searchLocation)



    return (
        <Container>
           <AppHeader
                headingText={'Edit OutwardGate Pass'}
                leftIconName={'arrow-back'}
                leftIconType={'Ionicons'}
                leftBtnPress={goBack}
                dashbord={false}
                heading={true}
                search={false}
                edit={true}
            />
            <View style={[styles.blueBG]}></View>

            <Content style={styles.container}>
                <InputField
                    label="Voucher Number"
                    placeholder={'Voucher Number'}
                    editable={false}
                    onChangeText={(v) => updateState({
                        voucherNumber: v,
                        vnoError: false,
                        errMsg: '',
                    })}
                    value={voucherNumber}
                    error={vnoError}
                />

                <DatePicker
                    label="Voucher Date"
                    onPress={showCalendar}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={voucherDate}
                    error={vdateError}
                />

                <DropDown
                    label="Vendor"
                    placeholder={'Vendor'}
                    value={vendor}
                    // onPress={() => updateState({ showVendorList: true })}
                    error={vendorError}
                />

                <DropDown
                    label="Location"
                    placeholder={'Location'}
                    value={location}
                    // onPress={() => updateState({ showLocation: true })}
                    error={locationError}
                />

                <DropDown
                    label="Product"
                    placeholder={'Product'}
                    value={product}
                    // onPress={() => updateState({ showProducts: true })}
                    error={productError}
                />

                <InputField
                    label="Quantity"
                    placeholder={'Quantity'}
                    keyboardType={'number-pad'}
                    onChangeText={(v) => updateState({
                        quantity: v,
                        qtyError: false,
                        errMsg: ''
                    })}
                    value={quantity}
                    error={qtyError}
                />

                <InputField
                    label="Driver Name"
                    placeholder={'Driver Name'}
                    onChangeText={(v) => updateState({
                        driverName: v,
                        driverError: false,
                        errMsg: ''
                    })}
                    value={driverName}
                    error={driverError}
                />
                <InputField
                    label="Vehicle Number"
                    placeholder={'Vehicle Number'}
                    onChangeText={(v) => updateState({
                        vehicleNo: v,
                        vehicleNoError: false,
                        errMsg: ''
                    })}
                    value={vehicleNo}
                    error={vehicleNoError}
                />
                <TimePickerField
                    label="Outward Time"
                    placeholder={'12:00 PM'}
                    value={selectedTime}
                    onPress={() => updateState({ showTimePicker: true })}
                />

                {errMsg ?
                    <Error
                        error={errMsg}
                    /> : null
                }

             {qtyError ?
                    <Error
                        error={'Quantity should be greater then 0'}
                    /> : null
                }
                <View style={styles.btnView}>
                    <SaveBtn
                        onPress={saveOutwardGatePass}
                        title={'Save'}
                        saving={sending}
                    />
                    <CancelBtn
                        onPress={cancelOutwardGatePass}
                        title={'Cancel'}
                    />
                    <DocPicker
                        onPress={selectDocument}
                        fileType={fileType}
                        filePath={filePath}
                        cancelFile={cancelDocument}
                    />
                </View>
            </Content>




            <_Calendar
                is_Visible={isCalendar_Visible}
                hideDatePicker={showCalendar}
                onDayPress={onDayPress}
                currentDate={currentDate}
                markedDates={markedDates}
                minDate={startDay}
                maxDate={endDay}
            />


            <DropdownModal
                isVisible={showVendorList}
                closeFromModal={closeVendorModal}
                data={filteredVendro}
                onChangeText={handleSearchVendor}
                selectValue={selectVendor}
                isLoading ={isdropdownLoading}
            />

            <DropdownModal
                isVisible={showLocation}
                closeFromModal={closeLocationModal}
                data={filteredLocation}
                onChangeText={handleSearchLocation}
                selectValue={selectLocation}
                isLoading ={isdropdownLoading}

            />

            <DropdownModal
                isVisible={showProducts}
                closeFromModal={closeProductModal}
                data={filteredProduct}
                onChangeText={handleSearchProduct}
                selectValue={selectProduct}
                isLoading ={isdropdownLoading}

            />
            <TimePicker
                isVisible={showTimePicker}
                header={'Select Outward Time'}
                date={time}
                onDateChange={onTimeChange}
                onSelect={handleTime}
                onClose={closeTimePicker}
                mode="time"
            />

            <DialogBox ref={dialogboxRef} />
        </Container>
    )

}
const mapStateToProps = (state) => ({
    user: state.User.userInfo,
    vendor_dd: state.Dropdown.vendorDDs,
    loc_dd: state.Dropdown.locationDDs,
    product_dd: state.Dropdown.productDDs,
})
const mapDispatchToProps = (dispatch) => ({
});
export default connect(mapStateToProps, mapDispatchToProps)(OGP_edit)

