import React from "react";
import { Platform, StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { appColor, danger, grey, white } from "../../../Constants/Colors";
import { LatoRegular } from "../../../Constants/Fonts";
import { mediumText } from "../../../Constants/FontSize";
export default styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    blueBG: {
        backgroundColor: appColor,
        height: Platform.OS == 'android' ? hp(6) : hp(4),
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
        zIndex: -1
    },
    sheetStyle: {
        backgroundColor: white,
        // paddingHorizontal:20,
        // paddingVertical:20,
        paddingTop: 10,
        paddingBottom: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonContainer: {
        marginBottom: 20,
        width: wp(75)
    },
    closeBtn: {
        alignSelf: 'flex-end',
        marginBottom: 20,
        marginRight: 20,
    },
    yesBtnStyle: {
        color: danger,
        fontSize:14,
        fontWeight:'600'
    },
    noBtnStyle: {
        color: appColor,
        fontSize:14,
        fontWeight:'600'
    },
    loadingView:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
    },
    noVoucherFound:{
        color:grey,
        fontSize:mediumText,
        fontFamily:LatoRegular
    },
    nodata:{
        color:grey,
        fontFamily:LatoRegular,
        fontSize:mediumText,
    },
    nodataView:{
        marginTop:hp(40),
        justifyContent:'center',
        alignItems:'center',
    }

})