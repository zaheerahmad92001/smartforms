import React, { useEffect, useRef, useReducer } from "react";
import { View, ActivityIndicator,Text, Keyboard } from "react-native";
import { Container, Content, Icon, Footer, FooterTab } from "native-base";
import InputField from "../../Components/InputField";
import DatePicker from "../../Components/DatePicker";
import _Calendar from '../../Components/Calendar';
import styles from "./styles";
import AppHeader from "../../Components/AppHeader";
import DropDown from "../../Components/Dropdown";
import DropdownModal from "../../Components/dropDownModal";
import SaveBtn from "../../Components/SaveBtn";
import CancelBtn from "../../Components/CancelBtn";
import DocPicker from "../../Components/DocPicker";
import DocumentPicker from 'react-native-document-picker'
import BottomTabs from "../../Components/BottomTab";
import DialogBox from 'react-native-dialogbox';
import { connect } from 'react-redux'
import {From_To_dropdown} from '../../redux/actions/Dropdowns'
import RNHTMLtoPDF from 'react-native-html-to-pdf';
import Error from "../../Components/Error";
import axios from "axios";
import { baseUrl } from "../../Constants/server";
import { INVALID_VALUE, SERVER_ERROR, status, TO_FROM_VALUE } from "../../Constants/constValues";
import { NetworkInfo } from "react-native-network-info";
import { appColor } from "../../Constants/Colors";
import { Platform } from "react-native";
import { NativeModules } from 'react-native'
import { Button } from "react-native";
// NativeModules.NativeUI

const {NativeUI} = NativeModules ;
// const myModule = NativeUI.navigateToExample()

let filteredFrom = ''
let filteredTo = ''

function AccountTransfer({ navigation, user ,saveDropdown }) {
    let dialogboxRef = useRef()
    
    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            isCalendar_Visible: false,
            isChequeCalendar_Visible: false,
            currentDate: '',
            cheq_currentDate: '',
            markedDates: {},
            markedDates_cheque: {},
            voucherDate: '',
            vdateError: false,
            chequeNumber: '',
            chequeDate: '',
            chequeDateError: false,
            chequeNoError: false,
            is_from_dd_Visible: false,
            ipAddress: '',
            dropdownList: [],
            dropdowns: [],
            value_from: '',
            from_id: '',
            vfromError: false,
            chequeNoError: false,
            searchV_from: '',
            value_to: '',
            to_id: '',
            vtoError: false,
            searchV_to: '',
            is_to_dd_Visible: false,
            amount: '',
            amountError: false,
            filePath: '',
            fileType: '',
            errMsg: '',
            isdropdownLoading: false,
            sending: false,
        }
    )
    const {
        
        isCalendar_Visible,
        isChequeCalendar_Visible,
        currentDate,
        cheq_currentDate,
        markedDates,
        markedDates_cheque,
        voucherDate,
        vdateError,
        chequeNumber,
        chequeDate,
        chequeDateError,
        chequeNoError,
        is_from_dd_Visible,
        dropdownList,
        dropdowns,
        value_from,
        from_id,
        to_id,
        vfromError,
        searchV_from,
        value_to,
        vtoError,
        searchV_to,
        is_to_dd_Visible,
        amount,
        amountError,
        fileType,
        filePath,
        errMsg,
        isdropdownLoading,
        ipAddress,
        sending
    } = state
    // console.log('voucher date' , voucherDate)
    useEffect(() => {

        NetworkInfo.getIPAddress().then(ip => {
            updateState({ ipAddress: ip })
        });


        populateDropdown()

        // let [date, time] = new Date().toISOString().split('T')
        // let [y, m, d] = date.split('-')
    
        let d = new Date().getDate()
        let m = new Date().getMonth()+1
        let y = new Date().getFullYear()

        if(d<=9){
          d = '0'+d
        }
        if(m<=9){
            m = '0'+m
        }
        let date =`${y}-${m}-${d}`
        
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        updateState({
            currentDate: date,
            // voucherDate: date.replace(/-/g, '/'),
            voucherDate: _show,
            chequeDate: _show,
            cheq_currentDate,date,
            markedDates: selected,
            markedDates_cheque: selected
        })


        const unsubscribe = navigation.addListener('focus', async () => {
            populateDropdown()
            cancelSaveAccTransfer() /// to clear the form
            console.log('dropdowns data is ', dropdowns)
        });
        return unsubscribe;
    }, [])

    

    function populateDropdown() {

        updateState({ isdropdownLoading: true })
        /******** From && To dropdown *****/

        const headers = {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${user?.access_token}`
        }

        let tempVar = user?.sessions

        let formData = {
            userid: tempVar?.userid,
            companyid: tempVar?.companyid,
            locationid: tempVar?.locationid,
            accountgroupcode: 'B'
        }

        axios.post(`${baseUrl}/account/group-details`, formData, {
            headers: headers
        }).then((res) => {
            const { data } = res
            if (data.status === status) {
                /*********** dd Redux **********/
                saveDropdown(data.group_details)

                let ddValues = []
                if (data.group_details?.length > 0) {
                    data.group_details.map((item, index) => {
                        ddValues.push(item.vname)
                    })

                    updateState({
                        isdropdownLoading: false,
                        dropdownList: ddValues,// for filter 
                        dropdowns: data.group_details,
                    })
                } else {
                    updateState({ isdropdownLoading: false })
                }
            } else {
                alertMsg(data.message)
            }

            // console.log('dropdown response' ,data)
        })
        /******** From && To dropdown logic end  *****/

    }

    function openMenu() {
        Keyboard.dismiss()
        setTimeout(()=>{
            navigation.openDrawer();
        },500)
    }

    async function selectDocument() {
        try {
            const results = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
                // uri:Platform.OS
            });

            console.log('image', results)
            
            updateState({
                filePath: results,
                fileType: results[0].type
            })

            for (const res of results) {
                  console.log(
                    res.uri.replace('content','file'),
                    res.type, // mime type
                    res.name,
                    res.size,
                )
            }

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                console.log('Document picker cancel')
            } else {
                throw err;
            }
        }
    }


//    async function createPDF(){
//     let options = {
//         html:
//           '<h1 style="text-align: center;"><strong>Hello Guys</strong></h1><p style="text-align: center;">Here is an example of pdf Print in React Native</p><p style="text-align: center;"><strong>Team About React</strong></p>',
//         fileName: `test${Math.random()}`,
//         directory: 'Documents',
//       };

//       let file = await RNHTMLtoPDF.convert(options);
//       console.log(file.filePath);
//       setFilePath(file.filePath);
//     }
   

    function cancelDocument() {
        updateState({
            filePath: '',
            fileType: ''
        })
    }


    function showCalendar() {
        let isVisible = isCalendar_Visible
        updateState({ isCalendar_Visible: !isVisible })
    }

    function onDayPress(date) {
        let [y, m, d] = date.dateString.split('-')
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates: selected,
            // voucherDate: date.dateString.replace(/-/g, '/'),
            voucherDate: _show,
            isCalendar_Visible: false,
            vdateError: false,
            errMsg: '',
            currentDate:date.dateString

        })
    }

    function todayPress() {
        // let [date, time] = new Date(Date.now()).toISOString().split('T')
        // let selected = {}
        // selected[date] = {
        //     customStyles: {
        //         container: styles.dateContainer,
        //         text: styles.dateText
        //     }
        // }
        // updateState({
        //     isCalendar_Visible:false,
        //     markedDates: selected,
        //     voucherDate: date.replace(/-/g, '/'),
        //     vdateError: false,
        //     errMsg: '',

        // })
    }

    function tomorrowPress() {

        // const tomorrow = moment().add(1, 'days');
        // let [date, time] = tomorrow.toISOString().split('T')

        // let selected = {}
        // selected[currentDate] = {
        //     customStyles: {
        //         container: styles.currentDateContainer,
        //         text: styles.currentDateText
        //     }
        // }

        // selected[date] = {
        //     customStyles: {
        //         container: styles.dateContainer,
        //         text: styles.dateText
        //     }
        // }
        // updateState({
        //     markedDates: selected,
        //     voucherDate: date.replace(/-/g, '/')
        // })
    }

    function inTwoDayPress() {
        // const tomorrow = moment().add(1, 'days');
        // const inTwoDays = moment().add(2, 'days');
        // let [tm, time] = tomorrow.toISOString().split('T')
        // let [twodays, timeStirng] = inTwoDays.toISOString().split('T')

        // let selected = {}
        // selected[currentDate] = {
        //     customStyles: {
        //         container: styles.currentDateContainer,
        //         text: styles.currentDateText
        //     }
        // }

        // selected[tm] = {
        //     customStyles: {
        //         container: styles.dateContainer,
        //         text: styles.dateText
        //     }
        // }
        // selected[twodays] = {
        //     customStyles: {
        //         container: styles.dateContainer,
        //         text: styles.dateText
        //     }
        // }
        // updateState({
        //     markedDates: selected,
        //     voucherDate: twodays.replace(/-/g, '/')

        // })
    }


    function show_chequeDate_calendar() {
        let isVisible = isChequeCalendar_Visible
        updateState({ isChequeCalendar_Visible: !isVisible })
    }
    function onDayPress_chequeDate(date) {
        let [y, m, d] = date.dateString.split('-')
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates_cheque: selected,
            chequeDate: _show,
            cheq_currentDate:date.dateString,
            // c_dateShow:_show,
            chequeDateError: false,
            errMsg: '',
            isChequeCalendar_Visible: false,
        })
    }

    function closeFromModal() {
        updateState({ is_from_dd_Visible: false })
    }

    function selectValue_from(value, index) {
        var FOUND = dropdowns.find(function (post, index) {
            if (post.vname == value) {
                updateState({ from_id: post.id })
                // return post;
            }
        })

        updateState({
            value_from: value,
            is_from_dd_Visible: false,
            searchV_from: '',
            vfromError: false,
            errMsg: '',
        })
    }

    const handleSearchValue_from = (value) => {
        updateState({
            searchV_from: value,
        })
    }

    function selectValue_to(value, index) {
        var FOUND = dropdowns.find(function (post, index) {
            if (post.vname == value) {
                updateState({ to_id: post.id })
                //   return post;
            }
        })

        updateState({
            value_to: value,
            is_to_dd_Visible: false,
            searchV_to: '',
            vtoError: false,
            errMsg: '',
        })
    }
    const handleSearchValue_to = (value) => {
        updateState({
            searchV_to: value,
        })
    }
    function closeToModal() {
        updateState({ is_to_dd_Visible: false })
    }

    function navigateTo() {
        navigation.navigate('VoucherSearch')
    }

    function alertMsg(msg1 ,msg2){
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2],
            btn: {
                text: 'OK',
                callback: () => { },
            },
        })

    }


   async function SaveAccTransfer() {
        let err = false
        let _equal = ''
        if (value_from) {
            _equal = value_from.localeCompare(value_to);
        }

        // if (!voucherNumber) {
        //     updateState({ vnoError: true, })
        //     err = true
        // }
        if (!voucherDate) {
            updateState({ vdateError: true, })
            err = true
        }
        if (!chequeNumber) {
            updateState({ chequeNoError: true, })
            err = true
        }
        if (!chequeDate) {
            updateState({ chequeDateError: true, })
            err = true
        }
        if (!value_from) {
            updateState({ vfromError: true, })
            err = true
        }
        if (!value_to) {
            updateState({ vtoError: true })
            err = true
        }

        if (_equal === 0) {
            updateState({ vtoError: true, vfromError: true })
            alertMsg(
                    INVALID_VALUE,
                    TO_FROM_VALUE
                    )
            err = true
               }

        if (!amount || isNaN(amount) || amount < 1 ) {
            updateState({ amountError: true })
            err = true
        }
        if (!err) {
            updateState({ sending: true })
            /******* API call  */
            let tempVar = user?.sessions
            const headers = {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
                // 'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.access_token}`
            }
        

            let formData = new FormData()

            formData.append('vdate' ,voucherDate )
            formData.append('chequeno' ,chequeNumber )
            formData.append('chequedate' ,chequeDate )
            formData.append('draccountid' ,from_id )
            formData.append('craccountid' ,to_id )
            formData.append('amount' ,amount )
            if(filePath){
            formData.append('file' ,{
                name: filePath[0]?.name,
                type: filePath[0]?.type,
                uri: Platform.OS === 'ios' ? 
                     filePath[0]?.uri.replace('file://', '')
                     : filePath[0]?.uri,
              })
            }

            formData.append('companyid' ,tempVar?.companyid )
            formData.append('locationid' ,tempVar?.locationid )
            formData.append('projectid' ,tempVar?.projectid )
            formData.append('insertedby' ,tempVar?.userid )
            formData.append('insertedip' ,ipAddress )
            
            console.log('data sending to server', formData)

            // let formData = {
            //     // vdate: voucherDate,
            //     // chequeno: chequeNumber,
            //     // chequedate: chequeDate,
            //     // draccountid: from_id,
            //     // craccountid: to_id,
            //     // amount: amount,
            //     // companyid: tempVar?.companyid,
            //     // locationid: tempVar?.locationid,
            //     // projectid: tempVar?.projectid,
            //     // insertedby: tempVar?.userid,
            //     // insertedip: ipAddress,
            //     // file:filePath

            // }


            // const data = new FormData();
            // data.append('file', {
            //     name: filePath[0].name,
            //     type: filePath[0].type,
            //     uri: Platform.OS === 'ios' ? 
            //          filePath[0].uri.replace('file://', '')
            //          : filePath[0].uri,
            //   });

            // console.log('data sending to servr', (data))

    //         await fetch(`${baseUrl}/account/voucher-main-btv/create`, {
    //             method: "POST",
    //             body: data,
    //             headers: {
    //             'Accept': 'application/json, text/plain, */*',
    //              "Content-Type": "multipart/form-data; ",
    //              'Authorization': `Bearer ${user?.access_token}`
    //            },
    //          })
    //          .then(response => response.json())
    //        .then((res) => {
    //            console.log('here is file uplaod response' , res)
              
    //     })
    //   .catch( (error) => {
    //     console.log('error while submitting request', error)  
        
    //     });

    await axios.post(`${baseUrl}/account/voucher-main-btv/create`, formData,{
         headers: headers
             }).then((response) => {
                const { data } = response
                if (data.status === status) {
                    alertMsg('Account Transfer Voucher Saved successfully')
                    cancelSaveAccTransfer() // clear form 
                    updateState({
                         voucherDate:'',
                         chequeDate:'',
                    })
                } else {
                    updateState({ sending: false })
                    console.log('error while adding Acc transfer',data)
                    alertMsg(data?.message)
                }
            }).catch((error) => {
                console.log('error in catch Acc transfer' ,error )
                updateState({ sending: false })
                alertMsg('Internal Server Error')
            })

        } else {
            updateState({ errMsg: 'Please fill required fields' })
        }
    }

  function cancelSaveAccTransfer(){
    updateState({
        sending: false,
        voucherNumber: '',
        // voucherDate:'',
        // chequeDate:'',
        chequeNumber: '',
        value_from: '',
        from_id: '',
        value_to: '',
        to_id: '',
        amount: '',
        filePath:'',
        fileType:'',
        errMsg:'',

        vdateError:false,
        chequeNoError:false,
        chequeDateError:false,
        vfromError:false,
        vtoError:false,
        amountError:false
    })
  }






    /*********************** custom  dd from  *************************/
    const filterFromData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return dropdownList
            }
            const regex = new RegExp([query.trim()], 'i')
            return dropdownList.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom dd from  *************************/


    /*********************** custom  dd To  *************************/
    const filter_To_Data = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return dropdownList
            }
            const regex = new RegExp([query.trim()], 'i')
            return dropdownList.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom dd To  *************************/

    filteredFrom = filterFromData(searchV_from)
    filteredTo = filter_To_Data(searchV_to)

    return (
        // <View style={{flex:1,justifyContent:'center' , alignItems:'center'}}>
        //     <Button
        //         onPress={() => NativeUI.navigateToExample()}
        //         title='Start example activity'
        //     />
        // </View>
        <Container>
            
            <AppHeader
                rightIconName={'search'}
                rightIconType={'EvilIcons'}
                rightBtnPress={navigateTo}
                headingText={'Account Transfer'}
                leftBtnPress={openMenu}
                dashbord={false}
                heading={true}
                search={false}
            />
            <View style={[styles.blueBG]}></View>

            <Content style={styles.container}>
            
                <DatePicker
                    label="Voucher Date"
                    onPress={showCalendar}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={voucherDate}
                    error={vdateError}
                />

                <InputField
                    label="Cheque Number"
                    placeholder={'Cheque Number'}
                    onChangeText={(v) => updateState({
                        chequeNumber: v,
                        chequeNoError: false,
                        errMsg: '',
                    })}
                    value={chequeNumber}
                    error={chequeNoError}
                />
                <DatePicker
                    label="Cheque Date"
                    onPress={show_chequeDate_calendar}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={chequeDate}
                    error={chequeDateError}
                />

                <DropDown
                    label="From"
                    placeholder={'From'}
                    value={value_from}
                    onPress={() => updateState({ is_from_dd_Visible: true })}
                    error={vfromError}
                />
                <DropDown
                    label="To"
                    placeholder={'To'}
                    value={value_to}
                    onPress={() => updateState({ is_to_dd_Visible: true })}
                    error={vtoError}
                />
                <InputField
                    label="Amount"
                    placeholder={'amount'}
                    keyboardType={'decimal-pad'}
                    onChangeText={(v) => updateState({
                        amount: v,
                        amountError: false,
                        errMsg: '',
                    })}
                    value={amount}
                    error={amountError}
                />

                {errMsg ?
                    <Error
                        error={errMsg}
                    /> : null}


                {amountError ?
                    <Error
                        error={'Amount shoud be greater than 0'}
                    /> : null}

                <View style={styles.btnView}>
    
                        <SaveBtn
                            onPress={SaveAccTransfer}
                            title={'Save'}
                            saving ={sending}
                        />
                    
                    <CancelBtn
                       onPress={cancelSaveAccTransfer}
                        title={'Cancel'}
                    />
                    <DocPicker
                        onPress={selectDocument}
                        // onPress={createPDF}
                        fileType={fileType}
                        filePath={filePath}
                        cancelFile={cancelDocument}
                    />
                </View>

            </Content>




            <_Calendar
                is_Visible={isCalendar_Visible}
                hideDatePicker={showCalendar}
                onDayPress={onDayPress}
                currentDate={currentDate}
                markedDates={markedDates}
                todayPress={todayPress}
                tomorrowPress={tomorrowPress}
                inTwoDayPress={inTwoDayPress}
                DoneBtnPress={todayPress}
                removeBtnPress={showCalendar}
            />

            <_Calendar
                is_Visible={isChequeCalendar_Visible}
                hideDatePicker={show_chequeDate_calendar}
                onDayPress={onDayPress_chequeDate}
                currentDate={cheq_currentDate}
                markedDates={markedDates_cheque}
            />

            <DropdownModal
                isVisible={is_from_dd_Visible}
                closeFromModal={closeFromModal}
                data={filteredFrom}
                onChangeText={handleSearchValue_from}
                selectValue={selectValue_from}
                isLoading={isdropdownLoading}
            />


            <DropdownModal
                isVisible={is_to_dd_Visible}
                closeFromModal={closeToModal}
                data={filteredTo}
                onChangeText={handleSearchValue_to}
                selectValue={selectValue_to}
                isLoading={isdropdownLoading}

            />
            <BottomTabs
                navigation={navigation}
                jvPress={()=>navigation.navigate('journalVoucher')}
                purchasePress={()=>navigation.navigate('Purchase')}
                saleInvoicePress={()=>navigation.navigate('saleInvoice')}
                leaveApplyPress={()=>navigation.navigate('leaveApply')}
            />
            
            <DialogBox ref={dialogboxRef} />

        </Container>
       
        
            

    )
}
const mapStateToProps = (state) => ({
    user: state.User.userInfo,
    dropdowns: state.Dropdown.FromToDD
})
const mapDispatchToProps = (dispatch) => ({
    // saveUser: (user) => dispatch(loginUser(user)),
    saveDropdown:(d)=>dispatch(From_To_dropdown(d))
});

export default connect(mapStateToProps, mapDispatchToProps)(AccountTransfer)