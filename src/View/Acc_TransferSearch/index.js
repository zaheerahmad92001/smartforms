import React, { useEffect, useRef, useReducer } from "react";
import { View, Text, Image, TouchableOpacity, TouchableWithoutFeedback, Platform, } from "react-native";
import styles from "./styles";
import AppHeader from "../../Components/AppHeader";
import TransferSearch from '../../Components/TransferSearch'
import { widthPercentageToDP as wp  } from "react-native-responsive-screen";
import {Divider} from 'react-native-elements'
import { FlatList } from "react-native";



let data=[
    {id:1},
    {id:2},
    {id:3},
    {id:4},
    {id:5},
    {id:6},
    {id:7},
    {id:8},
    {id:9},
    {id:10},
    {id:11},
    {id:12},
    {id:13},
    {id:14},

]

function AccTransferSearch({navigation}){

    const [state , updateState]  = useReducer(
        (state , newState)=> ({...state, ...newState}),
        {
            searchValue:''
        }
    )
   const {searchValue} = state

   useEffect(() => {} );

   function handleSearchText(value) {
      updateState({searchValue:value})
     }

  function goBack() { navigation.pop()}

  const  renderItem=({item ,index})=>{
      let val = index +1
      return(
          <TransferSearch
           item={item}
           index={val}
          />
      )
  }


    return(
        <View style={styles.container}>
         <AppHeader
            rightIconName={'search'}
            rightIconType={'EvilIcons'}
            leftIconName={'arrow-back'}
            leftIconType={'Ionicons'}
            leftBtnPress={goBack}
            search={true}
            onChangeText={handleSearchText}
            value={searchValue}
          />
     <View style={[styles.blueBG]}></View>
      <View style={styles.content}>
         <View style={styles.headings}>
             <Text style={[styles.headingStyle,{width:wp(10),}]}>Sr#</Text>
             <Text style={[styles.headingStyle,{width:wp(20) ,textAlign:'center'}]}>Quote</Text>
             <Text style={[styles.headingStyle,{width:wp(20),textAlign:'center'}]}>Date</Text>
             <Text style={[styles.headingStyle,{width:wp(40),textAlign:'center'}]}>Customer</Text>
         </View>
     </View>
     <Divider
        rientation="horizontal"
        style={styles.divider} />

      <FlatList
       data={data}
       keyExtractor={(item)=>{item.id+'1'}}
       renderItem={renderItem}
       style={{paddingHorizontal:10 , marginBottom:20,}}
      />
  
        </View>
    )
}
export default AccTransferSearch;