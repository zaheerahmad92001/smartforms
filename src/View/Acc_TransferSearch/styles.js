import React from "react";
import { Platform, StyleSheet } from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { appColor, borderColor } from "../../Constants/Colors";
import { LatoBold } from "../../Constants/Fonts";
import { mediumText } from "../../Constants/FontSize";
export default styles = StyleSheet.create({
    container:{
        flex:1, 
    },
    blueBG:{
        backgroundColor:appColor,
        height:Platform.OS=='android'? hp(6) : hp(4),
        borderBottomRightRadius:20,
        borderBottomLeftRadius:20,
        zIndex:-1
    },
    content:{
        paddingTop:30,
    },
    headings:{
      flexDirection:'row',
      alignItems:'center',
      alignSelf:'center',
    },
    headingStyle:{
        color:appColor,
        fontFamily:LatoBold,
        fontSize:mediumText,
        
    },
    divider: {
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: borderColor,
        marginTop: 10,
        marginBottom: 10,
    },
})