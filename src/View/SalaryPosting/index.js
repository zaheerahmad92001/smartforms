import React, { useEffect, useRef, useReducer } from 'react'
import { View, Text } from 'react-native'
import { Container, Content, Icon, Item } from "native-base";
import AppHeader from "../../Components/AppHeader";
import SaveBtn from "../../Components/SaveBtn";
import CancelBtn from "../../Components/CancelBtn";
import styles from './styles'
import _Calendar from '../../Components/Calendar';
import DatePicker from "../../Components/DatePicker";
import DocPicker from "../../Components/DocPicker";
import DocumentPicker from 'react-native-document-picker'
import { SERVER_ERROR, status, __error } from "../../Constants/constValues";
import axios from "axios";
import { baseUrl } from "../../Constants/server";
import DialogBox from 'react-native-dialogbox';
import {Employee_dd } from '../../redux/actions/Dropdowns'
import { connect } from "react-redux";
import { Keyboard } from 'react-native';
import { appColor, danger } from '../../Constants/Colors';
import BottomTabs from '../../Components/BottomTab';


let filteredEmployee = ''


function SalaryPosting({ navigation ,user ,saveEmployee  }) {
    let dialogboxRef = useRef()

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.access_token}`
            },
             
            // emp_id: '',
            // emp_name:'',
            // employee_list:[],
            // employee_dd:[],

            // showEmployeeCodeList: false,
            isCalendarVisible: false,
            markedDates:{},
            // employeeCode: '',
            // employeeCodeError:false,
            // searchEmployeeCode: '',
            // all:false,
            // oldSalary:false,
           sending:false,
            currentDate: new Date(),
            fromDate: '',
            fromDateError:false,
            filePath: '',
            fileType: '',
            errMsg: '',
            last_date: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0)
        }
    )

    const {
        headers,
        // emp_id,
        // emp_name,
        // employee_list,
        // employee_dd,
        markedDates,
        // showEmployeeCodeList,
        // employeeCodeError,
        // searchEmployeeCode,
        sending,
        currentDate,
        all,
        oldSalary,
        fromDate,
        fromDateError,
        isCalendarVisible,
        fileType,
        filePath,
        errMsg,
        last_date,

    } = state

    useEffect(() => {

        // load_Employee()

        let d = new Date().getDate()
        let m = new Date().getMonth()+1
        let y = new Date().getFullYear()

        if(d<=9){
          d = '0'+d
        }
        if(m<=9){
            m = '0'+m
        }
        let date =`${y}-${m}-${d}`
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        updateState({
            fromDate:_show,
            currentDate:date,
            markedDates:selected,
        })


    }, [])

    // function load_Employee(){
    //     updateState({isdropdownLoading:true})
    
    //     let tempVar = user?.sessions
    //     let formData = {
    //         userid: tempVar?.userid,
    //         companyid: tempVar?.companyid,
    //         locationid: tempVar?.locationid,
    //         isactive:1
    //     }
    //     axios.post(`${baseUrl}/attendance/employees`, formData, {
    //        headers: headers
    //    }).then((response)=>{
    //        const {data} = response
    //        if(data.status===status){
    
    //      /********* Redux  *********/
    //          saveEmployee(data.employees)
    
    //            let ddValue=[]
    //            data.employees.map((item,index)=>{
    //                ddValue.push(item.employeename)
    //            })
    //            updateState({
    //                employee_list:ddValue,
    //                employee_dd:data.employees
    //           })
    //        }else{
    //         updateState({isdropdownLoading:false})
    //         alertMsg(SERVER_ERROR)
    //         console.log('error in else' ,)
    //        }
    //    }).catch((err)=>{
    //        updateState({isdropdownLoading:false})
    //         alertMsg(SERVER_ERROR)
    //         console.log('error in catch' , err)
    //    })
    //   }

      function alertMsg(msg1 ,msg2){
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2],
            btn: {
                text: 'OK',
                callback: () => { },
            },
        })

    }

    function openMenu() {
        Keyboard.dismiss()
        setTimeout(()=>{
            navigation.openDrawer();
        },500)
    }

    async function selectDocument() {
        try {
            const results = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });

            updateState({
                filePath: results,
                fileType: results[0].type
            })
            for (const res of results) {
                console.log(
                    res.uri,
                    res.type, // mime type
                    res.name,
                    res.size,
                )
            }

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                console.log('Document picker cancel')
            } else {
                throw err;
            }
        }
    }

    function cancelDocument() {
        updateState({
            filePath: '',
            fileType: ''
        })
    }

    // function closeEmployeeCodeList() {
    //     updateState({ showEmployeeCodeList: false })
    // }
    // const handleSearchEmployeeCode = (value) => {
    //     updateState({ searchEmployeeCode: value })
    // }

    // function selectCode(value, index) {
    //     var FOUND = employee_dd.find(function (post, index) {
    //         if (post.employeename == value) {
    //             return post;
    //         }
    //     })
    //     updateState({
    //         emp_id:FOUND.empcode,
    //         emp_name:value,
    //         errMsg:'',
    //         employeeCodeError:false,
    //         showEmployeeCodeList: false,
    //         searchEmployeeCode: ''
    //     })
    // }


    function showCalendar() {
        let isVisible = isCalendarVisible
        updateState({ isCalendarVisible: !isVisible })
    }
    
    function onDayPress(date) {

        let [y,m,d] = date.dateString.split('-')
        let _show= `${d}-${m}-${y}`
        
        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates: selected,
            currentDate:date.dateString,
            fromDate:_show,
            fromDateError:false,
            isCalendarVisible:false,
        })
    }

    // function selectAll(){
    //     let _all = all
    //     updateState({all:!_all})
    // }
    // function retainOldSalary(){
    //     let old_salary = oldSalary
    //     updateState({oldSalary:!old_salary})
    // }
    

    function saveAdvanceSalary(){
        let err = false

        if(!fromDate){
          updateState({fromDateError:true})
          err = true
        }
        
           
          if(!err){
              //api
        dialogboxRef.current.confirm({
          title: 'Message',
            content: ['Do you want to Process the Salary ?'],
            cancel: {
                text: 'No',
                style: {
                    color: danger
                },
                callback: () => {},
            },
            ok: {
                text: 'Yes',
                style: {
                    color:appColor
                },
                callback: () => {SalaryTransfer()},
            },

            })

          }

          else{
            updateState({ errMsg: 'Please fill required fields' })
          }
    }

  async function SalaryTransfer(){
    const __headers = {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
        'Authorization': `Bearer ${user?.access_token}`
    }

    updateState({sending:true})
    let tempVar = user?.sessions
    // let formData = {
    //     vmonth:fromDate,
    //     companyid:tempVar.companyid,
    //     locationid:tempVar.locationid,
    //     userid:tempVar?.userid
    // }

    let formData = new FormData()

    formData.append('vmonth',fromDate)    
    formData.append('companyid',tempVar.companyid)
    formData.append('locationid',tempVar.locationid)
    formData.append('userid',tempVar?.userid)
    if(filePath){
    formData.append('file' ,{
        name: filePath[0]?.name,
        type: filePath[0]?.type,
        uri: Platform.OS === 'ios' ? 
             filePath[0]?.uri.replace('file://', '')
             : filePath[0]?.uri,
      })
    }
    
   await axios.post(`${baseUrl}/attendance/closing-month/save`, formData,{
        headers: __headers
    }).then((response) => {
        const {data} = response
        if(data?.status===status){
            console.log('salary post response' , data)
            updateState({sending:false})
            alertMsg(data?.message)
        }else if(data?.status===__error){
            console.log('here is response', data)
            updateState({sending:false})
             
            alertMsg(data?.message)
        }
    }).catch((err)=>{
        updateState({sending:false})
        alertMsg(SERVER_ERROR)
    })
    
   }

  function cancelSalaryPosting(){
      updateState({
          sending:false,
          fromDate:'',
          filePath:'',
          fileType:'',
          fromDateError:false,
      })
  } 

    /*********************** custom dd vendor  *************************/
    // const filterEmployeeData = (query) => {
    //     if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
    //         if (query === '') {
    //             return employee_list
    //         }
    //         const regex = new RegExp([query.trim()], 'i')
    //         return employee_list.filter((c) => c.search(regex) >= 0)
    //     } else {
    //         console.log('invalid query', query)
    //     }
    // }
/*********************** custom dd vendor  *************************/
    
    // filteredEmployee = filterEmployeeData(searchEmployeeCode)

    return(
        <Container>
            <AppHeader
                rightIconName={'search'}
                rightIconType={'EvilIcons'}
                // rightBtnPress={navigateTo}
                headingText={'Salary Posting'}
                leftBtnPress={openMenu}
                dashbord={false}
                heading={true}
                search={false}
                edit={true}
            />
            <View style={[styles.blueBG]}></View>
            <Content style={styles.container}>
            {/* <DropDown
                    label="Employee Code"
                    placeholder={'Employee Code'}
                    value={emp_name}
                    onPress={() => updateState({ showEmployeeCodeList: true })}
                    error={employeeCodeError}
                /> */}
                <DatePicker
                     label="Month"
                     onPress={showCalendar}
                     iconName={'calendar-month-outline'}
                     iconType={'MaterialCommunityIcons'}
                     date={fromDate}
                     error ={fromDateError}
                    />
             
               {/* <View style={styles.checkBoxesView}>
                   <CheckBox2
                    onPress={selectAll}
                    title={'All'}
                    isCheck={all}
                   />
                   <CheckBox2
                    onPress={retainOldSalary}
                    title={'Retain Old Salary'}
                    checkboxContainer={{marginLeft:30,}}
                    isCheck={oldSalary}
                   />
               </View> */}
                
                <View style={styles.btnView}>
                    <SaveBtn
                       onPress={saveAdvanceSalary}
                        title={'Save'}
                        saving={sending}
                    />
                    <CancelBtn
                        onPress ={cancelSalaryPosting}
                        title={'Cancel'}
                    />
                    {/* <DocPicker
                        onPress={selectDocument}
                        fileType={fileType}
                        filePath={filePath}
                        cancelFile={cancelDocument}
                    /> */}
                </View>
            </Content>

            {/* <DropdownModal
                isVisible={showEmployeeCodeList}
                closeFromModal={closeEmployeeCodeList}
                data={filteredEmployee}
                onChangeText={handleSearchEmployeeCode}
                selectValue={selectCode}
            /> */}
            <_Calendar
                is_Visible={isCalendarVisible}
                hideDatePicker={showCalendar}
                onDayPress={onDayPress}
                currentDate={currentDate}
                markedDates={markedDates}
                maxDate={last_date}
            />
            <BottomTabs
                navigation={navigation}
                jvPress={()=>navigation.navigate('journalVoucher')}
                purchasePress={()=>navigation.navigate('Purchase')}
                saleInvoicePress={()=>navigation.navigate('saleInvoice')}
                leaveApplyPress={()=>navigation.navigate('leaveApply')}
            />
           
          <DialogBox ref={dialogboxRef}/>
        </Container>
    )
}

const mapStateToProps = (state) => ({
    user: state.User.userInfo,
})
const mapDispatchToProps = (dispatch) => ({
    saveEmployee:(d)=>dispatch(Employee_dd(d))
});

export default connect(mapStateToProps, mapDispatchToProps)(SalaryPosting)
