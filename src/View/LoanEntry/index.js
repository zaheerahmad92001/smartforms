import React, { useEffect, useRef, useReducer } from 'react'
import { View, Text } from 'react-native'
import { Container, Content, Icon, Item } from "native-base";
import AppHeader from "../../Components/AppHeader";
import SaveBtn from "../../Components/SaveBtn";
import CancelBtn from "../../Components/CancelBtn";
import styles from './styles'
import DropdownModal from '../../Components/dropDownModal';
import DropDown from '../../Components/Dropdown';
import _Calendar from '../../Components/Calendar';
import DatePicker from "../../Components/DatePicker";
import InputField from '../../Components/InputField';
import DocPicker from "../../Components/DocPicker";
import DocumentPicker from 'react-native-document-picker'
import DialogBox from 'react-native-dialogbox';
import { NetworkInfo } from "react-native-network-info";
import { SERVER_ERROR, status, __error } from "../../Constants/constValues";
import axios from "axios";
import { baseUrl } from "../../Constants/server";
import Error from '../../Components/Error';
import { Employee_dd } from '../../redux/actions/Dropdowns'
import {From_To_dropdown} from '../../redux/actions/Dropdowns'

import { connect } from "react-redux";
import { Keyboard } from 'react-native';
import BottomTabs from '../../Components/BottomTab';

let filteredBankAcc = ''
let filteredEmployee = ''


function LoanEntry({ navigation, user, saveEmployee, saveDropdown }) {
    let dialogboxRef = useRef()

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.access_token}`
            },
            emp_id: '',
            emp_name: '',
            employee_list: [],
            employee_dd: [],

            b_dd: [],
            b_dd_List: [],
            bank_id: '',
            ipAddress: '',
            showEmployeeCodeList: false,
            isCalendar_Visible_cheque: false,
            installment: '',
            installmentError: '',
            showBankList: false,
            bankAccError: false,
            searchBank: '',
            bankAcc: '',
            chequeNumber,
            chequeNoError,
            markedDates: {},
            markedDates_cheque: {},
            chequeDate: '',
            chequeDateError:false,
            c_currentDate: '',
            markedDatesTo: {},
            employeeCodeError: false,
            searchEmployeeCode: '',
            amount: '',
            amountError: false,
            currentDate: new Date(),
            fromDate: '',
            formDateError:false,
            isCalendarVisible: false,
            filePath: '',
            fileType: '',
            sending: false,
            isdropdownLoading:false,
            errMsg: '',
            last_date: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0)

        })

    const {
        headers,
        emp_id,
        emp_name,
        employee_list,
        employee_dd,
        b_dd,
        b_dd_List,
        bank_id,

        markedDates,
        showBankList,
        searchBank,
        showEmployeeCodeList,
        isCalendar_Visible_cheque,
        employeeCodeError,
        searchEmployeeCode,
        installment,
        installmentError,
        bankAcc,
        bankAccError,
        chequeNumber,
        chequeNoError,
        chequeDate,
        chequeDateError,
        markedDates_cheque,
        amount,
        amountError,
        currentDate,
        c_currentDate,
        fromDate,
        formDateError,
        isCalendarVisible,
        fileType,
        filePath,
        errMsg,
        ipAddress,
        sending,
        isdropdownLoading,
        last_date,

    } = state

    useEffect(() => {

        NetworkInfo.getIPAddress().then(ip => {
            updateState({ ipAddress: ip })
        });

        load_Employee()
        populateBank_dd()

        let d = new Date().getDate()
        let m = new Date().getMonth() + 1
        let y = new Date().getFullYear()

        if (d <= 9) {
            d = '0' + d
        }
        if (m <= 9) {
            m = '0' + m
        }
        let date = `${y}-${m}-${d}`
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        updateState({
            fromDate:_show,
            chequeDate:_show,
            currentDate: date,
            c_currentDate: date,
            markedDates: selected,
            markedDates_cheque: selected,
        })
        const unsubscribe = navigation.addListener('focus', async () => {
            cancelLoan()
        });
        return unsubscribe;

    }, [])

    function load_Employee() {
        updateState({ isdropdownLoading: true })

        let tempVar = user?.sessions
        let formData = {
            userid: tempVar?.userid,
            companyid: tempVar?.companyid,
            locationid: tempVar?.locationid,
            isactive: 1
        }
        axios.post(`${baseUrl}/attendance/employees`, formData, {
            headers: headers
        }).then((response) => {
            const { data } = response
            if (data.status === status) {

                /********* Redux  *********/
                saveEmployee(data.employees)

                let ddValue = []
                data.employees.map((item, index) => {
                    ddValue.push(item.employeename)
                })
                updateState({
                    employee_list: ddValue,
                    employee_dd: data.employees
                })
            } else {
                updateState({ isdropdownLoading: false })
                alertMsg(SERVER_ERROR)
                console.log('error in else',)
            }
        }).catch((err) => {
            updateState({ isdropdownLoading: false })
            alertMsg(SERVER_ERROR)
            console.log('error in catch', err)
        })
    }



    function populateBank_dd() {
        let tempVar = user?.sessions
        let formData = {
            userid: tempVar?.userid,
            companyid: tempVar?.companyid,
            locationid: tempVar?.locationid,
            accountgroupcode: 'B'
        }

        updateState({ isdropdownLoading: true })
        axios.post(`${baseUrl}/account/group-details`, formData, {
            headers: headers
        }).then((res) => {
            const { data } = res
            if (data.status === status) {
                /*********** dd Redux **********/
                saveDropdown(data.group_details)

                let ddValues = []
                if (data.group_details?.length > 0) {
                    data.group_details.map((item, index) => {
                        ddValues.push(item.vname)
                    })

                    updateState({
                        isdropdownLoading: false,
                        b_dd_List: ddValues,// for filter 
                        b_dd: data.group_details,
                    })
                } else {
                    updateState({ isdropdownLoading: false })
                }
            } else {
                alertMsg(data.message)
            }
        })
    }

    function navigateTo(){ navigation.navigate('loanEntry_search')}
    
    function openMenu() {
        Keyboard.dismiss()
        setTimeout(() => {
            navigation.openDrawer();
        }, 500)
    }


    async function selectDocument() {
        try {
            const results = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });

            updateState({
                filePath: results,
                fileType: results[0].type
            })
            for (const res of results) {
                console.log(
                    res.uri,
                    res.type, // mime type
                    res.name,
                    res.size,
                )
            }

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                console.log('Document picker cancel')
            } else {
                throw err;
            }
        }
    }

    function cancelDocument() {
        updateState({
            filePath: '',
            fileType: ''
        })
    }

    function closeEmployeeCodeList() {
        updateState({ showEmployeeCodeList: false })
    }
    const handleSearchEmployeeCode = (value) => {
        updateState({ searchEmployeeCode: value })
    }
    function selectCode(value, index) {
        var FOUND = employee_dd.find(function (post, index) {
            if (post.employeename == value) {
                return post;
            }
        })
        updateState({
            emp_id: FOUND.id,
            emp_name: value,
            errMsg: '',
            employeeCodeError: false,
            showEmployeeCodeList: false,
            searchEmployeeCode: ''
        })
    }


    function showCalendar() {
        let isVisible = isCalendarVisible
        updateState({ isCalendarVisible: !isVisible })
    }

    function onDayPress(date) {

        let [y, m, d] = date.dateString.split('-')
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates: selected,
            fromDate: _show,
            formDateError:false,
            currentDate: date.dateString,
            isCalendarVisible: false,
        })
    }

    function closeBankModal() {
        updateState({ showBankList: false })
    }
    const handleSearchBank = (value) => {
        updateState({ searchBank: value })
    }


    function selectBank(value, index) {
        var FOUND = b_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })
        updateState({
            bankAcc: value,
            bank_id: FOUND.id,
            showBankList: false,
            searchBank: '',
            bankAccError: false,
            errMsg: '',

        })
    }

    function showCalendar_cheque() {
        let isVisible = isCalendar_Visible_cheque
        updateState({ isCalendar_Visible_cheque: !isVisible })

    }

    function alertMsg(msg1, msg2) {
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2],
            btn: {
                text: 'OK',
                callback: () => { },
            },
        })

    }

    function onDayPress_cheque(date) {

        let [y, m, d] = date.dateString.split('-')
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates_cheque: selected,
            chequeDate: _show,
            chequeDateError:false,
            c_currentDate: date.dateString,
            isCalendar_Visible_cheque: false,
            errMsg: '',
        })
    }

   async function saveLoan() {
        let err = false
        if(!fromDate){
            updateState({formDateError:true})
            err = true
        }
        if(!chequeDate){
            updateState({chequeDateError:true})
            err = true
        }
        if (!emp_name) {
            updateState({ employeeCodeError: true })
            err = true
        }
        if (!bankAcc) {
            updateState({ bankAccError: true })
            err = true
        }
        if (!chequeNumber) {
            updateState({ chequeNoError: true })
            err = true
        }
        if (!amount ||isNaN(amount)) {
            updateState({ amountError: true })
            err = true
        }
        if (!installment || isNaN(installment)) {
            updateState({ installmentError: true })
            err = true
        }
        if (!err) {
            //api 
            const __headers = {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${user?.access_token}`
            } 
            updateState({ sending: true })
            let tempVar = user?.sessions
            let formData =  new FormData()
                formData.append('employeeid',0)
                formData.append('amount',amount)
                formData.append('installment',installment)
                formData.append('accountid',bank_id)
                formData.append('chequeno', chequeNumber)
                formData.append('chequedate',chequeDate)
                formData.append('datein', fromDate)
                formData.append('companyid', tempVar.companyid)
                formData.append('locationid',tempVar.locationid)
                formData.append('insertedby',tempVar?.userid)
                formData.append('insertedip',ipAddress)
                formData.append('updatedby', tempVar?.userid)
                formData.append('updatedip', ipAddress)
                if(filePath){
                formData.append('file' ,{
                    name: filePath[0]?.name,
                    type: filePath[0]?.type,
                    uri: Platform.OS === 'ios' ? 
                         filePath[0]?.uri.replace('file://', '')
                         : filePath[0]?.uri,
                  })
                }
            
           await axios.post(`${baseUrl}/attendance/sal-loan/create`, formData, {
                headers: __headers
            }).then((response) => {
                const { data } = response
                console.log('Loan data respone' , data)
                if (data.status === status) {
                    alertMsg('Loan Save Successfully!')
                    updateState({
                        fromDate:'',
                        chequeDate:'',
                    })
                    cancelLoan()

                } else if (data?.status === __error) {
                    updateState({ sending: false })
                    alertMsg(data?.message)
                } else {
                    updateState({ sending: false })
                    alertMsg(SERVER_ERROR)
                }
            }).catch((err) => {
                updateState({ sending: false })
                alertMsg(SERVER_ERROR)
            })

        }
        else {
            updateState({ errMsg: 'Please fill required fields' })
        }
    }


    function cancelLoan() {
        updateState({
            sending: false,
            // fromDate:'',
            // chequeDate:'',
            emp_name: '',
            bankAcc: '',
            chequeNumber: '',
            amount: '',
            installment: '',
            filePath:'',
            fileType:'',
            errMsg:'',

            formDateError:false,
            chequeDateError:false,
            employeeCodeError:false,
            bankAccError:false,
            chequeNoError:false,
            amountError:false,
            installmentError:false,
        })
    }


    /*********************** custom  dd Bank  *************************/
    const filteredBankData = (query) => {

        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return b_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return b_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }


    /*********************** custom dd vendor  *************************/
    const filterEmployeeData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return employee_list
            }
            const regex = new RegExp([query.trim()], 'i')
            return employee_list.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom dd vendor  *************************/
    filteredBankAcc = filteredBankData(searchBank)
    filteredEmployee = filterEmployeeData(searchEmployeeCode)

    return (
        <Container>
            <AppHeader
                rightIconName={'search'}
                rightIconType={'EvilIcons'}
                rightBtnPress={navigateTo}
                headingText={'Loan Entry'}
                leftBtnPress={openMenu}
                dashbord={false}
                heading={true}
                search={false}
            />
            <View style={[styles.blueBG]}></View>
            <Content style={styles.container}>
                <DropDown
                    label="Employee"
                    placeholder={'Employee'}
                    value={emp_name}
                    onPress={() => updateState({ showEmployeeCodeList: true })}
                    error={employeeCodeError}
                />
                <DatePicker
                    label="From Date"
                    onPress={showCalendar}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={fromDate}
                    error={formDateError}
                />
                <DropDown
                    label="Bank"
                    placeholder={'Bank'}
                    value={bankAcc}
                    onPress={() => updateState({ showBankList: true })}
                    error={bankAccError}
                />
                <InputField
                    label="Cheque Number"
                    placeholder={'Cheque Number'}
                    onChangeText={(v) => updateState({
                        chequeNumber: v,
                        chequeNoError: false,
                        errMsg: '',
                    })}
                    value={chequeNumber}
                    error={chequeNoError}
                />
                <DatePicker
                    label="Cheque Date"
                    onPress={showCalendar_cheque}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={chequeDate}
                    error = {chequeDateError}
                />
                <InputField
                    label="Amount"
                    placeholder={'Amount'}
                    keyboardType={'decimal-pad'}
                    onChangeText={(v) => updateState({
                        amount: v,
                        amountError: false,
                        errMsg: '',
                    })}
                    value={amount}
                    error={amountError}
                />
                <InputField
                    label="Installment"
                    placeholder={'installment'}
                    keyboardType={'decimal-pad'}
                    onChangeText={(v) => updateState({
                        installment: v,
                        installmentError: false,
                        errMsg: '',
                    })}
                    value={installment}
                    error={installmentError}
                />
                {errMsg ?
                    <Error
                        error={errMsg}
                    /> : null
                }
                <View style={styles.btnView}>
                    <SaveBtn
                        onPress={saveLoan}
                        title={'Save'}
                        saving={sending}
                    />
                    <CancelBtn
                        title={'Cancel'}
                        onPress={cancelLoan}
                    />
                    <DocPicker
                        onPress={selectDocument}
                        fileType={fileType}
                        filePath={filePath}
                        cancelFile={cancelDocument}
                    />
                </View>
            </Content>

            <DropdownModal
                isVisible={showEmployeeCodeList}
                closeFromModal={closeEmployeeCodeList}
                data={filteredEmployee}
                onChangeText={handleSearchEmployeeCode}
                selectValue={selectCode}
                isLoading={isdropdownLoading}
            />
            <_Calendar
                is_Visible={isCalendarVisible}
                hideDatePicker={showCalendar}
                onDayPress={onDayPress}
                currentDate={currentDate}
                markedDates={markedDates}
                maxDate ={last_date}
            />
            <_Calendar
                is_Visible={isCalendar_Visible_cheque}
                hideDatePicker={showCalendar_cheque}
                onDayPress={onDayPress_cheque}
                currentDate={currentDate}
                markedDates={markedDates_cheque}
                maxDate ={last_date}
            />

            <DropdownModal
                isVisible={showBankList}
                closeFromModal={closeBankModal}
                data={filteredBankAcc}
                onChangeText={handleSearchBank}
                selectValue={selectBank}
            />
            <BottomTabs
                navigation={navigation}
                jvPress={()=>navigation.navigate('journalVoucher')}
                purchasePress={()=>navigation.navigate('Purchase')}
                saleInvoicePress={()=>navigation.navigate('saleInvoice')}
                leaveApplyPress={()=>navigation.navigate('leaveApply')}
            />

            <DialogBox ref={dialogboxRef} />
        </Container>
    )
}
const mapStateToProps = (state) => ({
    user: state.User.userInfo,
})
const mapDispatchToProps = (dispatch) => ({
    saveEmployee: (d) => dispatch(Employee_dd(d)),
    saveDropdown:(d)=>dispatch(From_To_dropdown(d))


});

export default connect(mapStateToProps, mapDispatchToProps)(LoanEntry)

