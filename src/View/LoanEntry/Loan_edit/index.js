import React, { useEffect, useRef, useReducer } from 'react'
import { View, Text } from 'react-native'
import { Container, Content, Icon, Item } from "native-base";
import AppHeader from "../../../Components/AppHeader";
import SaveBtn from "../../../Components/SaveBtn";
import CancelBtn from "../../../Components/CancelBtn";
import styles from '../styles'
import DropdownModal from '../../../Components/dropDownModal';
import DropDown from '../../../Components/Dropdown';
import _Calendar from '../../../Components/Calendar';
import DatePicker from "../../../Components/DatePicker";
import InputField from '../../../Components/InputField';
import DocPicker from "../../../Components/DocPicker";
import DocumentPicker from 'react-native-document-picker'
import DialogBox from 'react-native-dialogbox';
import { NetworkInfo } from "react-native-network-info";
import { SERVER_ERROR, status, __error } from "../../../Constants/constValues";
import axios from "axios";
import { baseUrl } from "../../../Constants/server";
import Error from '../../../Components/Error';
import { Employee_dd, Bank_dd } from '../../../redux/actions/Dropdowns'
import { connect } from "react-redux";
import moment from 'moment'
import { Keyboard } from 'react-native';

let filteredBankAcc = ''
let filteredEmployee = ''


function Loan_edit({ navigation, user,route, from_dd,emp_dd }) {

    let item = route.params.item
    let dialogboxRef = useRef()

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.access_token}`
            },
            emp_id:item?.employeeid,
            emp_name:item?.employeename,
            employee_list: [],
            employee_dd: [],

            b_dd: [],
            b_dd_List: [],
            bank_id:item?.accountid,
            ipAddress: '',
            showEmployeeCodeList: false,
            isCalendar_Visible_cheque: false,
            installment:Number(item?.installment).toFixed(2),
            installmentError: '',
            showBankList: false,
            bankAccError: false,
            searchBank: '',
            bankAcc:item?.accname,
            chequeNumber:item?.chequeno,
            chequeNoError:'',
            markedDates: {},
            markedDates_cheque: {},
            chequeDate: '',
            c_currentDate: '',
            markedDatesTo: {},
            employeeCodeError: false,
            searchEmployeeCode: '',
            amount:item?.amount,
            amountError: false,
            currentDate: new Date(),
            fromDate: '',
            isCalendarVisible: false,
            filePath: '',
            fileType: '',
            sending: false,
            errMsg: '',
            last_date: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0),

            c_startDay:'' , 
            c_endDay:'',
            startDay:'' , 
            endDay:''

        }
    )

    const {
        headers,
        emp_id,
        emp_name,
        employee_list,
        employee_dd,
        b_dd,
        b_dd_List,
        bank_id,

        markedDates,
        showBankList,
        searchBank,
        showEmployeeCodeList,
        isCalendar_Visible_cheque,
        employeeCodeError,
        searchEmployeeCode,
        installment,
        installmentError,
        bankAcc,
        bankAccError,
        chequeNumber,
        chequeNoError,
        chequeDate,
        markedDates_cheque,
        amount,
        amountError,
        currentDate,
        c_currentDate,
        fromDate,
        isCalendarVisible,
        fileType,
        filePath,
        errMsg,
        ipAddress,
        sending,
        last_date,

        c_startDay , 
        c_endDay,
        startDay , 
        endDay

    } = state

    useEffect(() => {


        NetworkInfo.getIPAddress().then(ip => {
            updateState({ ipAddress: ip })
        });

        load_Employee()
        populateBank_dd()


        let [_d,_t] = item?.chequedate.split(' ')
        let [y,m,d] = _d?.split('-')
 
        let c_date = `${y}-${m}-${d}`
        let c_show = `${d}-${m}-${y}`

        let c_fullYear = y
        let c_fullMonth = m - 1
        var c_my_date = new Date(c_fullYear, c_fullMonth);
        var c_first_date = new Date(c_my_date.getFullYear(), c_my_date.getMonth(), 1);
        var c_last_date = new Date(c_my_date.getFullYear(), c_my_date.getMonth() +1, 0);

        let c_firstDay = moment.utc(c_first_date).local().format('YYYY-MM-DD')
        let c_last_day = moment.utc(c_last_date).local().format('YYYY-MM-DD')

        let [e_d,e_t] = item?.posteddate.split(' ')
        let [_y,_m,_dd] = e_d?.split('-')
 
        let date = `${_y}-${_m}-${_dd}`
        let _show = `${_dd}-${_m}-${_y}`

        let fullYear = _y
        let fullMonth = _m -1
        var my_date = new Date(fullYear, fullMonth);

        var first_date = new Date(my_date.getFullYear(), my_date.getMonth(), 1);
        var last_date = new Date(my_date.getFullYear(), my_date.getMonth() +1, 0);

        let firstDay = moment.utc(first_date).local().format('YYYY-MM-DD')
        let last_day = moment.utc(last_date).local().format('YYYY-MM-DD')

        let c_selected = {}
        c_selected[c_date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        let selected = {}
        selected[date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        updateState({
            fromDate: _show,
            chequeDate: c_show,
            currentDate: date,
            c_currentDate: c_date,
            markedDates: selected,
            markedDates_cheque: c_selected,

            c_startDay:c_firstDay,
            c_endDay:c_last_day,
            startDay:firstDay,
            endDay:last_day
        })


    }, [])

    function load_Employee() {
            if (emp_dd?.length > 0) {

                let ddValue = []
                emp_dd.map((item, index) => {
                    ddValue.push(item.employeename)
                })
                updateState({
                    employee_list: ddValue,
                    employee_dd:emp_dd
                })
            } else {
                updateState({ isdropdownLoading: false })
                alertMsg(SERVER_ERROR)
                console.log('error in else', )
            }
        }



    function populateBank_dd() {

            let ddValues = []
            if (from_dd?.length > 0) {
                from_dd.map((item, index) => {
                    ddValues.push(item.vname)
                })

                updateState({
                    isdropdownLoading: false,
                    b_dd_List: ddValues,// for filter 
                    b_dd:from_dd,
                })
            } else {
                updateState({ isdropdownLoading: false })
            }
        }

    function goBack(){ navigation.pop()}

    async function selectDocument() {
        try {
            const results = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });

            updateState({
                filePath: results,
                fileType: results[0].type
            })
            for (const res of results) {
                console.log(
                    res.uri,
                    res.type, // mime type
                    res.name,
                    res.size,
                )
            }

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                console.log('Document picker cancel')
            } else {
                throw err;
            }
        }
    }

    function cancelDocument() {
        updateState({
            filePath: '',
            fileType: ''
        })
    }

    function closeEmployeeCodeList() {
        updateState({ showEmployeeCodeList: false })
    }
    const handleSearchEmployeeCode = (value) => {
        updateState({ searchEmployeeCode: value })
    }
    function selectCode(value, index) {
        var FOUND = employee_dd.find(function (post, index) {
            if (post.employeename == value) {
                return post;
            }
        })
        updateState({
            emp_id: FOUND.id,
            emp_name: value,
            errMsg: '',
            employeeCodeError: false,
            showEmployeeCodeList: false,
            searchEmployeeCode: ''
        })
    }


    function showCalendar() {
        let isVisible = isCalendarVisible
        updateState({ isCalendarVisible: !isVisible })
    }

    function onDayPress(date) {

        let [y, m, d] = date.dateString.split('-')
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates: selected,
            fromDate: _show,
            currentDate: date.dateString,
            isCalendarVisible: false,
        })
    }

    function closeBankModal() {
        updateState({ showBankList: false })
    }
    const handleSearchBank = (value) => {
        updateState({ searchBank: value })
    }


    function selectBank(value, index) {
        var FOUND = b_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })
        updateState({
            bankAcc: value,
            bank_id: FOUND.id,
            showBankList: false,
            searchBank: '',
            bankAccError: false,
            errMsg: '',

        })
    }

    function showCalendar_cheque() {
        let isVisible = isCalendar_Visible_cheque
        updateState({ isCalendar_Visible_cheque: !isVisible })

    }

    function alertMsg(msg1, msg2) {
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2],
            btn: {
                text: 'OK',
                callback: () => { },
            },
        })

    }

    function onDayPress_cheque(date) {

        let [y, m, d] = date.dateString.split('-')
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates_cheque: selected,
            chequeDate: _show,
            c_currentDate: date.dateString,
            isCalendar_Visible_cheque: false,
            errMsg: '',
        })
    }

   async function saveLoan() {
        let err = false
        if (!emp_name) {
            updateState({ employeeCodeError: true })
            err = true
        }
        if (!bankAcc) {
            updateState({ bankAccError: true })
            err = true
        }
        if (!chequeNumber) {
            updateState({ chequeNoError: true })
            err = true
        }
        if (!amount || isNaN(amount)) {
            updateState({ amountError: true })
            err = true
        }
        if (!installment || isNaN(installment)) {
            updateState({ installmentError: true })
            err = true
        }
        if (!err) {
            //api
            const __headers = {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${user?.access_token}`
            }  
            updateState({ sending: true })
            let tempVar = user?.sessions

            let formData =  new FormData()
            formData.append('allowdedid', 1)
            formData.append('employeeid',emp_id)
            formData.append('refno', '') //
            formData.append('amount',amount)
            formData.append('installment', installment)
            formData.append('usedamount',0) //
            formData.append('accountid',bank_id) // 
            formData.append('chequeno', chequeNumber)
            formData.append('chequedate', chequeDate)
            formData.append('datein', fromDate)
            formData.append('remarks', '') //
            formData.append('companyid',tempVar.companyid)
            formData.append('locationid',tempVar.locationid)
            formData.append('insertedby',tempVar?.userid)
            formData.append('insertedip', ipAddress)
            formData.append('updatedby', tempVar?.userid)
            formData.append('updatedip', ipAddress)
            if(filePath){
            formData.append('file' ,{
                name: filePath[0]?.name,
                type: filePath[0]?.type,
                uri: Platform.OS === 'ios' ? 
                     filePath[0]?.uri.replace('file://', '')
                     : filePath[0]?.uri,
              })
            }
            

        await axios.post(`${baseUrl}/attendance/sal-loan/update/${item?.id}`, formData, {
                headers: __headers
            }).then((response) => {
                const { data } = response
                console.log('Loan data respone' , data)
                if (data.status === status) {
                    updateState({sending: false,})
                    alertMsg('Success!')
                } else if (data?.status === __error) {
                    updateState({ sending: false })
                    alertMsg(data?.message)
                } else {
                    updateState({ sending: false })
                    alertMsg(SERVER_ERROR)
                }
            }).catch((err) => {
                updateState({ sending: false })
                alertMsg(SERVER_ERROR)
            })

        }
        else {
            updateState({ errMsg: 'Please fill required fields' })
        }
    }


    function cancelLoan() {
        navigation.pop()
        updateState({sending: false})
    }


    /*********************** custom  dd Bank  *************************/
    const filteredBankData = (query) => {

        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return b_dd_List
            }
            const regex = new RegExp([query.trim()], 'i')
            return b_dd_List.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }


    /*********************** custom dd vendor  *************************/
    const filterEmployeeData = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return employee_list
            }
            const regex = new RegExp([query.trim()], 'i')
            return employee_list.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }
    /*********************** custom dd vendor  *************************/
    filteredBankAcc = filteredBankData(searchBank)
    filteredEmployee = filterEmployeeData(searchEmployeeCode)

    return (
        <Container>
            <AppHeader
                headingText={'Edit Loan'}
                leftIconName={'arrow-back'}
                leftIconType={'Ionicons'}
                leftBtnPress={goBack}
                dashbord={false}
                heading={true}
                search={false}
                edit={true}
            />
            <View style={[styles.blueBG]}></View>
            <Content style={styles.container}>
                <DropDown
                    label="Employee"
                    placeholder={'Employee'}
                    value={emp_name}
                    onPress={() => updateState({ showEmployeeCodeList: true })}
                    error={employeeCodeError}
                />
                <DatePicker
                    label="From Date"
                    onPress={showCalendar}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={fromDate}
                />
                <DropDown
                    label="Bank"
                    placeholder={'Bank'}
                    value={bankAcc}
                    onPress={() => updateState({ showBankList: true })}
                    error={bankAccError}
                />
                <InputField
                    label="Cheque Number"
                    placeholder={'Cheque Number'}
                    onChangeText={(v) => updateState({
                        chequeNumber: v,
                        chequeNoError: false,
                        errMsg: '',
                    })}
                    value={chequeNumber}
                    error={chequeNoError}
                />
                <DatePicker
                    label="Cheque Date"
                    onPress={showCalendar_cheque}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={chequeDate}
                />
                <InputField
                    label="Amount"
                    placeholder={'Amount'}
                    keyboardType={'decimal-pad'}
                    onChangeText={(v) => updateState({
                        amount: v,
                        amountError: false,
                        errMsg: '',
                    })}
                    value={amount}
                    error={amountError}
                />
                <InputField
                    label="Installment"
                    placeholder={'installment'}
                    keyboardType={'decimal-pad'}
                    onChangeText={(v) => updateState({
                        installment: v,
                        installmentError: false,
                        errMsg: '',
                    })}
                    value={installment}
                    error={installmentError}
                />
                {errMsg ?
                    <Error
                        error={errMsg}
                    /> : null
                }
                <View style={styles.btnView}>
                    <SaveBtn
                        onPress={saveLoan}
                        title={'Save'}
                        saving={sending}
                    />
                    <CancelBtn
                        title={'Cancel'}
                        onPress={cancelLoan}
                    />
                    <DocPicker
                        onPress={selectDocument}
                        fileType={fileType}
                        filePath={filePath}
                        cancelFile={cancelDocument}
                    />
                </View>
            </Content>

            <DropdownModal
                isVisible={showEmployeeCodeList}
                closeFromModal={closeEmployeeCodeList}
                data={filteredEmployee}
                onChangeText={handleSearchEmployeeCode}
                selectValue={selectCode}
            />
            <_Calendar
                is_Visible={isCalendarVisible}
                hideDatePicker={showCalendar}
                onDayPress={onDayPress}
                currentDate={currentDate}
                markedDates={markedDates}
                minDate={startDay}
                maxDate={endDay}
            />
            <_Calendar
                is_Visible={isCalendar_Visible_cheque}
                hideDatePicker={showCalendar_cheque}
                onDayPress={onDayPress_cheque}
                currentDate={currentDate}
                markedDates={markedDates_cheque}
                maxDate ={last_date}
                minDate={c_startDay}
                maxDate={c_endDay}

            />

            <DropdownModal
                isVisible={showBankList}
                closeFromModal={closeBankModal}
                data={filteredBankAcc}
                onChangeText={handleSearchBank}
                selectValue={selectBank}
                maxDate ={last_date}
            />

            <DialogBox ref={dialogboxRef} />
        </Container>
    )
}
const mapStateToProps = (state) => ({
    user: state.User.userInfo,
    emp_dd: state.Dropdown.employeeDDs,
    from_dd:state.Dropdown.FromToDD,
})
const mapDispatchToProps = (dispatch) => ({
    
});

export default connect(mapStateToProps, mapDispatchToProps)(Loan_edit)

