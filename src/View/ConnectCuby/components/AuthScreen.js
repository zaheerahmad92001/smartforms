import React, {PureComponent} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  StatusBar,
  Image,
  Text,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {AuthService} from '../services';
import {users , credentials} from '../config';
import ConnectyCube from 'react-native-connectycube';
import NotificationHandler from '../../ConnectCuby/PushNotification_service'
import messaging from '@react-native-firebase/messaging'


// const pushNotification = new PushNotificationService()


export default class AuthScreen extends React.Component {


  componentDidMount(){
    NotificationHandler.createChannel()
   this.checkNotificationPermission()
  }
  

   checkNotificationPermission = async ()=>{
    messaging().hasPermission()
         .then(enabled => {
           if (enabled) {
             console.log("Permission granted");
              this.getFCMToken();

           } else {
             console.log("Request Permission");
              this.requestPermission_For_Notification();
           }
         });
     }

    async  requestPermission_For_Notification() {
        messaging().requestPermission()
         .then(() => {
           this.getFCMToken();
         })
         .catch(error => {
           console.log('permission rejected');
         });
    }

    async getFCMToken () {
        let fcmToken = await messaging().getToken();
         if (fcmToken) {
            console.log("fcmToken: ", fcmToken);
         }
     }

  state = {isLogging: false};
  init = () => ConnectyCube.init(credentials);

  setIsLogging = isLogging => this.setState({isLogging});
 

  login = async (currentUser) => {

    const _onSuccessLogin = () => {
      const {navigation} = this.props;
      const opponentsIds = users
        .filter(opponent => opponent.id !== currentUser.id)
        .map(opponent => opponent.id);

      navigation.push('VideoScreen', {opponentsIds});
    };


    const _onFailLogin = (error) => {
        console.log('errror', error)
      alert(`Error.\n\n${JSON.stringify(error)}`);
    };

    this.setIsLogging(true);

    AuthService.login(currentUser)
      
      .then(_onSuccessLogin)
      .catch(_onFailLogin)
      .then(() => this.setIsLogging(false));
  };

 

  render() {
    const {isLogging} = this.state;
    const logoSrc = require('../assets/logo.png');

    return (
      <View style={[styles.container, styles.f1]}>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <SafeAreaView style={[styles.centeredChildren, styles.f1]}>
          <Image resizeMode="contain" source={logoSrc} style={styles.logoImg} />
          <View
            style={[
              styles.f1,
              styles.centeredChildren,
              {flexDirection: 'row'},
            ]}>
            <Text>{isLogging ? 'Connecting... ' : 'Video Chat'}</Text>
            {isLogging && <ActivityIndicator size="small" color="#1198d4" />}
          </View>
        </SafeAreaView>
        <SafeAreaView style={[styles.authBtns, styles.f1]}> 
          {users.map(user => (
            <TouchableOpacity key={user.id} onPress={() => this.login(user)}>
              <View
                style={[styles.authBtn(user.color), styles.centeredChildren]}>
                <Text style={styles.authBtnText}>
                  {`Log in as ${user.name}`}
                </Text>
              </View>
            </TouchableOpacity>
          ))}
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  f1: {
    flex: 1,
  },
  centeredChildren: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    backgroundColor: 'white',
  },
  logoImg: {
    width: '90%',
    height: '80%',
  },
  authBtns: {
    justifyContent: 'flex-end',
    marginBottom: 20,
  },
  authBtn: backgroundColor => ({
    backgroundColor,
    height: 50,
    borderRadius: 25,
    marginHorizontal: 25,
    marginVertical: 5,
  }),
  authBtnText: {
    color: 'white',
    fontSize: 20,
    fontWeight: '700',
  },
});