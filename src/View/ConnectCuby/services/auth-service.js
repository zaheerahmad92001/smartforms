import { Platform } from 'react-native';
import ConnectyCube from 'react-native-connectycube';
import {credentials ,CONFIG} from '../config';
import DeviceInfo,{ getUniqueId, getManufacturer } from 'react-native-device-info';


export default class AuthService {
  init = () => {
      // ConnectyCube.init(credentials ,CONFIG);
      ConnectyCube.init(credentials ,);
    }

  login = user => {
    this.init()
    return new Promise((resolve, reject) => {
      ConnectyCube.createSession(user)
        .then((session) =>{

          // const params = {
          //   notification_channel: Platform.OS === 'ios' ? 'apns' : 'gcm',
          //   device: {
          //     platform: Platform.OS,
          //     udid: DeviceInfo.getUniqueId()
          //   },
          //   push_token: {
          //     environment: __DEV__ ? 'development' : 'production',
          //     client_identification_sequence: 'dWxSSbjSTo2ceP5ys_jljO:APA91bGZ9YlmYmmstY-MkIwBMBNjQpbC9XFZnAvjUK945WrE9xLsHRvriQhA2WwIxPuhadTWfIrGt0Ekvt908O8zLomlSoVEJSMv_9iZujMaIcUTWypSGt0ir12-QV2Jk4xUYO1zZunJ',
          //     bundle_identifier: "com.your.app.package.id"
          //   }
          // }
        
          // ConnectyCube?.pushnotifications?.subscriptions.create(params)
          //   .then(result => {
          //       console.log('subscription result', result)
          //   })
          //   .catch(error => {
          //       console.log('subscription error' , error)
          //   });

          ConnectyCube.chat.connect({
            userId: user.id,
            password: user.password,
          })
        }) 
        .then(resolve)
        .catch(reject);
    });
  };

  logout = () => {
    ConnectyCube.chat.disconnect();
    ConnectyCube.destroySession();
  };
}