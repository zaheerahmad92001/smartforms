import React, { Component } from "react";
import PushNotification, { Importance } from "react-native-push-notification";
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import messaging from '@react-native-firebase/messaging'
import ConnectyCube from 'react-native-connectycube'
import DeviceInfo,{ getUniqueId, getManufacturer } from 'react-native-device-info';
import { Platform } from "react-native";


class PushNotificationService extends Component {

    constructor(props){
        super(props)
    }

    subcribeToPushNotification (deviceToken){
        
        // console.log('here is device info' , DeviceInfo.getUniqueId())
        //   const params = {
        //     notification_channel: Platform.OS === 'ios' ? 'apns' : 'gcm',
        //     device: {
        //       platform: Platform.OS,
        //       udid: DeviceInfo.getUniqueId()
        //     },
        //     push_token: {
        //       environment: __DEV__ ? 'development' : 'production',
        //       client_identification_sequence: deviceToken,
        //       bundle_identifier: "com.your.app.package.id"
        //     }
        //   }
        
        // console.log('ConnectYcube', params)

        //   ConnectyCube?.pushnotifications?.subscriptions.create(params)
        //     .then(result => {
        //         console.log('subscription result', result)
        //     })
        //     .catch(error => {
        //         console.log('subscription error' , error)
        //     });
        }


    createChannel() {
        PushNotification.createChannel(
            {
                channelId: "customChinal", // (required)
                channelName: "My channel", // (required)
            },
            (created) => console.log(`createChannel returned '${created}'`)// (optional) callback returns whether the channel was created, false means it already existed.

        );
    }
    onRegister(token) {
        console.log('onRegistration:');
        const deviceToken = token.token;
        
         NotificationHandler.subcribeToPushNotification(deviceToken);
    }

    onNotification(notification) {
        console.log('onNotification', notification)
    }

    onAction(notification) {
        console.log('onAction', notification)
    }
    onRegistrationError(notification) {
        console.log('onRegistrationError', notification)

    }



}



const NotificationHandler = new PushNotificationService();


PushNotification.configure({

    onRegister: NotificationHandler.onRegister,
    // onRegister: function(token) {
    //     console.log("TOKEN:", token);
      
    //     const deviceToken = token.token;
      
    //     NotificationHandler.subcribeToPushNotification(deviceToken);
    //   },
    onNotification: NotificationHandler.onNotification,
    onAction: NotificationHandler.onAction,
    onRegistrationError: NotificationHandler.onRegistrationError,

    senderID: '1082030082725',
    permissions: {
        alert: true,
        badge: true,
        badge: true,
        sound: true,
    },
    popInitialNotification: true,

    requestPermissions: true,
});

export default NotificationHandler;
