import React, { useEffect, useRef, useReducer } from 'react'
import { View, Text, FlatList } from 'react-native'
import { Container, Content, Icon, Item } from "native-base";
import AppHeader from "../../Components/AppHeader";
import SaveBtn from "../../Components/SaveBtn";
import styles from './styles'
import DropdownModal from '../../Components/dropDownModal';
import DropDown from '../../Components/Dropdown';
import _Calendar from '../../Components/Calendar';
import DatePicker from "../../Components/DatePicker";
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { smallText } from '../../Constants/FontSize';
import DialogBox from 'react-native-dialogbox';
import { NetworkInfo } from "react-native-network-info";
import { SERVER_ERROR, status } from "../../Constants/constValues";
import axios from "axios";
import { baseUrl } from "../../Constants/server";
import { Department_dd } from '../../redux/actions/Dropdowns'
import { connect } from "react-redux";
import SearchComponent from '../../Components/SearchComponent';
import { Keyboard } from 'react-native';
import moment from 'moment'
import { ActivityIndicator } from 'react-native';
import { appColor } from '../../Constants/Colors';
import BottomTabs from '../../Components/BottomTab';


let filteredDep = ''
// let _data = ['aaaa', 'bbbb', 'cccc', 'dddd', 'eeee', 'aaaa', 'bbbb', 'cccc', 'dddd', 'eeee']

function LoanDeduction({ navigation, user, saveDepartment }) {
    let dialogboxRef = useRef()
    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.access_token}`
            },
            dep_id: '',
            dep_name: '',
            dep_list: [],
            dep_dd: [],

            sending: false,
            fetching: false,

            installmentError: '',
            showDepList: false,
            depError: false,
            searchDep: '',

            markedDates: {},
            month: '',
            __monthError:false,
            m_firstDay: '',
            m_lastDay: '',

            // data: _data,
            currentDate: new Date(),
            isCalendarVisible: false,
            errMsg: '',
            isdropdownLoading: false,
            deductionList: [],
            ipAddress: '',
            lastDayOfMonth: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0)


        }
    )

    const {
        dep_id,
        dep_name,
        dep_list,
        dep_dd,

        installmentError,
        showDepList,
        depError,
        searchDep,

        sending,
        fetching,

        markedDates,
        month,
        __monthError,

        currentDate,
        isCalendarVisible,
        errMsg,
        isdropdownLoading,
        headers,

        m_firstDay,
        m_lastDay,
        deductionList,
        ipAddress,
        lastDayOfMonth,

    } = state

    useEffect(() => {

        NetworkInfo.getIPAddress().then(ip => {
            updateState({ ipAddress: ip })
        });

        load_Department()

        let d = new Date().getDate()
        let m = new Date().getMonth() + 1
        let y = new Date().getFullYear()

        if (d <= 9) {
            d = '0' + d
        }
        if (m <= 9) {
            m = '0' + m
        }
        let date = `${y}-${m}-${d}`
        let _show = `${d}-${m}-${y}`



        var first_date = new Date(new Date().getFullYear(), new Date().getMonth(), 1);
        var last_date = new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0);


        let fday = moment.utc(first_date).local().format('YYYY-MM-DD')

        let l_day = moment.utc(last_date).local().format('YYYY-MM-DD')

        updateState({ m_firstDay: fday, m_lastDay: l_day })



        let selected = {}
        selected[date] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        updateState({
            month: _show, 
            // month: '',
            markedDates: selected,
            currentDate: date,
        })
        const unsubscribe = navigation.addListener('focus', async () => {
            updateState({dep_name:''})
        });
        return unsubscribe;


    }, [])




    function load_Department() {
        updateState({ isdropdownLoading: true })

        let tempVar = user?.sessions
        let formData = {
            userid: tempVar?.userid,
            companyid: tempVar?.companyid,
            locationid: tempVar?.locationid,
        }
        axios.post(`${baseUrl}/attendance/depts`, formData, {
            headers: headers
        }).then((response) => {
            const { data } = response
            console.log('department ', data)
            if (data.status === status) {

                /********* Redux  *********/
                saveDepartment(data.depts) // replace it wiht department

                let ddValue = []
                data.depts.map((item, index) => {
                    ddValue.push(item.vname) // replace it wiht department
                })
                updateState({
                    dep_list: ddValue,
                    dep_dd: data.depts, // replace it wiht department
                    isdropdownLoading: false
                })
            } else {
                updateState({ isdropdownLoading: false })
                alertMsg(SERVER_ERROR)
                console.log('error in else', err)
            }
        }).catch((err) => {
            updateState({ isdropdownLoading: false })
            alertMsg(SERVER_ERROR)
            console.log('error in catch', err)
        })
    }


    function openMenu() {
        Keyboard.dismiss()
        setTimeout(() => {
            navigation.openDrawer();
        }, 500)
    }

    function closeDepList() {
        updateState({ showDepList: false })
    }

    const handleSearchDep = (value) => {
        updateState({ searchDep: value })
    }

    function selectDepartment(value, index) {
        var FOUND = dep_dd.find(function (post, index) {
            if (post.vname == value) {
                return post;
            }
        })
        updateState({
            dep_id: FOUND.vcode,
            dep_name: value,
            errMsg: '',
            depError: false,
            showDepList: false,
            searchDep: ''
        })
    }

    function showCalendar() {
        let isVisible = isCalendarVisible
        updateState({ isCalendarVisible: !isVisible })
    }

    function onDayPress(date) {

        let fullYear = date.year
        let fullMonth = date.month
        var my_date = new Date(fullYear, fullMonth);
        var first_date = new Date(my_date.getFullYear(), my_date.getMonth(), 1);
        var last_date = new Date(my_date.getFullYear(), my_date.getMonth() + 1, 0);
        // console.log('last', last_date); 


        let _first = moment.utc(first_date).local().format('YYYY-MM-DD HH:mm:ss')
        let [fday, _time] = _first.split(' ')


        let _last = moment.utc(last_date).local().format('YYYY-MM-DD HH:mm:ss')
        let [l_day, l_time] = _last.split(' ')

        updateState({ m_firstDay: fday, m_lastDay: l_day })

        // console.log('first day' , fday , 'last day' , l_day)


        let [y, m, d] = date.dateString.split('-')
        let _show = `${d}-${m}-${y}`

        let selected = {}
        selected[currentDate] = {
            customStyles: {
                container: styles.currentDateContainer,
                text: styles.currentDateText
            }
        }
        selected[date.dateString] = {
            customStyles: {
                container: styles.dateContainer,
                text: styles.dateText
            }
        }

        updateState({
            markedDates: selected,
            month: _show,
            isCalendarVisible: false,
            currentDate: date.dateString,
        })
    }

    function handleDeduction(amount, value, index) {
        //    console.log('send index' , amount ,)
        let temp = deductionList.slice()
        temp.map((item, i) => {
            if (index === i) {
                temp[i].deduct_amount = Number(amount)
                temp[i].finalized = 1
            }
            updateState({ deductionList: temp })
        })
    }

    function handleFinalized(value, index) {
        //   console.log('cccc',Number(value.installment))
        let f = ''
        if (value.finalized == 1) {
            f = 0
        } else if (value.finalized == 0) {
            f = 1
        }

        let temp = deductionList.slice()
        temp.map((item, i) => {
            if (index === i) {
                temp[i].deduct_amount = value.installment
                temp[i].finalized = f
            }
            updateState({ deductionList: temp })
        })

    }

    function alertMsg(msg1, msg2) {
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2],
            btn: {
                text: 'OK',
                callback: () => { },
            },
        })

    }

    const renderItem = ({ item, index }) => {
        let i = index + 1
        return (
            <SearchComponent
                key={index}
                item={item}
                index={i}
                onChangeText={(text) => handleDeduction(text, item, index)}
                handleFinalized={() => handleFinalized(item, index)}
            />
        )
    }

    function handleLoanDeduction() {
        let err = false
        if (!m_firstDay || !m_lastDay) {
            // updateState({__monthError:true})
            err = true
            alertMsg('Please Select Month')
        } else {

            updateState({ fetching: true })

            let tempVar = user?.sessions
            let companyid = tempVar?.companyid
            let locationid = tempVar?.locationid

            let formData = {
                deptid: dep_id,
                userid: tempVar?.userid,
                companyid: companyid,
                locationid: locationid,
                datein: m_firstDay,
                dateout: m_lastDay,
                employeeid: 0
            }

            axios.post(`${baseUrl}/attendance/sal-loan-deduct`, formData, {
                headers: headers
            }).then((response) => {
                const { data } = response
                if (data.status === status) {
                    console.log('Loan deduction is here  ', data.sal_loan_deduct)

                    let temp = []
                    data?.sal_loan_deduct.map((item, i) => {
                        temp.push({ ...item, deduct_amount: item.installment, finalized: 0 })
                    })
                    updateState({
                        fetching: false,
                        deductionList: temp
                    })
                } else {
                    updateState({ fetching: false })
                    alertMsg(data?.message)
                    console.log('error in else ', data)
                }
            }).catch((error) => {
                console.log('err in catch', error)
                updateState({ fetching: false })
                alertMsg(SERVER_ERROR)
            })
        }
    }

    function UpdateRecords(){

        dialogboxRef.current?.confirm({
            title: 'Message',
            content: ['Are You Sure You want to Update Record!'],
            ok: {
                text: 'Yes',
                style: styles.yesBtnStyle,
                callback: () => { saveLoanDeduction() },
            },
            cancel: {
                text: 'No',
                style: styles.noBtnStyle,
                callback: () => { },
            },
        });
    }


    function saveLoanDeduction() {
        //api
        let err = false
        if (!m_firstDay || !m_lastDay) {
            err = true
            alertMsg('Please Select Month')
        }else{
        updateState({ sending: true })
        let tempVar = user?.sessions
        let companyid = tempVar?.companyid
        let locationid = tempVar?.locationid
        let insertedby = tempVar?.userid
        let updatedby = tempVar?.userid
        
        let temp = []
        deductionList.map((item, i) => {
            console.log('empoyee id' ,item.employeeid)
            let obj = {
                employeeid:item.employeeid,
                amount: item?.deduct_amount, //discuss
                accountid:item?.accountid, //discuss
                chequeno:item?.chequeno, // discuss
                chequedate:item?.chequedate, /// discuss
                installment: item?.installment,
                datein:m_firstDay,  //start of the month
                dateout:m_lastDay,   // end of month
                companyid: companyid,
                locationid: locationid,
                insertedby: insertedby,
                insertedip: ipAddress,
                updatedby: updatedby,
                updatedip: ipAddress,
                finalized:item?.finalized,
                loanid:item?.loanid,
                id: item?.id,

            }
            temp.push(obj)
        })

        let att = JSON.stringify(temp)
        let formData = {
            data: att,
            date: month
        }
        console.log('data sending to server', formData)
        axios.post(`${baseUrl}/attendance/sal-loan-deduct/create`, formData, {
            headers: headers
        }).then((response) => {
            const { data } = response
            if (data.status === status) {
                console.log('Loan deduction created ', data)
                updateState({
                    sending: false,
                    // emp_Attendance: data?.attendance
                })
                alertMsg('Loan Deducted Successfully')
            } else {
                updateState({ sending: false })
                alertMsg(data?.message)
                console.log('error in else ', data)
            }
        }).catch((error) => {
            console.log('err in catch', error)
            updateState({ sending: false })
            alertMsg(SERVER_ERROR)
        })
    }

    }

    /*********************** custom  dd Bank  *************************/
    const filteredDep_Data = (query) => {
        if (query.search(/[\[\]?*+|{}\\()@.\n\r]/) == -1) {
            if (query === '') {
                return dep_list
            }
            const regex = new RegExp([query.trim()], 'i')
            return dep_list.filter((c) => c.search(regex) >= 0)
        } else {
            console.log('invalid query', query)
        }
    }

    filteredDep = filteredDep_Data(searchDep)

    return (
        <Container>
            <AppHeader
                rightIconName={'search'}
                rightIconType={'EvilIcons'}
                // rightBtnPress={navigateTo}
                headingText={'Loan Deduction'}
                leftBtnPress={openMenu}
                dashbord={false}
                heading={true}
                search={false}
                edit={true}
            />
            <View style={[styles.blueBG]}></View>
            {/* <View style={styles.container}> */}
             <Content style={styles.container}>
                <DropDown
                    label="Department"
                    placeholder={'Department'}
                    value={dep_name}
                    onPress={() => updateState({ showDepList: true })}
                    error={depError}
                />
                <DatePicker
                    label="Month"
                    onPress={showCalendar}
                    iconName={'calendar-month-outline'}
                    iconType={'MaterialCommunityIcons'}
                    date={month}
                    error={__monthError}
                />
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>

                    <SaveBtn
                        title={'Save'}
                        onPress={UpdateRecords}
                        saving={sending}
                        buttonContainer={{ top: 5, width: wp(30) }}
                        textStyle={{ fontSize: smallText }}
                    />
                    <SaveBtn
                        title={'Fetch Record'}
                        onPress={handleLoanDeduction}
                        saving={fetching}
                        buttonContainer={{ top: 5, width: wp(30) }}
                        textStyle={{ fontSize: smallText }}
                    />
                </View>

                {fetching ?
                    <View style={styles.indicator}>
                        <ActivityIndicator
                            color={appColor}
                            size={'small'}
                        />
                    </View> :
                    deductionList?.length > 0 ?
                <FlatList
                    data={deductionList}
                    scrollEnabled={false}
                    keyExtractor={(item) => item.loanid}
                    renderItem={renderItem}
                    style={{ marginTop: 10, }}
                />:
                <View style={[styles.indicator,{marginTop:50}]}>
                    <Text style={styles.nodata}>No data Found</Text>
                </View>
        }
</Content>

            {/* </View> */}

            <DropdownModal
                isVisible={showDepList}
                closeFromModal={closeDepList}
                data={filteredDep}
                onChangeText={handleSearchDep}
                selectValue={selectDepartment}
                isLoading={isdropdownLoading}
            />
            <_Calendar
                is_Visible={isCalendarVisible}
                hideDatePicker={showCalendar}
                onDayPress={onDayPress}
                currentDate={currentDate}
                markedDates={markedDates}
                maxDate ={lastDayOfMonth}
            />
            <BottomTabs
                navigation={navigation}
                jvPress={()=>navigation.navigate('journalVoucher')}
                purchasePress={()=>navigation.navigate('Purchase')}
                saleInvoicePress={()=>navigation.navigate('saleInvoice')}
                leaveApplyPress={()=>navigation.navigate('leaveApply')}
            />
            <DialogBox ref={dialogboxRef} />
        </Container>
    )


}

const mapStateToProps = (state) => ({
    user: state.User.userInfo,
})
const mapDispatchToProps = (dispatch) => ({
    saveDepartment: (d) => dispatch(Department_dd(d))
});

export default connect(mapStateToProps, mapDispatchToProps)(LoanDeduction)