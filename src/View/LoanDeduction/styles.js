import { Platform } from 'react-native';
import { StyleSheet } from 'react-native'
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { appColor, black, danger, darkGrey, lightBlack, lightYellow, white } from "../../Constants/Colors";
import { LatoBlack, LatoRegular } from "../../Constants/Fonts";
import { largeText, mediumText } from "../../Constants/FontSize";

export default styles = StyleSheet.create({
    // container: {
    //     marginHorizontal: 15,
    //     paddingTop: 15,
    // },
    container: {
      marginHorizontal: 15,
      paddingTop: 15,
      flex:1,
      backgroundColor:'white'
  },
    blueBG: {
        backgroundColor: appColor,
        height: Platform.OS == 'android' ? hp(4) : hp(4),
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
    },
    
    
    currentDateContainer:{
        backgroundColor:white,
        borderRadius:5,
        borderColor:'#007BFF',
        borderWidth:1
      },
      currentDateText: {
        color:appColor,
        fontWeight: '500'
      },
      dateText:{
        color:white,
        fontWeight: '500'
      },
      dateContainer:{
        backgroundColor:appColor,
        borderRadius:5,
      },
      indicator:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
    },
    nodata:{
        color:lightBlack,
        fontSize:mediumText,
        fontWeight:'500'
    },
    yesBtnStyle: {
      color: danger,
      fontSize:14,
      fontWeight:'600'
  },
  noBtnStyle: {
      color: appColor,
      fontSize:14,
      fontWeight:'600'
  },

})