import * as React from 'react';
import { StyleSheet, Image, View, Text, } from 'react-native'
import { Icon } from 'native-base'

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import Splash from '../View/Splash'
import onboarding from '../View/OnBoarding'
import login from "../View/Login";
import dashboard from "../View/Dashboard";
// import search from "../View/Search";

import accountTransfer from '../View/AccountTransfer'
import Acc_Edit from '../View/AccountTransfer/Acc_Edit'
import VoucherSearch from '../View/AccountTransfer/VoucherSearch'

// import accTransferSearch from '../View/Acc_TransferSearch'
import journalVoucher from '../View/JournalVoucher'
import Jv_search from '../View/JournalVoucher/JV_search'
import Jv_edit from '../View/JournalVoucher/jv_Edit'

import expenses from '../View/Expenses'
import Exp_Search from "../View/Expenses/Exp_Search";
import Exp_edit from "../View/Expenses/Exp_edit";

import Purchase from '../View/Purchase'
import Purchase_search from '../View/Purchase/Purchase_search';
import Purchase_edit from '../View/Purchase/Purchase_edit'

import IssueNotes from '../View/IssueNotes'
import IssueNote_search from '../View/IssueNotes/IssueNote_Search'
import IssueNote_edit from '../View/IssueNotes/IssueNote_edit'

import mortality from '../View/Mortality'
import mortality_search from '../View/Mortality/Mortality_search'
import mortality_edit from '../View/Mortality/Mortality_edit'

import wasted from '../View/Wasted'
import wasted_search from '../View/Wasted/Wasted_search'
import wasted_edit from '../View/Wasted/Wasted_edit'

import serviceInvoice from '../View/ServiceInvoice'
import serviceInv_search from '../View/ServiceInvoice/ServiceInv_search'
import serviceInv_edit from '../View/ServiceInvoice/Service_edit'

import saleInvoice from '../View/SaleInvoice'
import SaleInv_search from '../View/SaleInvoice/sale_Inv_search'
import SaleInv_edit from '../View/SaleInvoice/Sale_Inv_edit'

import Reproduction from '../View/Reproduction'
import Repro_search from '../View/Reproduction/Repro_Search'
import Repro_edit from '../View/Reproduction/Repro_edit'


import outwardgatepass from '../View/OutwardGatePass'
import OGP_search from '../View/OutwardGatePass/OGP_Search'
import OGP_edit from '../View/OutwardGatePass/OGP_edit'

import inwardGatePass from '../View/InwardGatePass'
import IGP_search from '../View/InwardGatePass/IGP_search'
import IGP_edit from '../View/InwardGatePass/IGP_edit'

import attendance from '../View/Attendance'

import leaveApply from '../View/LeaveApply'
import leave_search from '../View/LeaveApply/Leave_Search'
import leave_edit from '../View/LeaveApply/Leave_edit'


import advanceSalary from '../View/AdvanceAgainstSalary';
import advanceSalary_search from '../View/AdvanceAgainstSalary/Adv_salary_search';
import advanceSalary_edit from '../View/AdvanceAgainstSalary/Adv_salary_edit'

import loanEntry from '../View/LoanEntry';
import loanEntry_search from '../View/LoanEntry/Loan_search';
import loanEntry_edit from '../View/LoanEntry/Loan_edit'



import LoanDeduction from '../View/LoanDeduction';
import salaryPosting from '../View/SalaryPosting'

import closingMonthDetail from '../View/ClosingMonthDetail'
import closingMonth_search from '../View/ClosingMonthDetail/ClosingMonth_search'
import closingMonth_edit from '../View/ClosingMonthDetail/ClosingMonth_edit'

// import PDCChequPosting from '../View/PDCChequePosting'

import CustomSidebarMenu from '../Components/CustomSidebarMenu';
import { black } from '../Constants/Colors';

import AuthScreen from '../View/ConnectCuby/components/AuthScreen';
import VideoScreen from '../View/ConnectCuby/components/VideoScreen';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();


function SplashNavigator() {
    return (

         <Stack.Navigator initialRouteName={"AuthScreen"} headerMode={'none'}>
             <Stack.Screen name={'AuthScreen'} component={AuthScreen} />
             <Stack.Screen name={'VideoScreen'} component={VideoScreen} />
         </Stack.Navigator>

        // <Stack.Navigator initialRouteName={"Splash"} headerMode={'none'}>
        //     <Stack.Screen name={'Splash'} component={Splash} />
        // </Stack.Navigator>
    )
}


function OnBoardingNavigator() {
    return (
        <Stack.Navigator initialRouteName={"onboarding"} headerMode={'none'}>
            <Stack.Screen name={'onboarding'} component={onboarding} />
        </Stack.Navigator>
    )
}

function AuthNavigator() {
    return (
        <Stack.Navigator initialRouteName={"login"} headerMode={'none'}>
            <Stack.Screen name={'login'} component={login} />
        </Stack.Navigator>
    )
}


function InventoryNav() {
    return (
        <Stack.Navigator initialRouteName={"dashboard"} headerMode={'none'}>

            <Stack.Screen name={'dashboard'} component={dashboard} />

            <Stack.Screen name={'VoucherSearch'} component={VoucherSearch} />
            <Stack.Screen name={'accountTransfer'} component={accountTransfer} />
            <Stack.Screen name={'Acc_Edit'} component={Acc_Edit} />

            <Stack.Screen name={'Jv_search'} component={Jv_search} />
            <Stack.Screen name={'Jv_edit'} component={Jv_edit} />
            <Stack.Screen name={'journalVoucher'} component={journalVoucher} />

            <Stack.Screen name={'Exp_Search'} component={Exp_Search} />
            <Stack.Screen name={'Exp_edit'} component={Exp_edit} />
            <Stack.Screen name={'expenses'} component={expenses} />

            <Stack.Screen name={'Purchase'} component={Purchase} />
            <Stack.Screen name={'Purchase_search'} component={Purchase_search} />
            <Stack.Screen name={'Purchase_edit'} component={Purchase_edit} />

            <Stack.Screen name={'IssueNotes'} component={IssueNotes} />
            <Stack.Screen name={'IssueNote_search'} component={IssueNote_search}/>
            <Stack.Screen name={'IssueNote_edit'} component={IssueNote_edit}/>

            <Stack.Screen name={'mortality'} component={mortality} />
            <Stack.Screen name={'mortality_search'} component={mortality_search}/>
            <Stack.Screen name={'mortality_edit'} component={mortality_edit}/>

            <Stack.Screen name={'wasted'} component={wasted}/>
            <Stack.Screen name={'wasted_search'} component={wasted_search}/>
            <Stack.Screen name={'wasted_edit'} component={wasted_edit}/>

            <Stack.Screen name={'serviceInvoice'} component={serviceInvoice}/>
            <Stack.Screen name={'serviceInv_search'} component={serviceInv_search}/>
            <Stack.Screen name={'serviceInv_edit'} component={serviceInv_edit}/>

            <Stack.Screen name={'saleInvoice'} component={saleInvoice}/>
            <Stack.Screen name={'SaleInv_search'} component={SaleInv_search}/>
            <Stack.Screen name={'SaleInv_edit'} component={SaleInv_edit}/>

            <Stack.Screen name={'Reproduction'} component={Reproduction} />
            <Stack.Screen name={'Repro_search'} component={Repro_search} />
            <Stack.Screen name={'Repro_edit'} component={Repro_edit} />


            <Stack.Screen name={'outwardgatepass'} component={outwardgatepass} />
            <Stack.Screen name={'OGP_search'} component={OGP_search} />
            <Stack.Screen name={'OGP_edit'} component={OGP_edit} />

            <Stack.Screen name={'inwardGatePass'} component={inwardGatePass} />
            <Stack.Screen name={'IGP_search'} component={IGP_search} />
            <Stack.Screen name={'IGP_edit'} component={IGP_edit} />

            <Stack.Screen name={'attendance'} component={attendance} />

            <Stack.Screen name={'leaveApply'} component={leaveApply} />
            <Stack.Screen name={'leave_search'} component={leave_search} />
            <Stack.Screen name={'leave_edit'} component={leave_edit} />

            <Stack.Screen name={'advanceSalary'} component={advanceSalary} />
            <Stack.Screen name={'advanceSalary_search'} component={advanceSalary_search} />
            <Stack.Screen name={'advanceSalary_edit'} component={advanceSalary_edit} />


            <Stack.Screen name={'loanEntry'} component={loanEntry} />
            <Stack.Screen name={'loanEntry_search'} component={loanEntry_search} />
            <Stack.Screen name={'loanEntry_edit'} component={loanEntry_edit} />


            <Stack.Screen name={'LoanDeduction'} component={LoanDeduction} />
            <Stack.Screen name={'salaryPosting'} component={salaryPosting} />

            <Stack.Screen name={'closingMonthDetail'} component={closingMonthDetail} />
            <Stack.Screen name={'closingMonth_search'} component={closingMonth_search} />
            <Stack.Screen name={'closingMonth_edit'} component={closingMonth_edit} />

        </Stack.Navigator>
    )
}




function MyDrawer() {
    return (
        <Drawer.Navigator
            drawerStyle={{ width: '80%', }}
            initialRouteName={'InventoryNav'}
            drawerContent={(props) => <CustomSidebarMenu {...props} />}
        >
            <Drawer.Screen name={'InventoryNav'} component={InventoryNav} />

        </Drawer.Navigator>
    );
}

function AppContainer() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="AuthScreen" headerMode={'none'}>
            {/* <Stack.Navigator initialRouteName="Splash" headerMode={'none'}> */}
                {/* <Stack.Screen name={'Splash'} component={SplashNavigator} /> */}
                <Stack.Screen name={'AuthScreen'} component={SplashNavigator} />
                <Stack.Screen name={'Onboarding'} component={OnBoardingNavigator} />
                <Stack.Screen name={'login'} component={AuthNavigator} />
                <Stack.Screen name={'MyDrawer'} component={MyDrawer} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}
export default AppContainer


const styles = StyleSheet.create({
    wraper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    activeView: {
        backgroundColor: black,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 8,
        borderRadius: 20,

    },
    label: {
        color: black,
        fontSize: 10,
        marginLeft: 5,
    },
    imgStyle: {
        width: 25,
        height: 25,
    },
    IconStyle: {
        fontSize: 20,
        color: black
    }
})
