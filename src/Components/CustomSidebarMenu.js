import React, { useState } from "react";
import { View, Text, StyleSheet, Pressable, Image, ScrollView } from "react-native";
import { Icon, Thumbnail, } from "native-base";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { appColor, black, borderColor, danger, jvcolor, lightBlack, white, yellow } from "../Constants/Colors";
import {
    userImg,
    journal,
    expense,
    accTransfer,
    account,
    inventory,
    sale,
    production,
    attendance,
    signoutIcon
} from "../Constants/images";
import { LatoBold, LatoRegular, } from "../Constants/Fonts";
import { largeText, mediumText, smallText, xlText } from "../Constants/FontSize";
import _Pressable from "../Components/Pressable";
import { Divider } from 'react-native-elements'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { USER, ON_BOARDING } from '../Constants/constValues'
import { StackActions } from '@react-navigation/native';
import { connect } from "react-redux";





function CustomSidebarMenu({ navigation , user}) {

    const [isAccoountList_Visible, setAccountList_Visible] = useState(false);
    const [isInventoryList_Visible, setInventoryList_Visible] = useState(false);
    const [isSalesList_Visible, setSalesList_Visible] = useState(false)
    const [isProductionList_Visible, setProductionList_Visible] = useState(false)
    const [isAttendanceList_Visible, setAttendanceList_Visible] = useState(false)

    function closeDrawer() {
        navigation.closeDrawer()
    }

    function navigateTo(routeName) {
        navigation.closeDrawer()
        navigation.navigate(routeName)
    }

function navigateTo_Inventory(routeName){
    navigation.navigate('InventoryNav',{
        screen:routeName
    })
}

    async function logOut() {
        await AsyncStorage.removeItem(USER)
        navigation.closeDrawer()
        setTimeout(() => {
            navigation.dispatch(StackActions.replace('login'));
        }, 1000)
    }

    function showAccountList() {

        if (isAccoountList_Visible) {
            setAccountList_Visible(false)
        } else {
            setAccountList_Visible(true)
        }
    }

    const showInventoryList = () => {

        if (isInventoryList_Visible) {
            setInventoryList_Visible(false)
        } else {
            setInventoryList_Visible(true)

        }
    }

    function showSalesList() {

        if (isSalesList_Visible) {
            setSalesList_Visible(false)
        } else {
            setSalesList_Visible(true)
        }
    }

    function showProductionList() {
        if (isProductionList_Visible) {
            setProductionList_Visible(false)
        } else {
            setProductionList_Visible(true)
        }
    }

    function showAttendanceList() {
        if (isAttendanceList_Visible) {
            setAttendanceList_Visible(false)
        } else {
            setAttendanceList_Visible(true)
        }
    }

    return (
        <ScrollView style={styles.container}>

            <View style={styles.blueBG}>
                <View style={styles.innerView}>
                    <Icon
                        onPress={closeDrawer}
                        name={'md-arrow-back-circle-outline'}
                        type={'Ionicons'}
                        style={{ color: white, fontSize: 25 }}
                    />
                    <Pressable
                        onPress={logOut}
                        style={styles.singOutView}>
                        <Text style={styles.signoutText}>Signout</Text>
                        <Image
                            source={signoutIcon}
                            resizeMode={'contain'}
                            style={styles.signOutImg}
                        />
                    </Pressable>

                </View>
            </View>

            <View style={styles.profileView}>
                <View style={styles.rowView}>
                    <Thumbnail
                        circular
                        large
                        source={userImg}
                    />
                    <View style={styles.nameView}>
                        <Text style={styles.nameText}>{user?.sessions?.employeename}</Text>
                        <Text style={styles.phoneText}>03327476323</Text>
                    </View>
                </View>
            </View>

            <View style={styles.balanceView}>
                <Text style={styles.totalBlnc}>Total Balance</Text>
                <Text style={styles.blncText}>RS 12000000.00</Text>
                <View style={styles.pressableContainer}>
                    <_Pressable
                        onPress={() => navigateTo_Inventory('journalVoucher')}
                        img={journal}
                        title={'Journal'}
                        title2={'Voucher'}
                        container={{ backgroundColor: jvcolor }}
                        titleText={{ color: white }}
                    />
                    <_Pressable
                        onPress={() => navigateTo_Inventory('expenses')}
                        img={expense}
                        title={'Expense'}
                        container={{ backgroundColor: danger }}
                        titleText={{ color: white }}
                    />
                    <_Pressable
                        onPress={() => navigateTo_Inventory('accountTransfer')}
                        img={accTransfer}
                        title={'Account'}
                        title2={'Transfer'}
                        container={{ backgroundColor: yellow }}
                        titleText={{ color: white }}
                    />
                </View>
            </View>

            <Text style={styles.menu}>Menu</Text>

            <Pressable onPress={showAccountList}>
                <View style={styles.headingView}>
                    <View style={styles.outerView}>
                        <View style={styles.blueView}>
                            <Image
                                source={account}
                                resizeMode={'contain'}
                                style={styles.imgStyle}
                            />
                        </View>
                        <Text style={styles.accHeading}>Account</Text>
                    </View>
                    <Icon
                        name={isAccoountList_Visible ? 'chevron-small-up' : 'chevron-small-down'}
                        type={'Entypo'}
                        style={styles.iconStyle}
                    />
                </View>
            </Pressable>

            {isAccoountList_Visible &&
                <View>
                    <Pressable
                        onPress={() => navigateTo_Inventory('accountTransfer')}>
                        <Text style={[styles.link, { marginTop: 10 }]}>Account Transfer</Text>
                    </Pressable>
                    <Divider
                        rientation="horizontal"
                        style={styles.divider} />

                    <Pressable
                        onPress={() => navigateTo_Inventory('journalVoucher')}
                    >
                        <Text style={styles.link}>Journal Voucher</Text>
                    </Pressable>
                    <Divider
                        rientation="horizontal"
                        style={styles.divider} />
                    <Pressable
                        onPress={() => navigateTo_Inventory('expenses')}>
                        <Text style={styles.link}>Expenses</Text>
                    </Pressable>
                </View>
            }

            <Pressable
                onPress={showInventoryList}
            >
                <View style={[styles.headingView]}>
                    <View style={styles.outerView}>
                        <View style={styles.blueView}>
                            <Image
                                source={inventory}
                                resizeMode={'contain'}
                                style={styles.imgStyle}
                            />
                        </View>
                        <Text style={styles.accHeading}>Inventory</Text>
                    </View>
                    <Icon
                        name={isInventoryList_Visible ? 'chevron-small-up' : 'chevron-small-down'}
                        type={'Entypo'}
                        style={styles.iconStyle}
                    />
                </View>
            </Pressable>

            {isInventoryList_Visible &&
                <View>
                    <Pressable
                        onPress={() => navigateTo_Inventory('Purchase')}>
                        <Text style={[styles.link, { marginTop: 10 }]}>Purchase</Text>
                    </Pressable>
                    <Divider
                        rientation="horizontal"
                        style={styles.divider} />


                    <Pressable
                        onPress={() => navigateTo_Inventory('IssueNotes')} >
                        <Text style={styles.link}>Issue Note</Text>
                    </Pressable>
                    <Divider
                        rientation="horizontal"
                        style={styles.divider} />


                    <Pressable
                        onPress={() => navigateTo_Inventory('mortality')}>
                        <Text style={[styles.link]}>Mortality</Text>
                    </Pressable>
                    <Divider
                        rientation="horizontal"
                        style={styles.divider} />

                    <Pressable
                        onPress={() => navigateTo_Inventory('wasted')}>
                        <Text style={[styles.link]}>Wasted</Text>
                    </Pressable>
                    <Divider
                        rientation="horizontal"
                        style={styles.divider} />

                    <Pressable
                        onPress={() => navigateTo_Inventory('serviceInvoice')}>
                        <Text style={[styles.link]}>Service Invoice</Text>
                    </Pressable>
                </View>
            }

            <Pressable onPress={showSalesList}>
                <View style={styles.headingView}>
                    <View style={styles.outerView}>
                        <View style={styles.blueView}>
                            <Image
                                source={sale}
                                resizeMode={'contain'}
                                style={styles.imgStyle}
                            />
                        </View>
                        <Text style={styles.accHeading}>Sales</Text>
                    </View>
                    <Icon
                        name={isSalesList_Visible ? 'chevron-small-up' : 'chevron-small-down'}
                        type={'Entypo'}
                        style={styles.iconStyle}
                    />
                </View>
            </Pressable>

            {isSalesList_Visible &&
                <View>
                    <Pressable
                        onPress={() => navigateTo_Inventory('saleInvoice')}>
                        <Text style={[styles.link, { marginTop: 10 }]}>Sale Invoice</Text>
                    </Pressable>

                </View>
            }

            <Pressable onPress={showProductionList}>
                <View style={styles.headingView}>
                    <View style={styles.outerView}>
                        <View style={styles.blueView}>
                            <Image
                                source={production}
                                resizeMode={'contain'}
                                style={styles.imgStyle}
                            />
                        </View>
                        <Text style={styles.accHeading}>Production</Text>
                    </View>
                    <Icon
                        name={isProductionList_Visible ? 'chevron-small-up' : 'chevron-small-down'}
                        type={'Entypo'}
                        style={styles.iconStyle}
                    />
                </View>
            </Pressable>
            {isProductionList_Visible &&
                <View>
                    <Pressable
                        onPress={() => navigateTo_Inventory('Reproduction')}>
                        <Text style={[styles.link, { marginTop: 10 }]}>ReProduction</Text>
                    </Pressable>
                    <Divider
                        rientation="horizontal"
                        style={styles.divider} />

                    <Pressable
                        onPress={() => navigateTo_Inventory('outwardgatepass')}>
                        <Text style={[styles.link]}>Outward gate pass</Text>
                    </Pressable>
                    <Divider
                        rientation="horizontal"
                        style={styles.divider} />

                    <Pressable
                        onPress={() => navigateTo_Inventory('inwardGatePass')}>
                        <Text style={[styles.link]}>Inward gate pass</Text>
                    </Pressable>
                </View>
            }


            <Pressable onPress={showAttendanceList}>
                <View style={styles.headingView}>
                    <View style={styles.outerView}>
                        <View style={styles.blueView}>
                            <Image
                                source={attendance}
                                resizeMode={'contain'}
                                style={styles.imgStyle}
                            />
                        </View>
                        <Text style={styles.accHeading}>Attendance & PayRoll</Text>
                    </View>
                    <Icon
                        name={isAttendanceList_Visible ? 'chevron-small-up' : 'chevron-small-down'}
                        type={'Entypo'}
                        style={styles.iconStyle}
                    />
                </View>
            </Pressable>
            {isAttendanceList_Visible &&
                <View>
                    <Pressable
                        onPress={() => navigateTo_Inventory('attendance')}>
                        <Text style={[styles.link, { marginTop: 10 }]}>Attendance Entry</Text>
                    </Pressable>
                    <Divider
                        rientation="horizontal"
                        style={styles.divider} />

                    <Pressable
                        onPress={() => navigateTo_Inventory('leaveApply')}>
                        <Text style={[styles.link]}>Leave Apply</Text>
                    </Pressable>
                    <Divider
                        rientation="horizontal"
                        style={styles.divider} />

                    <Pressable
                        onPress={() => navigateTo_Inventory('advanceSalary')}
                    // style={{ marginBottom:0 }}
                    >
                        <Text style={[styles.link]}>Advances</Text>
                    </Pressable>
                    <Divider
                        rientation="horizontal"
                        style={styles.divider} />

                    <Pressable
                        onPress={() => navigateTo_Inventory('loanEntry')}>
                        <Text style={[styles.link]}>Loan Entry</Text>
                    </Pressable>
                    <Divider
                        rientation="horizontal"
                        style={styles.divider} />

                   <Pressable
                        onPress={() => navigateTo_Inventory('LoanDeduction')}>
                        <Text style={[styles.link]}>Loan Deduction</Text>
                    </Pressable>
                    <Divider
                        rientation="horizontal"
                        style={styles.divider} />

                    <Pressable
                        onPress={() => navigateTo_Inventory('salaryPosting')}>
                        <Text style={[styles.link]}>Salary Posting</Text>
                    </Pressable>
                    <Divider
                        rientation="horizontal"
                        style={styles.divider} />

                    <Pressable
                        onPress={() => navigateTo_Inventory('closingMonthDetail')}>
                        <Text style={[styles.link]}>Closing Month Cheque Detail</Text>
                    </Pressable>
                    <Divider
                        rientation="horizontal"
                        style={styles.divider} />

                    {/* <Pressable
                        onPress={() => navigateTo('PDCChequPosting')}>
                        <Text style={[styles.link]}>PDC Cheque Posting</Text>
                    </Pressable>
                    <Divider
                        rientation="horizontal"
                        style={styles.divider} /> */}



                </View>
            }

        </ScrollView>
    )
}

const mapStateToProps = (state) => ({
    user: state.User.userInfo,
})
const mapDispatchToProps = (dispatch) => ({
});
export default connect(mapStateToProps, mapDispatchToProps)(CustomSidebarMenu)


const styles = StyleSheet.create({
    container: {
        //   backgroundColor:'red'
    },
    blueBG: {
        height: hp(15),
        backgroundColor: appColor,
        justifyContent: 'flex-end',
        // alignItems: 'flex-end',
        paddingBottom: 20,
        paddingRight: 20,
    },
    innerView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "space-between",
        marginLeft: 20,
    },
    singOutView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    signOutImg: {
        width: 18,
        height: 18,
        marginTop: 3,
    },
    signoutText: {
        color: white,
        fontSize: mediumText,
        marginRight: 10,
    },
    profileView: {
        backgroundColor: white,
        paddingVertical: 10,
    },
    rowView: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15,
        marginTop: 5,
    },
    nameView: {
        marginLeft: 10,
    },
    nameText: {
        color: black,
        fontFamily: LatoBold,
        fontSize: largeText,
    },
    phoneText: {
        color: black,
        fontWeight: '500',
        fontSize: mediumText,
        fontFamily: LatoRegular,
        marginTop: 3,
    },
    balanceView: {
        backgroundColor: appColor,
        paddingHorizontal: 15,
        paddingVertical: 15,
        // borderTopRightRadius:20,
        // borderBottomRightRadius:20,
    },
    totalBlnc: {
        color: white,
        fontFamily: LatoRegular,
        fontSize: smallText,
        fontWeight: '500',
    },
    blncText: {
        color: white,
        fontFamily: LatoRegular,
        fontWeight: '700',
        fontSize: xlText,
        marginTop: 5
    },
    pressableContainer: {
        marginTop: 15,
        paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    menu: {
        color: black,
        fontWeight: LatoRegular,
        fontSize: mediumText,
        fontWeight: '500',
        paddingLeft: 15,
        marginTop: 10,
    },
    headingView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginHorizontal: 15,
        marginTop: -2,
    },

    blueView: {
        width: 35,
        height: 35,
        borderRadius: 35 / 2,
        backgroundColor: appColor,
        marginTop: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },

    imgStyle: {
        width: 17,
        height: 17,
    },
    outerView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    accHeading: {
        marginLeft: 20,
        marginTop: 10,
        color: lightBlack,
        fontFamily: LatoBold,
        fontWeight: '600',
        fontSize: mediumText,
    },
    iconStyle: {
        marginTop: 10,
    },
    link: {
        paddingLeft: '23%',
        fontSize: mediumText,
        fontFamily: LatoRegular,
        color: lightBlack,
        fontWeight: '400',
        fontStyle: 'normal',
        paddingVertical: 3
    },
    divider: {
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: borderColor,
        marginTop: 10,
        marginBottom: 10,
    },


})