import React from "react";
import { TouchableOpacity } from "react-native";
import { Text, StyleSheet, View, } from "react-native";
import { Divider } from "react-native-elements/dist/divider/Divider";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { appColor, borderColor, darkGrey } from "../Constants/Colors";
import { LatoBlack, LatoRegular } from "../Constants/Fonts";
import { mediumText } from "../Constants/FontSize";
import InputField from "./InputField";
import TextView from "./TextView";
const SearchView = (props) => {
    const {serialNo ,item} = props
    return (
        <TouchableOpacity
            onLongPress={props.onLongPress}
            style={styles.wraper}>
            <View style={styles.row1}>
                <TextView
                    label="Sr#"
                    infoText={serialNo}
                    infoStyle={styles.infoStyle}
                    inputViewStyle={styles.inputViewStyle}
                />
                <TextView
                    label="VNo"
                    infoText={item.vno}
                    inputViewStyle={styles.inputViewStyle}
                />
            </View>
            <TextView
                label="Remarks"
                infoText={item.remarks}
                inputViewStyle={styles.remarks}
            />


        </TouchableOpacity>
    )
}
export default SearchView;

const styles = StyleSheet.create({
    wraper: {
        backgroundColor: 'white',
        marginBottom: 20,
        paddingVertical: 20,
        paddingHorizontal: 20
    },
    row1: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    inputViewStyle: {
        width: wp(42)
    },
    remarks: {
        width: wp(90)
    },
    infoStyle: {
        fontFamily: LatoBlack,
        fontWeight: '500',
        fontSize: mediumText
    },

})