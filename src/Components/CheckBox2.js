import React from "react";
import { View,Text, StyleSheet, Platform } from "react-native";
import {Icon} from "native-base";
import { appColor, darkGrey, grey, lightBlack } from "../Constants/Colors";
import {LatoBlack, LatoBold, LatoLight, LatoRegular} from "../Constants/Fonts";
import { mediumText } from "../Constants/FontSize";
const CheckBox2 =(props)=>{
    const {isCheck} = props
    return(
     <View style={[styles.container,props.checkboxContainer]}>
      {isCheck ?
       <Icon
        onPress={props.onPress}
        name={'check-square'}
        type={'Feather'}
        style={styles.activeIcon}
       />
      :
         <Icon
         onPress={props.onPress}
         name={'square'}
         type='Feather'
         style={styles.inactiveIcon}
         />
      }
     <Text style={styles.titleText}>{props.title}</Text>
    
     </View>
    )
}
export default CheckBox2;
const styles = StyleSheet.create({
    container:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
    },

    activeIcon: {
        color: appColor,
        fontSize: 22,
        fontFamily: LatoRegular,
        alignSelf:'center',
    },
    inactiveIcon: {
        color: appColor,
        fontSize:22,
        fontFamily: LatoRegular,
        alignSelf:'center',
    },

    titleText:{
     fontFamily:LatoRegular,
     fontSize:mediumText,
     fontWeight:Platform.OS=='ios'? '400' :'500',
     fontStyle:'normal',  
     color:lightBlack,
     marginLeft:8
    }
})