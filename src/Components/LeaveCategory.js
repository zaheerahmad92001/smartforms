import React from 'react'
import { Platform } from 'react-native'
import { View, Text, StyleSheet, Pressable } from 'react-native'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
import { appColor, black, darkGrey, grey, lightGrey, white } from '../Constants/Colors'
import { LatoBold, LatoLight ,LatoRegular } from '../Constants/Fonts'
import { mediumText } from '../Constants/FontSize'
const Category = (props) => {
    const {index ,item , selectedIndex}  = props
    // console.log('selected index' , selectedIndex) 
    return (
        <Pressable
         key={index}
         onPress={props.onPress}
            style={
                item?.id===selectedIndex?
                [styles.selectedIndex]
                 :
                [styles.container]
            }>
            <Text style={
                item?.id===selectedIndex?
                [styles.leaveTypeSeleced]
                :
                [styles.leaveType]
                }>
             {item.vname}
         </Text>

        </Pressable>
    )
}
export default Category

const styles = StyleSheet.create({
    container: {
        width:90,
        height:90,
        shadowOffset: { width:0, height:0},
        shadowColor:black,
        shadowOpacity:0.4,
        elevation: 1,
        backgroundColor:Platform.OS=='android'? '#FAFAFA':white,
        // paddingVertical: 10,
        paddingHorizontal: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius:17,
        marginTop:20,
        marginBottom:10,
        marginLeft:10,
        marginRight:20,
        // marginRight:wp(15),
        
    },
    selectedIndex:{
        width:90,
        height:90,
        backgroundColor:appColor,
        // paddingVertical: 10,
        paddingHorizontal: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 17,
        marginTop:20,
        marginBottom:10,
        marginLeft:10,  
        marginRight:20,
        // marginRight:wp(15),

    },
    leaveType:{
        color:darkGrey,
        fontSize:mediumText,
        fontFamily:LatoBold,
    },
    leaveTypeSeleced:{
        color:white,
        fontSize:mediumText,
        fontFamily:LatoBold,
    }
})