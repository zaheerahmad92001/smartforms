import React from 'react'
import { Image } from 'react-native';
import { Pressable, View, Text, StyleSheet } from 'react-native'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { appColor, danger, lightYellow, white } from '../Constants/Colors';
import { LatoRegular } from '../Constants/Fonts';
import { largeText } from '../Constants/FontSize';
import {file}from '../Constants/images'


import Doc from '../Constants/FileIcons/Doc'
import Excel from '../Constants/FileIcons/Excel';
import Jpg from '../Constants/FileIcons/Jpg';
import MP from '../Constants/FileIcons/Mp';
import Pdf from '../Constants/FileIcons/Pdf';
import PowerPoint from '../Constants/FileIcons/PowerPoint';
import FileIcon from '../Components/fileIcon'

const DocPicker = (props) => {
   const {fileType} =props

  let [img, type] = fileType.split('/')
   
    return (
        <View>
          {fileType ?  
          <FileIcon
            onPress={props.cancelFile}
            type={type}
            removable={true}
          />
          :
        <Pressable 
          style={styles.docPicker}
          onPress={props.onPress}
        >
            <Image
                resizeMode={'contain'}
                source={file}
                style={{ width: 30, height: 30 }}
            />
        </Pressable>
 }
        </View>
    )
}

export default DocPicker;

const styles = StyleSheet.create({
    docPicker:{
        width:wp(19),
        paddingVertical:5,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:5,
        backgroundColor:white,
        borderColor:lightYellow,
        borderWidth:2,
      },
})