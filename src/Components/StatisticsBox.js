import React from "react";
import {View,Text,StyleSheet,} from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { borderColor, lightBlack, lightRed, offWhite, white } from "../Constants/Colors";
import { LatoBlack, LatoBold, LatoRegular } from "../Constants/Fonts";
import { largeText, mediumText ,xsmallText,xlText } from "../Constants/FontSize";

const Statistics =(props)=>{
 return(
     <View style={styles.box}>
         <Text style={styles.heading}>{props.heading}</Text>
         <Text style={[styles.amount,props.style]}>{`RS ${props.amount}`}</Text>
     </View>
 )
}
export default Statistics

const styles = StyleSheet.create({
    box:{
        backgroundColor:white,
        paddingVertical:8,
        paddingHorizontal:15,
        borderRadius:10,
        elevation:2,
        shadowRadius:StyleSheet.hairlineWidth,
        shadowColor:'#000',
        shadowOffset:{height:0,width:0},
        shadowOpacity:0.8,
        width:wp(40)
    },
    heading:{
        color:borderColor,
        fontFamily:LatoBlack,
        fontSize:xsmallText,
        fontWeight:'700',
    },
    amount:{
        fontSize:largeText,
        fontWeight:'700',
        fontFamily:LatoBold,
        color:lightRed,
        marginTop:2
    }
})