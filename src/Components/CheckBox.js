import React from "react";
import { View,Text, StyleSheet, Platform } from "react-native";
import {Icon} from "native-base";
import { appColor, darkGrey, grey } from "../Constants/Colors";
import {LatoBlack, LatoBold, LatoLight, LatoRegular} from "../Constants/Fonts";
import { mediumText } from "../Constants/FontSize";
const CheckBox =(props)=>{
    const {isCheck} = props
    return(
     <View style={styles.container}>
      {isCheck ?
       <Icon
        onPress={props.onPress}
        name={'checksquareo'}
        type={'AntDesign'}
        style={styles.check}
       />
      :
         <Icon
         onPress={props.onPress}
         name={'square'}
         type='Feather'
         style={styles.unCheck}
         />
      }
     <Text style={styles.titleText}>{props.title}</Text>
    
     </View>
    )
}
export default CheckBox;
const styles = StyleSheet.create({
    container:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
    },

    unCheck:{
        width:20,
        height:20,
        backgroundColor:grey,
        color:grey
    },
    check:{
      fontSize:20,
     color:appColor
    },

    titleText:{
     fontFamily:LatoRegular,
     fontSize:mediumText,
     fontWeight:Platform.OS=='ios'? '400' :'500',
     fontStyle:'normal',  
     color:grey,
     marginLeft:8
    }
})