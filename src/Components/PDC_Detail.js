import { Icon, Image } from 'native-base'
import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { appColor, black, borderColor, darkSky, lightBlack, lightGrey, red } from '../Constants/Colors'
import { LatoBold, LatoRegular } from '../Constants/Fonts'
import { mediumText } from '../Constants/FontSize'
import TimePickerField from "../Components/TimePickerField";
import TimePicker from "../Components/TimePicker";
import { Pressable } from 'react-native'
import CheckBox from './CheckBox'

const PDC_Detail = (props) => {
    const {timeIn, timeOut , showTimeOut , showTimeIn} = props
    console.log('showTimeOut',showTimeOut)
    return (
        <View 
        style={styles.container}
        key={props.index}
        >
            <View style={styles.leftView}>
                <Text style={styles.nameStyle}>07-0721-0001 : SBPV : 17/Aug/2021 : 123434 : 230705 : Cross cheque for the salaries of Employee of Jul 2021 For Bank Transfer.</Text>
            </View>

            <View style={styles.verticalLine}></View>
              
            <View style={styles.rightView}>
            {props.isChecked?
             <Icon
              name={'check-square'}
              type={'Feather'}
              style={styles.activeIcon}
             />
             :
             <Icon
              name={'square'}
              type={'Feather'}
              style={styles.inactiveIcon}
             />
            }
            </View>
            
        </View>
    )
}
export default PDC_Detail;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'center',
        // paddingVertical: 10,
        paddingLeft:10,
        paddingRight:5,
        // paddingHorizontal: 10,
        marginTop: 10,
        borderRadius:5,
        borderColor: appColor,
        borderWidth: 1,

    },
    rightView: {
        flex: 0.2,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection:'row',
        paddingLeft:5,
        // backgroundColor:'red'
    },
    leftView: {
        flex: 0.8,
        justifyContent: 'center',
        paddingHorizontal:5,
        // alignItems: 'center'
    },
    imgView: {
        width: 70,
        height: 70,
        borderRadius: 70 / 2,
        backgroundColor: lightGrey,
    },
    tinyCircle: {
        width: 10,
        height: 10,
        borderRadius: 10 / 2,
        marginRight: 5,
        // position:'absolute',
        bottom: 10,
        left: 20,
    },
    nameStyle: {
        color:lightBlack,
        fontFamily:LatoRegular,
        fontSize:mediumText,
        paddingVertical:10,
        marginTop: -5
    },
    activeIcon: {
        color: appColor,
        fontSize: 22,
        fontFamily: LatoRegular,
        alignSelf:'center',
    },
    inactiveIcon: {
        color: appColor,
        fontSize:22,
        fontFamily: LatoRegular,
        alignSelf:'center',
    },
    verticalLine: {
        height: '100%',
        width: 1,
        backgroundColor: lightGrey
    },
    heading:{
     color:appColor,
     fontFamily:LatoRegular,
     fontSize:mediumText,
     fontWeight:'500',   
    },
    timeStyle:{
        marginTop:7,
        color:black,
        fontSize:mediumText,
        fontFamily:LatoBold,
        fontWeight:'500'
    }
})