import React from "react";
import { Pressable, View, Text, StyleSheet ,Image } from "react-native";
import { jvcolor ,lightBlack } from "../Constants/Colors";
import { journal } from "../Constants/images";
import { LatoRegular } from "../Constants/Fonts";
import { mediumText, xsmallText } from "../Constants/FontSize";

const _Pressable = (props) => {
    return (
        <Pressable style={styles.innerContainer}
         onPress={props.onPress}
        >
            <View style={[styles.journalView,props.container]}>
                <Image
                    source={props.img}
                    resizeMode={'contain'}
                    style={styles.imgStyle}
                />
            </View>
            <Text style={[styles.titleStyle,{ marginTop: 5 },props.titleText]}>{props.title}</Text>
            <Text style={[styles.titleStyle,{ marginTop: 5 },props.titleText]}>{props.title2}</Text>
        </Pressable>
    )
}
export default _Pressable

const styles  = StyleSheet.create({
    innerContainer:{
        justifyContent:'center',
        alignItems:'center'
        },
        journalView:{
            backgroundColor:jvcolor,
            paddingVertical:10,
            borderRadius:10,
            width:40,
            height:40,
            justifyContent:'center',
            alignItems:'center',
        },
        imgStyle:{
            width:20,
            height:20,
        },
        titleStyle:{
            color:lightBlack,
            fontFamily:LatoRegular,
            fontSize:xsmallText,
            fontStyle:'normal',
            fontWeight:'400',
        },
})