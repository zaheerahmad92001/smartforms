import { Icon } from "native-base";
import React, { useRef } from "react";
import { Pressable } from "react-native";
import { View, Text, TextInput, StyleSheet, Platform } from 'react-native'
import { RFValue } from "react-native-responsive-fontsize";
import { widthPercentageToDP } from "react-native-responsive-screen";
import { appColor, danger, darkGrey, grey, lightGrey, white } from "../Constants/Colors";
import { mediumText } from "../Constants/FontSize";



const DatePicker = (props) => {
    const {error} =props

    return (
        <View>
            <View style={{ flexDirection: 'row', }}>
                <Text style={error?[styles.labelError]:[styles.label]}>
                    {`${props.label} :`}
                </Text>
            </View>
            <Pressable
             onPress={props.onPress}
             style={error? 
                 [styles.InputViewError, props.inputViewStyle]
                 :
                 [styles.InputView, props.inputViewStyle]
                }>
{props.date ?
            <Text style={styles.Input}>{props.date}</Text>
            :
            <Text style={styles.placeholder}>{'Select Date'}</Text>
}
                <Icon
                    name={props.iconName}
                    type={props.iconType}
                    style={styles.iconStyle}
                    onPress={props.iconPress}
                />
            </Pressable>
        </View>
    )
}
export default DatePicker;
const styles = StyleSheet.create({
    InputView: {
        marginBottom: 10,
        borderWidth:1,
        borderColor:appColor,
        borderRadius: 5,
        fontSize: 12,
        flexDirection: 'row',
        alignItems: "center",
        paddingHorizontal: 10,
        paddingVertical: Platform.OS === 'ios' ? 6 : 2,
        zIndex: -10,
        backgroundColor: 'white'
    },

    InputViewError: {
        marginBottom: 10,
        borderWidth:1,
        borderColor:danger,
        borderRadius: 5,
        fontSize: 12,
        flexDirection: 'row',
        alignItems: "center",
        paddingHorizontal: 10,
        paddingVertical: Platform.OS === 'ios' ? 6 : 2,
        zIndex: -10,
        backgroundColor: 'white'
    },



    Input: {
        backgroundColor:white,
        paddingVertical: 10,
        paddingHorizontal: 10,
        color: 'black',
        fontSize: RFValue(mediumText),
        flex: 1,

    },

    placeholder:{
        backgroundColor:white,
        paddingVertical: 10,
        paddingHorizontal: 10,
        color:grey,
        fontSize: RFValue(mediumText),
        flex: 1,
    },

    label: {
        marginLeft: 20,
        paddingLeft: 10,
        paddingRight: 10,
        color:darkGrey,
        fontSize: RFValue(14),
        backgroundColor: 'white',
        top: 8
    },
    labelError: {
        marginLeft: 20,
        paddingLeft: 10,
        paddingRight: 10,
        color:danger,
        fontSize: RFValue(14),
        backgroundColor: 'white',
        top: 8
    },


    iconStyle: {
        fontSize: RFValue(23),
        color:grey
    },
    

})