import React from 'react'
import { View,Text,TouchableOpacity, StyleSheet } from "react-native";
import { RFValue } from 'react-native-responsive-fontsize';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { red } from '../Constants/Colors';
import { LatoRegular } from '../Constants/Fonts';
import { mediumText } from '../Constants/FontSize';

const AppButton =(props)=>{
 return(
     <TouchableOpacity 
     onPress={props.onPress}
     >
         <View style={[styles.wraper,props.btnWraper]}>
             <Text style={styles.buttonTitle}>
                 {props.title}
             </Text>
         </View>
     </TouchableOpacity>
 )
}
export default AppButton;
const styles = StyleSheet.create({
    wraper:{
    width:wp('92%'),
    backgroundColor:red,
    paddingVertical:11,
    borderRadius:25,
    justifyContent:'center',
    alignItems:'center',
    },
    buttonTitle:{
        color:'white',
        fontSize:mediumText,
        fontWeight:'500',
        fontFamily:LatoRegular,
    }
})