import { Icon } from "native-base";
import React,{useRef} from "react";
import {
    View,
    Text,
    TextInput,
    StyleSheet,
    Platform
} from "react-native";
import { RFValue } from "react-native-responsive-fontsize";
import { appColor, darkGrey, darkSky, lightGrey, white } from "../Constants/Colors";
import { mediumText } from "../Constants/FontSize";

const SimpleTextField = (props) => {

    let text = useRef()
    let vRef = useRef()

    focusedInput = () => {
        if (props.onFocus) {
            props.onFocus();
        }
        vRef.setNativeProps({
            style: { borderColor: appColor, borderWidth: 1 },
        });
    };

    blurredInput = () => {
        vRef.setNativeProps({
            style: { borderColor:darkGrey, borderWidth: StyleSheet.hairlineWidth },
        });
    };

    return (
        <View
        ref={(a) => { vRef = a }}
        style={[styles.inPutView,props.inputContainer]}
        >
            <TextInput
                secureTextEntry={props.secureTextEntry}
                editable={props.editable}
                onFocus={focusedInput}
                onBlur={blurredInput}
                keyboardType={props.keyboardType}
                placeholder={props.placeholder}
                placeholderTextColor={'black'}
                onChangeText={props.onChangeText}
                value={props.value}
                multiline={props.multiline}
                autoCapitalize={'none'}
                numberOfLines={props.numberOfLines}
                style={[styles.Input]}
            />
            <Icon
            onPress={props.onPress}
             name={props.iconName}
             type={props.iconType}
             style={styles.iconStyle}
            />
        </View>
    )
}
export default SimpleTextField;

const styles = StyleSheet.create({

    Input: {
        backgroundColor:white,
        paddingVertical: Platform.OS==='ios'?13: 10,
        color: 'black',
        marginHorizontal:15,
        borderRadius:Platform.OS==='ios'? 25:25,
        fontSize: RFValue(mediumText),
        flex:1,
    },

    inPutView:{
        borderColor:darkGrey,
        borderWidth:1,
        borderRadius:Platform.OS==='ios'? 25:25,
        marginBottom:15,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
    },
    iconStyle:{
        color:darkGrey,
        marginRight:15,
    }

})