import React from "react";
import { Text, StyleSheet, View, } from "react-native";
import { widthPercentageToDP, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { appColor, borderColor, darkGrey, grey } from "../Constants/Colors";
import { LatoRegular } from "../Constants/Fonts";
import { mediumText } from "../Constants/FontSize";
import {Divider} from 'react-native-elements'


const TransferSearch = (props) => {
    
    return(
        <View>
            <View style={styles.container}>
                <Text style={[styles.textStyle,{width:wp(10),textAlign:'center',}]}>{props.index}</Text>
                <Text style={[styles.textStyle,{width:wp(20),textAlign:'center'}]}>Q-20321</Text>
                <Text style={[styles.textStyle,{width:wp(20),textAlign:'center',marginLeft:5}]}>13/07/2021</Text>
                <Text style={[styles.textStyle,{width:wp(40),paddingLeft:15 , textAlign:'center'}]}>Jupiter Network Trading center</Text>
            </View>
        <Divider
            rientation="horizontal"
            style={styles.divider} />
       </View> 
        
    )
}
export default TransferSearch

const styles = StyleSheet.create({
    container:{
        flexDirection:'row',
        alignItems:'center', 
        justifyContent:'space-between'
    },
    textStyle:{
        color:darkGrey,
        fontSize:mediumText,
        fontFamily:LatoRegular,
        fontWeight:'500',
        alignSelf:'flex-start',
    },
    divider: {
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: borderColor,
        marginTop: 10,
        marginBottom: 10,
        width:wp(100),
        alignSelf:'center',
    },
})