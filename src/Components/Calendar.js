import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { Overlay } from 'react-native-elements'
import { appColor, grey, lightBlack, lightGrey, white } from '../Constants/Colors';
import { Icon } from 'native-base'
import { LatoBlack, LatoRegular } from '../Constants/Fonts';
import { mediumText, smallText } from '../Constants/FontSize';
import { Pressable } from 'react-native';
import { Platform } from 'react-native';
import { TouchableOpacity } from 'react-native';


// let selected ={}   


const _Calendar = (props) => {
    // console.log('pros received' , props)

    Arrow = (direction) => {
        return (
            <View>
                {direction.direction === 'left' ?
                    <View style={styles.arrowBgView}>
                        <Icon
                            name={'chevron-back'}
                            type={'Ionicons'}
                            style={styles.iconStyle}
                        />
                    </View>
                    :
                    <View style={styles.arrowBgView}>
                        <Icon
                            name={'chevron-forward'}
                            type={'Ionicons'}
                            style={styles.iconStyle}
                        />
                    </View>
                }
            </View>
        )
    }

    return (
        <Overlay
            isVisible={props.is_Visible}
            onBackdropPress={props.hideDatePicker}
            overlayStyle={styles.overlayStyle}
        >
            <View style={styles.container}>
                {/* <View style={styles.btnContainer}>

                    <Pressable style={styles.btnStyle}
                        onPress={props.todayPress}>
                        <Text style={styles.btnText}>Today</Text>
                    </Pressable>

                    <Pressable style={styles.btnStyle}
                        onPress={props.tomorrowPress}>
                        <Text style={styles.btnText}>Tomorrow</Text>
                    </Pressable>

                    <Pressable style={styles.btnStyle}
                        onPress={props.inTwoDayPress}>
                        <Text style={styles.btnText}>in 2 days</Text>
                    </Pressable>
                </View> */}

                <Calendar
                    style={styles.calendarStyle}
                    renderArrow={(direction) => (
                        <Arrow
                            direction={direction}
                        />)}
                    onDayPress={props.onDayPress}
                    current={props.currentDate}
                    markingType={'custom'}
                    markedDates={props.markedDates}
                    minDate={props.minDate}
                    // minDate={'2012-05-10'}
                    // maxDate={'2012-05-30'}
                    maxDate={props.maxDate ?props.maxDate: new Date()}

                    theme={{
                        backgroundColor: 'red',
                        calendarBackground: "transparent",
                        textSectionTitleColor: '#b6c1cd',
                        textSectionTitleDisabledColor: '#d9e1e8',
                        selectedDayBackgroundColor: '#00adf5',
                        selectedDayTextColor: '#ffffff',
                        todayTextColor: '#00adf5',
                        dayTextColor: '#2d4150',
                        textDisabledColor: '#d9e1e8',
                        dotColor: '#00adf5',
                        selectedDotColor: '#ffffff',
                        arrowColor: 'orange',
                        disabledArrowColor: '#d9e1e8',
                        monthTextColor: lightBlack,
                        indicatorColor: 'blue',
                        textDayFontFamily: LatoRegular,
                        textMonthFontFamily: LatoRegular,
                        textDayHeaderFontFamily: LatoRegular,
                        textDayFontWeight: '300',
                        textMonthFontWeight: 'bold',
                        textDayHeaderFontWeight: '500',
                        textDayFontSize: 20,
                        textMonthFontSize: 16,
                        textDayHeaderFontSize: mediumText
                    }}
                />
                {/* <TouchableOpacity style={styles.addReminder}>
                    <Text style={styles.removeText}>+ Add Reminder</Text>
                </TouchableOpacity> */}

                <View style={styles.actionBtn}>
                    {/* <Pressable 
                     onPress={props.removeBtnPress}
                     style={[styles.removeBtn, { marginRight: 7 }]}
                    >
                        <Text style={styles.removeText}>Close</Text>
                    </Pressable> */}
                    {/* <Pressable 
                     onPress={props.DoneBtnPress}
                     style={[styles.removeBtn, styles.doneBtn]}
                    >
                        <Text style={[styles.doneText, { color: white }]}>Done</Text>
                    </Pressable> */}
                </View>

            </View>
        </Overlay>
    )
}
export default _Calendar;

const styles = StyleSheet.create({
    overlayStyle: {
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0,
        borderRadius: 20,
        backgroundColor: white,
    },
    calendarStyle: {
        elevation: 4,
        width: wp(90),
        paddingHorizontal: 0,
        paddingVertical: 0,
    },
    arrowBgView: {
        width: 30,
        height: 30,
        backgroundColor: white,
        borderRadius: 10,
        elevation: 2,
        shadowColor: '#000',
        shadowOffset: { height: 1, width: 1 },
        shadowOpacity: 0.6,
        shadowRadius: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconStyle: {
        fontSize: 20,
    },
    container: {
        paddingVertical: 10,
        borderRadius: 20,
    },
    btnContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 20,
        marginBottom: 10,
        justifyContent: 'space-between',
    },
    btnStyle: {
        backgroundColor: appColor,
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderRadius: 10,
    },
    btnText: {
        color: white,
        fontFamily: LatoRegular,
        fontSize: mediumText,
        fontWeight: '500',
    },
    actionBtn: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15,
        marginBottom:10,
    },
    removeBtn: {
        backgroundColor: lightGrey,
        // paddingHorizontal:15,
        paddingVertical: 12,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        width: wp(25),
    },
    removeText: {
        color: lightBlack,
        fontWeight: '500',
        fontSize: mediumText,
        fontFamily: LatoRegular,
    },
    doneBtn: {
        backgroundColor: appColor,
        marginLeft: 7,
        paddingVertical: Platform.OS == 'android' ? 10 : 12
    },
    addReminder: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,

    }


})