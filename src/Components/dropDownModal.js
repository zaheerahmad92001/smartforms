import { Icon } from 'native-base'
import React, { useEffect, useState } from 'react'
import { Pressable } from 'react-native'
import { View, Text, StyleSheet, FlatList } from 'react-native'
import Modal from 'react-native-modal'
import { Overlay } from 'react-native-elements'
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen'
import { appColor, grey,lightBlack,lightGrey, white } from '../Constants/Colors'
import { LatoRegular } from '../Constants/Fonts'
import { mediumText, largeText } from '../Constants/FontSize'
import InputField from './InputField'
import { ActivityIndicator } from 'react-native'

const DropdownModal = (props) => {
    const { isLoading } = props

    function renderItem({ item, index }) {
       let rndm =  Math.random()
        return (

            <Pressable 
               onPress={() => props.selectValue(item, index)}
               key={`${index}${rndm}`}
               >
                <View style={styles.contentView}>
                    {/* <Text style={styles.indexStyle}>{`${index}`}</Text> */}
                    <Text style={styles.labelStyle} >{item}</Text>
                </View>
            </Pressable>
        )
    }

    return (
        <Overlay
            isVisible={props.isVisible}
            onBackButtonPress={props.closeFromModal}
            onBackdropPress={props.closeFromModal}
            useNativeDriver={true}
            fullScreen={true}
            style={styles.modal}>

            <View style={styles.modalView}>
                <InputField
                    placeholder={"Type here"}
                    label={'Search'}
                    onChangeText={props.onChangeText}
                    value={props.value}
                    // inputViewStyle={styles.modalView}
                />
                {isLoading ?
                <View style={styles.loader}>
                    <ActivityIndicator
                      color={appColor}
                      size={'small'}
                    /> 
                    </View>:
                    <FlatList
                       style={{marginTop:10}}
                       key={'flatlist'}
                    // style={styles.modalView}
                        data={props.data}
                        keyExtractor={(item) => { item.id + Math.random() }}
                        initialNumToRender={20}
                        maxToRenderPerBatch={20}
                        renderItem={renderItem}
                    />
                }

            </View>

            <Pressable
                style={styles.actionBtn}
                onPress={props.closeFromModal}
            >
                <Text>Cancel</Text>
            </Pressable>
        </Overlay>
    )
}

export default DropdownModal
const styles = StyleSheet.create({
    modal: {
       
    },
    modalView: {
        backgroundColor: white,
        height:heightPercentageToDP(98),
        paddingHorizontal: 20,
        paddingVertical: 20,
    },
    iconStyle: {
        fontSize: 25,
        color: grey
    },
    selectedIcon: {
        fontSize: 25,
        color: appColor
    },
    contentView: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom:20,
    },
    labelStyle: {
        // marginLeft: 15,
        fontSize:mediumText,
        fontFamily: LatoRegular,
        fontWeight: '500',
        fontStyle: 'normal',
        flex:1,

    },
    actionBtn: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        backgroundColor: appColor,
        paddingVertical: 12,
        width: widthPercentageToDP(25),
        borderRadius: 10,
        position: 'absolute',
        bottom: 50,
    },
   indexStyle:{
       alignSelf:'flex-start',
       color:lightBlack,
       fontSize:mediumText,
       fontWeight:'normal',
       marginRight:10,
    },
   loader:{
       flex:1 , 
       justifyContent:'center' ,
       alignItems:'center'
    }
})