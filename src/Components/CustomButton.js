import { Icon } from "native-base";
import React, { useRef } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { appColor, white } from "../Constants/Colors";
import { LatoLight, LatoRegular } from "../Constants/Fonts";
import { mediumText } from "../Constants/FontSize";
const CustomButton = (props) => {
    return (
        <TouchableOpacity
           onPress={props.onPress}
            style={[styles.container,props.btnContainer]}
         >
            <View style={styles.content}>
                <Text style={[styles.title,props.titleStyle]}>{props.title}</Text>
                <Icon
                    name={props.iconName}
                    type={props.iconType}
                    style={styles.iconStyle}
                />
            </View>
        </TouchableOpacity>

    )
}
export default CustomButton
const styles = StyleSheet.create({
    container: {
        backgroundColor: appColor,
        width: wp(50),
        paddingVertical:10,
        borderRadius:20,
        
    },
    content: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'space-around'
    },
    iconStyle: {
        color: white,
        fontSize: 15,
        marginRight:5,

    },
    title: {
        color: white,
        fontSize: mediumText,
        fontWeight: '500',
        fontFamily:LatoRegular,
        width:wp(40),
        textAlign:'center',
    }
})
