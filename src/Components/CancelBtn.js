import React from 'react'
import { Pressable, View, Text, StyleSheet } from 'react-native'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { appColor, danger, white } from '../Constants/Colors';
import { LatoRegular } from '../Constants/Fonts';
import { largeText } from '../Constants/FontSize';
const CancelBtn = (props) => {
    return (
        <Pressable style={styles.cancelBtn}
          onPress={props.onPress}
        >
            <Text style={styles.saveText}>{props.title}</Text>
        </Pressable>
    )
}

export default CancelBtn ;

const styles = StyleSheet.create({
    cancelBtn:{
        backgroundColor:danger,
        width:wp(27),
        paddingVertical:12,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:5,
      },

    saveText: {
        color: white,
        fontWeight: '500',
        fontStyle: 'normal',
        fontFamily: LatoRegular,
        fontSize: largeText,
    },
})