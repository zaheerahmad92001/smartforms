import React from 'react'
import { StyleSheet } from 'react-native';
import {View,Text , } from 'react-native'
import { danger } from '../Constants/Colors';
import { LatoRegular } from '../Constants/Fonts';
import { smallText } from '../Constants/FontSize';
const Error =(props)=>{
    return(
        <Text style={[styles.text,props.style]}>{`* ${props.error}`}</Text>
    )
}
export default Error;

const styles = StyleSheet.create({
    text:{
        color:danger,
        fontFamily:LatoRegular,
        fontWeight:'500',
        fontSize:smallText
    }
})