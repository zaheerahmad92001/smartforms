import React from 'react'
import { ActivityIndicator } from 'react-native';
import { Pressable, View, Text, StyleSheet } from 'react-native'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { appColor, white } from '../Constants/Colors';
import { LatoRegular } from '../Constants/Fonts';
import { largeText } from '../Constants/FontSize';
const SaveBtn = (props) => {
    const {saving} = props
    return (
        <Pressable style={[styles.saveBtn,props.buttonContainer]}
          disabled={saving}
          onPress={props.onPress}
        >
            {saving?
            <ActivityIndicator
              color={white}
              size={'small'}
            />:
            <Text style={[styles.saveText,props.textStyle]}>{props.title}</Text>
            }
        </Pressable>
    )
}

export default SaveBtn ;

const styles = StyleSheet.create({
    saveBtn: {
        backgroundColor: appColor,
        paddingVertical: 13,
        width: wp(37),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius:5,
    },

    saveText: {
        color: white,
        fontWeight: '500',
        fontStyle: 'normal',
        fontFamily: LatoRegular,
        fontSize: largeText,
    },
})