import React ,{useState, useReducer , useRef , useEffect} from 'react'
import {Footer , FooterTab,} from 'native-base'
import {View,Text,Pressable ,StyleSheet , Image} from 'react-native'
import { black, darkGrey, grey, lightGrey, white } from '../Constants/Colors'
import { LatoRegular ,mediumText } from '../Constants/Fonts'
import {saleInvoice ,purchase ,journalVoucher ,leaveApply}from '../Constants/images'


function BottomTabs ({navigation ,jvPress ,purchasePress, saleInvoicePress,leaveApplyPress }){
    return(
      <Footer style={styles.container}>
          <FooterTab style={styles.container}>
              <Pressable
                 onPress={jvPress}
                 style={styles.buttonStyle}
                 >
                  <Image
                   source={journalVoucher}
                    resizeMode={'contain'}
                    style={styles.imgStyle}
                  />   
                  <Text style={styles.textStyle}>Journal</Text>
                  <Text style={styles.textStyle}>Voucher</Text>
              </Pressable>

              <Pressable
                 onPress={purchasePress}
                 style={styles.buttonStyle}

                 >
                  <Image
                   source={purchase}
                    resizeMode={'contain'}
                    style={styles.imgStyle}
                  />   
                  <Text style={styles.textStyle}>Purchase</Text>
              </Pressable>
              <Pressable
                 onPress={saleInvoicePress}
                 style={styles.buttonStyle}

                 >
                  <Image
                   source={saleInvoice}
                    resizeMode={'contain'}
                    style={styles.imgStyle}
                  />   
                  <Text style={styles.textStyle}>Sale Invoice</Text>
              </Pressable>
              <Pressable
                 onPress={leaveApplyPress}
                 style={styles.buttonStyle}
                 >
                  <Image
                   source={leaveApply}
                    resizeMode={'contain'}
                    style={styles.imgStyle}
                  />   
                  <Text style={styles.textStyle}>Leave Apply</Text>
              </Pressable>
          </FooterTab>
      </Footer>
    )
}
export default BottomTabs

const styles  = StyleSheet.create({
    container:{
        backgroundColor:white,
        borderTopColor:grey,
        borderTopWidth:StyleSheet.hairlineWidth,
        paddingHorizontal:10,
    },
    buttonStyle:{
       justifyContent:'center',
       alignItems:'center'
    },
    imgStyle:{
        // marginTop:10,
        width:30,
        height:30,

    },
    textStyle:{
        color:darkGrey,
        fontFamily:LatoRegular,
        fontSize:11,
    }
})
