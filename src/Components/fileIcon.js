import React, { Component } from 'react'
import { View, Text, TouchableOpacity,  StyleSheet } from 'react-native'
import {Icon} from 'native-base'

import Doc from '../Constants/FileIcons/Doc'
import Excel from '../Constants/FileIcons/Excel';
import Jpg from '../Constants/FileIcons/Jpg';
import MP from '../Constants/FileIcons/Mp';
import Pdf from '../Constants/FileIcons/Pdf';
import PowerPoint from '../Constants/FileIcons/PowerPoint';
import { borderColor } from '../Constants/Colors';

const FileIcon = (props) => {
    let fileType  = props.type
    return (
        <TouchableOpacity
            onPress={props.onPress}
            style={[styles.container,props.containerStyle]}
        >
            <View>
                { fileType === 'pdf' ?
                    <Pdf
                        style={[styles.iconStyle,props.icnStyle]}
                    />
                    : fileType == 'docx' || fileType == 'doc' ?
                        <Doc
                            style={[styles.iconStyle,props.icnStyle]}
                        />
                        : fileType == 'xls' ||
                        fileType == 'xlsb' ||
                        fileType == 'xlsm' ||
                        fileType == 'xlsx' ?
                            <Excel
                                style={[styles.iconStyle,props.icnStyle]}
                            /> :
                            fileType == 'jpg' || fileType == 'png' || fileType == 'jpeg' ?
                                <Jpg
                                    style={[styles.iconStyle,props.icnStyle]}
                                /> : null
                }
                {
                props.removable ?
                 <View style={styles.crossIcon}>
                    <Icon
                    type={'Entypo'}
                    name={'cross'}
                    style={{fontSize:15,alignSelf:'center',}}
                    />
                </View>:null
                }
            </View>
        </TouchableOpacity>
    )

}
export default FileIcon
const styles = StyleSheet.create({
    container:{
    //   marginLeft:5,
    marginRight:15,
    },
    iconStyle: {
        width: 40,
        height: 40
    },
    crossIcon:{
        position:'absolute',
        top:0,width:15,height:15,
        borderRadius:15/2,
        backgroundColor:borderColor,
        justifyContent:'center',
        alignItems:'center'
    }
   
})