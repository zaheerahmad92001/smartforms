import { Image } from 'native-base'
import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { appColor, black, borderColor, darkGrey, lightGrey, red } from '../Constants/Colors'
import { LatoBold, LatoRegular } from '../Constants/Fonts'
import { mediumText } from '../Constants/FontSize'
import TimePickerField from "../Components/TimePickerField";
import TimePicker from "../Components/TimePicker";
import { Pressable } from 'react-native'

const AttendanceTimming = (props) => {
    const {
        //  timeIn,
         timeOut ,
         showTimeOut , 
        showTimeIn,
        item
    } = props
    
    let [h, m] = item?.datein?.split(':')
    let time_in =''
   if(m=='00' && h=='00'){
        time_in ='00:00'
   }else{
        var format = h >= 12 ? 'PM' : 'AM';
        h = h % 12;
        h = h ? h : 12;
        m = m
        time_in = `${h}:${m} ${format}`
   }


   let [h_o, m_o] = item?.dateout?.split(':')
    let time_out =''
   if(m_o=='00' && h_o =='00'){
        time_out ='00:00'
   }else{
        var format_o = h_o >= 12 ? 'PM' : 'AM';
        h_o = h_o % 12;
        h_o = h_o ? h_o : 12;
        m_o = m_o
        time_out = `${h_o}:${m_o} ${format_o}`
   }

    return (
        <View style={styles.container}>
            <View style={styles.leftView}>
                <View style={styles.imgView}>
                    {/* <Image/> */}
                </View>
                <View style={[styles.tinyCircle, { backgroundColor: appColor }]}></View>
                <Text style={styles.nameStyle}>{item?.employeename}</Text>
                <Text style={styles.badgeStyle}>{item?.empcode}</Text>
            </View>

            <View style={styles.verticalLine}></View>

            <View style={styles.rightView}>
                <Pressable
                  onPress={props.TimeInPress}>
                  <Text style={styles.heading}>Time-In</Text>
                  <Text style={styles.timeStyle}>{time_in}</Text>
                </Pressable>


                <Pressable 
                  onPress={props.TimeOutPress}
                  style={{marginLeft:15}}>
                  <Text style={styles.heading}>Time-Out</Text>
                  <Text style={styles.timeStyle}>{time_out}</Text>
                </Pressable>

                
            </View>
            <TimePicker
                isVisible={showTimeIn?showTimeIn:false}
                date={props.time_in}
                onDateChange={props.onTimeInChange}
                onSelect={props.handleTimeIn}
                onClose={props.closeTimeInPicker}
                mode="time"
            />
            <TimePicker
                isVisible={showTimeOut?showTimeOut: false}
                date={props.time_out}
                onDateChange={props.onTimeOutChange}
                onSelect={props.handleTimeOut}
                onClose={props.closeTimeOutPicker}
                mode="time"
            />
        </View>
    )
}
export default AttendanceTimming;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'center',
        paddingVertical: 10,
        paddingHorizontal: 10,
        marginTop: 10,
        borderRadius: 10,
        borderColor: borderColor,
        borderWidth: 1,

    },
    rightView: {
        flex: 0.55,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection:'row',
        //   backgroundColor:'green'
    },
    leftView: {
        flex: 0.45,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imgView: {
        width: 70,
        height: 70,
        borderRadius: 70 / 2,
        backgroundColor: lightGrey,
    },
    tinyCircle: {
        width: 10,
        height: 10,
        borderRadius: 10 / 2,
        marginRight: 5,
        // position:'absolute',
        bottom: 10,
        left: 20,
    },
    nameStyle: {
        color: appColor,
        fontFamily: LatoBold,
        fontSize: mediumText,
        marginTop: -5
    },
    badgeStyle: {
        color: black,
        fontSize: mediumText,
        fontFamily: LatoRegular,
    },
    verticalLine: {
        height: '100%',
        width: 1,
        backgroundColor: lightGrey
    },
    heading:{
     color:appColor,
     fontFamily:LatoRegular,
     fontSize:mediumText,
     fontWeight:'500',   
    },
    timeStyle:{
        marginTop:7,
        color:black,
        fontSize:mediumText,
        fontFamily:LatoBold,
        fontWeight:'500'
    }
})