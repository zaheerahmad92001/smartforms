
import { Icon } from 'native-base';
import React, { Component } from 'react'
import { View, Text, StyleSheet,TouchableOpacity } from 'react-native'
import DatePicker from 'react-native-date-picker';
import Modal from 'react-native-modal';
import { RFValue } from 'react-native-responsive-fontsize';
import { appColor, white } from '../Constants/Colors';
import { mediumText } from '../Constants/FontSize';

export const TimePicker = (props) => {
    return (
        <Modal
            isVisible={props.isVisible}
            useNativeDriver={true}
        >
            <View style={styles.wraper}>
                <View style={styles.header}>
                <Text style={styles.textStyle}>{props.header}</Text>
                    <Icon
                        onPress={props.onClose}
                        name={props.iconName ? props.iconName : 'close'}
                        type={props.iconType ? props.iconType : 'Fontisto'}
                        style={styles.iconStyle}
                    />
                </View>

                <DatePicker
                    date={props.date}
                    onDateChange={props.onDateChange}
                    mode={props.mode}
                    value={'testing'}
                    minuteInterval={10}
                    style={{ position: 'relative', marginVertical: 20 }}
                   />
                
                <TouchableOpacity
                     onPress={props.onSelect}
                     style={styles.btnView}
                   >
              <Text style={styles.okBtnStyle}>OK</Text>      
                </TouchableOpacity>

            </View>

        </Modal>
    )
}
export default TimePicker;

const styles = StyleSheet.create({
    wraper: {
        backgroundColor:white,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
        marginHorizontal: 25,
    },
    header: {
        backgroundColor:appColor,
        height: RFValue(40),
        width: 'auto',
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
    },
    textStyle: {
        color:white,
        fontSize:mediumText,
        marginLeft: 10,
        // fontWeight: 'bold'
    },
    titleStyle: {
        fontSize: 15,
        color: white
    },
    iconStyle: {
        marginRight: 10,
        color:white,
        fontSize: 18,

    },
    btnView: {
        paddingVertical: 15,
        backgroundColor:appColor,
        borderBottomEndRadius: 20,
        borderBottomLeftRadius: 20,
    },
    okBtnStyle: {
        fontSize: mediumText,
        marginLeft: 5,
        fontWeight: '600',
        color: white,
        textAlign:'center'
    },
})