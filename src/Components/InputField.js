import { Icon } from "native-base";
import React, { useRef } from "react";
import { View, Text, TextInput, StyleSheet, Platform } from 'react-native'
import { RFValue } from "react-native-responsive-fontsize";
import { widthPercentageToDP } from "react-native-responsive-screen";
import { appColor, danger, darkGrey } from "../Constants/Colors";
import { mediumText } from "../Constants/FontSize";



const InputField = (props) => {
    const {error} =props
    let textInput = useRef()
    let text = useRef()
    let vRef = useRef()


    // focusedInput = () => {
    //     if (props.onFocus) {
    //         props.onFocus();
    //     }
        // vRef.setNativeProps({
        //     style: { borderColor: appColor, borderWidth: 1 },
        // });
        // text.setNativeProps({
        //     style: { color: appColor },
        // });

    // };

    // blurredInput = () => {
    //     console.log('vRef',vRef)
    //     vRef.setNativeProps({
    //         style: { borderColor: 'gray', borderWidth: StyleSheet.hairlineWidth },
    //     });
    //     text.setNativeProps({
    //         style: { color: 'gray' },
    //     });
    // };

    return (
        <View>
            <View style={{ flexDirection: 'row', }}>
                <Text
                    ref={(c) => { text = c }}
                    style={error? [styles.labelError]:[styles.label]}
                >
                    {`${props.label} :`}
                </Text>
            </View>
            <View
                ref={(a) => { vRef = a }}
                style={error?
                     [styles.InputViewError, props.inputViewStyle]
                     :
                     [styles.InputView, props.inputViewStyle]}>

                <Icon
                    ref={(c) => { icn = c }}
                    name={props.iconName}
                    type={props.iconType}
                    style={styles.iconStyle}
                    onPress={props.iconPress}
                />


                <TextInput
                    ref={(c) => { textInput = c }}
                    secureTextEntry={props.secureTextEntry}
                    editable={props.editable}
                    // onFocus={focusedInput}
                    // onBlur={blurredInput}
                    keyboardType={props.keyboardType}
                    placeholder={props.placeholder}
                    onChangeText={props.onChangeText}
                    value={props.value}
                    multiline={props.multiline}
                    numberOfLines={props.numberOfLines}
                    style={[styles.Input]}
                />
            </View>
        </View>
    )
}
export default InputField;
const styles = StyleSheet.create({
    InputView: {
        marginBottom: 10,
        borderWidth:1,
        borderRadius: 5,
        fontSize: 12,
        borderColor:appColor,
        flexDirection: 'row',
        alignItems: "center",
        paddingHorizontal: 10,
        paddingVertical: Platform.OS === 'ios' ? 6 : 2,
        zIndex: -10,
        backgroundColor: 'white'
    },

    InputViewError: {
        marginBottom: 10,
        borderWidth:1,
        borderRadius: 5,
        fontSize: 12,
        borderColor:danger,
        flexDirection: 'row',
        alignItems: "center",
        paddingHorizontal: 10,
        paddingVertical: Platform.OS === 'ios' ? 6 : 2,
        zIndex: -10,
        backgroundColor: 'white'
    },

    Input: {
        backgroundColor: 'white',
        paddingVertical: 7,
        paddingHorizontal: 10,
        color: 'black',
        fontSize: RFValue(mediumText),
        flex: 1,
    },


    label: {
        marginLeft: 20,
        paddingLeft: 10,
        paddingRight: 10,
        color:darkGrey,
        fontSize: RFValue(14),
        backgroundColor: 'white',
        top: 8
    },
    labelError: {
        marginLeft: 20,
        paddingLeft: 10,
        paddingRight: 10,
        color:danger,
        fontSize: RFValue(14),
        backgroundColor: 'white',
        top: 8
    },
    iconStyle: {
        fontSize: RFValue(23),
        color: appColor
    },
    

})