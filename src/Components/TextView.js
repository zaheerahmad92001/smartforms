import { Icon } from "native-base";
import React, { useRef } from "react";
import { View, Text, TextInput, StyleSheet, Platform } from 'react-native'
import { RFValue } from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { appColor, black, danger, darkGrey, lightBlack } from "../Constants/Colors";
import { LatoBlack, LatoLight, LatoRegular } from "../Constants/Fonts";
import { mediumText } from "../Constants/FontSize";



const TextView = (props) => {
    const {error} =props
    let textInput = useRef()
    let text = useRef()
    let vRef = useRef()


    return (
        <View>
            <View style={{ flexDirection: 'row', }}>
                <Text
                    ref={(c) => { text = c }}
                    style={[styles.label]}
                >
                    {props.label}
                </Text>
            </View>
            <View
                ref={(a) => { vRef = a }}
                style={[styles.InputView, props.inputViewStyle]}>
            <Text style={[styles.textStyle,props.infoStyle]}>{props.infoText}</Text>
            </View>
        </View>
    )
}
export default TextView;
const styles = StyleSheet.create({
    InputView: {
        marginBottom: 10,
        borderWidth:1,
        borderRadius: 5,
        fontSize: 12,
        borderColor:appColor,
        flexDirection: 'row',
        alignItems: "center",
        justifyContent:'center',
        paddingHorizontal: 10,
        paddingVertical:10,
        // paddingVertical: Platform.OS === 'ios' ? 6 : 2,
        zIndex: -10,
        backgroundColor: 'white',
        width:wp(42)
    },

    label: {
        marginLeft: 20,
        paddingLeft: 10,
        paddingRight: 10,
        color:darkGrey,
        fontWeight:'500',
        fontFamily:LatoBlack,
        fontSize:mediumText,
        backgroundColor: 'white',
        top: 8
    },
    textStyle:{
        color:lightBlack,
        fontSize:mediumText,
        fontWeight:'400',
        fontFamily:LatoRegular,
        // width:wp(35)
    }
   
    

})