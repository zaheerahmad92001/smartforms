import { Icon } from "native-base";
import React from "react";
import { TouchableOpacity } from "react-native";
import { TextInput } from "react-native";
import { Text, StyleSheet, View, } from "react-native";
import { Divider } from "react-native-elements/dist/divider/Divider";
import { appColor, black, borderColor, darkGrey, lightGrey } from "../Constants/Colors";
import { LatoRegular } from "../Constants/Fonts";
import { mediumText } from "../Constants/FontSize";
import InputField from "../Components/InputField";
import { Platform } from "react-native";

const SearchComponent = (props) => {
    
    const {item , index} = props
    return (
        <View
          onLongPress={props.onLongPress}
          style={{
            //   marginTop:props.srerial==1? 30 : 0,
            backgroundColor:'#FAFAFA' , 
            marginBottom:30,
            paddingVertical:5,
             }}
        >
        
            <View style={styles.container}>
                <Text style={styles.product}>Sr #</Text>
                <Text style={styles.detail}>{index}</Text>
            </View>
            <Divider
                rientation="horizontal"
                style={styles.divider} />

            
            <View style={styles.container}>
                <Text style={styles.product}>Employee</Text>
                <Text style={styles.detail}>{item.companyname}</Text>
            </View>
            <Divider
                rientation="horizontal"
                style={styles.divider} />

            <View style={styles.container}>
                <Text style={styles.product}>Detail</Text>
                <Text style={styles.detail}>{item?.remarks}</Text>
            </View>
            <Divider
                rientation="horizontal"
                style={styles.divider} />

            <View style={styles.container}>
                <Text style={styles.product}>Installment</Text>
                <Text style={styles.detail}>{item?.installment}</Text>
            </View>

            <Divider
                rientation="horizontal"
                style={styles.divider} />

            <View style={styles.container}>
                <Text style={styles.product}>Deduction</Text>
                <TextInput
                 placeholder={'Amount'}
                 keyboardType={'decimal-pad'}
                 onChangeText={props.onChangeText}
                 value={item?.deduct_amount}
                 style={styles.textinput}
                />
            </View>
           
            <Divider
                rientation="horizontal"
                style={styles.divider} />

           <View style={styles.container}
           >
                <Text style={[styles.product,{flex:0.25}]}>Finalize</Text>
                <Icon
                 onPress={props.handleFinalized}
                  name={item?.finalized==1? 'checkbox-active': 'checkbox-passive'}
                  type={'Fontisto'}
                  style={styles.inactiveIcon}
               />
               
            </View>
            
        </View>
    )
}
export default SearchComponent;
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        paddingHorizontal: 20,
    },
    product: {
        flex: 0.4,
        color:appColor,
        fontWeight:'700',
        fontSize:12,
        fontFamily:LatoRegular,
        
    },
    detail: {
        flex: 0.6,
        color:darkGrey,
        fontWeight:'500',
        fontSize:mediumText,
        fontFamily:LatoRegular,
        
    },
    divider: {
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: borderColor,
        marginTop: 10,
        marginBottom: 10,
    },
    inactiveIcon: {
        color: appColor,
        fontSize:18,
        fontFamily: LatoRegular,
        alignSelf:'center',
    },
    textinput:{
        // backgroundColor:"red",
        paddingVertical:Platform.OS=='android'? 0:5,
        flex:0.6,
        color:black,
        borderColor:borderColor,
        borderRadius:5,
        paddingHorizontal:5,
        borderWidth:StyleSheet.hairlineWidth

    }
})