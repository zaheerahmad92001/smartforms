import React from "react";
import { View, Text, StyleSheet, TextInput, Image, Platform } from "react-native";
import { appColor, black, borderColor, darkGrey, grey, lightBlack, white } from "../Constants/Colors";
import { Header, Body, Icon, Form, } from "native-base";
import { LatoBlack, LatoRegular } from "../Constants/Fonts";
import { largeText, mediumText } from "../Constants/FontSize";
import { Divider } from 'react-native-elements'
import { search, drawer } from '../Constants/images'
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { Pressable } from "react-native";

const topMargin = 20

const AppHeader = (props) => {
    return (
        <Header
            style={{
                backgroundColor: appColor,
                paddingLeft: 0,
                // paddingRight:0,
            }}
            noShadow
            transparent
        >

            <View style={
                props.search ?
                    [styles.left, { marginTop: topMargin }] : [styles.left]}>
                {!props.leftIconName ?       // drawer or back arrow
                    <Pressable
                        onPress={props.leftBtnPress}
                    >
                        <Image
                            source={drawer}
                            resizeMode={'contain'}
                            style={{ width: 30, height: 30 }}
                        />
                    </Pressable>
                    :
                    <Icon
                        onPress={props.leftBtnPress}
                        name={props.leftIconName}
                        type={props.leftIconType}
                        style={[styles.iconStyle, { color: white }, props.leftIconStyle]}
                    />
                }
            </View>

            {props.dashboard &&
                <View style={styles.body}>
                    <View style={styles.bodyContent}>
                        <Image
                            source={props.logo}
                            resizeMode={'contain'}
                            style={styles.imgStyle}
                        />
                        <View style={styles.middle}>
                            <Text style={styles.firstName}>{props.text1}</Text>
                            <Text style={styles.lastName}>{props.text2}</Text>
                        </View>
                    </View>
                </View>
            }

            {props.heading &&
                <View style={[styles.headerbody]}>
                    <Text style={styles.headingStyle}>{props.headingText}</Text>
                    {/* {Platform.OS=='ios'&&
            <Divider
             style={styles.divider}
            />
           } */}
                </View>

            }


            {!props.search ?
                !props.edit ?
                    <View style={styles.right}>
                        <Pressable
                            onPress={props.rightBtnPress}
                            style={styles.searchView}
                        >
                            <Image
                                style={styles.searchimg}
                                resizeMode={'contain'}
                                source={search}
                            />

                        </Pressable>
                    </View>
                    :
                    <View style={styles.right}></View>
                    : null

            }

            {props.search ?
                <View style={styles.searchBox}>
                    <TextInput
                        placeholder={'Search'}
                        placeholderTextColor={lightBlack}
                        onChangeText={props.onChangeText}
                        value={props.value}
                        style={styles.textInput}
                    />
                    <Icon
                        // onPress={props.rightBtnPress}
                        name={props.rightIconName}
                        type={props.rightIconType}
                        style={[styles.iconStyle, { color: appColor }, props.rightIconStyle]}
                    />
                </View>
                : null
            }

        </Header>
    )
}
export default AppHeader

const styles = StyleSheet.create({
    container: {

    },
    left: {
        flex: 0.2,
        alignItems: 'center',
        justifyContent: 'center'
    },
    body: {
        flex: 0.6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerbody: {
        flex: 0.6,
        justifyContent: 'center',

    },

    right: {
        flex: 0.2,
        // backgroundColor:'red',
        alignItems: 'center',
        justifyContent: 'center'
    },

    iconStyle: {
        fontSize: 30
    },
    bodyContent: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    imgStyle: {
        width: 50,
        height: 50,
    },
    middle: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    firstName: {
        color: white,
        fontSize: 15,
        fontFamily: LatoBlack,
        fontWeight: '900',
        marginRight: 5,
    },
    lastName: {
        color: white,
        fontSize: mediumText,
        fontFamily: LatoRegular,
        fontWeight: '500',
    },
    searchView: {
        backgroundColor: white,
        width: 30,
        height: 30,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 5
        // overflow:"hidden"

    },
    searchBox: {
        backgroundColor: white,
        flex: 1,
        justifyContent: "center",
        alignItems: 'center',
        flexDirection: 'row',
        height: 50,
        borderRadius: 10,
        paddingHorizontal: 10,
        marginRight: 20,
        marginTop: topMargin,
    },
    textInput: {
        flex: 1,
        borderBottomColor: borderColor,
        borderBottomWidth: 1,
        marginBottom: Platform.OS == 'android' ? 10 : 0,
        // paddingBottom: 10,
        paddingVertical:10,
        fontFamily: LatoRegular,
        fontSize: mediumText,
        fontWeight: '500',
        // backgroundColor:'red',
        color:black
    },
    headingStyle: {
        color: white,
        fontFamily: LatoRegular,
        fontSize: largeText,
        fontStyle: 'normal',
        fontWeight: '700',
        // borderBottomColor:white,
        // borderBottomWidth:3,
        alignSelf: "center",
    },
    divider: {
        borderWidth: 2,
        borderColor: white,
        marginTop: 5,
        marginBottom: 10,
        width: wp(40),
        alignSelf: 'center'
    },
    searchimg: {
        width: 20,
        height: 20,
    }
})