
import { Icon } from "native-base";
import React, { useRef } from "react";
import { Pressable } from "react-native";
import { View, Text, TextInput, StyleSheet, Platform } from 'react-native'
import { RFValue } from "react-native-responsive-fontsize";
import { widthPercentageToDP } from "react-native-responsive-screen";
import { appColor, danger, darkGrey, grey, lightGrey, white } from "../Constants/Colors";
import { LatoRegular } from "../Constants/Fonts";
import { mediumText } from "../Constants/FontSize";


const DropDown = (props) => {
    const {error} = props
    
    return (
        <View>
            <View style={{ flexDirection: 'row', }}>
                <Text style={error? [styles.labelError] : [styles.label]}>
                    {`${props.label} :`}
                </Text>
            </View>
            <Pressable
                onPress={props.onPress}
                style={error? 
                    [styles.InputViewError, props.inputViewStyle]
                    :
                    [styles.InputView, props.inputViewStyle]
                }>

                {!props.value ?
                    <Text style={styles.placeholder}>{props.placeholder}</Text> :
                    <Text style={styles.Input}>{props.value}</Text>
                }

                <Icon
                    name={'chevron-small-down'}
                    type={'Entypo'}
                    style={styles.iconStyle}
                    onPress={props.iconPress}
                />
            </Pressable>
        </View>
    )
}
export default DropDown;

const styles = StyleSheet.create({
    InputView: {
        marginBottom: 10,
        borderWidth: 1,
        borderColor: appColor,
        borderRadius: 5,
        fontSize: 12,
        flexDirection: 'row',
        alignItems: "center",
        paddingHorizontal: 10,
        paddingVertical: Platform.OS === 'ios' ? 6 : 2,
        zIndex: -10,
        backgroundColor: 'white'
    },
    InputViewError: {
        marginBottom: 10,
        borderWidth: 1,
        borderColor: danger,
        borderRadius: 5,
        fontSize: 12,
        flexDirection: 'row',
        alignItems: "center",
        paddingHorizontal: 10,
        paddingVertical: Platform.OS === 'ios' ? 6 : 2,
        zIndex: -10,
        backgroundColor: 'white'
    },

    Input: {
        backgroundColor: white,
        paddingVertical: 10,
        paddingHorizontal: 10,
        color: 'black',
        fontSize: RFValue(mediumText),
        flex: 1,

    },

    label: {
        marginLeft: 20,
        paddingLeft: 10,
        paddingRight: 10,
        color: darkGrey,
        fontSize: RFValue(14),
        backgroundColor: 'white',
        top: 8
    },

    labelError:{
        marginLeft: 20,
        paddingLeft: 10,
        paddingRight: 10,
        color:danger,
        fontSize: RFValue(14),
        backgroundColor: 'white',
        top: 8
    },

    iconStyle: {
        fontSize: RFValue(23),
        color: grey
    },
    placeholder:{
        color:lightGrey,
        backgroundColor: white,
        paddingVertical: 10,
        paddingHorizontal: 10,
        color:grey,
        fontSize: RFValue(mediumText),
        flex: 1,
    }


})