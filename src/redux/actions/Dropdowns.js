import types from './types'

export const From_To_dropdown = (payload) => {
    
    return{
        type:types.FROM_TO_DROPDOWN,
        payload:payload
    }
}

export const Jv_To_dropdown = (payload) => {
    
    return{
        type:types.JV_To_DROPDOWN,
        payload:payload
    }
}

export const Exp_dropdowns = (payload) => {
    return{
        type:types.EXP_DROPDOWN,
        payload:payload
    }
}
export const Vendor_dropdowns = (payload) => {
    return{
        type:types.VENDOR_DROPDOWN,
        payload:payload
    }
}

export const Bank_dd = (payload) => {
    return{
        type:types.BANK_DROPDOWN,
        payload:payload
    }
}
export const Product_dd = (payload) => {
    return{
        type:types.PRODUCT_DROPDOWN,
        payload:payload
    }
    
}
export const Location_dd = (payload) => {
    return{
        type:types.LOCATION_DROPDOWN,
        payload:payload
    }
}
export const Service_dd = (payload) => {
    return{
        type:types.SERVICE_DROPDOWN,
        payload:payload
    }
}
export const Customer_dd = (payload) => {
    return{
        type:types.CUSTOMER_DROPDOWN,
        payload:payload
    }
}
export const OGP_dd = (payload) => {
    return{
        type:types.OGP_DROPDOWN,
        payload:payload
    }
}
export const Employee_dd = (payload) => {
    return{
        type:types.EMP_DROPDOWN,
        payload:payload
    }
}
export const Department_dd = (payload) => {
    return{
        type:types.DEP_DROPDOW,
        payload:payload
    }
}
export const leave_Types = (payload) => {
    return{
        type:types.LEAVE_TYPES,
        payload:payload
    }
}
