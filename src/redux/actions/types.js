const types ={
    LOGIN:'login',
    FROM_TO_DROPDOWN:'fromToDropDown',
    JV_To_DROPDOWN:'jvToDropDown',
    EXP_DROPDOWN:'exToDropDowns',
    VENDOR_DROPDOWN :'vendorDropDown',
    BANK_DROPDOWN:'bankDropDown',
    PRODUCT_DROPDOWN:'productDropDown',
    LOCATION_DROPDOWN:'locationDropDown',
    SERVICE_DROPDOWN:'serviceDropDown',
    CUSTOMER_DROPDOWN:'customerDropdown',
    OGP_DROPDOWN:'ogpDropdown',
    EMP_DROPDOWN:'employeeDropdown',
    DEP_DROPDOW:'departmentDropdown',
    LEAVE_TYPES:'leaveTypes',
    
}
export default types