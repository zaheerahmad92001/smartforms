import types from '../actions/types'

const initialState = {
    FromToDD: '',
    jvToDD:'',
    expDDs:'',
    vendorDDs:'',
    bankDDs:'',
    locationDDs:'',
    productDDs:'',
    serviceDDs:'',
    customerDDs:'',
    ogpDDs:'',
    employeeDDs:'',
    department_dd:'',
    leave_category:''


}

export default (state = initialState, action) => {
    console.log('type',action.type, 'action payload',action.payload)
    switch (action.type) {

        case types.FROM_TO_DROPDOWN:
            return {
                ...state,
                FromToDD: action.payload
            }
        case types.JV_To_DROPDOWN:
            return{
                ...state,
                jvToDD:action.payload
            }
        case types.EXP_DROPDOWN:
           return{
              ...state,
              expDDs:action.payload
        }
        case types.VENDOR_DROPDOWN:
           return{
              ...state,
              vendorDDs:action.payload
        }
        case types.BANK_DROPDOWN:
           return{
              ...state,
              bankDDs:action.payload
        }
        case types.LOCATION_DROPDOWN:
           return{
              ...state,
              locationDDs:action.payload
        }
        case types.PRODUCT_DROPDOWN:
           return{
              ...state,
              productDDs:action.payload
        }
        case types.SERVICE_DROPDOWN:
           return{
              ...state,
              serviceDDs:action.payload
        }
        case types.CUSTOMER_DROPDOWN:
            return{
               ...state,
               customerDDs:action.payload
         }
         case types.OGP_DROPDOWN:
            return{
               ...state,
               ogpDDs:action.payload
         }
         case types.EMP_DROPDOWN:
            return{
               ...state,
               employeeDDs:action.payload
         }
         case types.DEP_DROPDOW:
            return{
               ...state,
               department_dd:action.payload
         }
         case types.LEAVE_TYPES:
            return{
               ...state,
               leave_category:action.payload
         }       
        default:
            return state
    }
}