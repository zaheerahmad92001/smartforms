import types from '../actions/types'

const initialState = {
    userInfo: '',
}

export default (state = initialState, action) => {
    switch (action.type) {

        case types.LOGIN:
            return {
                ...state,
                userInfo: action.payload
            }
        default:
            return state
    }
}