import { createStore ,applyMiddleware } from 'redux';
import { persistStore, persistReducer,createTransform } from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import thunk from 'redux-thunk';
import reducers from '../reudcers'

const persistConfig = {
    key: 'root',
    storage:AsyncStorage,
    whitelist: ['User'],
    setTimeout:null
   
  }
 
const persistedReducer = persistReducer(persistConfig, reducers)	
const store = createStore(persistedReducer,applyMiddleware(thunk));
let persistor = persistStore(store)

export {store,persistor};